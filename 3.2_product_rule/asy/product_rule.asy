// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "product_rule%03d";

import graph;


// ================ product rule rectangle =======
picture pic;
int picnum = 0;
unitsize(pic,1cm);

real u=3;  // width
real v=2;  // height
real delta_u=0.5; 
real delta_v=0.4; 

path main_rectangle=(0,0)--(u,0)--(u,v)--(0,v)--cycle;
path right_rectangle=(u,0)--(u+delta_u,0)--(u+delta_u,v)--(u,v)--cycle;
path top_rectangle=(0,v)--(u,v)--(u,v+delta_v)--(0,v+delta_v)--cycle;
path corner_rectangle=(u,v)--(u+delta_u,v)--(u+delta_u,v+delta_v)
                         --(u,v+delta_v)--cycle;

filldraw(pic,main_rectangle,white,black);
filldraw(pic,right_rectangle,verylight_color,black);
filldraw(pic,top_rectangle,background_color,black);
filldraw(pic,corner_rectangle,bold_color,black);

real LINE_OFFSET = 0.25;
// vertical label
draw(pic, L=Label("$v$",MidPoint,W),
     (-LINE_OFFSET,0)--(-LINE_OFFSET,v),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta v$",MidPoint,W),
     (-LINE_OFFSET,v)--(-LINE_OFFSET,v+delta_v),
     highlight_color,
     bar=Bars);
// horizontal label
draw(pic, L=Label("$u$",MidPoint,S),
     (0,-LINE_OFFSET)--(u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta u$",MidPoint,S),
     (u,-LINE_OFFSET)--(u+delta_u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ product rule example =======
real f1(real x) {return (x^2+1)*exp(log(3)*x);}
real tangent1(real x) {return (6*log(3)+6)*(x-1)+6;}

picture pic;
int picnum = 1;

size(pic,0,4cm,keepAspect=true);


real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f1,xleft-0.1,1.2);
draw(pic, f, FCNPEN);
draw(pic, graph(tangent1,0.8,1.2), highlight_color);

filldraw(pic, circle((1,6),0.075), FCNPEN_COLOR,  FCNPEN_COLOR);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ rate of change of a square =======
picture pic;
int picnum = 2;
unitsize(pic,1cm);

real u=3;  // width
real v=u;  // height
real delta_u=0.85; 
real delta_v=delta_u; 

path main_rectangle=(0,0)--(u,0)--(u,v)--(0,v)--cycle;
path right_rectangle=(u,0)--(u+delta_u,0)--(u+delta_u,v)--(u,v)--cycle;
path top_rectangle=(0,v)--(u,v)--(u,v+delta_v)--(0,v+delta_v)--cycle;
path corner_rectangle=(u,v)--(u+delta_u,v)--(u+delta_u,v+delta_v)
                         --(u,v+delta_v)--cycle;

filldraw(pic,main_rectangle,white,black);
label(pic,"$x^2$",(u/2,u/2));

filldraw(pic,right_rectangle,verylight_color,black);
label(pic,"$x\Delta x$",(u+delta_u/2,u/2));

filldraw(pic,top_rectangle,background_color,black);
label(pic,"$x\Delta x$",(u/2,u+delta_u/2));

filldraw(pic,corner_rectangle,bold_color,black);
label(pic,"$(\Delta x)^2$",(u+delta_u/2,u+delta_u/2));

real LINE_OFFSET = 0.25;
// vertical label
draw(pic, L=Label("$x$",MidPoint,W),
     (-LINE_OFFSET,0)--(-LINE_OFFSET,v),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta x$",MidPoint,W),
     (-LINE_OFFSET,v)--(-LINE_OFFSET,v+delta_v),
     highlight_color,
     bar=Bars);
// horizontal label
draw(pic, L=Label("$x$",MidPoint,S),
     (0,-LINE_OFFSET)--(u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta x$",MidPoint,S),
     (u,-LINE_OFFSET)--(u+delta_u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



