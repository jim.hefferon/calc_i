\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.2\ \ Product and quotient rules}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................






\begin{frame}{So far}
The derivative of the sum of two functions is the sum of the derivatives.
The derivative of the difference of two functions
is the difference of the derivatives.

We next tackle the derivative of the product.
A first guess might be that the derivative of the product 
equals the product of the derivatives.

\pause
That guess is mistaken.
Let $f(x)=x^2$ and $g(x)=x^3$.
\begin{equation*}
  \begin{array}{cc}
   \text{derivative of the product}
     &\text{product of the derivatives}  \\
   \hline
     \rule{0em}{18pt}
     \displaystyle\frac{d\,x^5}{dx}=5x^4
     &\displaystyle\frac{d\,x^2}{dx}\cdot\frac{d\,x^3}{dx}=2x\cdot 3x^2
  \end{array}
\end{equation*}
\end{frame}



\begin{frame}{Picture of the derivative}
Let $f(x)=x^2\!$.
\begin{align*}
  f'(x)
  =\lim_{\Delta x\to 0}\frac{f(x+\Delta x)-f(x)}{\Delta x}
  &=\lim_{\Delta x\to 0}\frac{(x+\Delta x)^2-x^2}{\Delta x}      \\
  &=\lim_{\Delta x\to 0}\frac{x^2+2x\Delta x +(\Delta x^2)-x^2}{\Delta x}
\end{align*}
\pause This is the geometry.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/product_rule002.pdf}}
  \quad
  \begin{minipage}{0.4\textwidth}
    \begin{equation*}
      (x+\Delta x)^2
        =x^2+2\cdot x\Delta x+(\Delta x)^2
    \end{equation*}
  \end{minipage}
\end{center}
\end{frame}
\begin{frame}
For a concrete example, take $x=3$ so that $f(x)=9$.
\begin{center}
    \begin{tabular}{l|lll}
      \multicolumn{1}{c}{$\Delta x$} 
          &\multicolumn{1}{c}{$f(x+\Delta x)$} 
          &\multicolumn{1}{c}{$2x \Delta x$} 
          &\multicolumn{1}{c}{$(\Delta x)^2$} \\ \hline
      $0.1$   &$9.61$     &$0.6$   &$0.01$  \\
      $0.01$  &$9.0601$   &$0.06$  &$0.001$  \\
      $0.001$ &$9.006001$ &$0.006$ &$0.000001$  \\
    \end{tabular}
\end{center}
In terms of the above geometry, if $\Delta x$ is small then the 
upper right square is small in both of its sides, so it is very small. 
Restated,
as $\Delta x\to 0$ the quadratic change term $(\Delta x)^2$
drops toward zero much more quickly than does 
the linear change term $2x\Delta x$.

\pause
The rest of the calculation is this.   
\begin{equation*}
   =\lim_{\Delta x\to 0}\frac{x^2+2x\Delta x +(\Delta x^2)-x^2}{\Delta x} 
  =\lim_{\Delta x\to 0}2x +\Delta x
   =2x  
\end{equation*}
The derivative tells you the linear rate of change.
% The difference quotient fraction
% \begin{equation*}
%   \frac{f(x+\Delta x)-f(x)}{\Delta x}
% \end{equation*}
% compares the change in the function to a linear change.
\end{frame}
% sage: def f(x):
% ....:     return x^2
% ....: 
% sage: for delta_x in [0.1, 0.01, 0.001]:
% ....:     print("3+delta_x=",3+delta_x," f(x+delta_x)=",f(3+delta_x))
% ....: 
% 3+delta_x= 3.10000000000000  f(x+delta_x)= 9.61000000000000
% 3+delta_x= 3.01000000000000  f(x+delta_x)= 9.06010000000000
% 3+delta_x= 3.00100000000000  f(x+delta_x)= 9.00600100000000
% ....: 
% 6*delta_x= 0.600000000000000  (delta_x)^2= 0.0100000000000000
% 6*delta_x= 0.0600000000000000  (delta_x)^2= 0.000100000000000000
% 6*delta_x= 0.00600000000000000  (delta_x)^2= 1.00000000000000e-6



\begin{frame}{Product Rule}
Where $f$ and $g$ are functions, this is the derivative of their product.
\begin{equation*}
  \frac{d\,f(x)\cdot g(x)}{dx}
  =f(x)\cdot\frac{d\,g(x)}{dx}
    +g(x)\cdot\frac{d\,f(x)}{dx}
\end{equation*}
\end{frame}


\begin{frame}{Argument for Product Rule}
Consider the functions $u(x)$ and $v(x)$.
Fix~$x$ and let 
$\Delta u=u(x+\Delta x)-u(x)$ and 
$\Delta v=v(x+\Delta x)-v(x)$. 
Note that if $\Delta x\to 0$ then $\Delta u\to 0$ and $\Delta v\to 0$.
\begin{center}
  \includegraphics{asy/product_rule000.pdf}    
\end{center}
The rectangle shows that as $\Delta x$ changes, the change in the product is 
$\Delta (uv)=u\Delta v+v\Delta u +\Delta u\Delta v$.
Take this limit.
\begin{align*}
  \lim_{\Delta x\to 0}\frac{\Delta (uv)}{\Delta x}
  &=\lim_{\Delta x\to 0} u\frac{\Delta v}{\Delta x}
                     +v\frac{\Delta u}{\Delta x}
                     +\Delta u\frac{\Delta v}{\Delta x}   \\
  &=u\frac{dv}{dx}+v\frac{du}{dx}+0\cdot\frac{dv}{dx}
\end{align*}
\end{frame}




\begin{frame}
\Ex Let $f(x)=x^2\cdot (x^5+x)$.
We could in principle multiply it out and then take the derivative of
the resulting polynomial, but we do this instead
for practice.
\begin{equation*}
  \frac{d\,f(x)}{dx}
  =x^2\cdot (5x^4+1)+(x^5+x)\cdot 2x
\end{equation*}

\pause
\Ex Let $g(x)=x^2e^x\!$.
\begin{equation*}
  \frac{d\,f(x)}{dx}
  =x^2\cdot (e^x)+e^x\cdot (2x)
  =xe^x\cdot(x+2)
\end{equation*}
\end{frame}


\begin{frame}
\Ex Find the line tangent to the graph of $(x^2+1)\cdot 3^x$ at $x=1$.
\pause
The derivative is this.
\begin{equation*}
  (x^2+1)\cdot 3^x\ln 3 +3^x\cdot(2x)
  =3^x\cdot(x^2+1+2x)\ln(3
\end{equation*}
At $x=1$ we get the slope of the tangent line.
\begin{equation*}
  3\cdot(2\ln 3+2)
\end{equation*}
And the equation of the tangent line is this.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/product_rule001.pdf}}
  \quad
  \begin{minipage}{0.4\linewidth}
    \begin{equation*}
       y-6=(6+6\ln 3)\cdot(x-1)
    \end{equation*}
  \end{minipage}
\end{center}
\end{frame}



\begin{frame}{Quotient Rule}
Where $f$ and $g$ are functions, this is the derivative of the quotient.
\begin{equation*}
  \frac{d\,{\displaystyle\frac{f(x)}{g(x)}}}{dx}
  =\frac{{\displaystyle g(x)\cdot\frac{df(x)}{dx}} 
         - {\displaystyle f(x)\cdot\frac{d\,g(x)}{dx}}}{g(x)^2}
\end{equation*}
  
\Ex Let $h(x)=e^x/(x+1)$.
\begin{equation*}
  \frac{{\displaystyle (x+1)\cdot \frac{d\,e^x}{dx} - e^x\cdot \frac{d\,(x+1)}{dx}}}{(x+1)^2}
  =\frac{(x+1)\cdot e^x - e^x\cdot 1}{(x+1)^2}
  =\frac{xe^x}{(x+1)^2}
\end{equation*}

\pause \Ex
Find $f'(2)$ where $f(x)=1/(x+4)$.
\pause
\begin{equation*}
  \frac{\displaystyle (x+4)\cdot\frac{d\,1}{dx} - 1\cdot\frac{d\,(x+4)}{dx}}{(x+4)^2}
  =\frac{(x+4)\cdot 0 - 1\cdot 1}{(x+4)^2}
  =\frac{-1}{(x+4)^2}
\end{equation*}
So $f'(2)=-1/36$.
\end{frame}


\begin{frame}{Practice}
\Ex\ 
\begin{equation*}
  \frac{d\,\left({ \displaystyle \frac{x^2+3x+4}{x^2-1} }\right)}{dx}
  \uncover<2->{
=\frac{(x^2-1)(2x+3)- (x^2+3x+4)(2x)}{(x^2-1)^2}    
  =\frac{-3x^2-10x-3}{(x^2-1)^2}}    
\end{equation*}

\uncover<2->{
\Ex Find the equation of the line tangent to $f(x)=(x^2+1)/(x^2-4)$
at $(3,1)$.}

\uncover<3->{
First find the derivative.
\begin{equation*}
  \frac{(x^2-4)2x-(x^2+1)2x}{(x^2-4)^2}
  =\frac{2x^3-8x-2x^3-2x}{(x^2-4)^2}
  =\frac{-10x}{(x^2-4)^2}
\end{equation*}
At $(3,2)$ the slope of the tangent line is $-6/5$.

Now put it into the equation of a line.
\begin{equation*}
  y-2=\frac{-6}{5}(x-3)
\end{equation*}}
\end{frame}


\begin{frame}{Argument for the Quotient Rule}
% Rewriting as $f(x)/g(x)=f(x)\cdot g(x)^{-1}$
% means that once we know how to find the derivative of $g(x)^{-1}$ 
% (using the Chain Rule) then we are done. 

Let $F(x)=f(x)/g(x)$.
\begin{equation*}
  F'(x)=\lim_{h\to 0}\frac{F(x+h)-F(h)}{h}
       =\lim_{h\to 0}\frac{\displaystyle \frac{f(x+h)}{g(x+h)}-\frac{f(x)}{g(x)}}{h}
\end{equation*}
Getting a common denominator and simplifying gives this.
\begin{equation*}
  =\lim_{h\to 0}\frac{f(x+h)g(x)-f(x)(g+h)}{h\cdot g(x+h)g(x)}
\end{equation*}
Here's a trick.  
Into the numerator stick $0=-f(x)g(x)+f(x)g(x)$.  
\begin{align*}
  &=\lim_{h\to 0}\frac{f(x+h)g(x){\color{blue}-f(x)g(x)+f(x)g(x)}-f(x)(g+h)}{h\cdot g(x+h)g(x)}  \\ 
  &=\lim_{h\to 0}\frac{\displaystyle g(x)\cdot\bigl(\frac{f(x+h)-f(x)}{h}\bigr)
                  - f(x)\cdot\bigl(\frac{g(x+h)-g(x)}{h}\bigr)}{%
              g(x+h)g(x)}
\end{align*}
\end{frame}

\begin{frame}
Appply the Limit Laws.
\begin{align*}
  &=\lim_{h\to 0}\frac{\displaystyle g(x)\cdot\bigl(\frac{f(x+h)-f(x)}{h}\bigr)
                  - f(x)\cdot\bigl(\frac{g(x+h)-g(x)}{h}\bigr)}{%
              g(x+h)g(x)}                   \\
  &=\frac{\displaystyle \lim_{h\to 0} g(x)\cdot\bigl(\lim_{h\to 0}\frac{f(x+h)-f(x)}{h}\bigr)
                  - \lim_{h\to 0} f(x)\cdot\bigl(\lim_{h\to 0} \frac{g(x+h)-g(x)}{h}\bigr)}{%
              \lim_{h\to 0} g(x+h)\cdot \lim_{h\to 0}g(x)}
\end{align*}
Apply these.
\begin{equation*}
  \lim_{h\to 0}f(x)=f(x)
  \quad
  \lim_{h\to 0}g(x)=g(x)
  \quad
  \lim_{h\to 0}g(x+h)=g(x)
\end{equation*}
We get the desired formula.
\begin{equation*}
  F'(x)=\frac{g(x)\cdot f'(x)-f(x)\cdot g'(x)}{g(x)^2}
\end{equation*}
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
