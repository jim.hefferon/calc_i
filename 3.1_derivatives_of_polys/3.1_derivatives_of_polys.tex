\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.1\ \ Derivatives of polynomials}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Derivative of the function $f(x)=x^2$}
We have found the derivative of $f(x)=x^2$ at $x=a$.
\begin{multline*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
  =\lim_{h\to 0}\frac{(a^2+2ah+h^2)-(a^2)}{h}           \\
  =\lim_{h\to 0}\frac{2ah+h^2}{h}  
  =\lim_{h\to 0}\frac{h(2a+h)}{h}  
  =\lim_{h\to 0}2a+h=2a  
\end{multline*}
So $f'(a)=2a$.
\end{frame}


\begin{frame}{Derivative of the function $f(x)=x^3$}
Recall that this is the expansion of a cube.
\begin{equation*}
  (a+h)^3
  =
  a^3+3a^2h+3ah^2+h^3  
\end{equation*}
\pause
So this the derivative of $f(x)=x^3$ at $x=a$.
\begin{align*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
  &=\lim_{h\to 0}\frac{(a^3+3a^2h+3ah^2+h^3)-(a^3)}{h}           \\
  &=\lim_{h\to 0}\frac{3a^2h+3ah^2+h^3}{h}             \\
  &=\lim_{h\to 0}\frac{h(3a^2+3ah+h^2)}{h}            \\  
  &=\lim_{h\to 0}3a^2+3ah+h^2=3a^2  
\end{align*}
So $f'(a)=3a^2\!$.
\end{frame}


\begin{frame}{Derivative of the function $f(x)=x^3$}
For $x^4$ this is the algebra.
\begin{equation*}
  (a+h)^4
  =
  a^4+4a^3h+6a^2h^2+4ah^3+h^4  
\end{equation*}
% \pause
We get this derivative.
\begin{align*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
  &=\lim_{h\to 0}\frac{(a^4+4a^3h+6a^2h^2+4ah^3+h^4)-(a^4)}{h}           \\
  &=\lim_{h\to 0}\frac{4a^3h+6a^2h^2+4ah^3+h^4}{h}             \\
  &=\lim_{h\to 0}\frac{h(4a^3+\text{terms with $h$'s})}{h}            \\  
  &=\lim_{h\to 0}4a^3+\text{terms with $h$'s}=4a^3  
\end{align*}
So $f'(a)=4a^3\!$.
\end{frame}


\begin{frame}{Reason for the pattern that $f(x)=x^n$ gives $f'(x)=nx^{n-1}$?}
Focus on $f(x)=x^3\!$ and examine the difference quotient computation.
% In the difference quotient the $a^3$'s cancel.
\pause
The derivative is determined by the term with one~$h$.
Why does that term have a coefficient of three?
  \begin{align*}
    {\color{red}(a+h)}{\color{blue}(a+h)}(a+h) 
    \uncover<2->{&={\color{red}a}\cdot{\color{blue}(a+h)}(a+h)
       +{\color{red}h}\cdot{\color{blue}(a+h)}(a+h)   \\}
    \uncover<3->{&={\color{red}a}{\color{blue}a}(a+h)
      +{\color{red}a}{\color{blue}h}(a+h)
      +{\color{red}h}{\color{blue}a}(a+h)
      +{\color{red}h}{\color{blue}h}(a+h)            \\}
    \uncover<4->{&={\color{red}a}{\color{blue}a}a+{\color{red}a}{\color{blue}a}h  \\
    &\quad +{\color{red}a}{\color{blue}h}a+{\color{red}a}{\color{blue}h}h  \\
    &\quad +{\color{red}h}{\color{blue}a}a+{\color{red}h}{\color{blue}a}h  \\
    &\quad +{\color{red}h}{\color{blue}h}a+{\color{red}h}{\color{blue}h}h  \\ 
    &=a^3+3a^2h+3ah^2+h^3}
  \end{align*}
To be an $a^2h$ term, among the three letters there needs to be
exactly one factor of~$h$.
This $h$ either comes first as in ${\color{red}h}{\color{blue}a}a$, 
or second as in ${\color{red}a}{\color{blue}h}a$, or third as
in ${\color{red}a}{\color{blue}a}h$.
So the coefficient of $a^2h$ is $3$.
This reasoning extends to any positive integer power~$n$.
\end{frame}


\section{Derivative rules for polynomials}

\begin{frame}[fragile]
  \frametitle{Derivative laws for polynomials}
\begin{enumerate}
\item The derivative of a constant function $f(x)=k$ is $f'(x)=0$.
\item The derivative of a power $f(x)=x^k$ is $f'(x)=kx^{k-1}\!$.
\item The derivative of a constant multiple of a function $f(x)=k\cdot u(x)$ 
  is $f'(x)=k\cdot u'(x)$.
\item The derivative of a sum is the sum of the derivatives.
\begin{equation*}    
  \text{If $h(x)=f(x)+g(x)$ then $h'(x)=f'(x)+g'(x)$}
\end{equation*}
\item The derivative of a difference is the difference of the derivatives.
\begin{equation*}    
  \text{If $h(x)=f(x)-g(x)$ then $h'(x)=f'(x)-g'(x)$}
\end{equation*}
\end{enumerate}

\pause
\textit{Caution:}
There are lots of things we don't yet know with derivatives.
We do not yet know how to do the product of functions,
$f(x)\cdot g(x)$ 
or the quotient, $f(x)/g(x)$.
We also do not know how to do the composition $f\circ g$.
And we don't know exponential or logarithmic functions,
or trigs or inverse trigs.
\end{frame}



\begin{frame}{Same rules, in the other notation}
\begin{enumerate}
\item $\displaystyle \frac{d\,k}{dx}=0$ 
\item $\displaystyle \frac{d\,x^k}{dx}=k\cdot x^{k-1}$ 
\item $\displaystyle \frac{d\,k\cdot f(x)}{dx}=k\cdot\frac{d\,f(x)}{dx}$ 
\item $\displaystyle \frac{d\,(f(x)+g(x))}{dx}=\frac{d\,f(x)}{dx}+\frac{d\,g(x)}{dx}$
\item $\displaystyle \frac{d\,(f(x)-g(x))}{dx}=\frac{d\,f(x)}{dx}-\frac{d\,g(x)}{dx}$
\end{enumerate}
\end{frame}


\begin{frame}{Examples}
Differentiate these
\begin{enumerate}
\item $\displaystyle g(x)=5x-4x^2$ 
\pause\item $\displaystyle x^2/4-3x+9$ 
\pause\item $\displaystyle \sqrt{x}-\sqrt{3}$\quad
  \textit{Hint:} use the fractional power.
  We have not justified the derivative rule for non-integers
  but ignore that.
\pause\item $\displaystyle x^3(x^2-4)$\quad
  \textit{Hint:} we don't yet know how to take the derivative of the
  product of $x^3$ with $x^2-4$, so multiply through first.   
\end{enumerate}
\end{frame}



\section{Derivative rules for exponentials}
\begin{frame}{Derivatives of exponential functions}

Where $b>0$ is a constant base, the derivative is this.
\begin{equation*}
  \frac{d\, b^x}{dx}=b^x\cdot \ln(b)
\end{equation*}
In particular, where $e\approx 2.781$ is the base for the natural log,
we get this.
\begin{equation*}
  \frac{d\, e^x}{dx}=e^x
\end{equation*}
We haven't given a argument for the exponential law, but
one is in the book.

Differentiate these.
\begin{enumerate}
\item $\displaystyle g(t)=5^t$ 
\pause\item $\displaystyle 3^t+2t^3$ 
\pause\item $\displaystyle e^x+4$ 
\pause\item $\displaystyle x^2-e^x$ 
\end{enumerate}
\end{frame}

\begin{frame}{An aside}
  \href{https://www.youtube.com/watch?v=m2MIpDrF7Es}{3Blue1Brown}
  has a video description
  (although, we haven't yet had the Chain Rule so we can skip that
  part).
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
