// science.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "science%03d";

import graph;


// ================ flight of plane =======
real f0(real x) {return -1.06*x^3+1.61*x^2;}

real deriv0(real x) {return -3.18*x^2+3.22*x;}
real tangent_line0(real x) {return deriv0(0.5)*(x-0.5)+f0(0.5);}

picture pic;
int picnum = 0;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=0.6;
ybot=0; ytop=0.6;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f0,0,0.6), black);
path tline = graph(tangent_line0,0.1,0.6);
draw(pic, tline, highlight_color);

real tline_time = intersect(tline,(0,0)--(1,0))[0];
pair circle_center = point(tline, tline_time);
path arc_circle = circle(circle_center, 0.05);

real arc_circle_time = intersect(arc_circle,tline)[0];
draw(pic, subpath(arc_circle, 0, arc_circle_time), black);
label(pic, "{\tiny $\theta$}", (0.29,0.04));

filldraw(pic, circle((0.5,f0(0.5)),0.01), highlight_color, highlight_color);
label(pic,  "$(0.5,f(0.5))$", (0.5,f0(0.5)), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ ball off platform =======
picture pic;
int picnum = 1;

size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=16;
ybot=0; ytop=0;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

real offset = 1;
path ball = (9.6,offset)--(15.0,offset){E}..(16.0,offset+0.5)..{W}(15.0,offset+1)--(0,offset+1);
draw(pic,ball,highlight_color, arrow=MidArrow);

filldraw(pic, circle((9.6,offset),0.1), highlight_color, highlight_color);
label(pic,  "$t=0$", (9.6,offset), W, filltype=Fill(white));
filldraw(pic, circle((16.0,offset+0.5),0.1), highlight_color, highlight_color);
label(pic,  "$t=2$", (16.0,offset+0.5), E, filltype=Fill(white));
filldraw(pic, circle((0,offset+1),0.1), highlight_color, highlight_color);
label(pic,  "$t=5.16$", (0,offset+1), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=16,step=1),
      arrow=Arrows(TeXHead));
labelx(pic,"$s=0$",0,S);
labelx(pic,"$s=96$",9.6,S);
labelx(pic,"$s=160$",16.0,S);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ sign chart =======
picture pic;
int picnum = 2;

size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=4;
ybot=0; ytop=0;

real offset = 0.15;
label(pic,  "\makebox[0em][l]{vel neg}", (2,offset), N, filltype=Fill(white));
label(pic,  "\makebox[0em][r]{vel pos}", (0,offset), N, filltype=Fill(white));

real[] T2 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=Ticks("%", T2, Size=4pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$1$",1,S);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ particle =======
picture pic;
int picnum = 3;

size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-10; xright=10;
ybot=0; ytop=0;


real offset = 1;
path particle = (8,offset)--(8.5,offset){E}..(9,offset+0.5)..{W}(8.5,offset+1)--(-7,offset+1);
draw(pic,particle,highlight_color, arrow=MidArrow);

filldraw(pic, circle((8,offset),0.1), highlight_color, highlight_color);
label(pic,  "$t=0$", (8,offset), W, filltype=Fill(white));
filldraw(pic, circle((9,offset+0.5),0.1), highlight_color, highlight_color);
label(pic,  "$t=1$", (9,offset+0.5), E, filltype=Fill(white));
filldraw(pic, circle((-7,offset+1),0.1), highlight_color, highlight_color);
label(pic,  "$t=5$", (-7,offset+1), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$8$",8,S);
labelx(pic,"$9$",9,S);
labelx(pic,"$-7$",-7,S);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ projectile =======

// ..... motion in the two directions is independent .........
picture pic;
int picnum = 4;

size(pic,3.75cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-15; ytop=0;

real f4(real x) {
  return( (ybot/25)*x^2 );
}

draw_graphpaper(pic, xleft, xright, ytop, ybot);

real eps = 0.2;
path no_horiz = (0,0)--(0,ybot);
path f = graph(f4, 0, 5);
path horiz = f;

draw(pic, shift(eps,0)*no_horiz, FCNPEN);
draw(pic, shift(eps,0)*horiz, FCNPEN);
dotfactor =  5;
for (int i=1; i<6; ++i) {
  dot(pic, format("$t=%d$",i), shift(eps,0)*(0,f4(i)), W, red);
  dot(pic, format("$t=%d$",i), shift(eps,0)*(i,f4(i)), E, red);
}
label(pic,graphic("../pix/baseball.png","width=0.25cm"),(eps,0)); 

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);
