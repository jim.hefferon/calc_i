\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.7\ \ Rates of change in the natural sciences}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

% \section{Rate of change}

% \begin{frame}{Area of a circle}
% The area of a circle as a function of the radius is
% $A(r)=\pi r^2$.
% \begin{enumerate}
% \item Find the rate of change of the area with respect to the radius.
% \pause
% \begin{equation*}
%   \frac{dA}{dr}=2\pi r
% \end{equation*}
% \item Find the rate at $r=2$ and $r=5$.
% Briefly give intuition as to why $dA/dr$ is larger at $r=5$.

% \pause
% We have
% $A'(2)\approx 12.57$ and $A'(5)\approx 31.42$.
% \end{enumerate}
% \end{frame}


\section{Motion along a line}


\begin{frame}{Position, velocity, acceleration in one dimension}
The \alert{position function $y=s(t)$} gives the position of a
particle at time~$t$.
Similarly the \alert{velocity function $v(t)$} gives the velocity.   
The \alert{speed} is the absolute value of the velocity.
Likewise, the \alert{aceleration function $a(t)$} gives the 
acceleration.

Notice that velocity is the rate of change of position.
Likewise, acceleration is the rate of change of velocity.
\begin{equation*}
  v(t)=\frac{d\,s(t)}{dt}
  \qquad
  a(t)=\frac{d\,v(t)}{dt}
\end{equation*}

All of this is for motion in a line.
(We like to draw the line with positive to the right.)
Also, we will usually work in seconds, meters, etc.
\end{frame}

\begin{frame}{From position to velocity and acceleration}
A particle moves in one dimension 
so that its position at the times~$0\leq t\leq 5$ is 
given by $s(t)=8+2t-t^2\!$.
\begin{enumerate}
\item Find the velocity and acceleration functions.
\pause
\begin{equation*}
  v(t)=s'(t)=2-2t
  \qquad
  a(t)=s''(t)=-2
\end{equation*}
\item When is the particle stationary?
\pause \\
The velocity is zero at $t=1$.
\item When is it moving left to right?
Right to left?
\pause
The velocity is positive, so the particle is moving left to right, 
for $t<1$.
The velocity is negative when $t>1$.
\item When is the particle speeding up?  
Slowing down?
\pause  \\
The acceleration is negative for all~$t$. 
% This pictures when the particle is moving left, and when it is moving right.
% \begin{center}
%   \includegraphics{asy/science002.pdf}
% \end{center}
\item Sketch the particle's motion along the number line.
\pause
\begin{center}
  \includegraphics{asy/science003.pdf}
\end{center}
\end{enumerate}
\end{frame}


\begin{frame}{From acceleration to position}
\begin{wrapfigure}{l}{0.3\textwidth}
  \includegraphics[width=0.3\textwidth]{pix/pisa.png}    
\end{wrapfigure}
  We hold a ball out from the Leaning Tower of Pisa and let it drop.
The acceleration due to gravity at the earth's surface is about
$9.8\,\text{m}/\text{sec}^2$\!.
The tower is $56.67\,\text{m}$ tall.
Find the position function.

\pause
With the given acceleration, and with an initial velocity of zero,
this is the formula for the velocity at time~$t\,\text{sec}$.
\begin{equation*}
  v(t)=-9.8t+0
\end{equation*}
The position function's derivative is the velocity.
To get a derivative of $-9.8t$ we need this.
\begin{equation*}
  s(t)=-4.9t^2+56.67
\end{equation*}
\end{frame}


% \begin{frame}{Example}
% After taking off, an airplane has a flight path described by this path.
% \begin{equation*}
%   y(x)=-1.06x^3+1.61x^2 
%   \qquad
%   0\leq x\leq 0.6
% \end{equation*}
% Find the angle of climb when $x=0.5$.
% \begin{center}
%   \includegraphics{asy/science000.pdf}
% \end{center}

% \pause
% The slope of the line tangent to the curve at that point is this.
% \begin{equation*}
%   \frac{d\,y(x)}{dx}=-3.18x^2+3.22x
% \end{equation*}
% At $x=0.5$ we get $0.815$.
% That slope corresponds to the angle
% $
%   \theta=\tan^{-1}(0.815)=0.684 \text{\ rads}
% $
% (about $39.18$~degrees).
% \end{frame}



\begin{frame}
\Ex  From a $96$ foot platform we throw a ball vertically upward with an
initial velocity of $64$~ft/sec.   
By Newton's Laws, the height of the ball above ground after
$t$~seconds is
$
  s(t)=-16t^2+64t+96
$.
\begin{enumerate}
\item Find the velocity.
\pause
\begin{equation*}
  v(t)=\frac{ds}{dt}=-32t+64
\end{equation*}
\item Find the acceleration.
\pause
\begin{equation*}
  a(t)=\frac{dv}{dt}=-32
\end{equation*}
\item At what time is the ball at its highest point?
\pause \\ Solve $v(t)=0$ so that $-32t+64=0$, giving $t=2$.
\item What is that maximum height?
\pause \\ $s(2)=-16(2)^2+64\cdot 2+96=160$
\item At what time will it reach the ground?
\pause \\ Solve $s(t)=0$ with $-16t^2+64t+96=0$.
The quadratic formula gives $t\approx 5.16$ or $t\approx -1.16$.
The relevant root is the first.
\item With what velocity will it hit the ground?
\pause \\
$v(5.16)=-32(5.16)+64=-101.1$
\end{enumerate}
\end{frame}

\begin{frame}
This number line illustrates the path of the ball.
\begin{center}
  \includegraphics{asy/science001.pdf}
\end{center}  
\end{frame}


\begin{frame}{Projectile motion}
\begin{wrapfigure}{r}{4cm}
  \vspace*{-1ex}\includegraphics{asy/science004.pdf}    
\end{wrapfigure}
  We can use our dimension one analysis to apply to two-dimensional motion.
Assume that all forces except gravity are negligible.
That includes air resistance and friction.

The key is that motions along perpendicular axes are independent,
as illustrated by the graphic.

\pause
At time~$t$, the ball on the left has position $(0,-4.9t^2)$
while the one on the right is at $(t,-4.9t^2)$.
The injection of a horizontal component to the motion does not
affect the vertical component at all.
\end{frame}


\begin{frame}
Fire a cannonball with a velocity $v_0$ at an angle 
of elevation~$\theta$.
Here is 
the horizontal velocity and initial vertical velocity.
\begin{equation*}
  v_x=\text{horizontal velocity}=v_0\cos\theta
  \quad
  v_y=\text{initial vertical velocity}=v_0\sin\theta
\end{equation*}
\pause
We ignore air resistance.
Again, the horizontal and vertical motions are independent of each other.
Throughout its flight the ball's horizontal velocity remains unchanged.
But the vertical velocity decreases over time by the acceleration due 
to gravity~$g$.
\begin{equation*}
  v_y(t)=v_0\sin\theta-gt
\end{equation*}
Velocity is the derivative of position so the position at time~$t$ is here.
\begin{equation*}
  x(t)=(v_0\cos\theta)\cdot t
  \quad
  y(t)=(v_0\sin\theta)\cdot t-g\cdot t^2/2
\end{equation*}
\pause
Write $t=x/(v_0\cos\theta)$ and substitute into the other equation.
\begin{align*}
  y(x) &=(v_0\sin\theta)\cdot x/(v_0\cos\theta) -g\cdot (x/(v_0\cos\theta))^2/2 \\
       &=(\tan\theta)\cdot x-\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr)\cdot x^2
\end{align*}
\end{frame}

\begin{frame}{Parabolic motion}
\begin{equation*}
  y =(\tan\theta)\cdot x-\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr)\cdot x^2
\end{equation*}
This is a downwards-opening parabola.
It factors to give two roots.
\begin{equation*}
  y=x\cdot\Bigl(tan\theta- x\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr)\Bigr)
\end{equation*}
The first root $x=0$ is the starting spot.
The second its its range.
\begin{align*}
  tan\theta -x\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr) 
   &=0 \\
  \tan\theta &= x\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr) \\
  \frac{\tan\theta}{\bigl( \frac{g}{2v_0^2\cos^2\theta} \bigr)}
     &=x  \\
  \frac{2v_0^2\sin\theta\cos\theta}{g} &= x
\end{align*}
\end{frame}



% \begin{frame}
% A particle moves in one dimension, along the number line, 
% so that its position at time~$t$ is $f(t)=t^2-4t$.
% \begin{enumerate}
% \item Sketch the graph of the position function, above the number line.
% \pause
% \item Find the velocity function and graph it above the number line.
% \pause
% \item Find the velocity and acceleration at time $t=1$.
% \pause
% \item Find the acceleration when the velocity is zero.
% \end{enumerate}
% \end{frame}



% \begin{frame}{Business}\vspace*{-1ex}
% A company produces a product.  
% The \alert{cost function} $C(x)$ gives the cost of producing $x$~many units
% of that product.
% The \alert{average cost} $\bar{C}(x)$ is $C(x)/x$.
% The derivative  $C'(x)$, the \alert{marginal cost},
% is the cost to produce one more unit.

% \pause
% \Ex We make refrigators.
% The cost function is $C(x)=100+50 x-0.02x^2$
% \begin{enumerate}
% \item Find the average cost function.
% \pause
% \begin{equation*}
%   \bar{C}(x)=-.02x+50+(100/x)
% \end{equation*}
% \item Find the marginal cost function.
% \pause
% \begin{equation*}
%   \frac{dC}{dx}=-0.04x+50
% \end{equation*}
% \item Find the marginal cost when $x=100$.
% \pause
% \begin{equation*}
%   \bar{C}(100)=\$49
%   \qquad
%   C'(100)=\$46    
% \end{equation*}
% \item Do the same when $x=900$.
% \begin{equation*}
%   \bar{C}(900)\approx\$32
%   \qquad
%   C'(900)=\$14      
% \end{equation*}
% The cost per unit has dropped sharply when we work in volume.
% \end{enumerate}
% \end{frame}


% \begin{frame}
% \Ex Suppose that the weekly revenue from sale of cell phones is this.
% \begin{equation*}
%   R(x)=-0.000078x^3-0.0016x^2+80x
%   \qquad 0\leq x\leq 800
% \end{equation*}
% \begin{enumerate}
% \item Find the marginal revenue function.
% \pause
% \begin{equation*}
%    R'(x)=-0.000234x^2-0.0032x+80
% \end{equation*}
% \item Imagine the company sells $200$ phones per week.
% About how much will revenue increase if they sell one more?
% \pause
% \begin{equation*}
%   R'(200)=70
% \end{equation*}
% \end{enumerate}
% \end{frame}

% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
