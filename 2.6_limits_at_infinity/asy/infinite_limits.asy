// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "infinite_limits%03d";

import graph;





// ================ 1/(x-2) =======
real f0(real x) {return 1/(x-2);}

picture pic;
int picnum = 0;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-4; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft-0.1,1.76), FCNPEN);
draw(pic, graph(f0,2.24,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================ 1/(x-2)^2 =======
real f1(real x) {return 1/(x-2)^2;}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=6;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f1,xleft-0.1,1.6), FCNPEN);
draw(pic, graph(f1,2.4,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ 1/(x-2) with asymptote =======

picture pic;
int picnum = 2;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-4; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft-0.1,1.76), FCNPEN);
draw(pic, graph(f0,2.24,xright+0.1), FCNPEN);

draw(pic,(2,ybot-0.1)--(2,ytop+0.1),highlight_color+dashed);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================ 1/(x-2)^2 with asymptote =======
picture pic;
int picnum = 3;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=6;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f1,xleft-0.1,1.6), FCNPEN);
draw(pic, graph(f1,2.4,xright+0.1), FCNPEN);

draw(pic,(2,ybot-0.1)--(2,ytop+0.1),highlight_color+dashed+linewidth(0.6pt));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Limit dne =======
real f4(real x) {return x^3;}
real f4a(real x) {return 3-x;}

picture pic;
int picnum = 4;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=-1; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f4,xleft-0.1,1), FCNPEN);
draw(pic, graph(f4a,1,xright+0.2), FCNPEN);

filldraw(pic, circle((1,1),0.05), white, black);
label(pic,  "$(1,1)$", (1,1), NW);
filldraw(pic, circle((1,2),0.05));
label(pic,  "$(1,2)$", (1,2), NE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ More than one asymptote =======
real f5(real x) {return 1/((x-1)*(x-3)^2);}

picture pic;
int picnum = 5;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-2; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f5,xleft-0.5,0.9), FCNPEN);
draw(pic, graph(f5,1.05,2.67), FCNPEN);
draw(pic, graph(f5,3.28,xright+0.5), FCNPEN);
// draw(pic,(2,ybot-0.1)--(2,ytop+0.1),highlight_color, Arrows);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ Cubic =======
real f6(real x) {return x^3;}

picture pic;
int picnum = 6;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-4; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f6,-1.6,+1.6);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ Square =======
real f7(real x) {return x^2;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f7,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .... tan x ...
real f8(real x) {return tan(x);}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4.25;
ybot=-5; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f8,xleft-0.1,(3.14159/2)-0.2), FCNPEN);
draw(pic, graph(f8,(3.14159/2)+0.2,xright+0.1), FCNPEN);

draw(pic, (pi/2,ybot-0.1)--(pi/2,ytop+0.1),highlight_color+dashed+linewidth(0.8pt));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .... ln x ...
real f9(real x) {return log(x);}

picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-4; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f9,0.03,xright+0.1);
draw(pic, f, FCNPEN);

draw(pic, (0,ybot-0.1)--(0,ytop+0.1),highlight_color+dashed+linewidth(1pt));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .... essentially x^2/x^2 ...
real f10(real x) {return sqrt(x^4+3)/(x^2+1);}

picture pic;
int picnum = 10;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f10,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

draw(pic, (xleft-0.1,1)--(xright+0.1,1),ASYMPTOTEPEN+highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .... essentially (sin x)/x^2 ...
real f11(real x) {return sin(2x)/x^2;}

picture pic;
int picnum = 11;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-3; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f11,xleft-0.1,-0.5), // L=L_fcn,
     black);
draw(pic, graph(f11,0.5,xright+0.1), // L=L_fcn,
     black);

draw(pic, (xleft-0.1,0)--(xright+0.1,0),ASYMPTOTEPEN+highlight_color);
draw(pic, (0,ybot-0.1)--(0,ytop+0.1),ASYMPTOTEPEN+highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



//  ===== 2x^2-5/x^2+4 ========= 
real f12(real x) {return (2*x^2-5)/(x^2+4);}

picture pic;
int picnum = 12;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=-1; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f12,xleft-0.1,xright+0.1), // L=L_fcn,
     black);

draw(pic, (xleft-0.1,2)--(xright+0.1,2),ASYMPTOTEPEN+highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

