\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.3\ \ Limit laws}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Limit laws}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \begin{frame}
%   For some functions~$f$ and points~$a$ the limit is easy,
%   while for some you have to be careful.
% \begin{center}
%   \includegraphics{asy/limit_laws000.pdf}   \\
% \end{center}
% \begin{enumerate}
% \item $\lim_{x\to 2}f(x)=3$
% \uncover<2->{\item $\lim_{x\to 3}f(x)=-\infty$}
% \uncover<3->{\item $\lim_{x\to 1}f(x)$ does not exist.
%   The limit from the left exists
%   $\lim_{x\to 1^-}f(x)=1$, 
%   and the limit from the right exists
%   $\lim_{x\to 1^+}f(x)=2$,
%   but they are not equal.}
% \end{enumerate}
% \end{frame}


\begin{frame}
We shall give laws for how to take easy limits.
These are all established using the precise definition of a limit, 
but we will not do the derivations.

% For example, if a function~$f$ is given by a polynomial then you just 
% plug in\Dash the 
% limit at~$a$ is $f(a)$.
% For instance, $\lim_{x\to 4} 9+x-x^2$ equals $9+4-16=-3$.

\pause\medskip
Here are the most basic laws (the numbering matches the book's).
\begin{enumerate}
\item[8] The limit of a constant function equals that constant, that is, 
  if $f(x)=c$ then $\lim_{x\to a} f(x)=c$.
\item[9] If $f(x)=x$ then $\lim_{x\to a} f(x)=a$.
\end{enumerate}  

\medskip
An example of the first is that 
if $g(x)=7$ then $\lim_{x\to 4}g(x)=7$.

An example of the second is that if $h(t)=t$ then $\lim_{t\to -3}h(t)=-3$.
\end{frame}


\begin{frame}{Algebraic combinations of functions}
\begin{enumerate}
\item[3] The limit of a constant times a function is the constant times the 
limit.
  \begin{equation*}
    \lim_{x\to a}\,c\cdot f(x)\,
    =c\cdot \lim_{x\to a}f(x)
  \end{equation*}
\item[1] The limit of a sum is the sum of the limits.
  \begin{equation*}
    \lim_{x\to a}\,f(x)+g(x)\,
    =\lim_{x\to a}f(x)+\lim_{x\to a} g(x)
  \end{equation*}
\item[2] Likewise for the difference.
\begin{equation*}
  \lim_{x\to a}\,f(x)-g(x)\,=\lim_{x\to a}f(x)-\lim_{x\to a}g(x)
\end{equation*}
\item[4] The limit of the product of functions is the product of the 
limits.
  \begin{equation*}
    \lim_{x\to a}\,f(x)\cdot g(x)\,
    =\lim_{x\to a}f(x)\,\cdot\,\lim_{x\to a} g(x)
  \end{equation*}
\item[5] The limit of a quotient of functions is the quotient of the 
limits, provided that the denominator is not zero.
  \begin{equation*}
    \text{if $\lim_{x\to a} g(x)\neq 0$ then}
    \qquad
    \lim_{x\to a}\frac{f(x)}{g(x)}
    =\frac{\lim_{x\to a}f(x)}{\lim_{x\to a} g(x)}
  \end{equation*}
\end{enumerate}
\end{frame}




\begin{frame}{Two more laws}
\begin{enumerate}
\item[6] The limit of a power is the power of the limit.
\begin{equation*}
  \lim_{x\to a}\left[f(x)\right]^n 
  =\left[\,\lim_{x\to a}f(x)\,\right]^n   
\end{equation*}
We haven't talked about powers~$n$ other than positive integers, but 
the rule extends to real number powers, with some exceptions.

\item[7] A special case of the prior rule, for instance with a 
power of $1/2$, is that
the limit of a root is the root of the limit.
\begin{equation*}
  \lim_{x\to a} \sqrt[n]{f(x)} 
  =\sqrt[n]{\lim_{x\to a}f(x)}   
\end{equation*}
This brings out the exceptions:~we can't take the square root of 
a negative number so
when $n$ is even this rule only applies when the limit is greater than~$0$.
\end{enumerate}
\end{frame}


\begin{frame}{Easy limits: direct substitution}
We can take a polynomials apart algebraically.
\begin{align*}
  \lim_{x\to 3}x^2+2x
  &=\lim_{x\to 3}x^2+\lim_{x\to 3}2x                 \\
  \uncover<2->{&=(\lim_{x\to 3}x\cdot \lim_{x\to 3}x)+(2\cdot\lim_{x\to 3}x)  \\}
  \uncover<3->{&=3\cdot 3+2\cdot 3           \\
            &=9+6=15}
\end{align*}
\uncover<3->{Similarly $\lim_{x\to -1}4x^2=4\cdot \lim_{x\to -1}x\cdot \lim_{x\to -1}x=4$.}

\uncover<4->{
Evaulate each using Limit Laws.
\begin{enumerate}
\item $\displaystyle \lim_{x\to -3}2x^3+6x^2-9$
\pause\item $\displaystyle \lim_{t\to 7}\frac{3t^2+1}{t^2-5t+2}$
\pause\item $\displaystyle \lim_{x\to 3}-3x^2+\frac{1-2x}{x^2+1}$
\end{enumerate}}
\end{frame}

\begin{frame}
\Co
If $\map{f}{\R}{\R}$ is a polynomial function
then $\lim_{x\to a}f(x)$ equals $f(a)$.
If it is a rational function $f(x)=n(x)/d(x)$ and
if $\lim_{x\to a}d(x)\neq 0$ then $\lim_{x\to a}f(x)$ equals 
$\lim_{x\to a}n(x)/\lim_{x\to a}d(x)$.

Two examples.
\begin{enumerate}
\item $\lim_{x\to -1}(3x^2+2x+1)^4
       =(3(-1)^2+2(-1)+1)^4=16$
\item \uncover<2->{$\displaystyle\lim_{x\to 3}\frac{4x^2-3x+1}{2x-4}
                   =\frac{\lim_{x\to 3}4x^2-3x+1}{\lim_{x\to 3}2x-4}
                   =\frac{4(3)^2-3(3)+1}{2(3)-4}=14$}
\end{enumerate}
\end{frame}


\section{Tough case: what if the denominator goes to zero?}

\begin{frame}
We said that if $\lim_{x\to a}d(x)\neq 0$ then $\lim_{x\to a}f(x)$ equals 
$\lim_{x\to a}n(x)/\lim_{x\to a}d(x)$.
What if the denominator has a limit of zero?

It could go either way.  
\end{frame}


\begin{frame}{The limit could fail to exist}

Find $\lim_{x\to 2} 1/(x-2)$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limit_laws002.pdf}}   
\end{center}
The limit from the left $\lim_{x\to 2^-}f(x)$ exists, 
as does the limit from the right $\lim_{x\to 2^+}f(x)$, 
but they are not equal. 
So the overall limit, $\lim_{x\to 2}f(x)$, does not exist. 
\end{frame}


\begin{frame}{Second case: the limit could exist}
Let $f(x)=(x^2-4)/(x-2)$ and consider $a=2$.
Because $\lim_{x\to 2}d(x)=0$ substituting $2$ for~$x$ isn't allowed.

\pause
To get the limit, do some algebra.
\begin{equation*}
  \lim_{x\to 2}\frac{x^2-4}{x-2}
  =\lim_{x\to 2}\frac{(x+2)(x-2)}{x-2}
  =\lim_{x\to 2}x+2
  =4
\end{equation*}
\pause
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limit_laws001.pdf}}   
  \qquad
  \begin{minipage}{2in}\raggedright
    The function~$f$ is defined near $a=2$,
    although not at $a=2$.
    When we take a limit we ask about
    what happens near~$a$ but not at~$a$.
  \end{minipage}
\end{center}
\end{frame}


\begin{frame}{A very important example of a limit of a quotient that exists
  although the denominator has a limit of zero}
Consider $\lim_{h\to 0} \frac{(h+1)^2-1}{h}$.
The denominator has a limit of zero.
But some algebra works to give an existing limit.
\begin{align*}
  \lim_{h\to 0} \frac{(h+1)^2-1}{h}
    &=\lim_{h\to 0} \frac{(h^2+2h+1)-1}{h}  \\
    &=\lim_{h\to 0} \frac{h^2+2h}{h}  \\
    &=\lim_{h\to 0} h+2 =2
\end{align*}

\bigskip
\fbox{\begin{minipage}{0.9\textwidth}
\alert{When we take a limit we ask about
    what happens near~$a$ but we do not care what happens exactly at~$a$.}
\end{minipage}}
\end{frame}


% Didn't get to it.
% \begin{frame}{Another example}
% Consider $\lim_{h\to 0} \frac{(h+2)^2-1}{h}$.
% Again the denominator has a limit of zero.
% \begin{align*}
%   \lim_{h\to 0} \frac{(h+2)^2-4}{h}
%     &=\lim_{h\to 0} \frac{(h^2+4h+4)-4}{h}  \\
%     &=\lim_{h\to 0} \frac{h^2+4h}{h}  \\
%     &=\lim_{h\to 0} h+4 =4
% \end{align*}
% \end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:

