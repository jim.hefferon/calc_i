#!/usr/bin/env python
import math

def f(x):
    y=x**2
    return y

def g(x):
    y=2*(x**2)
    return y

x_list=[]
for i in range(-60,61):
    x_list.append(i/10.0)
# x_list.sort()
dex=0
for x in x_list:
    x=x*1.0
    try:
        y=f(x)
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,f(x))
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (200+dex,x,g(x))
        dex+=1
    except:
        print "  %% f(%0.2f) or g(%0.02) not defined" % (x,x)
        
