#!/usr/bin/env python
import math

def f(x):
    y=(x+2)**3
    return y

x_list=[]
for i in range(-60,61):
    x_list.append(i/10.0)
# x_list.sort()
dex=0
for x in x_list:
    try:
        y=f(x)
        if math.fabs(y)>200:
            print "  %% |f(%0.2f)| > 200 so not used" % (x,)
        else:
            print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,f(x))
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
