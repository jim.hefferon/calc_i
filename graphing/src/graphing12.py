#!/usr/bin/env python
import math

def f(x):
    y=math.exp(x)
    return y

x_list=[-3.5,-3,-2.5,-2,-1.5,-1,-.5,0,.25,.5,.75,1.25,1.5,1.75,2]
x_list.sort()
dex=0
for x in x_list:
    x=x*1.0
    try:
        y=f(x)
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,y)
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
