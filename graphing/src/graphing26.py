#!/usr/bin/env python
import math

def f(x):
    y=1/(x**2)
    return y

x_list=[]
for i in range(-60,61):
    x_list.append(i/10.0)
# x_list.sort()
dex=0
for x in x_list:
    try:
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,f(x))
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
