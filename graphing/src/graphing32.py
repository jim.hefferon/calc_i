#!/usr/bin/env python
import math

def f(x):
    y=(x-4)/((x-2)*(x+1))
    return y

x_list=[]
for i in range(-20,-5):
    x_list.append(i*1.0)
for i in range(-49,51):
    x_list.append(i/10.0)
for i in range(5,21):
    x_list.append(i*1.0)
# x_list.sort()
dex=0
for x in x_list:
    try:
        y=f(x)
        if math.fabs(y)>200:
            print "  %% |f(%0.2f)| > 200 so not used" % (x,)
        else:
            print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,f(x))
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
