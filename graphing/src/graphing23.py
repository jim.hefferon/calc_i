#!/usr/bin/env python
import math
omega=2.0
phi0=.5

def f(omega,x,phi0):
    y=math.cos(omega*x+phi0)
    return y

x_list=[]
for i in range(-70,70):
    x_list.append(i/10.0)
# x_list.sort()
dex=0
for x in x_list:
    try:
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,f(omega,x,phi0))
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
