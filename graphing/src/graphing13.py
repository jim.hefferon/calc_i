#!/usr/bin/env python
import math

def f(x):
    y=math.log(x)
    return y

x_list=[0.01,0.025,0.05,0.10,0.25,0.5,0.75,1.25,1.5,1.75,2,3,4,5,6]
x_list.sort()
dex=0
for x in x_list:
    x=x*1.0
    try:
        y=f(x)
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,y)
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
