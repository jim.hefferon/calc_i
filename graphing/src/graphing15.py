#!/usr/bin/env python
import math

def f(x):
    y=math.sin(x)
    return y

x_list=[]
for i in range(-70,70):
    x_list.append(i/10.0)
# x_list.sort()
dex=0
for x in x_list:
    x=x*1.0
    try:
        y=f(x)
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,y)
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
