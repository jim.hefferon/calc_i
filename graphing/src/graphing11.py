#!/usr/bin/env python

def f(x):
    y=((x+2)**(-1))*((x-1)**(-2))
    return y

x_list=[2,3,4,5,0,-1,-2,-3,-4,1.5,1.45,1.35,1.25,1,.75,.65,.6,.5,-1.5,-1.75,-1.9,-1.95,-2.05,-2.1,-2.25,-2.5]
x_list.sort()
dex=0
for x in x_list:
    x=x*1.0
    try:
        y=f(x)
        print "  z%d=(%0.2fw,%0.2fv) transformed hgt_adjust;" % (dex,x,y)
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
