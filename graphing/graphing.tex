\documentclass[10pt,xcolor={pdftex,table},t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{present}
\usepackage{colortbl}


\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

\usepackage[english]{babel}
% or whatever


\title[Graphing] % (optional, use only with long paper titles)
{Graphing}

%\subtitle
%{A Variety of Methods}

\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu}
}
\date{}


\usepackage[T1]{fontenc}

% \usepackage{settobox}
% I use this in tables where the header is centered and the body is right justified single digit numbers
% \newsavebox{\jhnumbox}  % as wide as a sign and a digit
% \sbox{\jhnumbox}{$-5$}
% \newlength{\jhnumlength}
% \settoboxwidth{\jhnumlength}{\jhnumbox}

%  \setcounter{secnumdepth}{2}
%\usepackage{dcolumn}
%  \newcolumntype{.}{D{.}{.}{-1}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}



\section{Graphing without calculus}
\begin{frame}
  \frametitle{Polynomial functions}

Polynomial graphs
with positive exponents have two basic shapes.
\begin{center}
  \includegraphics{graphing-0.mps}
  \qquad
  \includegraphics{graphing-1.mps}    
\end{center}

\pause
There are two edge cases, $n=1$ and $n=0$.
\begin{center}
  \includegraphics{graphing-2.mps}
  \qquad
  \includegraphics{graphing-3.mps}    
\end{center}
\end{frame}



\begin{frame}
Constants make the graph somewhat steeper or shallower.  
\begin{center}
  \includegraphics{graphing-19.mps}
\end{center}
\pause
But still, higher-powered $x$'s race ahead
\begin{center}
  \includegraphics{graphing-20.mps}
\end{center}
so we will not talk about constant multiples here.
\end{frame}



\begin{frame}
We can shift the graph to the right or left
\begin{center}
  \includegraphics{graphing-33.mps}
  \qquad
  \includegraphics{graphing-34.mps}    
\end{center}
\pause
or up or down.
\begin{center}
  \includegraphics{graphing-35.mps}
\end{center}  
\pause
This brings in lower-order terms: $(x-1)^2=x^2-2x+1$.
\end{frame}



\begin{frame}
Polynomials can have all kinds of lower-order terms.
For instance, $y=x^2-1$ has a constant term but no linear term.
\begin{center}
  \includegraphics{graphing-5.mps}
\end{center}
\pause
Lower-order terms alter the behavior near $0$ but
the graphs keep the same 
behavior out toward infinity.
\begin{center}
  \includegraphics{graphing-6.mps}
  \quad
  \includegraphics{graphing-7.mps}    
\end{center}
\end{frame}



\begin{frame}
We factor polynomials to find their roots.
\begin{center}
  \includegraphics{graphing-21.mps}
\end{center}
\pause
There is a best way to do this.
\begin{center}
  \includegraphics{graphing-22.mps}    
\end{center}
\end{frame}



\begin{frame}
The behavior of the graph at the root is determined by the exponent.
\begin{center}
  \includegraphics{graphing-33.mps}
  \quad
  \includegraphics{graphing-34.mps}
\end{center}
\pause
\begin{center}
  \includegraphics{graphing-36.mps}
  \quad
  \includegraphics{graphing-37.mps}    
\end{center}
\end{frame}


\begin{frame}
\frametitle{Rational polynomial functions}
Negative powers behave similarly to positive powers.
\begin{center}
  \includegraphics{graphing-9.mps}
  \qquad
  \includegraphics{graphing-8.mps}
\end{center}

\pause
Shifting the graph shifts the vertical asymptote.
\begin{center}
  \includegraphics{graphing-27.mps}
  \quad
  \includegraphics{graphing-28.mps}    
\end{center}
\end{frame}


\begin{frame}
With lower-order terms, the behavior of the graph 
at the asymptote is determined by the exponent.  
\begin{center}
  \includegraphics{graphing-11.mps}    
\end{center}
\pause
Here is another.
\begin{center}
  \includegraphics{graphing-29.mps}
\end{center}
\end{frame}



\begin{frame}
Adding numerators to those fractions combines the two things
we have discussed.  
\begin{center}
  \includegraphics{graphing-30.mps}    
\end{center}
In the lower right the graph crosses the x-axis at $x=4$. 
That is, near infinity this graph acts like $y=1/x^2$.
\end{frame}




\begin{frame}
  \frametitle{Transcendental functions}
Here are the exponential
$y=e^x$ and the natural logarithm $y=\ln(x)$.
\begin{center}
  \includegraphics{graphing-12.mps}
  \qquad    
  \includegraphics{graphing-13.mps}
\end{center}
\pause
They are inverse, and are thus symmetric about the diagonal line $y=x$.
\begin{center}
  \includegraphics{graphing-14.mps}
\end{center}
\end{frame}




\begin{frame}
The graphs of 
$y=\sin(x)$ and $y=\cos(x)$ are related by a phase shift.
\begin{center}
  \includegraphics{graphing-15.mps}
  \qquad    
  \includegraphics{graphing-16.mps}
\end{center}
The graphs of $y=\tan(x)$ and $y=\sec(x)$ are undefined at the same $x$'s.
\begin{center} 
  \includegraphics{graphing-17.mps}
  \qquad    
  \includegraphics{graphing-18.mps}
\end{center}
\end{frame}


\begin{frame}
We can shift and scale the graphs of these functions as we did for polynomials.

\begin{overprint} % oops! figures different sizes so I added hack space
\onslide<1>\hspace*{3em}\hspace*{.65em}\rule{0pt}{7.25ex}\includegraphics{graphing-23.mps}
\onslide<2>\hspace*{3em}\includegraphics{graphing-24.mps}
\end{overprint}
\end{frame}



\end{document}




