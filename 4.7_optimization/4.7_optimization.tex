\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.7:\ \ Optimization under constraints}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Optimizing over a closed interval}

\begin{frame}{Maximal area rectangle}
\Ex We want to enclose a rectangular region. 
We have 100 meters of fencing.
What is the largest area that we can get? 
\begin{center}
  \includegraphics[height=0.25\textheight]{pix/sheep.jpg}
\end{center}

We want to optimize $\text{Area}=x\cdot y$ subject to $2x+2y=100$
(with $x,y\geq 0$).
\pause
Recast the constraint as $y=50-x$ and substitute into the goal 
to get 
$A=x(50-x)$ with $0\leq x\leq50$.

\pause
The derivative is $A'(x)=-2x+50=-2(x-25)$ so the only
critical number is~$25$.
Compute the value at the critical number and the endpoints:
$A(25)=625$ and $A(0)=A(50)=0$
(with $A''(x)=-2$, the Second Derivative Test shows the same thing).
The area is at a maximum when the region is a square,
at $A(25)=625$~square meters.
\end{frame}

\begin{frame}
  \begin{center}
   \includegraphics{asy/optimization003.pdf} 
  \end{center}
\end{frame}



\begin{frame}{Maximize enclosed area}
A farmer has 2400 meters of fence, and will enclose a yard by using 
an existing wall as the fourth side of a rectangle.  
How to get the largest area?
\begin{center}
  \includegraphics{asy/optimization000.pdf}
\end{center}

\pause
We want to maximize $A=x\cdot y$ subject to the constraint 
that $2400=2x+y$.
Solving $y=2400-2x$ and substituting gives $A(x)=x(2400-2x)=2400x-2x^2$,
subject to $0\leq x\leq 1200$.

\pause
With $A'(x)=2400-4x$ the only critical number is $x=600$.
Check the three points: $A(600)=720\,000$, $A(0)=A(1200)=0$, 
to get that the maximum is $7200$.
(Other tests give the same conclusion.)
\end{frame}

\begin{frame}
  \begin{center}
   \includegraphics{asy/optimization004.pdf} 
  \end{center}
\end{frame}
% sage: def f(x):
% ....:     return 2400*x-2*x^2
% ....: 
% sage: f(600)
% 720000



\begin{frame}{Application}
We will make a rain gutter out of a long piece of metal that is $12$~inches 
wide, by
bending it into a ``U'' shape where the sides are $4$ inches long.
What angle maximizes the cross-sectional area?
\begin{center}
  \only<1>{\includegraphics{asy/optimization006.pdf}}%    
  \only<2->{\includegraphics{asy/optimization007.pdf}}    
\end{center}
\only<2->{The area of the left triangle is
\begin{equation*}
 \frac{1}{2}\cdot \text{base}\cdot\text{height}
 =\frac{1}{2}\cdot 4\cos\theta\cdot 4\sin\theta 
\end{equation*}
and so the total area of the two triangles plus the middle is this.
\begin{equation*}
  2\cdot\left[\frac{1}{2}\cdot 4\cos\theta\cdot 4\sin\theta\right]
    +4\cdot 4\sin\theta
  =16\sin\theta\cdot(\cos\theta+1)
  \quad 0\leq\theta\leq\frac{\pi}{2}
\end{equation*}
}
\end{frame}

\begin{frame}
% We are maximizing $A(\theta)=16\sin\theta\cdot(\cos\theta+1)$ 
% on the interval $\closed{0}{\pi/2}$.
Some calculation gets the derivative.
\begin{equation*}
  A'(\theta)=16\cdot (2\cos\theta-1)\cdot(\cos\theta+1)
\end{equation*}
The critical numbers are where $\cos\theta=1/2$ or 
$\cos\theta=-1$.
In the interval of interest that's $\theta=\pi/3$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/optimization008.pdf}}    
  \quad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End number $0$            &$A(0)=0$ \\
    End number $\pi/2$         &$A(\pi/2)=16$ \\
    Critical number $\pi/3$    &$A(\pi/3)=12\sqrt{3}\approx 20.8$ 
  \end{tabular}
\end{center}
\end{frame}



\begin{frame}{Minimize travel time}
We want to get from $A$ to~$B$.
Travelling West to East we can move at $300$ units per second
but once we leave that horizontal line we travel slower, 
at $200$~units per second.
We want to find the intermediate point~$C$ from which we should
cut over for a minimal time trip.
\begin{center}
  \includegraphics{asy/optimization002.pdf}
\end{center}
Of course, $\text{distance}=\text{rate}\cdot\text{time}$ and so
$\text{time}=\text{distance}/\text{rate}$.
\end{frame}

\begin{frame}
Here is the total time for $x$ in $\closed{0}{10}$.
\begin{equation*}
  T(x)=\frac{10-x}{300}+\frac{\sqrt{16+x^2}}{200}
  \qquad 
  T'(x)=\frac{-1}{300}+\frac{x}{200\sqrt{16+x^2}}
\end{equation*}
Set the derivative equal to zero to get 
$\sqrt{16-x^2}=3x/2$, and so $x=\sqrt{4\cdot 16/5}\approx 3.58$.
\end{frame}
% sage: n(sqrt(64/5))
% 3.57770876399966

\begin{frame}
This function $T(x)$ gives a curve with a very small curvature.
\begin{center}
 \includegraphics{asy/optimization005.pdf} 
\end{center}
\textit{Remark:}
To get from one place to another, light takes the path of minimal time.
The problem's numbers are $300$ and $200$ because in air 
light travels at $300\,000$~km per second while in glass it travels
at $200\,000$ km per second.
\end{frame}
% sage: f=((10-x)/300) + ((sqrt(16+x^2))/200)
% sage: plot(f, 0, 10)





\section{Optimizing over an open interval}

\begin{frame}{Second Derivative Test special case}
Fix an interval
$\open{a}{b}$ (the endpoints may be infinite).
Consider a twice differentiable function
that on the interval
has a single critical point.
If  
the second derivative is always positive 
then  critical point is a minimum,
while if it is always negative  then the critical point is a maximum.
\end{frame}

\begin{frame}{Enclosed volume}
\Ex
We will make a cylindrical can that encloses $64$~cubic centimeters.
What radius and height uses the least material?
\begin{center}
  \includegraphics[height=0.2\textheight]{pix/can.jpg}
\end{center}
The goal is to minimize the surface area $S(r,h)=2\pi r^2+2\pi rh$ subject to
the constraint that $64=\pi r^2h$.

\pause 
Solve the constraint for a variable, $h=64/\pi r^2$ and substitute
into the goal equation.
That gives $S(r)=2\pi r^2+2\pi r(64/\pi r^2)=2\pi r^2+(128/r)$
with $0<r<\infty$.

\pause
The derivative is $S'(r)=4\pi r-(128/r^2)$, and setting it to zero
gives the critical number $r=(32/\pi)^{1/3}\approx 2.17$.
The second derivative is $S''(r)=4\pi+(256/r^3)$ and 
$S''((32/\pi)^{1/3})=12\pi$ is positive, so it is a minimum.
\end{frame}



\begin{frame}{Minimal area cylinder}
A cylinder will have volume~$10$.
Minimize its surface area.

\pause
We must minimize $S=2\pi r^2+2\pi rh$
subject to the constraint that $\pi r^2h=10$. 

\pause
Solve the constraint to get $h=10/(\pi r^2)$, 
and then $A(r)=2\pi r^2+2\pi r(10/\pi r^2)=2\pi r^2+(20/r)$ with $0<r<\infty$.

\pause
This interval is open so we can't just plug in the endpoints.
Instead, observe that as $r\to\infty$ or as $r\to 0$ we have $A(r)\to \infty$.
So the minimum must be a critical point.
Solving the derivative $0=A'(r)=4\pi r^2-(20/r^2)$ 
gives $r=(5/\pi)^{1/3}\approx 1.17$.


\begin{center}\small
  \vcenteredhbox{\includegraphics{asy/optimization001.pdf}}
  \qquad
  \begin{minipage}{0.4\linewidth}
    Note that the cylinder has no maximal area.
  \end{minipage}
\end{center}
\end{frame}



\section{More exercises}

\begin{frame}{Practice}
\begin{enumerate}
\item Maximize the product of two nonnegative numbers $x,y$ that add to $16$.

\pause\smallskip
We want to maximize $P=xy$ subject to $x+y=16$.
Solve the constraint $y=16-x$ and substitute to get $P=x(16-x)=-x^2+16x$.
Setting the derivative equal to zero and solving leads to $x=8$.
Since this is a concave-down parabola, it is obviously a local maximum. 

\item We will make a rectangular box with an open top by starting with a
$4$-by-$3$ sheet, cutting out square corners all of the same size, and
then bending up the sides.
Maximize the box's volume.

\pause\smallskip
The volume is $V=(3-2x)(4-2x)\cdot x=4x^3-14x^2+12x$, for $x$ in 
$\closed{0}{3/2}$.
The derivative is $V'=12-28x+12x^2$ and the quadratic formula
gives two solutions, $(7-\sqrt{13})/6$ and $(7+\sqrt{13})/6$.
The second is outside the range as it is about $1.77$.
The maximum is the first, at approximately 
$((7-\sqrt{13})/6,3.032)$.
% \item Maximize the area of a right triangle, subject to the restriction
% that the length of the legs is $10$.
% \item Find the point on the curve $y=\sqrt{x}$ closest to $(3,0)$.
% \item Find the rectangle of maximum area whose base lies on the
% $x$~axis and that is inscribed in the 
% parabola $y=9-x^2$.
\end{enumerate}
\end{frame}
% sage: def f(x):
% ....:     return( 4*x^3-14*x^2+12*x)
% ....: 
% sage: f(-1/6*sqrt(13) + 7/6)
% -1/54*(sqrt(13) - 7)^3 - 7/18*(sqrt(13) - 7)^2 - 2*sqrt(13) + 14
% sage: n(f(-1/6*sqrt(13) + 7/6))
% 3.03230246596414
% sage: n(f(+1/6*sqrt(13) + 7/6))
% -0.439709873371553
% sage: n(+1/6*sqrt(13) + 7/6)
% 1.76759187924400
% sage: n(-1/6*sqrt(13) + 7/6)
% 0.565741454089335


\begin{frame}{Additional exercises}
\begin{enumerate}
\item Maximize the area of a right triangle, subject to the restriction
that the lengths of the legs adds to ten.

\pause\item Find the point on the curve $y=\sqrt{x}$ closest to $(3,0)$.

\pause\item Find the rectangle of maximum area whose base lies on the
$x$-axis, and that is instcibed in the parabola $y=9-x^2$. 
\end{enumerate}
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
