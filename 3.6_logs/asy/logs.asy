// logs.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "logs%03d";

import graph;



// ================ Graph of a function and its inverse =======
real f0(real x) {return exp(x);}
real f0a(real x) {return log(x);}

picture pic;
int picnum = 0;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft-0.1,log(3)+0.1), FCNPEN);
draw(pic, graph(f0a,0.18,xright+0.2), FCNPEN);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), highlight_color, highlight_color);
label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), highlight_color, highlight_color);
label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add line y=x .............
picture pic;
int picnum = 1;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft-0.1,log(3)+0.1), FCNPEN);
draw(pic, graph(f0a,0.18,xright+0.2), FCNPEN);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add tangent lines .............
picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path fcn =  graph(f0,xleft-0.1,log(3)+0.1);
path fcn_inv = graph(f0a,0.18,xright+0.2); 
draw(pic,fcn,black);
draw(pic,fcn_inv,black);

real t = 0.3;
pair fcn_pt = (t,exp(t));
pair fcn_inv_pt = (fcn_pt.y,fcn_pt.x);

filldraw(pic, circle(fcn_pt, 0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle(fcn_inv_pt, 0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

// draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);
real fcn_pt_time = times(fcn, fcn_pt)[0];
pair fcn_tangent_dir = 1.5*dir(fcn, fcn_pt_time);
path fcn_tangent = (fcn_pt-fcn_tangent_dir)--(fcn_pt+fcn_tangent_dir);
draw(pic, fcn_tangent, highlight_color);

real fcn_inv_pt_time = times(fcn_inv, fcn_inv_pt)[0];
pair fcn_inv_tangent_dir = 1.5*dir(fcn_inv, fcn_inv_pt_time);
path fcn_inv_tangent = (fcn_inv_pt-fcn_inv_tangent_dir)--(fcn_inv_pt+fcn_inv_tangent_dir); 
draw(pic, fcn_inv_tangent, highlight_color);

real p = 0.6; // length of fcn_tangent_slope
path fcn_tangent_slope_path = fcn_pt--(fcn_pt+(p,0))--point(fcn_tangent,times(fcn_tangent,fcn_pt.x+p)[0]);
draw(pic, fcn_tangent_slope_path,highlight_color+dotted);
draw(pic, reflect((0,0),(1,1))*fcn_tangent_slope_path,highlight_color+dotted);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// === From 3.4 asy file ===
// === Composition across side-by-side axes === 

picture pic;
int picnum = 3;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

path mapstopath = (0,2)--(AXIS_SEP,4);
draw(pic,"$f$", subpath(mapstopath,0.1,0.9),
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));
path mapstopath_a = (AXIS_SEP,4)--(2*AXIS_SEP,2);
draw(pic,"$g$", subpath(mapstopath_a,0.1,0.9),
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

real delta =.3;
real epsilon = 2*delta;
real phi = delta;
draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);
draw(pic,(2*AXIS_SEP,2+phi)--(2*AXIS_SEP,2-phi), highlight_color);
  filldraw(pic, circle((2*AXIS_SEP,2+phi),0.05), white, highlight_color);
  filldraw(pic, circle((2*AXIS_SEP,2-phi),0.05), white, highlight_color);

real[] T5 = {2};
real[] T5a = {4};
real[] T5b = {2};

yaxis(pic, L="",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$a$",LeftSide),T5,Size=AXIS_TICK_SIZE),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("\raisebox{6pt}{$f(a)=b$}",RightSide),T5a,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XEquals(2*AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$g(f(a))=a$",RightSide),T5b,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// === Geometric derivation of derivative of arcsin y === 
real f4(real x) {
  return(sqrt(1-x^2));
}

// ........... theta and delta theta .............
picture pic;
int picnum = 4;

unitsize(pic,3cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

path f = graph(pic, f4, 0, 1);
draw(pic, f);

// angles
real theta = radians(35);
real delta_theta = radians(20);

// points
real x = cos(theta);
real y = sin(theta);
real x_prime = cos(theta+delta_theta);
real y_prime = sin(theta+delta_theta);
pair xy = (x,y);
pair xy_prime = (x_prime,y_prime);
dotfactor = 4;
// radii
draw(pic, (0,0)--xy, highlight_color);
draw(pic, (0,0)--xy_prime, highlight_color);
dot(pic, "$(x,y)$", xy, E);
dot(pic, "$(x+\Delta x,y+\Delta y)$", xy_prime, E);

// arcs for angles
real theta_rad = 0.2;
real delta_theta_rad = 0.25;
path theta_arc = arc((0,0), theta_rad, 0, degrees(theta), CCW);
path delta_theta_arc = arc((0,0), delta_theta_rad, degrees(theta), degrees(theta+delta_theta), CCW);
draw(pic, "$\theta$", theta_arc);
draw(pic, Label("$\Delta\theta$",Relative(0.7)), delta_theta_arc);

// real [] T4 = {cos(theta)};
xaxis(pic, L="",  // label
      axis=XEquals(0),
      xmin=ybot-0.1, xmax=ytop+0.1,
      p=currentpen,
      ticks=NoTicks, 
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XEquals(0),
      ymin=ybot-0.1, ymax=ytop+0.1,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........... lines .............
picture pic;
int picnum = 5;

unitsize(pic,3cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

path f = graph(pic, f4, 0, 1);
draw(pic, f);

// angles
real theta = radians(35);
real delta_theta = radians(20);

// points
real x = cos(theta);
real y = sin(theta);
real x_prime = cos(theta+delta_theta);
real y_prime = sin(theta+delta_theta);
pair xy = (x,y);
pair xy_prime = (x_prime,y_prime);
dotfactor = 4;
// radii
draw(pic, (0,0)--xy);
draw(pic, (0,0)--xy_prime);

// arcs for angles
real theta_rad = 0.2;
real delta_theta_rad = 0.25;
path theta_arc = arc((0,0), theta_rad, 0, degrees(theta), CCW);
path delta_theta_arc = arc((0,0), delta_theta_rad, degrees(theta), degrees(theta+delta_theta), CCW);
draw(pic, "$\theta$", theta_arc);
draw(pic, Label("$\Delta\theta$",Relative(0.7)), delta_theta_arc);

// lines
path y_line = (0,y)--xy;
path x_line = (x,0)--xy;
path x_prime_line = (x_prime,0)--xy_prime;
draw(pic, y_line, highlight_color);
draw(pic, x_line, highlight_color);
draw(pic, x_prime_line, highlight_color);
dot(pic, "$(x,y)$", xy, E);
dot(pic, "$(x+\Delta x,y+\Delta y)$", xy_prime, E);

// locate cos and sin
real loc_sin=1.2;    // how far out to put the label line  
draw(pic, "$y=\sin\theta$", (loc_sin,0)--(loc_sin,y), highlight_color, Arrows(4), Bars);
real loc_cos=-0.15;   // how far down to put the label line
draw(pic, "$x=\cos\theta$", (0,loc_cos)--(x,loc_cos), highlight_color, Arrows(4), Bars);

real [] T5 = {cos(theta)};
xaxis(pic, L="",  // label
      axis=XEquals(0),
      xmin=ybot-0.1, xmax=ytop+0.1,
      p=currentpen,
      ticks=RightTicks("%", T5, Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$x$", cos(theta));

real [] T5a = {sin(theta)};
yaxis(pic, L="",  // label
      axis=XEquals(0),
      ymin=ybot-0.1, ymax=ytop+0.1,
      p=currentpen,
      ticks=LeftTicks("%",T5a, Size=3pt),
      arrow=Arrows(TeXHead));
// labely(pic, "$y$", sin(theta));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........... angles .............
picture pic;
int picnum = 6;

unitsize(pic,3cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

path f = graph(pic, f4, 0, 1);
draw(pic, f);

// angles
real theta = radians(35);
real delta_theta = radians(20);

// points
real x = cos(theta);
real y = sin(theta);
real x_prime = cos(theta+delta_theta);
real y_prime = sin(theta+delta_theta);
pair xy = (x,y);
pair xy_prime = (x_prime,y_prime);
dotfactor = 4;
// radii
draw(pic, (0,0)--xy);
draw(pic, (0,0)--xy_prime);

// arcs for angles
real theta_rad = 0.2;
real delta_theta_rad = 0.25;
path theta_arc = arc((0,0), theta_rad, 0, degrees(theta), CCW);
path delta_theta_arc = arc((0,0), delta_theta_rad, degrees(theta), degrees(theta+delta_theta), CCW);
draw(pic, "$\theta$", theta_arc);
draw(pic, Label("$\Delta\theta$",Relative(0.7)), delta_theta_arc);

// lines
path y_line = (0,y)--xy;
path x_line = (x,0)--xy;
path x_prime_line = (x_prime,0)--xy_prime;
draw(pic, y_line);
draw(pic, x_line);
draw(pic, x_prime_line);
// dot(pic, "$(x,y)$", xy, E);
// dot(pic, "$(x+\Delta x,y+\Delta y)$", xy_prime, e);

// locate cos and sin
// real loc_sin=1.2;    // how far out to put the label line  
// draw(pic, "$y=\sin\theta$", (loc_sin,0)--(loc_sin,y), highlight_color, Arrows(4), Bars);
// real loc_cos=-0.15;   // how far down to put the label line
// draw(pic, "$x=\cos\theta$", (0,loc_cos)--(x,loc_cos), highlight_color, Arrows(4), Bars);

// triangle of interest
real lower_left_time = intersect(y_line, x_prime_line)[0];
pair lower_left = point(y_line, lower_left_time);
draw(pic, lower_left--xy--xy_prime--cycle, highlight_color);  

real loc_delta_theta=0.05;    // how far out to put the label line  
draw(pic, Label("$\Delta \theta$",Relative(0.6)), shift(loc_delta_theta,loc_delta_theta)*(xy--xy_prime), 1.5*E, highlight_color, arrow=Arrows(4), Bars);

// label(pic, Label("$\Delta y$",filltype=Fill(white)), (x_prime-0.1,y+0.125), highlight_color);
pair theta_pt = xy_prime-(0.15,0.175);
path theta_pointer = (theta_pt+(-0.02,0.06)){E}..{E}(xy_prime+(0.025,-0.05));
draw(pic, theta_pointer, highlight_color);
label(pic, Label("$\theta$",filltype=Fill(white)), theta_pt, NW, highlight_color);
// other_theta; intermediate angle
pair other_theta_pt = xy+(0.16,-0.14);
path other_theta_pointer = (other_theta_pt+(-0.00,0.00)){W}..{W}(xy+(-0.08,-0.025));
draw(pic, other_theta_pointer, black);
label(pic, Label("$\theta$",filltype=Fill(white)), other_theta_pt, black);

real [] T6 = {x, x_prime};
xaxis(pic, L="",  // label
      axis=XEquals(0),
      xmin=ybot-0.1, xmax=ytop+0.1,
      p=currentpen,
      ticks=RightTicks("%", T6, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);
labelx(pic, "$x+\Delta x$", x_prime);

real [] T6a = {y, y_prime};
yaxis(pic, L="",  // label
      axis=XEquals(0),
      ymin=ybot-0.1, ymax=ytop+0.1,
      p=currentpen,
      ticks=LeftTicks("%", T6a, Size=3pt),
      arrow=Arrows(TeXHead));
labely(pic, "$y$", y);
labely(pic, "$y+\Delta y$", y_prime);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



