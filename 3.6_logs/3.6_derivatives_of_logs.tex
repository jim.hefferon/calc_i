\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.6\ \ Derivatives of logarithms and inverse trigonometric functions}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}
% Change the default thickness of the underbars.
% https://tex.stackexchange.com/a/559081/339
% \usepackage{xpatch}
% \MHInternalSyntaxOn
% \xpatchcmd\upbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}

% \xpatchcmd\downbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}
% \MHInternalSyntaxOff

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Derivatives of logarithmic functions}


\begin{frame}{Logarithm formulas}
By definition, $\log_b(y)=x$ means that $b^x=y$, that is,
$\log_b(y)$ asks the question $b^{\text{what?}}=y$.
When the base is $e$ we have $\ln(y)=x$ 
if and only if $e^x=y$.
So $\ln(y)$ asks the question $e^{\text{what?}}=y$.

\begin{itemize}
\item Use this to translate between bases.
  \begin{equation*}
    \log_b(x)=\frac{\ln x}{\ln b}
  \end{equation*}
\item The logarithm of a product is the sum of the logarithms. 
  \begin{equation*}
    \ln(x\cdot y)=\ln x+\ln y
  \end{equation*}
\item Because of that, powers move out front, $\ln(x^n)=n\cdot \ln x $,
  reciprocals become negatives, $\ln(1/x)=-\ln x $, and 
  quotients become differences, $\ln(x/y)=\ln x-\ln y $.
\end{itemize}
\end{frame}



\begin{frame}{Derivative of $\log_b(x)$}
Start with $y=\log_b(x)$.
By definition $b^y=x$.
Differentiate implicitly.
\begin{align*}
   b^{y(x)} &=x         \\
  \underbracket{b^y\ln(b)}\cdot\underbracket{\frac{dy}{dx}} &=1  \\
  \frac{dy}{dx}  &=\frac{1}{b^y\ln b}=\frac{1}{x\ln b}
\end{align*}

\medskip
This is the \alert{derivative of a logarithmic function} $f(x)=\log_b(x)$,
including where the base is $e$.
\begin{equation*}
  {\color{red}
  \frac{d\,\log_b(x)}{dx}=\frac{1}{x\ln(b)}
  \qquad
  \frac{d\,\ln (x)}{dx}=\frac{1}{x}}  
\end{equation*}
\end{frame}


\begin{frame}{Practice}
Find the derivative of each.
\begin{enumerate}
\item $\displaystyle f(x)=x^2\cdot\ln x$
  \pause \\ The Product Rule gives $\displaystyle f'(x)=x^2\cdot(1/x)+\ln(x)\cdot 2x
                             =x\cdot(1+2\ln(x))$

\item $\displaystyle \ln (x^3+3x+4)$
  \pause \\ The Chain Rule gives $\displaystyle \frac{1}{x^3+3x+4}\cdot(3x^2+3)$.
% \item $\displaystyle \ln 4x$
% \pause \\ Another Chain Rule: $\displaystyle \frac{1}{4x}\cdot 4=\frac{1}{x}$
\item $\displaystyle x\ln x$
\pause \\ Product Rule: $\displaystyle x\cdot\frac{1}{x}+\ln(x)\cdot 1 =\ln(x)+1$
\item $\displaystyle \ln (\sec x)$
\pause \\ $\displaystyle \frac{1}{\sec x}\cdot \sec x\tan x=\tan x$
\item $\displaystyle \frac{\ln x^2}{x^2}$
\pause \\ $\displaystyle \frac{x^2\cdot (\frac{1}{x^2}\cdot 2x)-\ln(x^2)\cdot (2x)}{(x^2)^2}=\frac{2x(1-\ln(x^2))}{x^4}=\frac{2-2\ln x^2}{x^3}$
\end{enumerate}
\end{frame}




\section{Derivatives of inverse trig functions}

\begin{frame}{Derivative of the arcsine function $f(x)=\sin^{-1}(x)$}
% Consider the function $f(x)=\sin^{-1}(x)$.
If $y=\sin^{-1}(x)$ then $\sin y=x$.
To differentiate implicitly, 
think of $y$ as a function of $x$, giving $\sin y(x)=x$.
Use the Chain Rule.
\begin{equation*}
  \underbracket{\cos y}\cdot\underbracket{\frac{dy}{dx}}=1
  \quad\Longrightarrow\quad
  \frac{dy}{dx}=\frac{1}{\cos y}
\end{equation*}
Convert that to a function of $x$
with $\cos^2y+\sin^2 y=1$. 
\begin{equation*}
  \cos y=\sqrt{1-\sin^2 y}=\sqrt{1-x^2}
\end{equation*}
This is the derivative.
\begin{equation*}
  \frac{d\,\sin^{-1} x}{dx}=\frac{1}{\sqrt{1-x^2}}
\end{equation*}

\pause\medskip
(In particular, the derivative of $f(x)=\sin^{-1}(x)$ is not
$f'(x)=-1\cdot\sin^{-2}(x)$.
The notation $\sin^{-1}(x)$ does not mean $1/\sin(x)$.)
\end{frame}





\begin{frame}{Extra: geometric proof with a unit circle}
Arcsine's output is an angle.
On a unit circle we have $sin\theta=y$, so it is convenient to 
change the independent variable to $y$, and think about $\theta=\arcsin y$.

\pause
We want the derivative $d\theta/dy$. 
To get the relationship between $dy$ and $d\theta$,
here is a picture of what happens when we 
go from $\theta$ to $\theta+\Delta\theta$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/logs004.pdf}}
\end{center}
% (Recall that a radius intersects a circle at a right angle, that is, 
% the radius is perpendicular to a tangent.)
\end{frame}


\begin{frame}
Drop perpendiculars from the two points and also make the
perpendicular horizontal line from $(x,y)$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/logs005.pdf}}
\end{center}
Note that $x=\cos\theta=\sqrt{1-y^2}$.  

We will relate $\Delta \theta$ to $\Delta y$
with the small triangle in the upper right.
\end{frame}


\begin{frame}
This small triangle is a right triangle.
The angle in its upper left is $\theta$. 
The hypoteneuse is approximately $\Delta \theta$,
because we work in radians so the arc length equals the angle measure.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/logs006.pdf}}
\end{center}
Its leg on the left is this.
\begin{equation*}
  \Delta y=\Delta\theta\cdot\cos\theta=\Delta\theta\cdot\sqrt{1-y^2}
\end{equation*}
Solve to get $\Delta y/\Delta \theta=1/\sqrt{1-y^2}$.
\end{frame}


\begin{frame}{Derivatives of inverse trig functions}
\begin{center}
  \begin{tabular}{l@{\hspace{3em}}l}
    $\displaystyle \frac{d\,\sin^{-1}x}{dx}=\frac{1}{\sqrt{1-x^2}} $  
      &$\displaystyle \frac{d\,\cos^{-1}x}{dx}=-\frac{1}{\sqrt{1-x^2}} $  
   \\[2.5ex]
    $\displaystyle \frac{d\,\tan^{-1}x}{dx}=\frac{1}{1+x^2} $  
      &$\displaystyle \frac{d\,\cot^{-1}x}{dx}=-\frac{1}{1+x^2} $  
   \\[2.5ex]
    $\displaystyle \frac{d\,\sec^{-1}x}{dx}=\frac{1}{x\,\sqrt{x^2-1}} $  
      &$\displaystyle \frac{d\,\csc^{-1}x}{dx}=-\frac{1}{x\,\sqrt{x^2-1}} $  
  \end{tabular}
\end{center}
\pause Practice: differentiate each.
\begin{enumerate}
\item $\displaystyle x^2\sin^{-1} x$
  \pause
  \begin{equation*}
    x^2\frac{1}{\sqrt{1-x^2}}+\sin^{-1}x\cdot 2x
    =\frac{x^2}{\sqrt{1-x^2}}+ 2x\sin^{-1} x
  \end{equation*}
\item $\displaystyle \tan^{-1}\sqrt{x}$
  \pause
  \begin{equation*}
    \underbracket{\frac{1}{1+(\sqrt{x})^2}}\cdot \underbracket{\frac{1}{2}x^{-1/2}}
    =\frac{1}{2(1+x)\sqrt{x}}
  \end{equation*}
\end{enumerate}
\end{frame}




\section{Derivatives of general inverse functions}

\begin{frame}{Derivative of inverse functions}
We found the deriviative of the inverse of the exponential function
and the inverses of the trigonometric functions.
The same implicit differentiation approach works more generally.

\Tm Let $f$ be differentiable and invertible, with inverse~$g$.
Then this is the derivative of $g(x)$.
\begin{equation*}
  g'(b)=\frac{1}{f'(g(b))}
\end{equation*}

Where $y=g(x)$ we have $f(y)=x$, by definition.
Use implicit differentiation.
\begin{equation*}
  \underbracket{f'(y)}\cdot\underbracket{\frac{df}{dx}}=1
\end{equation*}
Finish by solving for $dy/dx$ and substituting $g(x)$ for~$y$.
\end{frame}


\begin{frame}{Pictures: first}
Recall that if we start with the graph of the function then
the graph of inverse is the reflection of it across the line $y=x$.
\begin{center}
  \only<1>{\includegraphics{asy/logs000.pdf}}%    
  \only<2>{\includegraphics{asy/logs001.pdf}}%    
  \only<3->{\includegraphics{asy/logs002.pdf}}%    
\end{center}
\only<3->{If the line tangent to the function at $(a,b)$ has slope
$p/q$ then the line tangent to the inverse at the point $(b,a)$ has
slope $q/p$.}
\end{frame}

\begin{frame}{Pictures: second}
Also recall the side-by-side axes view of function composition.
Here $g$ is the inverse of~$f$ so the picture has 
$g\circ f\,(a)=g(b)=a$.
\begin{center}
  \includegraphics{asy/logs003.pdf}%    
\end{center}
The factor by which $g$ expands its input interval must be the reciprocal of the
factor by which $f$ expands its interval, $g'(b)=1/f'(a)$.
\end{frame}

\begin{frame}{The derivative of a function's inverse}
\begin{equation*}
  \bigl( f^{-1} \bigr)'\,(b)
  =\frac{1}{f'(\, f^{-1}(b) \,)}    
\end{equation*}

\pause
\Ex Find the derivative of the inverse of $f(x)=x^3$ 
from $f$ and its inverse $g(x)=f^{-1}(x)=\sqrt[3]{x}$.

\pause
\begin{equation*}
  \bigl( f^{-1} \bigr)'\,(x)
  =\frac{1}{f'(\, f^{-1}(x) \,)}    
  =\frac{1}{3(x^{1/3})^2}=\frac{1}{3}x^{-2/3}
\end{equation*}

\Ex Do the same for $f(x)=e^x\!$.

\pause
Of course $f'(x)=e^x$.
The inverse is $f^{-1}(x)=\ln(x)$.
\begin{equation*}
  \bigl( f^{-1} \bigr)'\,(x)=\frac{1}{e^{\ln(x)}}=\frac{1}{x}
\end{equation*}

  
\end{frame}

% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
