// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "derivative_function%03d";

import graph;


// ================ f(x)=x^2 =======
real f0(real x) {return x^2;}
real tangent0(real x) {return 4*(x-2)+4;}

picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=0; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft-0.1,xright-0.1), FCNPEN);
draw(pic, graph(tangent0,2-0.75,2+0.75), highlight_color);

filldraw(pic, circle((2,4),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ f(x)=absolute value =======
real f1(real x) {return abs(x);}

picture pic;
int picnum = 1;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f1,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ f(x)=step function =======
real f2(real x) {if (x<1) {
    return 0;
  } else {
    return 1;
  };}

picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f2,xleft-0.1,0.99), FCNPEN);
draw(pic, graph(f2,1,xright-0.1), FCNPEN);
filldraw(pic, circle((1,0),0.05), white, FCNPEN_COLOR);
filldraw(pic, circle((1,1),0.05), FCNPEN_COLOR, FCNPEN_COLOR);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ f(x)=vertical drop =======

picture pic;
int picnum = 3;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path fcn3 = (xleft-0.1,ytop-0.5)..(2,2){down}::(xright+0.1,ybot+0.5); 
draw(pic, fcn3, FCNPEN);
filldraw(pic, circle((2,2),0.05), FCNPEN_COLOR,  FCNPEN_COLOR);
draw(pic,(2,0.75)--(2,3.25), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ f(x)=x^{2/3} =======
real f4(real x) {return (x^2)^(1/3);}  // x^(2/3) fails https://math.stackexchange.com/a/326576/12012

picture pic;
int picnum = 4;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f4,xleft-0.1,-0.01), FCNPEN);
draw(pic, graph(f4,0.01,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


