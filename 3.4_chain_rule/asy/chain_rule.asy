// trigs.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "chain_rule%03d";

import graph;



// ================ horizontal fcn =======
real f0(real x) {return x^2;}

picture pic;
int picnum = 0;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 5;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=-1; ytop=10;

for (int i=0; i<=3; ++i) {
  path mapstopath = (0,i)--(AXIS_SEP,f0(i));
  draw(pic,subpath(mapstopath,0.1,0.9),
       arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE),
       highlight_color);
}

yaxis(pic, L="$x$",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$%.4g$",LeftSide),Step=5,step=1),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L=Label("$y$",RightSide),  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$%.4g$",RightSide)),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ 1/x drawn horizontally =======
real f1(real x) {return 1/x;}

picture pic;
int picnum = 1;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 5;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=10;

for (int i=1; i<=9; ++i) {
  path mapstopath = (0,i)--(AXIS_SEP,f1(i));
  draw(pic,subpath(mapstopath,0.1,0.9),
       arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE),
       highlight_color);
}

yaxis(pic, L="$x$",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$%.4g$",LeftSide),Step=5,step=1),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L=Label("$y$",RightSide),  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$%.4g$",RightSide),Step=5,step=1),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ expansion =======
real f2(real x) {return x^2;}

picture pic;
int picnum = 2;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

path mapstopath = (0,2)--(AXIS_SEP,4);
draw(pic,subpath(mapstopath,0.1,0.9),
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

real delta =.2;
real epsilon = 4*delta;
draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);

real[] T2 = {2};
real[] T2a = {4};

yaxis(pic, L="",  // label
      axis=XEquals(0),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Label("$%.4g$",LeftSide),T2,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$%.4g$",RightSide),T2a,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .. in symbols ...
real f2(real x) {return x^2;}

picture pic;
int picnum = 3;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

path mapstopath = (0,2)--(AXIS_SEP,4);
draw(pic,subpath(mapstopath,0.1,0.9),  arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

real delta =.5;
real epsilon = 1.5*delta;
draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);

real[] T2 = {2};
real[] T2a = {4};

yaxis(pic, L="",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$a$",LeftSide),T2, Size=AXIS_TICK_SIZE),
  arrow=Arrows(TeXHead));
labely(pic,"$a+\delta$",2+delta);
labely(pic,"$a-\delta$",2-delta);
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$f(a)$",RightSide),T2a, Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
labely(pic,"$f(a)-\varepsilon$",(AXIS_SEP,4-epsilon),align=E);
labely(pic,"$f(a)+\varepsilon$",(AXIS_SEP,4+epsilon),align=E);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .. positive derivative ...
picture pic;
int picnum = 4;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

real delta =.5;
real epsilon = 1.5*delta;

path mapstopath = (0,2+delta)--(AXIS_SEP,4+epsilon);
draw(pic,subpath(mapstopath,0.1,0.9), highlight_color,
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));
path mapstopath_a = (0,2-delta)--(AXIS_SEP,4-epsilon);
draw(pic,subpath(mapstopath_a,0.1,0.9), highlight_color,
         arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);

real[] T2 = {2};
real[] T2a = {4};

yaxis(pic, L="",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$a$",LeftSide),T2, Size=AXIS_TICK_SIZE),
  arrow=Arrows(TeXHead));
labely(pic,"$a+\delta$",2+delta);
labely(pic,"$a-\delta$",2-delta);
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$f(a)$",RightSide),T2a, Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
labely(pic,"$f(a)+\varepsilon$",(AXIS_SEP,4+epsilon),align=E);
labely(pic,"$f(a)-\varepsilon$",(AXIS_SEP,4-epsilon),align=E);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .. negative derivative ...
picture pic;
int picnum = 5;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

real delta =.5;
real epsilon = 1.5*delta;

path mapstopath = (0,2+delta)--(AXIS_SEP,4-epsilon);
draw(pic,subpath(mapstopath,0.1,0.9), black,
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));
path mapstopath_a = (0,2-delta)--(AXIS_SEP,4+epsilon);
draw(pic,subpath(mapstopath_a,0.1,0.9), black,
         arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);

real[] T2 = {2};
real[] T2a = {4};

yaxis(pic, L="",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$a$",LeftSide),T2, Size=AXIS_TICK_SIZE),
  arrow=Arrows(TeXHead));
labely(pic,"$a+\delta$",2+delta);
labely(pic,"$a-\delta$",2-delta);
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$f(a)$",RightSide),T2a, Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
labely(pic,"$f(a)+\varepsilon$",(AXIS_SEP,4-epsilon),align=E);
labely(pic,"$f(a)-\varepsilon$",(AXIS_SEP,4+epsilon),align=E);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .. Composition ...

picture pic;
int picnum = 6;

size(pic,0,5cm,keepAspect=true);

real AXIS_SEP = 2;
real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
ybot=0; ytop=5;

path mapstopath = (0,2)--(AXIS_SEP,4);
draw(pic,"$g$", subpath(mapstopath,0.1,0.9),
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));
path mapstopath_a = (AXIS_SEP,4)--(2*AXIS_SEP,1.5);
draw(pic,"$f$", subpath(mapstopath_a,0.1,0.9),
     arrow=EndArrow(ARROWSIZE), bar=BeginBar(BARSIZE));

real delta =.2;
real epsilon = 4*delta;
real phi = 2*delta;
draw(pic,(0,2+delta)--(0,2-delta), highlight_color);
  filldraw(pic, circle((0,2+delta),0.05), white, highlight_color);
  filldraw(pic, circle((0,2-delta),0.05), white, highlight_color);
draw(pic,(AXIS_SEP,4+epsilon)--(AXIS_SEP,4-epsilon), highlight_color);
  filldraw(pic, circle((AXIS_SEP,4+epsilon),0.05), white, highlight_color);
  filldraw(pic, circle((AXIS_SEP,4-epsilon),0.05), white, highlight_color);
draw(pic,(2*AXIS_SEP,1.5+phi)--(2*AXIS_SEP,1.5-phi), highlight_color);
  filldraw(pic, circle((2*AXIS_SEP,1.5+phi),0.05), white, highlight_color);
  filldraw(pic, circle((2*AXIS_SEP,1.5-phi),0.05), white, highlight_color);

real[] T5 = {2};
real[] T5a = {4};
real[] T5b = {1.5};

yaxis(pic, L="",  // label
      axis=XEquals(0),
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Label("$a$",LeftSide),T5,Size=AXIS_TICK_SIZE),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XEquals(AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("\raisebox{7.5pt}{$g(a)$}",RightSide),T5a,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XEquals(2*AXIS_SEP),
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=RightTicks(Label("$f(g(a))$",RightSide),T5b,Size=AXIS_TICK_SIZE),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ================ sin(x) =======
// real f0(real x) {return sin(x);}

// picture pic;
// int picnum = 0;

// size(pic,5cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
// ybot=-1; ytop=1;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f0,xleft-0.1,xright+0.1), highlight_color);

// // filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// // label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

// real pi=3.14159;
// real[] T={pi/2, pi, 3*pi/2, 2*pi};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//       ticks=RightTicks("%", T, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
// labelx(pic,"{\tiny $\pi$}",pi,0.5*S);
// labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,S);
// labelx(pic,"{\tiny$ 2\pi$}",2*pi,0.5*S);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

