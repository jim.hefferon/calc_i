% \DocumentMetadata{
%   lang        = en,
%   pdfversion  = 2.0,
%   pdfstandard = ua-2,
%   pdfstandard = a-4f, %or a-4
%   testphase   = 
%    {phase-III,
%     title,
%     table,
%     math,
%     firstaid}  
% }
\documentclass[10pt,t,serif,professionalfont,lualatex]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.4\ \ Chain rule}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

% Change the default thickness of the underbars.
% https://tex.stackexchange.com/a/559081/339
% \usepackage{xpatch}
% \MHInternalSyntaxOn
% \xpatchcmd\upbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}

% \xpatchcmd\downbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}
% \MHInternalSyntaxOff

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\section{Composition of functions}

\begin{frame}{Function composition}
The function $F(x)=\sin(x^2)$ is the \alert{composition} of the two
functions $f(x)=\sin x$ and $g(x)=x^2\!$.
We write $f\circ g(x)$ for $f(\,g(x)\,)$.

Observe that in $f\circ g$, while the $f$ is written first, 
it is performed second.
\begin{equation*}
  x\;\mapsto\; x^2\; \mapsto\; \sin(x^2)
\end{equation*}
Better than saying ``first'' and ``second'' is to think of 
$\sin$ as wrapping around $x^2$ and
to describe $f$ and~$g$ as the \alert{outer function} and
\alert{inner function}. 

\pause\medskip
We might guess that the derivative of a composition is the composition 
of the derivatives.
That's not right:~take $f(x)=x^3$ and $g(x)=x^2\!$, so that 
$f\circ g\,(x)=(x^2)^3=x^6\!$.
% So the derivative of the composition is $(f\circ g)'(x)=6x^5$,
% but the composition of the derivatives is 
% $f'(\,g'(x)\,)=3(2x)^2=12x^2\!$.
\begin{center}
  \begin{tabular}{c@{\hspace{2em}}c}
    \multicolumn{1}{c}{\textit{$(f\circ g)'(x)$}}
      &\multicolumn{1}{c}{\textit{$f'(\,g'(x)\,)$}}  \\ \hline
    \rule{0pt}{11pt}
    % $\displaystyle \frac{d\, x^6}{dx}=6x^5$
    $6x^5$
    &$3\cdot(2x)^2=12x^2$
  \end{tabular}
\end{center}
\end{frame}



\begin{frame}{Picturing composition with side-by-side axes}
Functions associate inputs with outputs. 
We are used to showing the input and output axes at right angles but 
for composition it helps to put them side-by-side.

Here are two examples of side-by-side pictures.
On the left is $f(x)=x^2$ and on the right is $g(x)=1/x$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/chain_rule000.pdf}}
  \hspace*{3em}
  \vcenteredhbox{\includegraphics{asy/chain_rule001.pdf}}
\end{center}
\end{frame}


\begin{frame}{The derivative is the local rate of expansion}
Consider $f(x)=x^2\!$.
Intervals around the input~$2$ are mapped to intervals around
the output~$4$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/chain_rule002.pdf}}
  \quad
  \begin{minipage}{0.5\linewidth}\small
    \begin{tabular}{l|l}
      \multicolumn{1}{c}{\textit{Input interval}} 
         &\multicolumn{1}{c}{\textit{Output interval}}  \\
      \hline
      $\open{1.9}{2.1}$ &\uncover<2->{$\open{3.61}{4.41}$}  \\
      $\open{1.99}{2.01}$ &\uncover<3->{$\open{3.9601}{4.0401}$}  \\
      $\open{1.999}{2.001}$ &\uncover<4->{$\open{3.996001}{4.004001}$}  \\
    \end{tabular}
  \end{minipage}
\end{center}
In the limit, the output interval is $f'(2)=4$ times as wide as the input
interval.
Another way to understand the derivative is as the function's local rate of 
expansion. 
\end{frame}
% sage: def f(x):
% ....:     return x*x
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("2-i=",2-i," 2+i=",2+i," f(2-i)=",f(2-i)," f(2+i)=",f(2+i))
% ....: 
% 2-i= 1.90000000000000  2+i= 2.10000000000000  f(2-i)= 3.61000000000000  f(2+i)= 4.41000000000000
% 2-i= 1.99000000000000  2+i= 2.01000000000000  f(2-i)= 3.96010000000000  f(2+i)= 4.04010000000000
% 2-i= 1.99900000000000  2+i= 2.00100000000000  f(2-i)= 3.99600100000000  f(2+i)= 4.00400100000000

% sage: def f(x):
% ....:     return (1/12)*x^2
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("i=",i, " f(3-i)-f(3)=",f(3-i)-f(3)," f(3+i)-f(3)=", f(3+i)-f(3)
% ....: )
% ....: 
% ....: 
% i= 0.100000000000000  f(3-i)-f(3)= -0.0491666666666667  f(3+i)-f(3)= 0.0508333333333334
% i= 0.0100000000000000  f(3-i)-f(3)= -0.00499166666666662  f(3+i)-f(3)= 0.00500833333333317
% i= 0.00100000000000000  f(3-i)-f(3)= -0.000499916666666600  f(3+i)-f(3)= 0.000500083333333290


% \begin{frame}{In symbols}
% The traditional symbols are: the input interval is $\open{a-\delta}{a+\delta}$ 
% and the output interval is $\open{f(a)-\varepsilon}{f(a)+\varepsilon}$.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/chain_rule003.pdf}}
% \end{center}
% The relationship between them is that as $\delta\to 0$ then 
% $\varepsilon\to f'(a)\cdot\delta$.
% \end{frame}



% \begin{frame}{Remark: negative derivatives}
% Incidentally, the prior picture is for positive derivatives.
% It is shown again on the left below 
% while on the right is a picture for negative derivatives.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/chain_rule004.pdf}}
%   \qquad
%   \vcenteredhbox{\includegraphics{asy/chain_rule005.pdf}}
% \end{center}
% However, we are not going to go into a full explication of this view of the
% derivative.
% We just want the right formula for
% composition and for that we use the picture on the left.
% \end{frame}
\begin{frame}
There is much more to say here.
But in this course 
we won't go into a full explication of this view of the
derivative.
We will just use the side-by-side picture of function
composition to see the right formula.  
\end{frame}

\begin{frame}{Picture of function composition $f\circ g\,(x)$}\vspace*{-1ex}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/chain_rule006.pdf}}
\end{center}
% We can read off the derivative of the composition.
Imagine that the left-hand arrow expands by a factor of $4$ and the right-hand
arrow expands by a factor of $0.5$. 
Then together the expansion factor of the composition
is the product $4\cdot 0.5=2$.
\begin{equation*}
  (f\circ g)'(a)=g'(a)\cdot f'(g(a))
  \qquad
  \left.\frac{d\,f\circ g}{dx}\right|_{x=a}
  =\left.\frac{d\,g}{dx}\right|_{x=a}
  \cdot\left.\frac{d\,f}{dx}\right|_{x=g(a)}
\end{equation*}
\end{frame}

\begin{frame}{Chain Rule}
If $g$ is differentiable at~$x$ and $f$ is differentiable
at $g(x)$ then $F=f\circ g$ is differentiable at~$x$ and the derivative is this.
\begin{equation*}
  F'(x)=f'(g(x))\cdot g'(x)
\end{equation*}

\pause
\textsc{Example}
The derivative of $\sin(x^2)$ is $\cos(\,x^2\,)\cdot 2x$.

\pause\textsc{Example}
If $f(x)=x^3$ and $g(x)=x^2$ then the Chain Rule gives that 
the derivative of the composition
$f\circ g\,(x)=(x^2)^3=x^6$ is $f'(g(x))\cdot g'(x)=3(x^2)^2\cdot 2x=6x^5\!$.

\alert{Important!}
The Chain Rule does not say that the derivative of the composition
is the product of the derivatives.
This is wrong:
\begin{equation*}
  \frac{d\,\sin(x^2)}{dx}=\cos(x)\cdot 2x
\end{equation*}
while this is right.
\begin{equation*}
  \frac{d\,\sin(x^2)}{dx}=\cos(x^2)\cdot 2x  
\end{equation*}
\end{frame}


\begin{frame}{Underbrackets}
For teaching I use underbrackets to show applications of
the Chain Rule.
\begin{equation*}
  \frac{d\,\sin(x^2)}{dx}=\underbracket{\cos(x^2)}\cdot\underbracket{2x}
\end{equation*}

\Ex
Consider $G(x)=\sqrt{2x^2-1}$.
It is the composition $f\circ g$ where $f(x)=\sqrt{x}=x^{1/2}$
with $g(x)=2x^2-1$.

The derivatives are
\begin{equation*}
  f'(x)=\frac{1}{2}\cdot x^{-1/2}=\frac{1}{2\sqrt{x}}
  \qquad
  g'(x)=4x
\end{equation*}
and this shows the derivative of the composition.
\begin{equation*}
  G'(x)=\underbracket{\frac{1}{2}\frac{1}{\sqrt{2x^2-1}}}\cdot\underbracket{4x}
       =\frac{2x}{\sqrt{2x^2-1}}
\end{equation*}


\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \frac{d\,\sin^3 x}{dx}$
\pause $=\displaystyle \underbracket{3\sin^2 x}\cdot\underbracket{\cos x}$
\item $\displaystyle \frac{d\, \sqrt{5x^2+1}}{dx}$
\pause $=\displaystyle \underbracket{\frac{1}{2\sqrt{5x^2+1}}}\cdot\underbracket{10x}=\frac{5x}{\sqrt{5x^2+1}}$
\item $\displaystyle \frac{d\, e^{\tan x}}{dx}$
\pause $=\displaystyle \underbracket{e^{\tan x}}\cdot \underbracket{\sec^2 x}$
% \item $\displaystyle \frac{d\,\frac{1}{(2x^4-x^2+1)^3}}{dx}$
% \pause $=\displaystyle \frac{6x(1-4x^2)}{(2x^4-x^2+1)^4}$
\item $\displaystyle \frac{d\,(\tan x+x)^9}{dx}$
\pause $=\displaystyle \underbracket{9(\tan x +x)^8}\cdot \underbracket{(\sec^2x+1)}$
\item $\displaystyle \frac{d\,\sec(x^3)}{dx}$
\pause $=\displaystyle \underbracket{\sec(x^3)\tan(x^3)}\cdot \underbracket{3x^2}$ 
% \item Find the line tangent to $\displaystyle y=3\cos(x^2)$ at $x=\sqrt{\pi/2}$.
% \pause \\[1ex] The derivative is 
%   $\displaystyle 3\cdot(-\sin(x^2))\cdot 2x=-6x\sin(x^2)$.
%   So the slope is $-6\sqrt{\pi/2}$.
%   The line goes through the point $(\sqrt{\pi/2},0)$. 
\end{enumerate}
\end{frame}


\begin{frame}{Compositions of more than two functions}
The Chain Rule wants us to proceed in steps.
We take the derivative of the outermost function, then take the 
derivative of what's inside.
The same applies when the composition is more deeply nested.

\Ex This is nested three-deep.
\begin{equation*}
  \frac{d\,\sin(\sqrt{x^2+1})}{dx}=\underbracket{\cos(\sqrt{x^2+1})}\cdot\underbracket{(1/2)(x^2+1)^{-1/2}}\cdot\underbracket{2x}
\end{equation*}

% \Rm Problems like this are the reason that I use underbrackets, since
% they help show the bookkeeping.

% \pause
\Ex Another three-deeper.
\begin{equation*}
  \frac{d\,\sin(e^{\cos x})}{dx}=\underbracket{\cos(e^{\cos x})}\cdot\underbracket{e^{\cos x}}\cdot\underbracket{(-\sin x)}
\end{equation*}
\end{frame}

\begin{frame}{More practice}
\begin{enumerate}
\item $\displaystyle \frac{d\,2^{\sin \sqrt{x}}}{dx}$
\pause $=\displaystyle \underbracket{2^{\sin \sqrt{x}}\ln(2)}\cdot\underbracket{\cos(\sqrt{x})}\cdot\underbracket{\frac{1}{2}x^{-1/2}}$ 
\item $\displaystyle \frac{d\,x\cdot\sin(x^2)}{dx}$ Note that it needs the Product Rule.
\pause $=\displaystyle 1\cdot\sin(x^2)+x\cdot\underbracket{\cos(x^2)}\cdot \underbracket{2x}$
\end{enumerate}  
\end{frame}

\begin{frame}{Why we do calculus in radians}
The derivatives of $f(x)=\sin x$ are easy in radians.
\begin{center}
\begin{tabular}{r|*{5}{c}}
   \textit{Derivatives of $f$}  &$f'$     &$f''$     &$f'''$    &$f^{(iv)}$ 
        &\ldots  \\
   \cline{2-6}
   \textit{Expression}         &$\cos x$ &$-\sin x$ &$-\cos x$ &$\sin x$  
        &\ldots   
\end{tabular}
\end{center}
Let the function which gives the sine if its argument is in degrees be 
$\operatorname{Sin}(x)$.
To convert from radians to degrees, multiply by $180/\pi$.
So $\operatorname{Sin}(x)=\sin((180/\pi)\cdot x)$.
Likewise 
$\operatorname{Cos}(x)=\cos((180/\pi)\cdot x)$. 

\pause
The first derivative is this.
\begin{equation*}
  \frac{d\,\operatorname{Sin}(x)}{dx}=\underbracket{\cos(\frac{180}{\pi}x)}\cdot\underbracket{\frac{180}{\pi}}
                 =\frac{180}{\pi}\operatorname{Cos}(x)
\end{equation*}
And it just gets worse.
\begin{equation*}
  \frac{d^2\,\operatorname{Sin}(x)}{dx^2}=\underbracket{\frac{180}{\pi}\cdot -\sin(\frac{180}{\pi}x)}\cdot\underbracket{\frac{180}{\pi}}
                 =-\bigl(\frac{180}{\pi}\bigr)^2\operatorname{Sin}(x)
\end{equation*}
We can stop there.
Radians are better.
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
