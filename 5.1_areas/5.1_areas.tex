\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.1:\ \ Introduction to integration}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{textcomp}
\usepackage{listings}
\lstset{basicstyle=\ttfamily\scriptsize,
        upquote=true,
        language=Python,
        keepspaces=true}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Background}

\begin{frame}{Origin story}
\begin{center}\small
  \begin{tabular}{@{}c@{}}
  \includegraphics[height=0.3\textheight]{pix/brahe.jpg} 
  \\ Tycho Brahe
  \end{tabular}
  \quad    
  \begin{tabular}{@{}c@{}}
  \includegraphics[height=0.3\textheight]{pix/kepler.jpg}    
  \\ Johannes Kepler
  \end{tabular}
  \quad
  \begin{tabular}{@{}c@{}}
  \includegraphics[height=0.3\textheight]{pix/leibniz.jpg}
  \\ Gottfried Leibniz
  \end{tabular}
  \quad    
  \begin{tabular}{@{}c@{}}
  \includegraphics[height=0.3\textheight]{pix/newton.jpg}    
  \\ Isaac Newton
  \end{tabular}
\end{center}
Brahe made very accurate observations of the night sky.
Kepler used those numbers to produce three laws that the planets follow.
Leibniz and Newton accounted 
for those three behaviors using only the single assumption
that gravity falls off with the inverse square.  
\end{frame}

\begin{frame}
To do this, 
Leibniz and Newton needed to solve the two great problems of Calculus.
The first problem is to, for instance, find the velocity 
of a planet.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/areas000.pdf}}}%
  \only<2->{\vcenteredhbox{\includegraphics{asy/areas001.pdf}}}%
\end{center}
\only<2->{This is the rate of change problem.
They developed the derivative.}
\end{frame}


\begin{frame}
We now tackle Calculus's other great problem. 
Over time the planet's orbit turns away from the tangent.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/areas002.pdf}}%
\end{center}
To describe the orbit we need to accumulate the total turn over time.

\pause
But we can't use addition. 
The turns differ from instant to instant,
and also there are infinitely many.
\end{frame}

\begin{frame}
Another example of this accumulation problem 
comes with Kepler's Second Law, 
that planets
sweep out equal areas in equal times.
This shows the area swept out in ten days.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/areas003.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{asy/areas004.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/areas005.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/areas006.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{asy/areas007.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{asy/areas008.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{asy/areas009.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{asy/areas010.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{asy/areas011.pdf}}}%
  \only<10->{\vcenteredhbox{\includegraphics{asy/areas012.pdf}}}%
\end{center}
\pause
At each instant 
the earth is moving at a slightly different rate and in a slightly different
direction.
So here also we cannot just add the slice areas to accumulate the total area,
because what is being swept out changes from instant to instant
and there are infinitely many instants.

\only<11->{
In grade school we learn how to accumulate discrete quantities\Dash
three apples plus two apples is five apples.
We will now cover the accumulation of continuous quantities.
This is sometimes called the \alert{area problem},
although perhaps a better name would be the accumulation problem.}
\end{frame}

% \section{Antiderivatives}


\begin{frame}{Example from everyday experience}
\Ex A house with solar panels sometimes will take energy
from the power grid
and sometimes instead supplies energy to the grid.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/rooftop-solar-panels.jpg}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/areas029.pdf}}
\end{center}
Units on the horizontal axis are hours and units on the vertical axis
are kilowatts, so each box is a kilowatt-hour.
Boxes below the axis are negative while those above the axis are positive. 
What is the total?
\end{frame}




\begin{frame}{Motivation}
\Ex
We start with a one-dimensional accumulation problem:
suppose that a particle has this velocity function.
\begin{equation*}
  v(t)=(1/2)t+2 
\end{equation*}
What is its position function?

\pause
This is an accumulation problem because at each instant the particle
changes its position some tiny bit, as described by the velocity function, 
and we want to put together all
of these infinitely many infinitesimal lengths to make the total,
the position.
\end{frame}


\begin{frame}
This is the graph of $v(t)=(1/2)t+2$.
The area shaded below $v(t)$ runs from $t=0$ to $t=x$. 
\begin{center}
  \includegraphics{asy/areas013.pdf}
\end{center}
On the $t$~axis the unit is seconds
while on the $v(t)$~axis the unit is meters per second.
Thus each box represents a meter.

This is an area problem in that
if we find the formula for the area below the curve between $t=0$ and $t=x$
then we will have the position in
meters (from the initial position).
\begin{align*}
  p(x) &=\text{rectangle}+\text{triangle}  \\
       &=x\cdot 2+(1/2)\cdot x\cdot (x/2)
        =(1/4)x^2+2x
\end{align*}
\end{frame}



\begin{frame}{Background to the Fundamental Theorem: sweeping out}
We will often picture what we are doing as here.
We want the area of the region below the function's graph and above the axis,
between $x=a$ and $x=b$.
We sweep out the red vertical columns from right to left.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/areas014.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{asy/areas015.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/areas016.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/areas017.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{asy/areas018.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{asy/areas019.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{asy/areas020.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{asy/areas021.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{asy/areas022.pdf}}}%
  \only<10->{\vcenteredhbox{\includegraphics{asy/areas023.pdf}}}%
\end{center}
\pause
% Each column adds an infinitesimal area to the whole. 
\only<10->{We say that we have \alert{integrated} these columns to make the 
whole.

Leibniz thought of this as taking as an infinite sum of infinitesimal summands
so his notation for integrals uses a letter `S' 
and he wrote the area of each column as the product $f(x)\, dx$.
\begin{equation*}
  \text{Area}=\int_{x=a}^{b}f(x)\,dx
\end{equation*}
}
\end{frame}


\begin{frame}{Prototype}
We will often draw this picture.
Sometimes we show one column, which we call a 
\alert{slice},
\begin{center}
  \vcenteredhbox{\includegraphics{asy/areas027.pdf}}%
\end{center}  
or a \alert{Reimann box}
after B Riemann.
\begin{center}
  \includegraphics[height=0.20\textheight]{pix/riemann.jpg}%  
\end{center}
\end{frame}




\section{Reimann's formalization}

\begin{frame}
Talk of assembling infinitely small slices may make a 
mathematically-inclined person uncomfortable.
To take a more precise approach, divide the interval $\closed{a}{b}$
into many subintervals $\closed{x_i}{x_{i+1}}$.
We make them of equal width, $\Delta x$. 
Over each subinterval erect a rectangle, a box, reaching up to the function.
Then the sum of the box areas approximates the area of the entire region.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/areas024.pdf}}%
\end{center}
We take that the approximation improves as $\Delta x$ gets smaller.
Consequently, we define the area to be the limit of the approximating sum,
as the width of the subintervals goes to zero.
\begin{equation*}
  \text{Total area}=\int_{x=a}^b f(x)\, dx
                   =\lim_{\Delta x\to 0}\;\biggl(\sum_{i}f(x_i)\Delta x\biggr)
\end{equation*}
\end{frame}


\begin{frame}\vspace*{-1ex}
\Ex Approximate the area under the curve $f(x)=1+x^2$
between $x=1$ and $x=2$ by dividing the base into four same-length subintervals
$\closed{x_i}{x_{i+1}}$.
Over each, erect a rectangle whose height is $f(x_i)$.
\begin{center}
  \includegraphics{asy/areas025.pdf}
\end{center}
\pause
\begin{center}\small
  \begin{tabular}{r|llcc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$f(x_i)$}}
      &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
    $1$ &$1$     &$5/4$  &$2$     &$0.500$   \\
    $2$ &$5/4$   &$3/2$  &$41/16$ &$0.641$   \\
    $3$ &$3/2$   &$7/4$  &$13/4$  &$0.812$   \\
    $4$ &$7/4$   &$2$    &$65/16$ &$1.020$   \\
  \end{tabular}
\end{center}
The total is about $2.969$.
\end{frame}
% sage: def f(x):
% ....:     return 1+x^2
% ....: 
% sage: accumulated_area=0
% sage: for i in range(4):
% ....:     xi = 1+i/4
% ....:     xiplus = 1+(i+1)/4
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(xi)*(1/4)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(xi)=",f(xi) ,"box_area=",
% ....: n(box_area,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 5/4  f(xi)= 2 box_area= 0.500
% i= 1  xi= 5/4  xiplus= 3/2  f(xi)= 41/16 box_area= 0.641
% i= 2  xi= 3/2  xiplus= 7/4  f(xi)= 13/4 box_area= 0.812
% i= 3  xi= 7/4  xiplus= 2  f(xi)= 65/16 box_area= 1.02
% sage: n(accumulated_area)
% 2.96875000000000


\begin{frame}
Get a better approximation with more intervals.
\begin{center}\small
  \begin{tabular}{r|cccc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$f(x_i)$}}
      &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
    $1$ &$1$     &$9/8$   &$2$       &$0.250$   \\
    $2$ &$9/8$   &$5/4$   &$145/64$  &$0.283$   \\
    $3$ &$5/4$   &$11/8$  &$41/16$   &$0.320$   \\
    $4$ &$11/8$  &$3/2$   &$185/64$  &$0.361$   \\
    $5$ &$3/2$   &$13/8$  &$13/4$    &$0.406$   \\
    $6$ &$13/8$  &$7/4$   &$233/64$  &$0.455$   \\
    $7$ &$7/4$   &$15/8$  &$65/16$   &$0.508$   \\
    $8$ &$15/8$  &$2$     &$289/64$  &$0.564$   
  \end{tabular}
\end{center}
The total is about $3.148$.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/definite_integral015.pdf}}%
% \end{center}
\end{frame}
% sage: accumulated_area=0
% sage: for i in range(8):
% ....:     xi = 1+i/8
% ....:     xiplus = 1+(i+1)/8
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(xi)*(1/8)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(xi)=",f(xi) ,"box_area=",
% ....: n(box_area,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 9/8  f(xi)= 2 box_area= 0.250
% i= 1  xi= 9/8  xiplus= 5/4  f(xi)= 145/64 box_area= 0.283
% i= 2  xi= 5/4  xiplus= 11/8  f(xi)= 41/16 box_area= 0.320
% i= 3  xi= 11/8  xiplus= 3/2  f(xi)= 185/64 box_area= 0.361
% i= 4  xi= 3/2  xiplus= 13/8  f(xi)= 13/4 box_area= 0.406
% i= 5  xi= 13/8  xiplus= 7/4  f(xi)= 233/64 box_area= 0.455
% i= 6  xi= 7/4  xiplus= 15/8  f(xi)= 65/16 box_area= 0.508
% i= 7  xi= 15/8  xiplus= 2  f(xi)= 289/64 box_area= 0.564
% sage: n(accumulated_area)
% 3.14843750000000


\begin{frame}\vspace*{-1ex}
\Ex Approximate the area under the curve $f(x)=1+x^2$
between $x=1$ and $x=2$ by dividing the base into four same-length subintervals.
Over each, erect a rectangle whose height is $f(m_i)$ where $m_i$ is the 
midpoint of the subinterval.
\begin{center}
  \includegraphics{asy/areas026.pdf}
\end{center}
\pause
\begin{center}
  \begin{tabular}{r|ccccc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$m_{i}$}}
      %&\multicolumn{1}{c}{\textit{$f(m_i)$}}
      &\multicolumn{1}{c}{\textit{Box area, rounded}} \\ \hline
    $1$ &$1$     &$5/4$  &$9/8$   &$0.566$   \\
    $2$ &$5/4$   &$3/2$  &$11/8$  &$0.723$   \\
    $3$ &$3/2$   &$7/4$  &$13/8$  &$0.910$   \\
    $4$ &$7/4$   &$2$    &$15/8$  &$1.130$   \\
  \end{tabular}       
\end{center}
The total is about $3.328$.
\end{frame}
% sage: accumulated_area=0
% sage: for i in range(4):
% ....:     xi = 1+i/4
% ....:     xiplus = 1+(i+1)/4
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(mi)*(1/4)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," mi=",mi,"box_area=",n(box_a
% ....: rea,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 5/4  mi= 9/8 box_area= 0.566
% i= 1  xi= 5/4  xiplus= 3/2  mi= 11/8 box_area= 0.723
% i= 2  xi= 3/2  xiplus= 7/4  mi= 13/8 box_area= 0.910
% i= 3  xi= 7/4  xiplus= 2  mi= 15/8 box_area= 1.13
% sage: accumulated_area
% 213/64
% sage: n(accumulated_area)
% 3.32812500000000



% \begin{frame}\vspace*{-1ex}
% \Ex Again approximate the area under $f(x)=1+x^2$
% between $x=1$ and $x=2$ by dividing the base into four same-length subintervals.
% This time, erect a rectangle whose height is the point on the right side
% of the subinterval, $f(x_{i+1})$.
% \begin{center}
%   \includegraphics{asy/definite_integral014.pdf}
% \end{center}
% \pause
% \begin{center}
%   \begin{tabular}{r|cccc}
%     \multicolumn{1}{c}{\textit{Interval $i$}}
%       &\multicolumn{1}{c}{\textit{$x_i$}}
%       &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
%       &\multicolumn{1}{c}{\textit{$f(m_i)$}}
%       &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
%     $1$ &$1$    &$5/4$  &$41/64$  &$0.641$   \\
%     $2$ &$5/4$  &$3/2$  &$13/16$  &$0.812$   \\
%     $3$ &$3/2$  &$7/4$  &$97/16$  &$1.020$   \\
%     $4$ &$7/4$  &$2$    &$137/16$ &$1.250$   \\
%   \end{tabular}       
% \end{center}
% The total is about $3.72$.
% \end{frame}
% % sage: def f(x):
% % ....:     return(1+x^2)
% % ....: 
% % sage: accumulated_area = 0
% % sage: for i in range(4):
% % ....:     xi = 1+i/4
% % ....:     xiplus = 1+(i+1)/4
% % ....:     box_area = f(xiplus)*(1/4)
% % ....:     accumulated_area = accumulated_area+box_area
% % ....:     print("i=",i," xi=",xi," xiplus=",xiplus,"box_area=",n(box_area,digits
% % ....: =3))
% % ....: 
% % i= 0  xi= 1  xiplus= 5/4 box_area= 0.641
% % i= 1  xi= 5/4  xiplus= 3/2 box_area= 0.812
% % i= 2  xi= 3/2  xiplus= 7/4 box_area= 1.02
% % i= 3  xi= 7/4  xiplus= 2 box_area= 1.25
% % sage: accumulated_area
% % 119/32
% % sage: n(accumulated_area)
% % 3.71875000000000




\begin{frame}\vspace*{-1ex}
\Ex Approximate the area under $f(x)=\sin x$
between $x=0$ and $x=\pi$ by dividing the base into four 
equal-length subintervals.
Use the midpoint rule:
over each subinterval, 
erect a rectangle whose height is $f(m_i)$ where $m_i$ is the 
midpoint of that interval.
% \begin{center}
%   \includegraphics{asy/areas026.pdf}
% \end{center}
% \pause
\begin{center}\small
  \begin{tabular}{r|lllcl}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$m_{i}$}}
      &\multicolumn{1}{c}{\textit{$f(m_i)\cdot (x_{i+1}-x_i)$}} 
      &\multicolumn{1}{c}{\textit{Value}}  \\ \hline
    $1$ &$0$      &$\pi/4$   &$\pi/8$  &$\pi/8\cdot \sqrt{-\sqrt{2} + 2}$ &$0.301$  \\
    $2$ &$\pi/4$  &$\pi/2$  &$3\pi/8$ &$\pi/8\cdot \sqrt{\sqrt{2} + 2}$ &$0.726$  \\
    $3$ &$\pi/2$  &$3\pi/4$ &$5\pi/8$ &$\pi/8\cdot \sqrt{\sqrt{2} + 2}$  &$0.726$  \\
    $4$ &$3\pi/4$ &$\pi$    &$7\pi/8$ &$\pi/8\cdot \sqrt{-\sqrt{2} + 2}$ &$0.301$  \\
  \end{tabular}  \\[2.5ex]
  \vcenteredhbox{\includegraphics{asy/areas028.pdf}}%
\end{center}
The total is about $2.052$.
\end{frame}

\begin{frame}[fragile]{Remark: computer code}
Approximating areas in this way is well suited to a computer.
This is the script that produced the prior table, in the computer algebra 
system \textit{Sage}.
\begin{lstlisting}
sage: accumulated_area = 0
sage: for i in range(4):
....:     xi = pi*(i/4)
....:     xiplus = pi*(i+1)/4
....:     mi = (xi+xiplus)/2
....:     reimann_area = sin(mi)*(pi/4)
....:     accumulated_area = accumulated_area+reimann_area
....:     print("i=",i," xi=",xi," xiplus=",xiplus,"mi=",mi,"box_area=",reimann_area)
....:     
i= 0  xi= 0  xiplus= 1/4*pi mi= 1/8*pi box_area= 1/8*pi*sqrt(-sqrt(2) + 2)
i= 1  xi= 1/4*pi  xiplus= 1/2*pi mi= 3/8*pi box_area= 1/8*pi*sqrt(sqrt(2) + 2)
i= 2  xi= 1/2*pi  xiplus= 3/4*pi mi= 5/8*pi box_area= 1/8*pi*sqrt(sqrt(2) + 2)
i= 3  xi= 3/4*pi  xiplus= pi mi= 7/8*pi box_area= 1/8*pi*sqrt(-sqrt(2) + 2)
sage: n(accumulated_area)
2.05234430595406
\end{lstlisting}
\end{frame}
% sage: accumulated_area = 0
% sage: for i in range(4):
% ....:     xi = pi*(i/4)
% ....:     xiplus = pi*(i+1)/4
% ....:     mi = (xi+xiplus)/2
% ....:     reimann_box_area = sin(mi)*(pi/4)
% ....:     accumulated_area = accumulated_area+reimann_box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus,"mi=",mi,"box_area=",reimann_box_area)
% ....: 
% i= 0  xi= 0  xiplus= 1/4*pi mi= 1/8*pi box_area= 1/8*pi*sqrt(-sqrt(2) + 2)
% i= 1  xi= 1/4*pi  xiplus= 1/2*pi mi= 3/8*pi box_area= 1/8*pi*sqrt(sqrt(2) + 2)
% i= 2  xi= 1/2*pi  xiplus= 3/4*pi mi= 5/8*pi box_area= 1/8*pi*sqrt(sqrt(2) + 2)
% i= 3  xi= 3/4*pi  xiplus= pi mi= 7/8*pi box_area= 1/8*pi*sqrt(-sqrt(2) + 2)
% sage: n(accumulated_area)
% 2.05234430595406



\begin{frame}{Remark: the limit}
We have defined the area below $f$ and above the axis, between $a$ and~$b$, 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/areas024.pdf}}%
\end{center}
to be this limit.
\begin{align*}
  \text{Area} &=\int_{x=a}^b f(x)\, dx    \\
              &=\lim_{\Delta x\to 0}\;\sum_{\text{rectangle}_i}\!f(x_i)\Delta x
\end{align*}                   
\end{frame}

\begin{frame}
But we left something unsaid and you may have a misleading impression.
There is reason to worry that the limit does not give the area.

Our intuition is that, taken one box at a time,
boxes with smaller width will more 
accurately approximate the area under the curve and over their base.
Close analysis bears that out, the per-box error is indeed smaller
for skinnier boxes.

\pause
But what if, say, when we make each box one tenth the width then 
the error per box falls by half?
Then there would be ten times as many boxes, each with
half the error, and the total error over all the boxes
would grow by a factor of five.

We need to do an analysis verifying that nothing like this happens,
and instead that as $\Delta x$ goes to zero then the total error falls.
The full analysis is beyond our scope but the next slide gives a
suggestion of how it goes.
\end{frame}


\begin{frame}
Fix $f(x)=x$, $a=0$, and $b=1$.
The left endpoint boxes give an underestimate of the area
while the right boxes give an overestimate.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/areas030.pdf}}%
  \quad
  \vcenteredhbox{\includegraphics{asy/areas031.pdf}}%
\end{center}
The sums are on the next slide.
For them we need this formula.
\begin{equation*}
  0+1+2+\cdots+(N-1)=\frac{N(N-1)}{2}
\end{equation*}
(Briefly, where the sum is $S=0+1+2+\cdots+N$ then $2S$ 
\begin{center}
\begin{tabular}{rc*{4}{@{$\hspace*{0.1111em}+\hspace*{0.1111em}$}c}}
      &$0$     &$1$     &$2$     &$\cdots$ &$N-1$  \\  
  $+$ &$(N-1)$ &$(N-2)$ &$(N-3)$ &$\cdots$ &$0$  \\ \hline
      &$(N-1)$ &$(N-1)$ &$(N-1)$ &$\cdots$ &$(N-1)$  \\  
\end{tabular}
\end{center}
equals $N(N-1)$.)
\end{frame}
\begin{frame}
Here is the sum for the left-endpoints boxes  
\begin{multline*}
\sum_{0\leq i<N}f(x_i)\,\Delta x
  =
  \Delta x\,\cdot\!\!\sum_{0\leq i<N}f(x_i)
  =
  \Delta x\,\cdot\!\!\sum_{0\leq i<N} x_i          \\
  =
  \frac{1}{N}\!\!\sum_{0\leq i<N} \frac{i}{N}
  =
  \frac{1}{N^2}\!\sum_{0\leq i<N} i
  =
  \frac{1}{N^2}\cdot \frac{N(N-1)}{2}
  =
  \frac{1}{2}\cdot\frac{N^2-N}{N^2}
\end{multline*}
and for the right-endpoint boxes.
\begin{multline*}
  \sum_{0\leq i<N}f(x_{i+1})\,\Delta x
  =
  \Delta x\,\cdot\!\!\sum_{0\leq i<N}f(x_{i+1})
  =
  \Delta x\,\cdot\!\!\sum_{0\leq i<N} x_{i+1}          \\
  =
  \frac{1}{N}\!\!\sum_{0\leq i<N} \frac{i+1}{N}
  =
  \frac{1}{N^2}\!\sum_{0\leq i<N} i+1
  =
  \frac{1}{N^2}\cdot \frac{(N+1)N}{2}
  =
  \frac{1}{2}\cdot\frac{N^2+N}{N^2}
\end{multline*}
For both the limit as $N\to\infty$ is $1/2$.
Since the over-\hbox{} and under-\hbox{} estimates agree, it must be
the right answer.
Besides, it is the area of the triangle. 
\end{frame}



\section{Indefinite integrals}

\begin{frame}{Antidifferentiation}
Recall the accumulation problem from earlier, where we were
given a velocity function
$
  v(t)=(1/2)t+2 
$
and asked for the position function.
We got that the area 
below $v(t)$ that is swept out between $t=0$ and $t=x$ is
$p(x)=(1/4)x^2+2x$.
\begin{center}
  \includegraphics{asy/areas013.pdf}
\end{center}
% \begin{align*}
%   p(x) &=\text{rectangle}+\text{triangle}  \\
%        &=x\cdot 2+(1/2)\cdot x\cdot (x/2)
%         =(1/4)x^2+2x
% \end{align*}

\pause
The point of this
problem is that it suggests a general method to solve accumulation
problems.
We know that
velocity is the derivative of position,
so starting with $v(t)$
we look for a~$p(t)$ whose derivative is~$v(t)$.
Observe that indeed the derivative of
our $p(t)=(1/4)t^2+2t$ is $v(t)=(1/2)t+2$.
\end{frame}


\begin{frame}
\Df Let the function~$f$ be defined on some interval. 
An \alert{antiderivative} of~$f$ is a function~$F$
such that $F'(x)=f(x)$ on that interval.

\pause
\Ex An antiderivative of $(1/2)x+2$ is $(1/4)x^2+2x$.
Another antiderivative of the same function is $(1/4)x^2+2x+1$.
Still another is $(1/4)x^2+2x-1/3$

% \Ex An antiderivative of $x^3+5x$ is $(1/4)x^4+(5/2)x^2$.
% Another is  $(1/4)x^4+(5/2)x^2-2$.

\Ex Name three antiderivatives of $x^2+2$.
Describe the family of all antiderivatives. 

\pause
Three are 
$F_1(x)=(1/3)x^3+2x$,
and $F_2(x)=(1/3)x^3+2x+1$,
and $F_3(x)=(1/3)x^3+2x-(1/5)$.
Every one has the form
$F(x)=(1/3)x^3+2x+C$ for some constant~$C$.

We saw this result in the Mean Value Theorem section. 

\Tm Given $f$, where $F$ is any particular 
antiderivative, all antiderivatives
have the form $F(x)+C$ for some real number~$C$.

We denote the operation of antidifferentiation 
with the \alert{indefinite integral}.
\begin{equation*}
  \int f(x)\,dx = F(x)+C
\end{equation*}
It represents a family of functions, one for each real constant $C$.

% \pause
% \Ex Find the family of antiderivatives: $\int e^x+x^5\,dx$.

% \pause Members of the family have the form
% $F(x)=e^x+(1/6)x^6+C$.
\end{frame}


\begin{frame}{Antidifferentiation formulas}\vspace*{-1ex}
Every differentiation rule is an antidifferention rule.
\begin{center}\small
  \begin{tabular}{@{}ll@{}}
    $\displaystyle \int k\,dx=kx+C$ 
     &$\displaystyle \int x^n\,dx=\frac{1}{n+1}x^{n+1}+C\quad n\neq 1$ \\
    $\displaystyle \int e^x\,dx=e^x+C$ 
     &$\displaystyle \int \frac{1}{x}\,dx=\ln |x|+C$  \\ 
    $\displaystyle \int \cos x\,dx=\sin x+C$ 
     &$\displaystyle \int \sin x\,dx=-\cos x+C$ 
  \end{tabular}
\end{center}
All of the formulas for differentiation apply in reverse.
\begin{itemize}
\item The antiderivative of a constant times a function is the
constant time the antiderivative of the function.
\begin{equation*}
  \int k\cdot f(x)\,dx=k\cdot\int f(x)\,dx
\end{equation*} 

\item The antiderivative of a sum or difference of two functions
is the sum or difference of the antiderivatives.
\begin{align*}
  &\int f(x)+ g(x)\,dx=\int f(x)\,dx + \int g(x)\,dx   \\
  &\int f(x)- g(x)\,dx=\int f(x)\,dx -\int g(x)\,dx   
\end{align*} 
\end{itemize}

% \begin{equation*}
%   \int c\cdot f(x)\,dx=c\cdot\int f(x)\,dx
%   \qquad \int f(x)\pm g(x)\,dx=\int f(x)\,dx \pm\int g(x)\,dx
% \end{equation*} 
\end{frame}




\begin{frame}{Practice}

Find the antiderivative of each.
\begin{enumerate}
\item $\displaystyle \int x^2+2x+4\,dx$
\pause
\item $\displaystyle \int \sqrt[4]{x^5}\,dx$
\pause
\item $\displaystyle \int 5e^x\,dx$
\pause
\item $\displaystyle \int (x-5)^2\,dx$
\pause
% \item $\displaystyle \frac{5x^2-6x+4}{x^2}$
% \pause
\item $\displaystyle \int 3\sec x\tan x\,dx$
\end{enumerate}
\end{frame}




\begin{frame}{Warning!}
The prior page says, for instance, that an integral of a sum 
equals the sum of the integrals.
But this does not extend to other operations.
\begin{enumerate}
\item Just as with derivatives, we must be careful about products.
This example shows that the antiderivative of a product
ncan't be the product of the antiderivatives.
\begin{equation*}
  \int x\cdot x\,dx\neq \int x\,dx\cdot\int x\,dx    
\end{equation*}
On the left is some kind of $x^3$ function while on the right is
some kind of $x^4$.
\item Similarly, watch out for quotients.
\begin{equation*}
  \int \frac{x^2}{x}\,dx\neq \frac{\int x^2\,dx}{\int x\,dx}    
\end{equation*}
\item Also be careful about composition.
Where $f(x)=x^3$ and $g(x)=x^2$, the integral of the
composition cannot equal the composition of the integrals.
\end{enumerate}
% (All of these examples take $C=0$.)
% There are separate and useful antiderivative rules for these but the above
% naive approaches do not work.
\end{frame}



\begin{frame}
Given a candidate for an antiderivative, we can easily check
whether it is right.

\Ex
Verify by taking the derivative that
$\displaystyle \int \ln x\,dx=x\ln x-x+C$.

\pause\bigskip
Two comments about antiderivatives.
\begin{itemize}
\item   Taking a derivatives is a straightforward computation.
But going in reverse, finding an antiderivative, can be like doing a puzzle.
We will see techniques, but still sometimes it takes cleverness.
% And, like most puzzles there is an easy way to verify the
% correctness of a claimed answer\Dash just check that it gives the
% required derivative.

\pause\item
In this class we work with \alert{elementary functions}, those given
by formulas using sums, products, roots and compositions of finitely many 
polynomial, rational, trigonometric, hyperbolic, and exponential functions,
and their inverse functions.
There are elementary functions that have no elementary antiderivative.
One is $e^{-x^2}$ and another is $\sqrt{1-x^4}$.
\end{itemize}
\end{frame}


\begin{frame}{What does $C$ mean?}
\Ex We throw a ball upwards with an inital speed of $48$~ft/s and from an
initial height of $432$~ft.  
Recall that the acceleration due to gravity is $32$~ft/s toward the earth.
Find the formula for its height above ground at time~$t$. 
% When does it hit?

\pause
The acceleration is $a(t)=p''(x)=-32$.
As this equation involves derivatives it is a \alert{differential equation}.
Taking the indefinite integral yields an expression for the velocity, 
$v(t)=-32t+C$ for some constant~$C$.
Use the initial condition that $v(0)=48$ 
to fix the value of~$C$, giving $v(t)=-32t+48$.

\pause
Using the indefinite integral on that equation gives
$p(t)=-16t^2+48t+\hat{C}$ for some~$\hat{C}$.
Fix the value of this constant with the other initial condition $p(0)=432$,
leaving the position function as
$p(t)=-16t^2+48t+432$.

% To see when the ball comes down, solve 
% $0=-16t^2+48t+432$.
% The quadratic formula gives $t=(3/2\pm (3/2)\sqrt{13}$.
% (The minus case gives a negative time, which we discard.)
\end{frame}






% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
