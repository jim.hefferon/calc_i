// areas.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "areas%03d";
real PI = acos(-1);

import graph;

// ===== planet in its orbit =======
picture pic;
int picnum = 0;
size(pic,4cm,0,keepAspect=true);

path orbit = scale(2,1)*arc( (0,0), 3cm, 15, 45);

draw(pic, orbit, black);
label(pic,graphic("../pix/earth.png","width=0.25cm"),
      point(orbit,0.5));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........ show tangent ...........
picture pic;
int picnum = 1;
size(pic,4cm,0,keepAspect=true);

path orbit = scale(2,1)*arc( (0,0), 3cm, 45, 15);

pair tvector = unit(dir(subpath(orbit, 0, 0.5)));
path tvector_vec = (0,0)--scale(0.5cm)*tvector;

draw(pic, orbit, black);
// draw(pic, (0,0)--scale(5)*tvector, highlight_color);
draw(pic, shift(point(orbit,0.5))*tvector_vec, highlight_color, Arrow);
label(pic,graphic("../pix/earth.png","width=0.25cm"),
      point(orbit,0.5));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........ show tangent with angle ...........
picture pic;
int picnum = 2;
size(pic,4cm,0,keepAspect=true);

path orbit = scale(2,1)*arc( (0,0), 3cm, 45, 15);

pair tvector = unit(dir(subpath(orbit, 0, 0.5)));
path tvector_vec = shift(point(orbit,0.5))*( (0,0)--scale(1cm)*tvector );

draw(pic, orbit, black);
draw(pic, tvector_vec, black);
label(pic,graphic("../pix/earth.png","width=0.25cm"),
      point(orbit,0.5));

path angle_arc_circle = arc(point(orbit,0.5), 0.85cm, 0, -90);  
real angle_arc_circle_tline_time = intersect(angle_arc_circle,tvector_vec)[0];
real angle_arc_circle_orbit_time = intersect(angle_arc_circle,orbit)[0];
path angle_arc = subpath(angle_arc_circle, angle_arc_circle_tline_time, angle_arc_circle_orbit_time);
draw(pic, angle_arc, highlight_color, Arrow);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===== planet sweeping out =======
for(int i=0; i < 10; ++i) {
  picture pic;
  int picnum = 3+i;
  size(pic,4cm,0,keepAspect=true);
  
  path orbit = scale(2,1)*arc( (0,0), 3cm, 45, 15);
  real planet_time = 0.25+(0.75-0.25)*(i/9);
  pair planet_loc = point(orbit,planet_time);
  
  if (i>0) {
    path swept_out_area = (0,0)--point(orbit, 0.25)..subpath(orbit,0.25,planet_time)--(0,0)--cycle;
    fill(pic, swept_out_area, gray(0.8));
  }
  
  draw(pic, orbit, black);
  draw(pic, (0,0)--planet_loc, highlight_color);
  label(pic,graphic("../pix/earth.png","width=0.25cm"),
	planet_loc);
  label(pic,graphic("../pix/sun.png","width=0.35cm"),
	(0,0));

  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}


// ==== given velocity, find position ====
real f13(real x) {return (1/2)*x+2;}

picture pic;
int picnum = 13;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

real x=4.25;

path g = graph(pic, f13, 0, x);
path region = ( (0,0)--point(g,0)&g--(x,0)--(0,0) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, graph(f13,xleft-0.1,xright+0.1), FCNPEN);
draw(pic, (x,0)--(x,f13(x)), highlight_color);
dotfactor = 5;
dot(pic, (0,2), FCNPEN_COLOR);
label(pic, "$(0,2)$", (0,2), 1.25*W);

real[] T13 = {x};
xaxis(pic, L="\tiny $t$",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T13, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);

Label L = rotate(0)*Label("\tiny $v(t)$");
yaxis(pic, L=L,
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== general function ====

path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;

for(int i=0; i < 10; ++i) {
  picture pic;
  int picnum = 14+i;
  size(pic,3.5cm,0,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=3;

  path f = shift(0,2)*general_fcn;

  // region to be integrated  
  real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
  real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
  path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
  fill(pic, region, background_color);
  // draw(pic, subpath(f, f_a_time, f_b_time), green);

  // sweep left to right
  real x = a+(b-a)*(i/9);
  real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
  path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
  fill(pic, region_done, bold_color+opacity(0.6));
  draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

  draw(pic, (a,ybot)--point(f,f_a_time));
  draw(pic, point(f,f_b_time)--(b,ybot));
  draw(pic, f, FCNPEN);

  real[] T13 = {a,b};
  xaxis(pic, L="\tiny $x$",  
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+.75,
	p=currentpen,
	ticks=RightTicks("%",T13,Size=2pt),
	arrow=Arrows(TeXHead));
  labelx(pic, "$a$", a);
  labelx(pic, "$b$", b);
  
  yaxis(pic, L="\tiny $y$",  
	axis=XZero,
	ymin=ybot-0.75, ymax=ytop,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}


// ===== usual integration picture ======
picture pic;
int picnum = 24;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, background_color, black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes
for(int i=0; i < 9; ++i) {
  real x_i = a+(b-a)*(i/9);
  real x_iplus = a+(b-a)*((i+1)/9);
  real f_x_i_time = intersect(f, (x_i,ybot)--(x_i,ytop))[0];
  real f_x_iplus_time = intersect(f, (x_iplus,ybot)--(x_iplus,ytop))[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}
draw(pic, f, FCNPEN);

real[] T23 = {a,b};
xaxis(pic, L="\tiny $x$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T23,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="\tiny $y$",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== approximate area below 1+x^2 ======
real f25(real x) {return 1+x^2;}

picture pic;
int picnum = 25;
scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=8;
real a=1; real b=2;

path f_graph = graph(pic, f25, xleft-0.1, xright+0.1);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, background_color, black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, left side
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop)))[0];
  path reimann_box = ( Scale(pic,(x_i,ybot))--point(f_graph,f_x_i_time)--Scale(pic,(x_iplus,point(f_graph,f_x_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}
draw(pic, f_graph, FCNPEN);

real[] T25 = {1, 2};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T25,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ..... same, with midpoints .......
picture pic;
int picnum = 26;
scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=8;
real a=1; real b=2;

path f_graph = graph(pic, f25, xleft-0.1, xright+0.1);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, background_color, black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, midpoint
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real m_i = (x_i+x_iplus)/2;
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop)))[0];
  real f_m_i_time = intersect(f_graph, Scale(pic,(m_i,ybot))--Scale(pic,(m_i,ytop)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop)))[0];
  path reimann_box = (Scale(pic,(x_i,ybot))--Scale(pic,(x_i,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}
draw(pic, f_graph, FCNPEN);

real[] T26 = {1, 2};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T26,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// ==== prototype picture ====

path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;

picture pic;
int picnum = 27;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, background_color, black);

// Reimann box
real x = a+(b-a)*0.7;
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

draw(pic, f, FCNPEN);

real[] T27 = {a,x,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T27,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$x$", x);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Reimann boxes for sin(x) ============
real f28(real x) {return sin(x);}

picture pic;
int picnum = 28;
size(pic,0,2.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.5;
ybot=0; ytop=1;
real a=0; real b=PI;

path f_graph = graph(pic, f28, xleft-0.1, xright+0.02);

// region to be integrated  
real f_a_time = intersect(f_graph, (a,ybot-1)--(a,ytop))[0];
real f_b_time = intersect(f_graph, (b,ybot-1)--(b,ytop))[0];
path region = ((a,ybot)--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, background_color);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, midpoint
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real m_i = (x_i+x_iplus)/2;
  real f_x_i_time = intersect(f_graph, (x_i,ybot-1)--(x_i,ytop+1))[0];
  real f_m_i_time = intersect(f_graph, (m_i,ybot)--(m_i,ytop+1))[0];
  real f_x_iplus_time = intersect(f_graph, (x_iplus,ybot-1)--(x_iplus,ytop+1))[0];
  path reimann_box = ((x_i,ybot)--(x_i,point(f_graph,f_m_i_time).y)--(x_iplus,point(f_graph,f_m_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}

draw(pic, f_graph, FCNPEN);

real[] T28 = {PI/2, PI};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T28,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\pi/2$", PI/2);
labelx(pic, "$\pi$", PI);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== solar panel ====

path panel_fcn = (0,-2)..(1,-0.5)..(2,1)..(3,2.25)..(4,2.0)..(5.25,0.25);

picture pic;
int picnum = 29;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-1; ytop=3;

real a = 0; real b = 4.6;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = shift(0,1)*panel_fcn;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_0_time = intersect(f, (0,ybot)--(0,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path left_region = ( point(f,f_a_time)&subpath(f, f_a_time, f_0_time)&point(f,f_0_time)--(0,0)--point(f,f_a_time) )--cycle;
path right_region = ( point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,0)--(0,0) )--cycle;
fill(pic, left_region, background_color+opacity(0.60));
fill(pic, right_region, background_color+opacity(0.60));
// fill(pic, left_region, gray(0.8)+opacity(0.60));
// fill(pic, right_region, gray(0.8)+opacity(0.60));
draw(pic, subpath(f, f_a_time, 4.75), FCNPEN);

// draw(pic, f, FCNPEN);

real[] T10 = {b};
xaxis(pic, L="\tiny hr",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", b);
// labelx(pic, "$b$", b);

Label L = Label("\tiny kw",EndPoint,W);
yaxis(pic, L=rotate(0)*L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ===== Over and under estimate ============
real f30(real x) {return x;}


// ...... Left side is an under estimate ....
picture pic;
int picnum = 30;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;
real a=0; real b=1;

path f_graph = graph(pic, f30, xleft-0.1, xright+0.1);

// region to be integrated  
real f_a_time = intersect(f_graph, (a,ybot-1)--(a,ytop))[0];
real f_b_time = intersect(f_graph, (b,ybot-1)--(b,ytop))[0];
path region = ( point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, background_color);
draw(pic, point(f_graph,f_b_time)--(b,0));

// Reimann boxes
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  // real m_i = (x_i+x_iplus)/2;
  real f_x_i_time = intersect(f_graph, (x_i,ybot-1)--(x_i,ytop+1))[0];
  // real f_m_i_time = intersect(f_graph, (m_i,ybot)--(m_i,ytop+1))[0];
  real f_x_iplus_time = intersect(f_graph, (x_iplus,ybot-1)--(x_iplus,ytop+1))[0];
  path reimann_box = ( (x_i,0)--point(f_graph,f_x_i_time)--(x_iplus,point(f_graph,f_x_i_time).y)--(x_iplus,0)--(x_i,0) )--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}

draw(pic, f_graph, FCNPEN);

// real[] T30 = {PI/2, PI};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+.2,
      p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      arrow=Arrow(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ...... Right side is an over estimate ....
picture pic;
int picnum = 31;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;
real a=0; real b=1;

path f_graph = graph(pic, f30, xleft-0.1, xright+0.1);

// region to be integrated  
real f_a_time = intersect(f_graph, (a,ybot-1)--(a,ytop))[0];
real f_b_time = intersect(f_graph, (b,ybot-1)--(b,ytop))[0];
path region = ( point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, background_color);
draw(pic, point(f_graph,f_b_time)--(b,0));

// Reimann boxes, midpoint
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  // real m_i = (x_i+x_iplus)/2;
  real f_x_i_time = intersect(f_graph, (x_i,ybot-1)--(x_i,ytop+1))[0];
  // real f_m_i_time = intersect(f_graph, (m_i,ybot)--(m_i,ytop+1))[0];
  real f_x_iplus_time = intersect(f_graph, (x_iplus,ybot-1)--(x_iplus,ytop+1))[0];
  path reimann_box = ( (x_i,0)--(x_i,point(f_graph,f_x_iplus_time).y)--point(f_graph,f_x_iplus_time)--(x_iplus,0)--(x_i,0) )--cycle;
  filldraw(pic, reimann_box, bold_color+opacity(0.6), highlight_color);
}

draw(pic, f_graph, FCNPEN);

// real[] T30 = {PI/2, PI};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+.2,
      p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      arrow=Arrow(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");







// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

