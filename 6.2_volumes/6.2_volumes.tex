\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\usepackage{graphicx}
\graphicspath{ {asy/} } % Path to poster graphic; set of dirs in curly braces,
\usepackage{asymptote}
\def\asydir{asy} % No trailing slash
\input asy/volumes_3d.pre

\title{Section 6.2:\ \ Volumes}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Volumes by integration}
When we integrate, with each point~$x$ we associate a density~$f(x)$.
\begin{equation*}
  \int_{x=a}^{b}f(x)\,dx
\end{equation*}
The density could represent area, or it could represent something else.

We now illustrate accumulation of a continuous quantity using volumes.
That is, the slices will be volume components.
\end{frame}

\begin{frame}
Expressing the height as a function of $y$ gives $h(y)=-(1/2)y+3$.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/volumes000.png}}%
  \input asy/volumes_3d000.tex
\end{center}
\end{frame}

\begin{frame}
\begin{center}
  %\vcenteredhbox{\includegraphics{asy/volumes001.png}}%  
  \input asy/volumes_3d001.tex
\end{center}
The slice at $(0,y,0)$ is $dy$-thick and has width~$1$.
Putting together the slices gives this.
\begin{equation*}
  \int_{y=0}^4 1\cdot (-\frac{y}{2}+3)\,dy
  =\int_{0}^4 -\frac{y}{2}+3\,dy
\end{equation*}
The Fundamental Theorem calculates the answer.
\begin{equation*}
  \biggl[ -\frac{y^2}{4}+3y\biggr]_0^4
  =\biggl[\,\bigl( -\frac{4^2}{4}+3\cdot 4\bigr)-\bigl(0\bigr)\,\biggr]
  =8
\end{equation*}
\end{frame}
% sage: def f(y):
% ....:     return ((y^2/4)+3*y)
% ....: 
% sage: f(4)
% 16
% sage: f(0)
% 0



\begin{frame}{Volume of a sphere}
This is the unit sphere.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/volumes002.png}}%
  \input asy/volumes_3d002.tex
\end{center}
The picture shows a $dy$-thick slice.
\end{frame}

\begin{frame}
That is a $dy$-thick cylinder whose outer edge lies on the
sphere $1=x^2+y^2+z^2\!$.
In the $yz$~plane the $x$~coordinate is zero so the gray curve 
is $z=\sqrt{1-y^2}$.
Thus each slice's base area as a function of~$y$ is $\pi(1-y^2)$.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/volumes003.png}}%
  \input asy/volumes_3d003.tex
\end{center}
Putting together the slices gives this.
\begin{equation*}
  \int_{y=-1}^1 \pi (1-y^2)\,dy
  =\pi\biggl[ y-\frac{y^3}{3} \biggr]_{-1}^1\!
  =\pi\biggl[ 1-\frac{1^3}{3} \biggr]
    -\pi\biggl[ (-1)-\frac{(-1)^3}{3} \biggr]
  =\frac{4}{3}\pi
\end{equation*}
\end{frame}


% \begin{frame}
% \Ex
% Generalize the prior example
% to derive the formula for the volume of a sphere with radius~$r$.  
% \pause
% \begin{align*}
%   \int_{y=-r}^r \pi (r^2-y^2)\,dy
%   &=\pi\biggl[ r^2y-\frac{y^3}{3} \biggr]_{-r}^r    \\
%   &=\pi \biggl[\, \bigl(r^3-\frac{r^3}{3}\bigr) 
%     -\bigl( (-r)^3-\frac{(-r)^3}{3} \bigr)\,\biggr]       \\
%   &=\frac{4}{3}\pi r^3
% \end{align*}
% \end{frame}


\begin{frame}{Volume of a cone}
This is a cone with base radius~$1$ and height~$1$.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/volumes004.png}}%
  \input asy/volumes_3d004.tex
\end{center}
It is symmetric about the $z$~axis, so we take slices 
that are $dz$ thick.
\end{frame}

\begin{frame}
The slice is a cylinder that is $dz$-thick and whose base area,
expressed as a function of~$z$, is 
$\pi(1-z)^2\!$.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/volumes005.png}}%
  \input asy/volumes_3d005.tex
\end{center}
\begin{multline*}
  \int_{z=0}^1 \pi (1-z)^2\,dz
  =\pi\int_{z=0}^1 1-2z+z^2\,dz                   \\
  =\pi\,\Bigl[ z-z^2+\frac{z^3}{3} \Bigr]_{0}^1 
  =\pi\,\Bigl[\,\bigl( 1-1^2+ \frac{1^3}{3}\bigr) 
        - \bigl( 0 \bigr)\Bigr]
  =\frac{1}{3}\pi
\end{multline*}
\end{frame}


% \begin{frame}
% \Ex
% Generalize the prior example to the volume of a cone with 
% base radius~$r$ and height~$h$.  
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/volumes006.png}}%
% \end{center}

% \pause
% \begin{align*}
%   \int_{z=0}^h \pi \bigl(\frac{-r}{h}(z-h)\bigr)^2\,dz
%     &=  \pi \frac{r^2}{h^2} \int_{z=0}^h z^2-2zh+h^2\,dz      \\
%     &=  \pi \frac{r^2}{h^2} \biggl[\frac{z^3}{3}-hz^2+h^2z\biggr]_0^h  \\
%     &=  \pi \frac{r^2}{h^2} \biggl[\bigl(\frac{z^3}{3}-hz^2+h^2z\bigr)
%             -\bigl(0\bigr)\biggr]_0^h 
%     =\frac{1}{3}\pi r^2h
% \end{align*}
% \end{frame}



\section{Solids of revolution}

\begin{frame}
\Ex
Consider the region between $z=y^2$ and the $z$~axis, for non-negative $y$'s, 
between $a=0$ and $b=1$.
Rotate that region about the $z$~axis. 
What is the volume of the solid? 

\pause
This is the two dimensional region.
\begin{center}
  \includegraphics{asy/volumes_2d001.pdf}
\end{center}
\pause
We take $dz$~thick slices.
The one shown starts at $(0,z)$ and its other end is at
$(\sqrt{z},z)$.
\end{frame}

\begin{frame}
This is the solid.
\begin{center}
  % \includegraphics{asy/volumes007.png}
  \input asy/volumes_3d007.tex
\end{center}  
The slice is a cylinder that is $dz$~thick 
and has base area $\pi(\sqrt{z})^2=\pi\, z$.
\begin{equation*}
  \int_{0}^1 \pi\, z\,dz
  =\pi\int_{0}^1  z\,dz
  =\pi\cdot\Bigl[ \frac{z^2}{2} \Bigr]_0^1
  =\frac{\pi}{2}
\end{equation*}
\end{frame}


\begin{frame}{Method of Discs}
Consider a solid of revolution that is rotated about the $x$~axis.
If we take slices that are $dx$-thick then the slice is  
a \alert{disc}.

In general, the Method of Discs uses slices that are 
$d\text{-axis of rotation}$.
\end{frame}

\begin{frame}{Practice}
\Ex
Consider the region between $(x+1)^2$ and the $x$~axis, from
$a=0$ to $b=2$.
Sketch it, and also sketch the result of rotating it 
about the $x$~axis. 
Include in the sketch
a $dx$-thick disc slice centered at $(x,0,0)$.
Integrate to find the volume of the solid.

\pause
Here is the two dimensional region.
\begin{center}
  \includegraphics{asy/volumes_2d004.pdf}
\end{center}
\end{frame}

\begin{frame}
The slice at $(x,0,0)$ is a disc.
\begin{center}
  % \includegraphics{asy/volumes011.png}
  \input asy/volumes_3d011.tex
\end{center}
It is $dx$~thick and has a base radius of $(1+x)^2\!$.
\begin{equation*}
  \int_{x=0}^{2} \pi\,(\,(1+x)^2\,)^2\,dx
  =  \pi\int_{x=0}^{2} (1+x)^4\,dx
\end{equation*}
\end{frame}
\begin{frame}
The Fundamental Theorem of Calculus gives this.
\begin{align*}
  \pi\int_{x=0}^{2} (x+1)^4\,dx
  &=  \pi\int_{x=0}^{2} x^4+4x^3+6x^2+4x+1\,dx  \\
  &= \pi\,\Bigl[ \frac{x^5}{5}+\frac{4x^4}{4}+\frac{6x^3}{3}+\frac{4x^2}{2} +x\Bigr]_{x=0}^{2}       \\
  &= \pi\cdot\bigl( \frac{2^5}{5}+2^4+2\cdot 2^3+2\cdot 2^2 +2 \bigr) \\
  &= \frac{242}{5} = 48.4
\end{align*}
\end{frame}
% sage: 2^5/5+2^4+2^4+2^3+2
% 242/5
% sage: n(2^5/5+2^4+2^4+2^3+2)
% 48.4000000000000



\begin{frame}{Practice}
\Ex
Draw the region below $e^{-x}$ and above the $x$~axis, and between
$a=0$ and $b=\ln(4)$.
Sketch the result of rotating that region about the $x$~axis, including
a disc slice.
Integrate $dx$ to find the volume of the solid.

\pause
\begin{center}
  \includegraphics{asy/volumes_2d003.pdf}
\end{center}
\end{frame}

\begin{frame}
The slice at $(x,0,0)$ is $dx$~thick.
\begin{center}
  % \includegraphics{asy/volumes010.png}
  \input asy/volumes_3d010.tex
\end{center}
It is a disc with base radius $e^{-x}\!$.
\begin{equation*}
  \int_{x=0}^{\ln(4)} \pi\,(e^{-x})^2\,dx
  =  \pi\int_{x=0}^{\ln(4)} e^{-2x}\,dx
\end{equation*}
\end{frame}
\begin{frame}
Use the substitution $u=-2x$ so $(-1/2)du=dx$.
\begin{align*}
  \pi\int_{x=0}^{\ln(4)} e^{-2x}\,dx
  &=  \pi\int_{u=0}^{-2\ln(4)} e^{u}\cdot(-1/2)\,du  \\
  &=  -\pi/2\int_{u=0}^{-2\ln(4)} e^{u}\,du         \\
  &= -\pi/2\,\Bigl[ e^u \Bigr]_{u=0}^{-2\ln(4)}     \\
  &= -\pi/2\,\Bigl[ (e^{\ln(4^{-2})})-(e^0) \Bigr]
  = (\pi/2)\cdot(1-(1/16))
\end{align*}
%(Recall the logarithm rule that says $a\ln(b)=\ln(b^a)$.)
\end{frame}



\begin{frame}{Multiple types of discs}
Let $f(x)=1/(1+x^2)$.
Consider the two dimensional region below $f$ but above the $x$~axis,
from $a=0$ to $b=1$.
Rotate that about the $y$~axis.
\begin{center}
  \includegraphics{asy/volumes_2d007.pdf}
\end{center}
In the method of discs because we are rotating about the $y$~axis, we
take slices that are $dy$-thick.
That requires two types of slices.
One, covering where $0\leq y\leq 1/2$, runs from $(0,y)$ to $(1,y)$.
The other covers $1/2< y\leq 1$ and runs from $(0,y)$ to $(\sqrt{(1/y)-1},y)$.
\end{frame}

\begin{frame}
Here is the solid.
\begin{center}
  % \only<1>{\vcenteredhbox{\includegraphics{asy/volumes013.png}}}%
  % \only<2->{\vcenteredhbox{\includegraphics{asy/volumes014.png}}}%
  \only<1>{\input asy/volumes_3d013.tex}%
  \only<2->{\input asy/volumes_3d014.tex}%
\end{center}
\only<2->{And here are the two kinds of discs.}
\end{frame}

\begin{frame}
\begin{align*}
  \int_{y=0}^{1/2}\pi\, 1^2\,dy+\int_{y=1/2}^1\pi\,\Bigl(\,\sqrt{\frac{1}{y}-1}\,\Bigr)^2\,dy 
    &=\pi\int_0^{1/2}y\,dy
      +\pi\int_{1/2}^1\frac{1}{y}-1\,dy  \\   
    &=\pi\,\Bigl[ y\Bigr]_{y=0}^{1/2}
       +\pi\,\Bigl[ \ln(y)-y \Bigr]_{y=1/2}^1   \\
    &=\pi\,[ \frac{1}{2}-0 ]   \\
    &\qquad  +\pi\,[ (\ln(1)-1)-(\ln(1/2)-1/2 )]  \\
    &=\pi\,[\ln(1)-\ln(1/2)]  \\
    &=\pi\,[\ln(1)-(-\ln(2))] 
     =\pi\ln(2)     
\end{align*}
\end{frame}





\begin{frame}
\Ex
Consider the region between $y=x^2$ and $y=x$.
Rotate it about the $y$~axis. 
What is the volume? 

\pause
This is the two dimensional region.
\begin{center}
  \includegraphics{asy/volumes_2d002.pdf}
\end{center}
We rotate about the $y$~axis so we take $dy$~thick slices.
This one starts at $(y,y)$ and its other end is at
$(\sqrt{y},y)$.
\end{frame}

\begin{frame}
This is the solid of revolution.
\begin{center}
  % \includegraphics{asy/volumes008.png}
  \input asy/volumes_3d008.tex
\end{center}  
\end{frame}

\begin{frame}
Here is a slice centered at $(0,y,0)$ that is $dy$-thick.
Because it has a hole, it is sometimes called a \alert{washer}.
\begin{center}
  % \includegraphics{asy/volumes009.png}
  \input asy/volumes_3d009.tex
\end{center}  
The base area is the difference of two circles, 
$\pi(\sqrt{y})^2-\pi y^2\!$.
\begin{equation*}
  \int_0^1 \pi y-\pi y^2\,dy
  =\pi\int_0^1 y-y^2\,dy
  =\pi\, \Bigl[ \frac{y^2}{2}-\frac{y^3}{3}\Bigr]_0^1
  =\frac{\pi}{6}
\end{equation*}
\end{frame}



\begin{frame}
\Ex
Consider the region between $y=\sqrt{x}$ and $y=x$.
Rotate it about the line $y=2$. 
Find the volume of that solid. 

\pause
This is the two dimensional region.
\begin{center}
  \includegraphics{asy/volumes_2d005.pdf}
\end{center}
The line $y=2$ is parallel to the $x$~axis so we take $dx$-thick slices.
This slice runs between $(c,c)$ and
$(c,\sqrt{c})$.
\end{frame}

\begin{frame}
Here are the distances from $y=2$. 
\begin{center}
  \includegraphics{asy/volumes_2d006.pdf}
\end{center}
\end{frame}

\begin{frame}
This is the solid of revolution.
\begin{center}
  % \includegraphics{asy/volumes012.png}
  \input asy/volumes_3d012.tex
\end{center}  
\begin{multline*}
  \int_{x=0}^1 \pi\,(2-x)^2-\pi\,(2-\sqrt{x})^2\,dx
  =\pi\int_{x=0}^1 x^2-5x+4\sqrt{x}\,dx                \\
  =\pi\, \Bigl[ \frac{x^3}{3}-\frac{5x^2}{2}+\frac{8x^{3/2}}{3} \Bigr]_{x=0}^1
  =\frac{\pi}{2}
\end{multline*}
\end{frame}




\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
