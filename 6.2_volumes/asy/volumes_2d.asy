// volumes_2d.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="pdf";
settings.render=0;
import fontsize;

import graph;

string OUTPUT_FN = "volumes_2d%03d";


// ==== surface of revolution =========

// ......... between (x+1)^2 and x axis ........
real f0(real x) {return (x+1)^2;}
 
picture pic;
int picnum = 0;
size(pic,0,5cm);

real a = 0;
real b = 1;

path f = graph(pic,f0,a-0.1, b+0.1);

real a_time = intersect(f, (a,0)--(a,6))[0];
real b_time = intersect(f, (b,0)--(b,6))[0];
path region = ( (a,0)--point(f,a_time)&subpath(f,a_time,b_time)&point(f,b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color);

draw(pic, f, FCNPEN);

// path a_line = (a,0)--(a,f0(a)); 
// path b_line = (b,0)--(b,f0(b)); 
// draw(pic, b_line);

real c = a+(2/3)*(b-a);
draw(pic, (c,0)--(c,f0(c)), highlight_color);

xaxis(pic,Label("$x$",position=EndPoint, align=S),
       0-0.1,1.5, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$y$"),
       0-0.1,4.5, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ========== between y^2 and the z axis ========
real f1(real y) {return y^2;}
 
picture pic;
int picnum = 1;
size(pic,0,4cm);

real a = 0;
real b = 1;

path f = graph(pic,f1,a-0.1, b+0.1);

real a_time = intersect(f, (a,0)--(a,6))[0];
real b_time = intersect(f, (b,0)--(b,6))[0];
path region = ( (a,0)--(a,1)--point(f,b_time)&subpath(f,b_time,a_time)&point(f,a_time)--(a,0) )--cycle; 
fill(pic, region, background_color);

draw(pic, f, FCNPEN);

real c = 2/3;
draw(pic, (0,c)--(sqrt(c),c), highlight_color);
dotfactor = 4;
dot(pic, (0,c), highlight_color);
label(pic, "$(0,z)$", (0,c), W);
dot(pic, (sqrt(c),c), highlight_color);
label(pic, "$(\sqrt{z},z)$", (sqrt(c),c), E);

xaxis(pic,Label("$y$",position=EndPoint, align=S),
       0-0.1,1.25, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$z$"),
       0-0.1,1.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========== between x^2 and x ========
real f2(real x) {return x^2;}
real f2a(real x) {return x;}
 
picture pic;
int picnum = 2;
size(pic,0,5cm);

real a = 0;
real b = 1;

path f = graph(pic,f2,a-0.1, b+0.075);
path fa = graph(pic,f2a,a-0.075, b+0.1);

real f_a_time = intersect(f, (a,0)--(a,6))[0];
real f_b_time = intersect(f, (b,0)--(b,6))[0];
real fa_a_time = intersect(fa, (a,0)--(a,6))[0];
real fa_b_time = intersect(fa, (b,0)--(b,6))[0];
path region = ( subpath(f,f_a_time,f_b_time)&subpath(fa,fa_b_time,fa_a_time) )--cycle; 
fill(pic, region, background_color);

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real c = 0.4;
draw(pic, (c,c)--(sqrt(c),c), highlight_color);

xaxis(pic,"$x$",
       0-0.1,1.25, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$y$"),
       0-0.1,1.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========== between e^{-x} and x-axis ========
real f3(real x) {return exp(-x);}
 
picture pic;
int picnum = 3;
size(pic,0,5cm);

real a = 0;
real b = log(4);
real ytop = 2;

path f = graph(pic,f3,a-0.1, b+0.1);

real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color);
draw(pic, point(f,f_b_time)--(b,0));

draw(pic, f, FCNPEN);

real c = a+0.618*(b-a);
draw(pic, (c,0)--(c,f3(c)), highlight_color);

real[] T3 = {1, log(4)};
xaxis(pic,Label("$x$",position=EndPoint, align=S),
      0-0.1,b+0.25, black,
      ticks=RightTicks("%", T3, Size=2pt),
      Arrow(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$\ln(4)$", log(4));
yaxis(pic,Label(""),
      0-0.1,1.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ========== between (x+1)^2 and x-axis ========
real f4(real x) {return (x+1)^2;}
 
picture pic;
int picnum = 4;
size(pic,0,4cm);

real a = 0;
real b = 2;
real ytop = f4(b)+1;

path f = graph(pic,f4,a-0.1, b+0.05);

real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color);
draw(pic, point(f,f_b_time)--(b,0));

draw(pic, f, FCNPEN);

real c = a+0.618*(b-a);
draw(pic, (c,0)--(c,f4(c)), highlight_color);

xaxis(pic,Label("$x$",position=EndPoint, align=SE),
      0-0.1,b+1.25, black,
      ticks=RightTicks(Step=1, OmitTick(0,3), Size=2pt),
      Arrow(TeXHead));

yaxis(pic,Label("$y$"),
      0-0.1,9.5, black,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=3pt, size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ========== between sqrt(x) and x ========
real f5(real x) {return sqrt(x);}
real f5a(real x) {return x;}
 
picture pic;
int picnum = 5;
size(pic,0,4cm);

real a = 0;
real b = 1;
real ytop = f5(b)+1;

path f = graph(pic,f5,a, b+0.1);
path fa = graph(pic,f5a, a-0.1, b+0.1);

real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
real fa_a_time = intersect(fa, (a,0)--(a,ytop))[0];
real fa_b_time = intersect(fa, (b,0)--(b,ytop))[0];
path region = ( subpath(f,f_a_time,f_b_time)&subpath(fa,fa_b_time,fa_a_time) )--cycle; 
fill(pic, region, background_color);

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real c = a+0.618*(b-a);
draw(pic, (c,f5a(c))--(c,f5(c)), highlight_color);

xaxis(pic,Label("$x$",position=EndPoint, align=SE),
      0-0.1,b+0.25, black,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

yaxis(pic,Label("$y$"),
      0-0.1,1+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........ rotate around y=2 ................
picture pic;
int picnum = 6;
size(pic,0,4cm);

real a = 0;
real b = 1;
real ytop = f5(b)+1;

path f = graph(pic,f5,a, b+0.1);
path fa = graph(pic,f5a, a-0.1, b+0.1);

real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
real fa_a_time = intersect(fa, (a,0)--(a,ytop))[0];
real fa_b_time = intersect(fa, (b,0)--(b,ytop))[0];
path region = ( subpath(f,f_a_time,f_b_time)&subpath(fa,fa_b_time,fa_a_time) )--cycle; 
filldraw(pic, region, background_color);

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real c = a+0.618*(b-a);
draw(pic, (c,f5a(c))--(c,f5(c)), highlight_color);

// draw y-2 line
path ax = (0-0.0,2)--(1+0.1,2);
draw(pic, ax, dashed);

// bars
real barx = 1.35;
draw(pic, "$2-\sqrt{x}$", (barx,2)--(barx,f5(c)), E, dotted, Bars);
draw(pic, Label("\;$2-x$",Relative(1.0)), (barx-0.2,2)--(barx-0.2,f5a(c)), E, dotted, Bars);

xaxis(pic,Label("$x$",position=EndPoint, align=SE),
      0-0.1,b+0.75, black,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

yaxis(pic,Label("$y$"),
      0-0.1,2+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ====  between (x+1)^2 and x axis ============
real f7(real x) {return 1/(1+x^2);}
 
picture pic;
int picnum = 7;
size(pic,0,4cm);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f7,a-0.05, b+0.1);

real a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,a_time)&subpath(f,a_time,b_time)&point(f,b_time)--(b,0)--(a,0) )--cycle; 
filldraw(pic, region, background_color);

draw(pic, f, FCNPEN);

// path a_line = (a,0)--(a,f0(a)); 
// path b_line = (b,0)--(b,f0(b)); 
// draw(pic, b_line);

real c0 = 0.25;
draw(pic, (a,c0)--(b,c0), highlight_color);
real c1 = 2/3;
draw(pic, (a,c1)--(sqrt((1/c1)-1),c1), highlight_color);


xaxis(pic,Label("$x$",position=EndPoint, align=S),
       0-0.1,1.5, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$y$"),
       0-0.1,1+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


