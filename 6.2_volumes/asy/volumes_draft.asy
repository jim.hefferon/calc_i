// volumes.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="png";
settings.render=16;
import fontsize;

import graph3; import solids;
currentprojection = orthographic(5,2,1.5,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "volumes_draft%03d";

material figure_material = material(diffusepen=light_color+opacity(0.40),
				 emissivepen=light_color,
				 specularpen=white+light_color);
material slice_material = material(diffusepen=highlight_color+white+opacity(0.5),
				   emissivepen=highlight_color+white,
				   specularpen=highlight_color+white);

currentlight = nolight;



// ============== between sqrt(x) and x, around y=2 ==========
real f12(real x) {return sqrt(x)-2;}
real f12a(real x) {return x-2;}
 
picture pic;
int picnum = 12;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

currentprojection = orthographic(1,2,1,up=Z);

real a = 0;
real b = 1;
real ybot = -3;
real ytop = 3;

path f = graph(pic,f12, a, b);
path fa = graph(pic,f12a, a, b);

// draw the solid
transform3 slide_t = shift(0,2,0);

real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
real fa_a_time = intersect(fa, (a,ybot)--(a,ytop))[0];
real fa_b_time = intersect(fa, (b,ybot)--(b,ytop))[0];
path region_pth = ( subpath(f,f_a_time,f_b_time)&subpath(fa,fa_b_time,fa_a_time) )--cycle; 
path3 region_pth3 = path3(region_pth);
draw(pic, slide_t*surface(region_pth3), gray(0.8), light=nolight);

// include end circles
// draw(pic, region_pth3, gray(0.5));
draw(pic, slide_t*circle((a,0,0), f12(a), normal=X));
draw(pic, slide_t*circle((b,0,0), f12(b), normal=X));

surface solid = slide_t*surface(region_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw y=2 line
path3 ax = (-1,2,0)--(2,2,0);
draw(pic, ax, dashed);
label(pic, "$y=2$", (2,2,0),SW);

// make the washer
real c = 1/3;
transform3 washer_t = shift(c,2,0)*rotate(90, Y);
surface washer = washer_t*surface(reverse(scale(f12(c))*unitcircle) ^^ scale(f12a(c))*unitcircle);
draw(pic, washer, surfacepen=slice_material, light=nolight);
draw(pic, washer_t*scale3(f12a(c))*unitcircle3, highlight_color);
draw(pic, washer_t*scale3(f12(c))*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,5+0.25, black,
       Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


