// volumes.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="png";
settings.render=16;
import fontsize;

import graph3; import solids;
currentprojection = orthographic(5,2,1.5,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "volumes%03d";

material figure_material = material(diffusepen=light_color+opacity(0.40),
				 emissivepen=light_color,
				 specularpen=white+light_color);
material slice_material = material(diffusepen=highlight_color+white+opacity(0.5),
				   emissivepen=highlight_color+white,
				   specularpen=highlight_color+white);

currentlight = nolight;

// === volume of a box =========
triple f(pair p) {return (p.x,p.y,(-p.y+6)/3);}

// ........ without the slice ..........
picture pic;
int picnum = 0;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);


xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,4.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,2.5, black, Arrow3(TeXHead2));

material box_material = material(diffusepen=light_color+opacity(0.85),
				 emissivepen=white,
				 specularpen=white);
// material slice_material = material(diffusepen=blue,
// 				   emissivepen=blue,
// 				   specularpen=white);

// real y =0.7;
// path3 slice = f((0,y))--f((1,y))--(1,y,0)--(0,y,0)--cycle;
// draw(pic, surface(slice), surfacepen=slice_material);

path3 box_top_path = f((0,0))--f((0,4))--f((1,4))--f((1,0))--cycle;
path3 box_rt_path = f((0,4))--f((1,4))--(1,4,0)--(0,4,0)--cycle;
path3 box_front_path = f((1,0))--f((1,4))--(1,4,0)--(1,0,0)--cycle;
draw(pic, surface(box_top_path), surfacepen=figure_material);
draw(pic, surface(box_rt_path), surfacepen=figure_material);
draw(pic, surface(box_front_path), surfacepen=figure_material);
draw(pic, box_top_path);
draw(pic, box_rt_path);
draw(pic, box_front_path);

dotfactor=4;
dot(pic, "$(0,0,3)$",(0,0,2),NE);
dot(pic, "$(0,4,1)$",f((0,4)),NE);
dot(pic, "$(1,4,1)$",f((1,4)),SW);
// label("$dy$", (0,0),NE, highlight_color);

// dot(pic, "$(0,y,0)$",(0,y,0),E,highlight_color);
// dot(pic, "$(1,y,0)$",(1,y,0),S,highlight_color);
// dot(pic, "$(0,y,f(0,y))$",f((0,y)),NE,highlight_color);
// dot(pic, "$(1,y,f(1,y))$",f((1,y)),W,highlight_color);

  
shipout(format(OUTPUT_FN,picnum),pic);


// ........ with the slice ..........
picture pic;
int picnum = 1;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);


xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
       0,4.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,2.5, black, Arrow3(TeXHead2));

material box_material = material(diffusepen=light_color+opacity(0.85),
				 emissivepen=white,
				 specularpen=white);
// material slice_material = material(diffusepen=blue,
// 				   emissivepen=blue,
// 				   specularpen=white);

real y =0.7;
path3 slice = f((0,y))--f((1,y))--(1,y,0)--(0,y,0)--cycle;
draw(pic, surface(slice), surfacepen=slice_material);
draw(pic,slice,highlight_color);

path3 box_top_path = f((0,0))--f((0,4))--f((1,4))--f((1,0))--cycle;
path3 box_rt_path = f((0,4))--f((1,4))--(1,4,0)--(0,4,0)--cycle;
path3 box_front_path = f((1,0))--f((1,4))--(1,4,0)--(1,0,0)--cycle;
// draw(pic, surface(box_top_path), surfacepen=figure_material);
// draw(pic, surface(box_rt_path), surfacepen=figure_material);
// draw(pic, surface(box_front_path), surfacepen=figure_material);
draw(pic, box_top_path, gray(0.80));
draw(pic, box_rt_path, gray(0.80));
draw(pic, box_front_path, gray(0.80));

dotfactor=4;
// dot(pic, "$(0,0,3)$",(0,0,2),NE);
// dot(pic, "$(0,4,1)$",f((0,4)),NE);
// dot(pic, "$(1,4,1)$",f((1,4)),SE);

dot(pic, "$(0,y,0)$",(0,y,0),NE);
dot(pic, "$(1,y,0)$",(1,y,0),S);
dot(pic, "$(0,y,h(y))$",f((0,y)),NE);
dot(pic, "$(1,y,h(y))$",f((1,y)),W);

  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== sphere =========
picture pic;
int picnum = 2;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);


draw(pic, unitsphere, surfacepen=figure_material);

real y = 2/3;
real r = sqrt(1-y^2);
transform3 slice_t = shift((0,y,0))*rotate(90,X)*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=highlight_color+white+opacity(0.75));
// add a circular edge
draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.5, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);


// .... slice of sphere ........
real f3(real x) {return sqrt(1-x^2);}

picture pic;
int picnum = 3;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);


// draw(pic, unitsphere, surfacepen=figure_material);


real y = 2/3;
real r = sqrt(1-y^2);
// draw outline of the sphere
path sphere_pth = graph(pic, f3, 0, 1);
draw(pic, rotate(-90, Y)*path3(sphere_pth), gray(0.80));
dot(pic, "$(0,y,\sqrt{1-y^2})$", (0,y,r), NE);
// draw slice 
transform3 slice_t = shift((0,y,0))*rotate(90,X)*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
// draw(pic, slice, surfacepen=highlight_color+white+opacity(0.75));
draw(pic, slice_t*unitcircle3, highlight_color);
// draw radius
draw(pic, (0,y,0)--(0,y,r));

dotfactor=4;
dot(pic, "$y$",(0,y,0),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.5, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== cone =========
picture pic;
int picnum = 4;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);


draw(pic, unitcone, surfacepen=figure_material);

real z = 1/3;
real r = 1-z;
transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);

dotfactor=4;
dot(pic, "$(0,0,1)$",(0,0,1),NE);
dot(pic, "$(0,1,0)$",(0,1,0),S);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("y"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.25, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);



// .............. slice of cone .........
picture pic;
int picnum = 5;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// draw(pic, unitcone, surfacepen=figure_material);

real z = 1/3;
real r = 1-z;
// draw outline of the cone
path3 cone_pth = (0,0,1)--(0,1,0);
draw(pic, cone_pth, gray(0.80));
dotfactor=4;
dot(pic, "$(0,1-z,z)$", (0,r,z), 2*E);
// dot(pic, "$(0,0,1)$",(0,0,1),NE);
// dot(pic, "$(0,1,0)$",(0,1,0),S);
// draw slice
transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);  // edge circle

dotfactor=4;
dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));
  
shipout(format(OUTPUT_FN,picnum),pic);

// ... side line of cone .........
picture pic;
int picnum = 6;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

real h = 2;
real r = 1.5;
draw(pic, "$y=\frac{-r}{h}\cdot(z-h)$", (0,0,h)--(0,r,0), E);
dotfactor =  4;
dot(pic,"$(0,0,h)$", (0,0,h));
dot(pic,"$(0,r,0)$", (0,r,0), 2*NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.75, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,2.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ==== surface of revolution =========

// ......... between y^2 and z axis ........
real f7(real y) {return y^2;}
 
picture pic;
int picnum = 7;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

path f = graph(pic,f7,0, 1);

path3 pth = rotate(90,Z)*rotate(90,X)*path3(f);
draw(pic, pth, gray(0.5));
draw(pic, circle((0,0,1), 1, normal=Z), gray(0.5));

surface region = surface(pth, c=O, axis=Z);
draw(pic, region, surfacepen=figure_material, light=nolight);

real z = 2/3;
transform3 slice_t = shift((0,0,z))*scale(sqrt(z),sqrt(z),1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);


// real r = 1-z;
// transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=highlight_color+opacity(0.95));

// draw(pic, slice_t*unitcircle3, highlight_color);


// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ......... between x^2 and x ........
real f8(real x) {return x^2;}
real f8a(real x) {return x;}
 
picture pic;
int picnum = 8;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

path f = graph(pic,f8,0, 1);
path fa = graph(pic,f8a,0, 1);

path3 pth = path3(f&fa);
draw(pic, pth, gray(0.5));
draw(pic, circle((0,1,0), 1, normal=Y), gray(0.5));

surface region = surface(pth, c=O, axis=Y);
draw(pic, region, surfacepen=figure_material, light=nolight);

// real z = 1/3;
// real r = 1-z;
// transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=highlight_color+opacity(0.95));

// draw(pic, slice_t*unitcircle3, highlight_color);


// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.75, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.25, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);




// ......... between x^2 and x, including washer ........
 
picture pic;
int picnum = 9;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

path f = graph(pic,f8,0, 1);
path fa = graph(pic,f8a,0, 1);

real c = 0.4;
real delta_y = 0.2;

// make the washer
transform3 washer_t = shift(0,c,0)*rotate(90, X);
surface washer = washer_t*surface(reverse(scale(sqrt(c))*unitcircle) ^^ scale(c)*unitcircle);
draw(pic, washer_t*scale3(c)*unitcircle3, highlight_color);
draw(pic, washer_t*scale3(sqrt(c))*unitcircle3, highlight_color);

// don't see what is wrong with this
// path3 washer_base = (c,c,0)--(sqrt(c),c,0);
// draw(pic, washer_base, red);
// surface washer = surface(washer_base, c=0, axis=Y);
draw(pic, washer, surfacepen=slice_material, light=nolight);

path3 pth = path3(fa&f);
draw(pic, pth, gray(0.5));
draw(pic, (c,c,0)--(sqrt(c),c,0), highlight_color);
draw(pic, circle((0,1,0), 1, normal=Y), gray(0.5));

surface region = surface(pth, c=O, axis=Y);
draw(pic, region, surfacepen=figure_material, light=nolight);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.75, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);





// ......... between e^{-x} and x axis ........
real f10(real x) {return exp(-x);}
 
picture pic;
int picnum = 10;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

currentprojection = orthographic(1,5,1.5,up=Z);

real a = 0;
real b = log(4);

// Get the 2D region 
path f = graph(pic,f10,a, b);
real ytop = 2;
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

// Rotate it about the x axis
draw(pic, region_pth3, gray(0.5));
draw(pic, circle((0,0,0), f10(a), normal=X), gray(0.7)+linewidth(0.4pt));
draw(pic, circle((b,0,0), f10(b), normal=X), gray(0.7)+linewidth(0.4pt));

surface solid = surface(region_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw the slice
real c = 0.5;  // where slice is on the x axis
transform3 slice_t = shift((c,0,0))*rotate(90,Y)*scale(f10(c),f10(c),1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$"),
       xmin=0, xmax=1.75,
       black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       ymin=0, ymax=1.5,
       black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),
       zmin=0, zmax=1.25,
       black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ......... between (x+1)^2 and x axis ........
real f11(real x) {return (x+1)^2;}
 
picture pic;
int picnum = 11;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

//currentprojection = orthographic(1,11,1.75,up=Z);
currentprojection = orthographic(1,20,1.5,up=Z);

real a = 0;
real b = 2;
real ytop = f11(b)+1;

path f = graph(pic,f11,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
draw(pic, circle((0,0,0), f11(a), normal=X), gray(0.7)+linewidth(0.4pt));
draw(pic, circle((b,0,0), f11(b), normal=X), gray(0.7)+linewidth(0.4pt));

surface solid = surface(region_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

real c = 2/3;
transform3 slice_t = shift((c,0,0))*rotate(90,Y)*scale(f11(c),f11(c),1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$"),
       0,b+1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,f11(a)+10.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,f11(a)+8.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);





// ============== between sqrt(x) and x, around y=2 ==========
real f12(real x) {return sqrt(x)-2;}
real f12a(real x) {return x-2;}
 
picture pic;
int picnum = 12;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

currentprojection = orthographic(1,2,1,up=Z);

real a = 0;
real b = 1;
real ybot = -3;
real ytop = 3;

path f = graph(pic,f12, a, b);
path fa = graph(pic,f12a, a, b);

// draw the solid
transform3 slide_t = shift(0,2,0);

real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
real fa_a_time = intersect(fa, (a,ybot)--(a,ytop))[0];
real fa_b_time = intersect(fa, (b,ybot)--(b,ytop))[0];
path region_pth = ( subpath(f,f_a_time,f_b_time)&subpath(fa,fa_b_time,fa_a_time) )--cycle; 
path3 region_pth3 = path3(region_pth);
draw(pic, slide_t*surface(region_pth3), gray(0.8), light=nolight);

// include end circles
// draw(pic, region_pth3, gray(0.5));
draw(pic, slide_t*circle((a,0,0), f12(a), normal=X), gray(0.7)+linewidth(0.4pt));
draw(pic, slide_t*circle((b,0,0), f12(b), normal=X), gray(0.7)+linewidth(0.4pt));

surface solid = slide_t*surface(region_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw y=2 line
path3 ax = (-1,2,0)--(2,2,0);
draw(pic, ax, dashed);
label(pic, "$y=2$", (2,2,0),SW);

// make the washer
real c = 1/3;
transform3 washer_t = shift(c,2,0)*rotate(90, Y);
surface washer = washer_t*surface(reverse(scale(f12(c))*unitcircle) ^^ scale(f12a(c))*unitcircle);
draw(pic, washer, surfacepen=slice_material, light=nolight);
draw(pic, washer_t*scale3(f12a(c))*unitcircle3, highlight_color);
draw(pic, washer_t*scale3(f12(c))*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,5+0.25, black,
       Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ......... between (x+1)^2 and x axis ........
real f13(real x) {return 1/(1+x^2);}
 
// ........ without discs ...........
picture pic;
int picnum = 13;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

currentprojection = orthographic(1,0.25,0.5,up=Z);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f13,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7)+linewidth(0.4pt));
draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7)+linewidth(0.4pt));

surface solid = surface(region_pth3, c=O, axis=Y);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// real c0 = 1/3;
// real c1 = 2/3;
// transform3 slice_t0 = shift((0,c0,0))*rotate(90, X); 
// surface slice0 = slice_t0*unitdisk;
// transform3 slice_t1 = shift((0,c1,0))*rotate(90, X)*scale(f0(c1),f0(c1),1); 
// surface slice1 = slice_t1*unitdisk;
// draw(pic, slice0, surfacepen=slice_material);
// draw(pic, slice_t0*unitcircle3, highlight_color);
// draw(pic, slice1, surfacepen=slice_material);
// draw(pic, slice_t1*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


 
// ........ with discs ...........
picture pic;
int picnum = 14;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// currentprojection = orthographic(1,0.25,0.25,up=Z);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f13,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
// draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7));
// draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7));

surface solid = surface(region_pth3, c=O, axis=Y);
draw(pic, solid, surfacepen=figure_material, light=nolight);

real c0 = 1/3;
real c1 = 2/3;
transform3 slice_t0 = shift((0,c0,0))*rotate(90, X); 
surface slice0 = slice_t0*unitdisk;
transform3 slice_t1 = shift((0,c1,0))*rotate(90, X)*scale(f13(c1),f13(c1),1); 
surface slice1 = slice_t1*unitdisk;
draw(pic, slice0, surfacepen=slice_material);
draw(pic, slice_t0*unitcircle3, highlight_color);
draw(pic, slice1, surfacepen=slice_material);
draw(pic, slice_t1*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




