#!/usr/bin/env python
import math

def f(x):
    y=100.0-4.9*x**2
    return y

x_list=[]
for i in range(0,51):
    x_list.append((i/10.0))
# x_list.sort()
dex=0
for x in x_list:
    try:
        y=f(x)
        if math.fabs(y)>200:
            print "  %% |f(%0.2f)| > 200 so not used" % (x,)
        else:
            print "  z%d=(%0.2f*x_unit,%0.2f*y_unit) transformed graph_transform;" % (dex,x,y)
        dex+=1
    except:
        print "  %% f(%0.2f) not defined" % (x,)
        
