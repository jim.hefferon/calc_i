\documentclass[10pt]{article}
\usepackage[top=1in,bottom=1in,
            headheight=0pt,headsep=0pt,
            footskip=0pt]{geometry}
\usepackage{amsmath}
\usepackage{moreverb}
\usepackage{graphics}
\usepackage{wrapfig}
\usepackage{sansserif}

\RequirePackage{etoolbox}
\newtoggle{hideanswers}
  \settoggle{hideanswers}{false}

\usepackage{dcolumn}
  \newcolumntype{.}{D{.}{.}{-1}}

\newcommand{\chapter}[1]{\noindent{\LARGE\textbf{#1}}\par\vspace*{2.5ex}\par\noindent}
\newcommand{\mapsunder}[1]{\mapsto^{#1}}

\newenvironment{ex}{
\begin{trivlist}\item[]\textbf{Example}\ 
}{%
\end{trivlist}}

\newenvironment{nums}{
\small\begin{center} 
}{%
\end{center}}

\newenvironment{exercises}{\small
\begin{trivlist}\item[]\textbf{Exercises}\begin{enumerate} 
}{%
\end{enumerate}\end{trivlist}}


\newenvironment{experiment}{
\begin{trivlist}\item[]\textbf{Experiment}\  
}{%
\end{trivlist}}


\newenvironment{computerconversation}{
\small\trivlist\item[]\begin{alltt} 
}{%
\end{alltt}}

\newcommand{\yousay}{\textit{You say: }}
\newcommand{\maplesays}{\hspace*{-3em}\textit{Maple says: }}


\pagestyle{empty}
\begin{document}
\chapter{Local Growth Rates of Polynomials}


Calculus is the language that we use to describe change.
Here we will look at polynomials and see how 
changes in their inputs lead to
changes in their outputs.
So we will fix an anchor number~$a$ and look at nearby inputs
$x=a+\Delta x$.
We are interested in the rate of change, in the 
relationship between $\Delta x$ and $\Delta y$.

We can write a function as $f(x)=a_nx^n+\cdots+a_1x+a_0$, or as 
$y=a_nx^n+\cdots+a_1x+a_0$, or
as $x\mapsto a_nx^n+\cdots+a_1x+a_0$, 
read ``$x$ maps to $a_nx^n+\cdots+a_1x+a_0$.''
We start with the squaring function.
We can write it as $f(x)=x^2$, or as $y=x^2$, or
as $x\mapsto x^2\!$.


\begin{ex}
Consider how this function changes near the anchor $a=3$.
\begin{nums}
  \begin{tabular}{|..|..|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             \\ \hline
     2.99    &-0.01   &8.9401    &-0.0599    \\
     2.999   &-0.001  &8.994001  &-0.005999  \\
     3       &0       &9         &0          \\
     3.001   &0.001   &9.006001  &0.006001   \\
     3.01    &0.01    &9.0601    &0.0601      \\ \hline
  \end{tabular}
\end{nums}
As the input rises, so does its output. 
More than that, the output changes correspond with the input changes
in a regular way:~near to $3$, the value of $\Delta y$ is about six times 
the value of $\Delta x$.

Why six times?
Expansion gives
$
  (3+\Delta x)^2=(3+\Delta x)(3+\Delta x)=9+6\Delta x+(\Delta x)^2
$
and therefore $\Delta y=6\Delta x+(\Delta x)^2\!$.
This table shows that of the two terms involving $\Delta x$ in that expansion. 
\begin{nums}
  \begin{tabular}{|.|...|}
     \hline
     \multicolumn{1}{|c|}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $6\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $(\Delta x)^2$
     \end{tabular}}                             \\ \hline
     -0.01   &-0.0599    &-0.06   &0.0001     \\
     -0.001  &-0.005999  &-0.006  &0.000001   \\
     0       &0          &0       &0          \\
     0.001   &0.006001   &0.006   &0.000001   \\
     0.01    &0.0601     &0.06    &0.0001     \\ \hline
  \end{tabular}
\end{nums}
% \begin{wrapfigure}{r}{0.2\textwidth}
%   \centering%
%   \includegraphics{asy/rates_of_change005.pdf}
% \end{wrapfigure}
The
$(\Delta x)^2$ numbers are much smaller 
than the numbers in the $6\Delta x$ column.
That's because, for instance, if $\Delta x=0.01$ then
$(\Delta x)^2=\Delta x\cdot \Delta x=0.01\cdot \Delta x$ is smaller than
$\Delta x$ by two orders of magnitude.
Squaring a small number exaggerates its smallness. 

Near $a=3$, that is, for small $\Delta x$, 
instead of working with the exact change relationship
$3+\Delta x \mapsto 9+\Delta y$ we can work with the approximate
relationship $3+\Delta x\mapsto 9+6\Delta x$.
This approximation is good enough for many purposes; for instance, if 
$\Delta x=0.004$ then $f(3+0.004)=9.024016$, making the exact $\Delta y$ be
$0.024016$, while the approximation gives $6\Delta x=6\cdot 0.004=0.024$.  

\begin{center}
  \includegraphics{asy/rates_of_change005.pdf}
\end{center}
First-power relationships graph as lines, so $3+\Delta x\mapsto 9+6\Delta x$
is a line with slope six passing through $(3,9)$.
This is the 
\emph{linear approximation}, or \emph{first-power appproximation}
or \emph{first approximation},
or the \emph{tangent-line approximation} because the
red line on the graph above
is tangent to the curve. 
% \begin{center}
%   \includegraphics{asy/rates_of_change005.pdf}
% \end{center}
% Near to the point $(3,6)$ the line is near to the curve; that's what we mean 
% by saying that for the approximation is good when $\Delta x$ is small.
\end{ex}


\begin{ex}
We can do a similar analysis of the behavior of $f(x)=x^2$ near $a=2$.
Fill out this table like the last one.
\begin{nums}
  \begin{tabular}{|..|..|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             \\ \hline
  \iftoggle{hideanswers}{
     1.99    &-0.01   &    &     \\
     1.999   &-0.001  &    &     \\
     2       &0       &4   &0          \\
     2.001   &0.001   &    &     \\
     2.01    &0.01    &    &      \\ \hline
  }{
     1.99    &-0.01   &3.9601    &-0.0399    \\
     1.999   &-0.001  &3.996001  &-0.003999  \\
     2       &0       &4         &0          \\
     2.001   &0.001   &4.004001  &0.004001   \\
     2.01    &0.01    &4.0401    &0.0401      \\ \hline
  }
  \end{tabular}
\end{nums}
% \begin{nums}
%   \begin{tabular}{|..|..|}
%      \hline
%      \multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{input} \\ $x$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \textit{change in input} \\ $\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{output} \\ $y$
%      \end{tabular}}
%      &\multicolumn{1}{c|}{\begin{tabular}{c}
%        \textit{change in output} \\ $\Delta y$
%      \end{tabular}}                             \\ \hline
%      1.99    &-0.01   &3.9601    &-0.0399    \\
%      1.999   &-0.001  &3.996001  &-0.003999  \\
%      2       &0       &4         &0          \\
%      2.001   &0.001   &4.004001  &0.004001   \\
%      2.01    &0.01    &4.0401    &0.0401      \\ \hline
%   \end{tabular}
% \end{nums}
Here, $\Delta y\approx 4\Delta x$.
The expansion $(2+\Delta x)^2=4+4\Delta x+(\Delta x)^2$ and this table
combine to explain this $\Delta x$-to-$\Delta y$ relationship,
\begin{nums}
  \begin{tabular}{|.|...|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $4\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $(\Delta x)^2$
     \end{tabular}}                             \\ \hline
  \iftoggle{hideanswers}{
     -0.01   &    &   &     \\
     -0.001  &    &   &    \\
     0       &0          &0       &0          \\
     0.001   &    &   &   \\
     0.01    &    &   &     \\ \hline
  }{
     -0.01   &-0.0399    &-0.04   &0.0001     \\
     -0.001  &-0.003999  &-0.004  &0.000001   \\
     0       &0          &0       &0          \\
     0.001   &0.004001   &0.004   &0.000001   \\
     0.01    &0.0401     &0.04    &0.0001     \\ \hline
  }
  \end{tabular}
\end{nums}
% \begin{nums}
%   \begin{tabular}{|.|...|}
%      \hline
%      \multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{change in input} \\ $\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{change in output} \\ $\Delta y$
%      \end{tabular}}                             
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \  \\ $4\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{c|}{\begin{tabular}{c}
%        \  \\ $(\Delta x)^2$
%      \end{tabular}}                             \\ \hline
%      -0.01   &-0.0399    &-0.04   &0.0001     \\
%      -0.001  &-0.003999  &-0.004  &0.000001   \\
%      0       &0          &0       &0          \\
%      0.001   &0.004001   &0.004   &0.000001   \\
%      0.01    &0.0401     &0.04    &0.0001     \\ \hline
%   \end{tabular}
% \end{nums}
As in the prior example,
the second-power term $\Delta x^2$ contributes much less to the
$\Delta y$ than does the first-power term $4\Delta x$.
Therefore, 
we can model the behavior of $f(x)=x^2$
near $a=2$ with $\Delta y=4\Delta x$.
\begin{center}
  \includegraphics{asy/rates_of_change006.pdf}
\end{center}
\end{ex}

\begin{ex}
Consider again the local behavior of $f(x)=x^2$, this time near $a=1$.
Because $(1+\Delta x)^2=1+2\Delta x+(\Delta x)^2$ we have
this table.  
\begin{nums}
  \begin{tabular}{|..|....|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $2\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $(\Delta x)^2$
     \end{tabular}}                             \\ \hline
  \iftoggle{hideanswers}{
     0.99  &-0.01  &     &    &   &     \\
     0.999 &-0.001 &     &    &   &     \\
     1     &0      &1    &0   &0  &0          \\
     1.001 &0.001  &     &    &   &   \\
     1.01  &0.01   &     &    &   &     \\  \hline
  }{
     0.99  &-0.01  &0.9801   &-0.0199    &-0.02   &0.0001     \\
     0.999 &-0.001 &0.998001 &-0.001999  &-0.002  &0.000001   \\
     1     &0      &1        &0          &0       &0          \\
     1.001 &0.001  &1.002001 &0.002001   &0.002   &0.000001   \\
     1.01  &0.01   &1.0201   &0.0201     &0.02    &0.0001     \\ \hline
  }
  \end{tabular}
\end{nums}
% \begin{nums}
%   \begin{tabular}{|..|....|}
%      \hline
%      \multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{input} \\ $x$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \textit{change in input} \\ $\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{output} \\ $y$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \textit{change in output} \\ $\Delta y$
%      \end{tabular}}                             
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \  \\ $2\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{c|}{\begin{tabular}{c}
%        \  \\ $(\Delta x)^2$
%      \end{tabular}}                             \\ \hline
%      0.99  &-0.01  &0.9801   &-0.0199    &-0.02   &0.0001     \\
%      0.999 &-0.001 &0.998001 &-0.001999  &-0.002  &0.000001   \\
%      1     &0      &1        &0          &0       &0          \\
%      1.001 &0.001  &1.002001 &0.002001   &0.002   &0.000001   \\
%      1.01  &0.01   &1.0201   &0.0201     &0.02    &0.0001     \\ 
%      \hline
%   \end{tabular}
% \end{nums}
Again, we form the linear approximation by discounting the contribution
of higher-order terms and get this.
\begin{center}
  \includegraphics{asy/rates_of_change007.pdf}
\end{center}
Near to $(a,a^2)=(1,1)$ the function's action is approximately 
equal to that of the line with slope $\Delta y/\Delta x=2$,
which is tangent to the curve at that point.
\end{ex}

In general, for small $\Delta x$, an expression involving a higher power
of $\Delta x$ such as $(\Delta x)^2$, $(\Delta x)^3$, etc.,\ 
gives much smaller values than does an expression involving
the first power of $\Delta x$.
% Note that
% this is the opposite of our experience for numbers much larger than zero,
% where a square is much bigger than a first-power term.

\begin{ex}
Next, consider the cubing function, written 
$y=x^3$, or $f(x)=x^3$, or $x\mapsto x^3$.
We will describe how this function changes near $a=2$.
Expanding the expression 
$(2+\Delta x)^3=8+12\Delta x+6(\Delta x)^2+(\Delta x)^3$
suggests this table.
\begin{nums}
  \begin{tabular}{|..|D{.}{.}{7}D{.}{.}{7}D{.}{.}{4}D{.}{.}{7}D{.}{.}{12}|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $12\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $6\Delta x^2$
     \end{tabular}}                             
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $\Delta x^3$
     \end{tabular}}                             \\ \hline
  \iftoggle{hideanswers}{
     1.99  &-0.01  &   &   &    &    &     \\
     1.999 &-0.001 &   &   &    &    &     \\
     2     &0      &8  &0  &0   &0   &0             \\
     2.001 &0.001  &   &   &    &    &   \\
     2.01  &0.01   &   &   &    &    &     \\ \hline
  }{
     1.99  &-0.01  &7.880599 &0.119401 &-0.12   &0.0006    &0.000001     \\
     1.999 &-0.001 &7.988006 &0.011994 &-0.012  &0.000006  &-0.000000001   \\
     2     &0      &8        &0        &0       &0         &0             \\
     2.001 &0.001  &8.012006 &0.012006 &0.012   &0.000006  &0.000000001   \\
     2.01  &0.01   &8.120601 &0.120601 &0.12    &0.0006    &0.000001     \\  \hline
  }
  \end{tabular}
\end{nums}
% \begin{nums}
%   \begin{tabular}{|..|D{.}{.}{7}D{.}{.}{7}D{.}{.}{4}D{.}{.}{7}D{.}{.}{12}|}
%      \hline
%      \multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{input} \\ $x$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \textit{change in input} \\ $\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{|c}{\begin{tabular}{c}
%        \textit{output} \\ $y$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \textit{change in output} \\ $\Delta y$
%      \end{tabular}}                             
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \  \\ $12\Delta x$
%      \end{tabular}}
%      &\multicolumn{1}{c}{\begin{tabular}{c}
%        \  \\ $6\Delta x^2$
%      \end{tabular}}                             
%      &\multicolumn{1}{c|}{\begin{tabular}{c}
%        \  \\ $\Delta x^3$
%      \end{tabular}}                             \\ \hline
%      1.99  &-0.01  &7.880599 &0.119401 &-0.12   &0.0006    &0.000001     \\
%      1.999 &-0.001 &7.988006 &0.011994 &-0.012  &0.000006  &-0.000000001   \\
%      2     &0      &8        &1        &0       &0         &0             \\
%      2.001 &0.001  &8.012006 &0.012006 &0.012   &0.000006  &0.000000001   \\
%      2.01  &0.01   &8.120601 &0.120601 &0.12    &0.0006    &0.000001     \\ 
%      \hline
%   \end{tabular}
% \end{nums}
As earlier, the numbers in the higher-power change columns $6\Delta x^2$ and
$\Delta x^3$ are much smaller than  the numbers in the first-power change
column $12\Delta x$.
Consequently,  near $a=2$ 
we can get a good approximation by focusing on the linear change
relationship $\Delta y\approx 12\Delta x$.
Graphically, because $\Delta y/\Delta x \approx 12$ near to $(2,8)$,
the function changes approximately as does the line with slope $12$ that is 
tangent to the curve at that point.
\begin{center}
  \includegraphics{asy/rates_of_change008.pdf}
\end{center}
\end{ex}


\begin{exercises}
\item Do the same analysis for $y=x^3$ at $a=3$.  
\iftoggle{hideanswers}{}{
\begin{nums}
  \begin{tabular}{|..|D{.}{.}{7}D{.}{.}{7}D{.}{.}{4}D{.}{.}{7}D{.}{.}{12}|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $12\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $6\Delta x^2$
     \end{tabular}}                             
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $\Delta x^3$
     \end{tabular}}                             \\ \hline
     2.99  &-0.01  &26.730899 &-0.2691  &-0.27   &0.0009    &-0.000001     \\
     2.999 &-0.001 &6.973009  &-0.026991&-0.027  &0.000009  &-0.000000001   \\
     3     &0      &27        &0        &0       &0         &0             \\
     3.001 &0.001  &27.027009001 &0.0270090001 &0.027 &0.000009 &0.000000009  \\
     3.01  &0.01   &27.270901 &0.270901 &0.27  &0.0009  &0.000001        \\ 
     \hline
  \end{tabular}
\end{nums}
}
% sage: def f(x):
% ....:     return x^3
% ....: 
% sage: a=3
% sage: for delta_x in [-0.01, -0.001, 0, 0.001, 0.01]:
% ....:     print("x=",a+delta_x,
% ....:           "delta_x=",delta_x,
% ....:           "y=",f(a+delta_x),
% ....:           "delta_y=", f(a+delta_x)-f(a),
% ....:           "linear change=", 3*a^2*delta_x,
% ....:           "quadratic_change=", 3*a*(delta_x^2),
% ....:           "cubic change=", delta_x^3)
% ....: 
% x= 2.99000000000000 delta_x= -0.0100000000000000 y= 26.7308990000000 delta_y= -0.269100999999996 linear change= -0.270000000000000 quadratic_change= 0.000900000000000000 cubic change= -1.00000000000000e-6
% x= 2.99900000000000 delta_x= -0.00100000000000000 y= 26.9730089990000 delta_y= -0.0269910009999954 linear change= -0.0270000000000000 quadratic_change= 9.00000000000000e-6 cubic change= -1.00000000000000e-9
% x= 3 delta_x= 0 y= 27 delta_y= 0 linear change= 0 quadratic_change= 0 cubic change= 0
% x= 3.00100000000000 delta_x= 0.00100000000000000 y= 27.0270090010000 delta_y= 0.0270090009999961 linear change= 0.0270000000000000 quadratic_change= 9.00000000000000e-6 cubic change= 1.00000000000000e-9
% x= 3.01000000000000 delta_x= 0.0100000000000000 y= 27.2709010000000 delta_y= 0.270900999999995 linear change= 0.270000000000000 quadratic_change= 0.000900000000000000 cubic change= 1.00000000000000e-6

\item Do the same analysis for $y=x^3$ at $a=0$, and $a=-2$.  
\iftoggle{hideanswers}{}{
\begin{nums}
  \begin{tabular}{|..|D{.}{.}{7}D{.}{.}{7}D{.}{.}{4}D{.}{.}{7}D{.}{.}{12}|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $12\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $6\Delta x^2$
     \end{tabular}}                             
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $\Delta x^3$
     \end{tabular}}                             \\ \hline
     -0.01  &-0.01  &-0.000001 &-0.000001&0       &0         &-0.000001   \\
     -0.001 &-0.001 &-0.000000001 &-0.000000001   &0  &0     &-0.000000001    \\
     0      &0      &0        &0        &0       &0         &0       \\
     0.001  &0.001  &0.000000001 &0.000000001 &0 &0          &0.000000001  \\
     0.01   &0.01   &0.000001    &0.000001    &0 &0          &0.000001    \\ 
     \hline
  \end{tabular}
\end{nums}
}
% sage: a=0
% sage: for delta_x in [-0.01, -0.001, 0, 0.001, 0.01]:
% ....:     print("x=",a+delta_x,
% ....:           "delta_x=",delta_x,
% ....:           "y=",f(a+delta_x),
% ....:           "delta_y=", f(a+delta_x)-f(a),
% ....:           "linear change=", 3*a^2*delta_x,
% ....:           "quadratic_change=", 3*a*(delta_x^2),
% ....:           "cubic change=", delta_x^3)
% ....: 
% x= -0.0100000000000000 delta_x= -0.0100000000000000 y= -1.00000000000000e-6 delta_y= -1.00000000000000e-6 linear change= -0.000000000000000 quadratic_change= 0.000000000000000 cubic change= -1.00000000000000e-6
% x= -0.00100000000000000 delta_x= -0.00100000000000000 y= -1.00000000000000e-9 delta_y= -1.00000000000000e-9 linear change= -0.000000000000000 quadratic_change= 0.000000000000000 cubic change= -1.00000000000000e-9
% x= 0 delta_x= 0 y= 0 delta_y= 0 linear change= 0 quadratic_change= 0 cubic change= 0
% x= 0.00100000000000000 delta_x= 0.00100000000000000 y= 1.00000000000000e-9 delta_y= 1.00000000000000e-9 linear change= 0.000000000000000 quadratic_change= 0.000000000000000 cubic change= 1.00000000000000e-9
% x= 0.0100000000000000 delta_x= 0.0100000000000000 y= 1.00000000000000e-6 delta_y= 1.00000000000000e-6 linear change= 0.000000000000000 quadratic_change= 0.000000000000000 cubic change= 1.00000000000000e-6

\iftoggle{hideanswers}{}{
\begin{nums}
  \begin{tabular}{|..|D{.}{.}{7}D{.}{.}{7}D{.}{.}{4}D{.}{.}{7}D{.}{.}{12}|}
     \hline
     \multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{input} \\ $x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in input} \\ $\Delta x$
     \end{tabular}}
     &\multicolumn{1}{|c}{\begin{tabular}{c}
       \textit{output} \\ $y$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \textit{change in output} \\ $\Delta y$
     \end{tabular}}                             
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $12\Delta x$
     \end{tabular}}
     &\multicolumn{1}{c}{\begin{tabular}{c}
       \  \\ $6\Delta x^2$
     \end{tabular}}                             
     &\multicolumn{1}{c|}{\begin{tabular}{c}
       \  \\ $\Delta x^3$
     \end{tabular}}                             \\ \hline
     -2.01  &-0.01  &-8.120601 &-0.120601 &-0.12  &-0.0006   &-0.000001   \\
     -2.001 &-0.001 &-8.012006001 &-0.012006001 &-0.012 &-0.000006 &-0.000000001  \\
     -2     &0      &-8        &0        &0       &0         &0             \\
     -1.999 &0.001  &-7.988005999 &0.011994 &0.012 &-0.000006 &0.000000001   \\
     -1.99  &0.01   &-7.880599 &0.119401 &0.12     &-0.0006   &0.000001          \\ 
     \hline
  \end{tabular}
\end{nums}
}
% sage: a=-2
% sage: for delta_x in [-0.01, -0.001, 0, 0.001, 0.01]:
% ....:     print("x=",a+delta_x,
% ....:           "delta_x=",delta_x,
% ....:           "y=",f(a+delta_x),
% ....:           "delta_y=", f(a+delta_x)-f(a),
% ....:           "linear change=", 3*a^2*delta_x,
% ....:           "quadratic_change=", 3*a*(delta_x^2),
% ....:           "cubic change=", delta_x^3)
% ....: 
% x= -2.01000000000000 delta_x= -0.0100000000000000 y= -8.12060100000000 delta_y= -0.120600999999997 linear change= -0.120000000000000 quadratic_change= -0.000600000000000000 cubic change= -1.00000000000000e-6
% x= -2.00100000000000 delta_x= -0.00100000000000000 y= -8.01200600100000 delta_y= -0.0120060009999978 linear change= -0.0120000000000000 quadratic_change= -6.00000000000000e-6 cubic change= -1.00000000000000e-9
% x= -2 delta_x= 0 y= -8 delta_y= 0 linear change= 0 quadratic_change= 0 cubic change= 0
% x= -1.99900000000000 delta_x= 0.00100000000000000 y= -7.98800599900000 delta_y= 0.0119940009999988 linear change= 0.0120000000000000 quadratic_change= -6.00000000000000e-6 cubic change= 1.00000000000000e-9
% x= -1.99000000000000 delta_x= 0.0100000000000000 y= -7.88059900000000 delta_y= 0.119401000000000 linear change= 0.120000000000000 quadratic_change= -0.000600000000000000 cubic change= 1.00000000000000e-6
\end{exercises}

\end{document}

In this prepatory section we have considered how some familiar functions
change near to a few points.
The next section continues this exploration 
with higher-powered polynomials, using the computer to do some of the
larger calculations.


\begin{exercises}
  \item
     Consider the squaring function at $a=3$.
     Find the change in $y$ divided by the change in $x$ when $x=5$.
     Do the same when $x=4$, and 
     note that they are different.
     \textit{Remark.} 
     This exercise shows that 
     it is impossible in general to assign a `slope' number to a curve
     that is not a line.
     Instead, to describe the rate of change of
     a curve we must do something like we did above~--- associate
     a line with the curve and use the slope of that line.
  \item 
    For the squaring function examples above, give the $y=mx+b$ form of 
    the equation of the tangent line in the $a=3$, $a=2$, and $a=1$ cases.
  \item
    For the squaring function, give  the linear approximation at
    $a=-3$, $a=-2$, and $a=-1$.
  \item
    Linear approximations are commonly used in science and engineering,
    as this passage from \textit{Surely You're Joking, Mr. Feynman}
    illustrates.
    (This book is the autobiography of the Nobel prize winning physicist, 
    and curious character, Richard Feynman.
    Here he is talking about his Ph.D. thesis advisor.)
    \begin{quotation}
      When I was at Los Alamos I found out that Hans Bethe was absolutely
      topnotch at calculating.
      For example, one timewe were putting some numbers into a formula
      and got $48$ squared.
      I reach for the Marchant calculator, and he says, ``That's $2300$.''
      I begin to push the buttons , and he says, 
      ``If you want it exactly, it's $2304$.''

      The machine says $2304$,
      ``Gee!  That's pretty remarkable!''  I say.
     
      ``Don't you know how to square numbers near $50$?'' he says
      ``You square $500$~-- that's $2500$~-- and subtract $100$ times the
       difference of your number from $50$ (in this case it's $2$),
       so you have $2300$.
       If you want the correction, square the difference and add it on.
       That makes $2304$.
    \end{quotation}
    Derive Bethe's formula, both the exact one and the approximate one.

  \item
    For the cubing function, give the equation of the tangent line
    in the $a=2$ case.
    Then give the equation in the $a=1$ case (for he first part you can 
    use the expansion of $(2+\Delta x)^3$ that is found above, 
    but  for this part you must expand $(1+\Delta x)^3$ yourself).  

  \item Above it says `for small $\Delta x$, an expression 
    involving a higher power of $\Delta x$ gives much smaller 
    values than does an expression involving
    the first power of $\Delta x$'.
    There a subtlety in that assertion.
    Make a table with columns headed $(\Delta x)^2$ and $0.0001\Delta x$.
    Fill in rows where $\Delta x$ has the value $0.1$ and $0.01$.
    Note that the first-power expression is not smaller than the second-power
    expression (because of the $0.0001$ coefficient).
    Next, fill in rows where $\Delta x$ is $0.001$, $0.0001$, and $0.00001$.
    Note the first-power expression is now smaller than the second-power
    expression.
    The discrepancy will only get greater for smaller and smaller $\Delta x$'s.
    That is, after we've seen this example, we see that the sentence
    above may be altered to say 
    `for \emph{sufficiently} small $\Delta x$, an expression 
    involving a higher power of $\Delta x$ gives much smaller 
    values than does an expression involving
    the first power of $\Delta x$'.


  \item
    Give the formula for the slope of the line tangent to the squaring
    function at $a$.
    That is, expand the expression $(a+\Delta x)^2$ and discard terms involving 
    higher power of $\Delta x$ to get that  
    $\Delta y$ is approximately equal to 
    an expression involving $a$ times $\Delta x$.

  \item
    We have focused on the behavior of the functions near to the point $a$.
    But the exact equations $a+\Delta x \mapsto f(a+\Delta x)$ remain true
    even when $\Delta x$ is large.
    Consider again the relationship for $a=3$ and the squaring function
    $3+\Delta x \mapsto 9+ 6\Delta x + (\Delta x)^2$.
    Make a table with columns headed $x$, $\Delta x$, $y$,
        $\Delta y$, $6\Delta x$, and $\Delta x^2$.
        Fill in the rows for $x=3$, $x=4$, $x=5$, $x=10$, and $x=30$.
        Also fill in rows for $x=2$, $x=0$, and $x=-30$.
    Compute how far off the linear approximation is in each case.
    (So, when $\Delta x$ is not small, 
    we cannot discount the contribution from 
    the quadratic term.
    On the contrary, those contributions come to dominate the
    expression $9+6\Delta x+(\Delta x)^2$.)
\end{exercises}





\section{At the Keyboard}

Now we will get the computer to carry out a similar analysis, 
in cases that are awkward for by-hand work.
(\textit{Remark:} a result called the Binomial Theorem would give us the 
conclusions also.
But, rather than introduce and prove that theorem,
we will get the numbers that we want by brute-force calculation.
This is not elegent, but it does get us quickly into how
functions change.)

\begin{experiment}
Again consider the squaring function in the $a=3$ case.
\begin{verbatim}
  > expand((3+Delta_x)^2);
                     2
  9+6*Delta_x+Delta_x 
\end{verbatim}
Maple can graph both the function and its linear approximation.
\begin{verbatim}
  > plot({(3+Delta_x)^2),9+6*Delta_x},Delta_x=-2..2);   
\end{verbatim}
This plot is shifted from the one shown in the prior section because
it takes $\Delta x$ as the variable whereas above the variable is $x$.
The first exercise asks you to get Maple to produce the original 
plot.
\end{experiment}
  
We can of course move beyond reproducing prior results.

\begin{experiment}
Consider $f(x)=x^4$ and $a=1$.
\begin{verbatim}
  > expand((1+Delta_x)^4);
                       2          3        4
  1+4*Delta_x+6*Delta_x +4*Delta_x +Delta_x 
\end{verbatim}
Discarding the terms with higher powers of $\Delta x$ gives the
first-power approximation $\Delta y=1+4\Delta x$.
Here is the plot (as in the prior example, this plot is shifted).
\begin{verbatim}
  > plot({(1+Delta_x)^4,1+4*Delta_x},Delta_x=-2..2);
\end{verbatim}
Note that this first computer case shws the same effect as
the by-hand cases: discarding higher-power terms gives a good 
approximation for small $\Delta x$. 
\end{experiment}

\begin{experiment}
To emphasize some of the advantage that the computer has for these 
experiments, we can do the $a=3$ case with the function $f(x)=x^{10}$.
\begin{verbatim}
  > expand((1+Delta_x)^10);}
                                  2              3       
  59049+196830Delta_x+29524Delta_x +262440Delta_x + ...
  > plot({(3+Delta_x)^10,59049+196830*Delta_x},Delta_x=-0.5..0.5);
\end{verbatim}
Even quite-high power functions are not too much for the computer.
\begin{verbatim}
  > expand((5+Delta_x)^20);
                         
  95367431640625+381469726562500Delta_x+ ...
  >plot({(5+Delta_x)^20,95367431640625+381469726562500*Delta_x},Delta_x=-0.2..0.2);
\end{verbatim}
The higher powers show the same effect, that the linear
approximation is close to the curve for small $\Delta x$.
\end{experiment}

We can ask the computer to find what is the pattern; 
how is the coefficient of the first power
of $\Delta x$ determined from the power~$n$ in $f(x)=x^n$ and the value $a$?

\begin{experiment}
Consider the case of the cubing function, but this time we do not give the
computer a specific $a$.
\begin{verbatim}
  > expand((a+Delta_x)^3);
   3   2                 2        3
  a +3a Delta_x+3aDelat_x +Delta_x 
\end{verbatim}
Thus, in general, the formula for the linear approximation is
$a+\delta_x\mapsto a^3+3a^2\Delta x$, that is, the line tangent to
$f(x)=x^3$ at $(a,a^3)$ has slope $3a^2$.
\end{experiment}

\begin{experiment}
The fourth power function gives this.
\begin{verbatim}
  > expand((a+Delta_x)^4);
   4   3          2       2          3        4
  a +4a Delta_x+6a Delta_x +4aDelta_x +Delta_x
\end{verbatim}
Thus the linear approximation is $a+\Delta x\mapsto a^4+4a^3\Delta x$,
and so the line tangent to the graph at $(a,a^4)$ has slope $4a^3$.
\end{experiment}

Those examples show an obvious similarity.
The second exercise carries on in the same direction.

These experiments show the strength, and the weakness, of the computer in 
mathematical investigations.
Oe would not be keen to expand the tenth power or hundredth
power by hand.
On the other hand, no number of examples constitute an ironclad proof.
On balance, the point is that the computer has become invaluable for 
experimentation, conjecture, and trial.

\begin{exercises}
  \item
    Which commands will shift the plot so they look like the
    ones in the prior section?

  \item Make a table with columns labelled `power $n$' and
    `coefficient of $\Delta x$ in the expansion of $(1+\Delta x)^n$.
     Fill out the row for the powers $n=2$, $3$, $4$, $5$, $10$, and $20$.
     What seems to be the pattern?

   \item 
      Repeat the prior exercise for $(2+\Delta x)^n$.
      Repeat also for $(-1+\Delta x)^n$.

  \item 
      Conjecture as to the formula for a general power $n$ at the point $a$.

  \item
    On the basis of the prior exercise, guess at the equation of the line 
    tangent to the graph of the square root function $f(x)=x^{1/2}$
    at the point $(9,3)$.
    Graph the function and your guessed tangent.
    Does you guess appear correct?

  \item
    Briefly state the strength of evidence that you have for your conjecture.
    Is it a proof?
    How could it be made better, short of a proof?
\end{exercises}



\section{After the Keyboard}
Certainly the first question brought out by the material here is, 
``Why?  Why look at approximations when we can consider the exact
function, especially when we have the computer to do any hard calculations?''
Exercise 1 below shows how the things studied above allow us to find
answers that we do not have any evident way to find otherwise.
Besides, the question of the exact formula for the linear approximation
is interesting in and of itself.

With that motivation, we can move forward in two ways.
First, we can try to prove that the patterns seen above always hold.
And second, we can expand the process of finding 
linear approximations
(for instance, how to approximate non-polynomials like $f(x)=\sin (x)$?).
Both ways are accomplished 
using the same technical device, limits.

\begin{exercises}
  \item 
    Why should we shift from studying exact functions to studying their 
    approximate actions?
    Consider the function $f(x)=x^3-x$.
    Graph it, and guess
    the value of $x$ at the local minimum and the local maximum.
    Next, observe that at the local minimum and the local maximum the
    tangent line is horizontal~--- has slope zero.
    Find the linear approximation at $a$, 
    set it to zero, and solve.
 
  \item
    Graph $f(x)=\sin (x)$,
    being sure that your graph's horizontal scale equals its vertical scale.  
    Estimate the slope of the tangent line
    at $a=0$, $\pi/5$, $2\pi/5$, \ldots, $2\pi$.
    Sketch a plot of $a$ versus your estimated slope at $a$.
    Make a conjecture about the formula for the slopes. 
\end{exercises}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:

