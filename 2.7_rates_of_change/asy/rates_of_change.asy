// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "rates_of_change%03d";

import graph;



// Repeat of first day's secant lines
// ================ 5-(1/2)*(x-3)^2 =======
real f(real x) {return 5-(1/2)*(x-3)^2;}

picture pic;
int picnum = 0;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
draw(pic, graph(f,-0.1,5.1), highlight_color);
filldraw(pic, circle((1,3),0.05), white, black);
label(pic,  "$(1,3)$", (1,3), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(3)
// 5
// sage: f(1)
// 3
// sage: f(2)
// 9/2
pair anchor_pt = (1,3);
pair first_secant_pt = (2,4.5);
real first_secant(real x) {return ((4.5-3)/(2-1))*(x-1)+3;}


picture pic;
int picnum = 1;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(first_secant,0.5,2.5), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(first_secant_pt,0.05), white, black);
label(pic,  "$(2,4.5)$", first_secant_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
pair second_secant_pt = (1.1,3.195);
real second_secant(real x) {return ((3.195-3)/(1.1-1))*(x-1)+3;}


picture pic;
int picnum = 2;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(second_secant,0.25,1.75), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(second_secant_pt,0.05), white, black);
label(pic,  "$(1.1,3.195)$", second_secant_pt, (0.85,0.1));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
real tangent_line(real x) {return 2*(x-1)+3;}


picture pic;
int picnum = 3;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(tangent_line,0.25,1.75), highlight_color);

// label(pic,graphic("climbing.png","width=0.80cm,angle=10"),
//       anchor_pt-(0.1,-0.15));

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======== t^3+3 =====
real f4(real x) {return x^2+3;}
real tangent_line4(real x) {return 4*(x-2)+7;}

picture pic;
int picnum = 4;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=11;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// sage: n(sqrt(8))
// 2.82842712474619
draw(pic, graph(f4,-0.1,2.84), black);
draw(pic, graph(tangent_line4,1.25,2.8), highlight_color);

filldraw(pic, circle((2,7),0.05), highlight_color, highlight_color);
label(pic,  "$(2,7)$", (2,7), NW, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======== local growth rate of polynomials =====

// ..... tangent line at (3,9) ........
real f5(real x) {return x^2;}
real tangent_line5(real x) {return 6*(x-3)+9;}

picture pic;
int picnum = 5;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=16;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f5,-0.2,4.05);
draw(pic, f, FCNPEN);
draw(pic, graph(tangent_line5,2,4), highlight_color);

filldraw(pic, circle((3,9),0.05), FCNPEN_COLOR);
label(pic, "$(3,9)$", (3,9), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... tangent line at (2,4) ........
real tangent_line6(real x) {return 4*(x-2)+4;}

picture pic;
int picnum = 6;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f5,-0.2,3.05);
draw(pic, f, FCNPEN);
draw(pic, graph(tangent_line6,1,3), highlight_color);

filldraw(pic, circle((2,4),0.05), FCNPEN_COLOR);
label(pic, "$(2,4)$", (2,4), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... tangent line at (1,1) ........
real tangent_line7(real x) {return 2*(x-1)+1;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f5,-0.2,2.05);
draw(pic, f, FCNPEN);
draw(pic, graph(tangent_line7,0,2), highlight_color);

filldraw(pic, circle((1,1),0.05), FCNPEN_COLOR);
label(pic, "$(1,1)$", (1,1), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ..... tangent line at (3,9) ........
real f8(real x) {return x^3;}
real tangent_line8(real x) {return 12*(x-2)+8;}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=14;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f8,-0.2,2.4);
draw(pic, f, FCNPEN);
draw(pic, graph(tangent_line8,1.5,2.5), highlight_color);

filldraw(pic, circle((2,8),0.05), FCNPEN_COLOR);
label(pic, "$(2,8)$", (2,8), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


