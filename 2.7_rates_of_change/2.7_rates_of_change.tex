\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.7\ \ Rates of change}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Rates of change}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Precisely defining rate of change}


\begin{frame}{Secant lines lead to a tangent line}
\begin{center}
  \only<1>{\includegraphics{asy/rates_of_change000.pdf}}%    
  \only<2>{\includegraphics{asy/rates_of_change001.pdf}}%    
  \only<3>{\includegraphics{asy/rates_of_change002.pdf}}%    
  \only<4->{\includegraphics{asy/rates_of_change003.pdf}}%    
\end{center}
\begin{center}
  \begin{tabular}{lc|c}
    \multicolumn{2}{c}{\textit{Inputs}}
      &\multicolumn{1}{c}{\textit{Slope of line}} \\ \hline
      $2$ &$1$       &\uncover<2->{$(4.5-3)/(2-1)=1.5$} \\
      $1.1$ &$1$     &\uncover<3->{$(3.195-3)/(1.1-1)=1.95$} \\
      $1.01$ &$1$    &\uncover<4->{$(3.0019995-3)(1.01-1)=1.999499$} \\
    $1.001$ &$1$   &\uncover<4->{$(3.000199995-3)(1.001-1)=1.999950$} \\
    $1$ &$1$       &\uncover<4->{$2$, as a limit}
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}{Difference quotient}
The fraction
\begin{equation*}
  \frac{f(a+h)-f(a)}{(a+h)-a}
  =\frac{f(a+h)-f(a)}{h}
\end{equation*}
is the \alert{difference quotient}.

We think of it in two ways.
\begin{itemize}
\item It is the average rate of change on the interval from $x=a$ to
$x=a+h$.
\item It is the slope of the line \alert{secant} to the curve,
that is, passing through the curve in two spots, $(a,f(a))$ and $(a+h,f(a+h))$.
We think of $(a,f(a))$ as being fixed or anchored, and as
$(a+h,f(a+h))$ as moving with~$h$.
And, we think of $h$ as a small number, such as $0.001$.
\end{itemize}
\end{frame}


\begin{frame}{The rate of change}
The \alert{instantaneous rate of change} of a $f(t)$
at $t=a$ is
the limit of the average rates of change, as the averages are taken
on smaller and smaller intervals
\begin{equation*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{(a+h)-a}
  =\lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
\end{equation*}
(provided that the limit exists).
We will often just call it \alert{the rate of change}.

We denote it with any of these.
\begin{equation*}
  f'(a)
  \qquad
  \left.\frac{df}{dt}\right|_{t=a}  
  \qquad
  \left.\frac{dy}{dt}\right|_{t=a}  
\end{equation*}
One advantage of the second notation is this. 
\begin{equation*}
  \lim_{\Delta x\to 0}\frac{\Delta y}{\Delta x}=\frac{dy}{dx}
\end{equation*}
\end{frame}

\begin{frame}
We think of the instantaneous rate of change in two ways (at least).
\begin{itemize}
\item The limit of the average rate of change on every-smaller intervals
is the instantaneous rate of change at the point $t=a$.
If $f(t)$ gives the position of an object at time~$t$ then this defines the
velocity at time~$t=a$.
\item The limit of the secant lines is the 
line \alert{tangent} to the curve at $(a,f(a))$.
\end{itemize}

The book mentions that this is also known as 
the \alert{derivative} of $f$ at $a$.
More in the next section.  
\end{frame}


\begin{frame}{A quadratic}
Let $f(t)=t^2+3$ and find the rate of change at $t=2$.
\begin{align*}
  \lim_{h\to 0}\frac{f(2+h)-f(2)}{h}
  &=
  \lim_{h\to 0}\frac{(2+h)^2+3-(2^2+3)}{h}       \\
  &=
  \lim_{h\to 0}\frac{(4+4h+h^2)+3-7}{h}         \\
  &=
  \lim_{h\to 0}\frac{4h+h^2}{h}
\end{align*}
So far this has been a $0/0$-type limit.  
Some algebra makes that go away, and we can apply the Limit Laws.
\begin{equation*}
  =
  \lim_{h\to 0}\frac{h(4+h)}{h}
  =
  \lim_{h\to 0}4+h=4
\end{equation*}
We can write any of these.
    \begin{equation*}
      f'(2)=4\quad\text{or}\quad\left.\frac{df}{dt}\right|_{t=2}=4
      \quad\text{or}\quad\left.\frac{dy}{dt}\right|_{t=2}=4
    \end{equation*}
\end{frame}


\begin{frame}{Equation of the tangent line}
We've just found that
for the curve of $f(t)=t^2+3$ at the point $(2,7)$, 
the tangent line has slope~$4$.
So this is an equation of that line.
\begin{equation*}
  y-7=4\cdot(x-2)
\end{equation*}
Often we like ``$mx+b$'' form for equations of lines.
\begin{equation*}
  y=4\cdot(x-2)+7=4x-8+7=4x-1
\end{equation*}
\begin{center}
    \vcenteredhbox{\includegraphics{asy/rates_of_change004.pdf}}
\end{center}
\end{frame}






\begin{frame}{Practice}
Find the equation of the tangent line for 
$f(x)=2x^2+3x$ at $a=1$.
\end{frame}



\begin{frame}{A rational function}
Let $f(x)=1/x$ and find the rate of change at $a=2$.
\begin{equation*}
  \lim_{h\to 0}\frac{f(2+h)-f(2)}{h}
  =\lim_{h\to 0}\frac{\frac{1}{2+h}-\frac{1}{2}}{h}
\end{equation*}
\pause
Algebraically simplify the difference quotient.
Here in the top we get a common denominator of $2(2+h)$.
\begin{align*}
  =\lim_{h\to 0}\frac{\frac{2}{2(2+h)}-\frac{2+h}{2(2+h)}}{h}
  &=\lim_{h\to 0}\frac{-\frac{h}{2(2+h)}}{h}   \\
  &=\lim_{h\to 0}\frac{-1}{2(2+h)}   
\end{align*}
This is no longer a $0/0$ type.
Finish by plugging in (that is, apply the Limit Laws).
\begin{equation*}
  \lim_{h\to 0}\frac{-1}{2(2+h)}=-\frac{1}{4}
\end{equation*}
\pause
\textsc{Practice:} Do the same for $a=3$.
\end{frame}





\begin{frame}{A radical}
Let $f(x)=\sqrt{x}$ and find the rate of change at $a=4$.
Note that we cannot cancel the $h$'s.
\begin{equation*}
  \lim_{h\to 0}\frac{f(4+h)-f(4)}{h}
  =\lim_{h\to 0}\frac{\sqrt{4+h}-\sqrt{4}}{h}
\end{equation*}
\pause
Algebraically simplify the difference quotient.
Here leverage the difference of two squares by 
multiplying top and bottom by $\sqrt{4+h}+\sqrt{4}$.
\pause
\begin{align*}
  &=\lim_{h\to 0}\frac{\sqrt{4+h}-\sqrt{4}}{h}\cdot\frac{\sqrt{4+h}+\sqrt{4}}{\sqrt{4+h}+\sqrt{4}}              \\
  &=\lim_{h\to 0}\frac{(4+h)-4}{h(\sqrt{4+h}+\sqrt{4})}   \\
  &=\lim_{h\to 0}\frac{h}{h(\sqrt{4+h}+\sqrt{4})} 
  =\lim_{h\to 0}\frac{1}{\sqrt{4+h}+\sqrt{4}}   
\end{align*}
This is no longer a $0/0$ type.
Plug in to get $1/4$.

\pause
\textsc{Practice} Do it for $a=9$.
\pause Also $a=5$.
\end{frame}



% \begin{frame}{Another}
% Again $f(x)=\sqrt{x}$ but this time find the rate of change at~$a=1$.
% \pause
% \begin{align*}
%   \lim_{h\to 0}\frac{\sqrt{1+h}-1}{h}
%   &=\lim_{h\to 0}\frac{\sqrt{1+h}-1}{h}\cdot\frac{\sqrt{1+h}+1}{\sqrt{1+h}+1} \\
%   \uncover<3->{&=\lim_{h\to 0}\frac{(\sqrt{1+h}-1)(\sqrt{1+h}+1)}{h(\sqrt{1+h}+1)}  \\
%   &=\lim_{h\to 0}\frac{(1+h)-1}{h(\sqrt{1+h}+1)}  \\
%   &=\lim_{h\to 0}\frac{h}{h(\sqrt{1+h}+1)}  \\
%   &=\lim_{h\to 0}\frac{1}{\sqrt{1+h}+1}=1/2}  
% \end{align*}
% \end{frame}




% \begin{frame}{Problems}
% Find the equation of the tangent line.

% \begin{enumerate}
% \item $f(x)=1/(x-2)$ at $a=1$
% \pause\item $f(x)=x^3$ at $a=2$
% \end{enumerate}
% \end{frame}


\begin{frame}{Patterns in the rate of change}
Fix the function $f(x)=x^2$.
Find the rate of change at these places.
\begin{enumerate}
\item $a=1$
\pause\item $a=2$
\pause\item $a=3$
\pause\item $a$
\end{enumerate}
\pause
Here is the last one.
\begin{equation*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
  =\lim_{h\to 0}\frac{(a^2+2ah+h^2)-(a^2)}{h}
\end{equation*}
\pause
\begin{equation*}
  =\lim_{h\to 0}\frac{2ah+h^2}{h}  
  =\lim_{h\to 0}\frac{h(2a+h)}{h}  
\end{equation*}
\pause
\begin{equation*}  
  =\lim_{h\to 0}2a+h=2a  
\end{equation*}
So we have a formula for the rate of change of~$f$ at $x=a$. 
It is $2a$.
This is the \alert{derivative} formula.
\end{frame}





% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
