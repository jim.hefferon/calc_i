\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.5:\ \ Substitution}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Every derivative rule is an antiderivative rule}

Recall the Chain Rule: where $F(x)=f\circ g\,(x)$, the derivative is
$F'(x)=f'(\,g(x)\,)\cdot g'(x)$.
Applied to find antiderivatives, this is  
the \alert{Substitution Method}.

\Tm Let $F$ be an antiderivative of $f\!$.
Where $u$ is a differentiable function whose range is some interval on
which~$f$ is continuous, this holds.
\begin{equation*}
  \int f(\,u(x)\,)\cdot u'(x)\,dx
  =F(u(x))+C
\end{equation*}

\medskip
We often write ``$u=\ldots$'' instead of ``$u(x)=\ldots$''  

\pause
The Chain Rule is how we differentiate when there is function composition.
In the context of integration it is called Substitution.
So a hint that this rule applies is the presence of composition.
\end{frame}


\section{For indefinite integrals}
\begin{frame}
\Ex Evaluate this indefinite integral.
\begin{equation*}
\int 2x\cdot \sqrt{1+x^2}\,dx  
\end{equation*}
\pause
A clue is that $\sqrt{1+2x^2}$ is a composition.
The $1+x^2$ is being square-rooted so do this.
\begin{align*}
  u=u(x)  &= 1+x^2  \\
  du/dx &= 2x  \\
  du    &= 2x\,dx 
\end{align*}
Substitution gives $\int u^{1/2}\,du$. 
Then antidifferentiation gives $(2/3)u^{3/2}+C$.
Translate back into $x$'s.
\begin{equation*}
  \int 2x\cdot \sqrt{1+x^2}\,dx
  =\frac{2}{3}(1+x^2)^{3/2}+C
\end{equation*}
Substitution allowed us to 
recognize that the starting integrand 
is the outcome of a Chain Rule applied
to $(1+x^2)^{3/2}$.
\end{frame}


\begin{frame}
\Ex $\displaystyle \int 2\cdot(2t+5)^3\,dt$

\pause
The $2t+5$ is being cubed.
\begin{align*}
  u             &=2t+5  \\
  \frac{du}{dt} &=2  \\
  du            &=2\,dt
\end{align*}
Substitution changes the integral to this.
\begin{equation*}
  \int (2t+5)^3\cdot 2\,dt
  =\int u^3\,du
  =\frac{1}{4}u^4+C
\end{equation*}
Put it back in terms of the starting variable $t$ to get 
$(1/4)(2t+5)^4+C$.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
% \item $\displaystyle \int (2x+1)^{19}\cdot 2\,dx$

% \pause
% Take $u=2x+1$ and then $du/dx=2$ so that $du=2\,dx$.
% The answer is $(2x+1)^{20}/20 +C$.

\item $\displaystyle \int e^{8x}\cdot 8\,dx$

\pause
The $8x$ is being exponentiated.
Take $u=8x$ to get $du/dx=8$, so that $du=8\,dx$.
\begin{equation*}
  \int e^u\,du = e^u+C
\end{equation*}
The answer is $e^{8x} +C$.

\item $\displaystyle \int \frac{3x^2}{x^3-1}\,dx$

\pause
The $x^3-1$ is begin one-overed.
Let $u=x^3-1$ so that $du/dx=3x^2\!$, giving $du=3x^2\,dx$.
\begin{equation*}
  \int \frac{1}{u}\,du = \ln|u|+C
\end{equation*}
The answer is $\ln|x^3-1| +C$.
\end{enumerate}  
\end{frame}


\section{Fooling with constant factors}
\begin{frame}
\Ex $\displaystyle \int \tan\theta \,d\theta$

\pause
First~rewrite.
\begin{equation*}
  \int \frac{\sin\theta}{\cos\theta}\,d\theta
  =\int \frac{1}{\cos\theta}\cdot \sin\theta\,d\theta
\end{equation*}
The $\cos\theta$ is being one-overed.
Let $u=\cos\theta$ so $du/d\theta=-\sin\theta$, giving  
$du=-\sin\theta\,d\theta$.
\pause
Write that as $-du=\sin\theta\,d\theta$.
\begin{align*}
  &=\int \frac{1}{u}\cdot(-1)\,du        \\
  &=-\int \frac{1}{u}\,du        \\
  &=-\ln|u|+C =-\ln|\cos\theta|+C
\end{align*}
(A rule of logarithms is that the negative of a log is the log of the
reciprocal.
So we can write the answer as 
$\ln\bigl|1/\cos\theta\bigr|+C=\ln\bigl|\sec\theta\bigr|+C$.)
\end{frame}


\begin{frame}
\Ex $\displaystyle \int e^{10x}\,dx$

\pause
Let $u=10x$.
Then $du/dx=10$ and so $dx=(1/10)du$.
\begin{equation*}
  \int e^{10x}\,dx
  =\int e^u\frac{1}{10}\,du
  =\frac{1}{10}\int e^u\,du
  =\frac{1}{10}\bigl[e^u+C\bigr]
  =\frac{e^{10x}}{10}+C  
\end{equation*}
Note that we rewrote $(1/10)\cdot C$ as $C$.
One tenth of an unknown constant is an unknown constant.

\pause
\Ex $\displaystyle \int 3\sin(3-6x)\,dx$

\pause
Let $u=3-6x$ so that $du=-6\,dx$.
Since what we want is $3\,dx$, rewrite as $(-1/2)\,du=3\,dx$.
\begin{align*}
  \int 3\sin(3-6x)\,dx
  &=\int \sin(u)\cdot(-1/2)\,du  \\
  &=\frac{-1}{2}\int \sin(u)\,du \\
  &=\frac{-1}{2}\bigl(-\cos(u)\bigr)+C     
  % &=\frac{\cos u}{2}+C     
  =\frac{\cos(3-6x)}{2}+C
\end{align*}
\end{frame}




\begin{frame}{Practice}
Use substitution.
\begin{enumerate}
\item $\displaystyle \int xe^{-x^2}\,dx$ 

\pause
Take $u=-x^2$ so $du=-2x\,dx$.
The integral becomes $\int e^u\cdot(-1/2)\,du=-(1/2)\int e^u\,du$,
which gives $-(1/2)e^u+C=(-1/2)e^{-x^2}+C$.

\item $\displaystyle \int (5-3x)^{10}\,dx$ 

\pause
Use $u=5-3x$ so $du=-3\,dx$.
Then the integral is 
$\int u^{10}\cdot(-1/3)\,du=-(1/3)\int u^{10}\,du$.
The answer is $-(1/3)\cdot(1/11)u^{11}+C=-(1/33)\cdot(5-3x)^{11}+C$.

% \item $\displaystyle \int \frac{2z^2}{z^3+3}\,dz$  

% \pause
% Take $u=z^3+3$, making $du=3z^2\,dz$ and $(2/3)du=2z^2\,dz$.
% The integral is 
% $\int (1/u)\cdot (2/3)\,du=(2/3)\int (1/u)\,du$.
% The answer is $(2/3)\ln|u|+C=(2/3)\cdot\ln|z^3+3|+C$.
\end{enumerate}\end{frame}
% \begin{frame}\begin{enumerate}\setcounter{enumi}{3}
% \item $\displaystyle \int (3t-1)^{50}\,dt$ 

% \pause
% Take $u=3t-1$, making $du=3\,dt$.
% The integral is 
% $\int u^{50}(1/3)\,du$, giving
% $(1/3)(1/51)u^{51}+C=(1/153)\cdot (3t-1)^{51}+C$.

% \item $\displaystyle \int \frac{x}{x^2+1}\,dx$

% \pause
% Use $u=x^2+1$ and so $du=2x\,dx$.
% The integral is 
% $\int (1/u)\cdot (1/2)\,du$, which gives
% $(1/2)\ln(u)+C=(1/2)\cdot \ln(x^2+1)+C$.

% \item  $\displaystyle \int e^{x^3}(9x^2)\,dx$

% \pause
% Use $u=x^3$, giving $du=3x^2\,dx$.
% Then the integral is 
% $3\cdot \int e^u\cdot \,du$, which gives
% $3e^u+C=3\cdot e^{x^3}+C$.
 
% \end{enumerate}
% \end{frame}





\section{Definite integrals}
\begin{frame}{Two possible paths}
To use substitution with a definite integral, there are two ways to go.

\Ex 
$\displaystyle \int_{x=0}^2e^{-3x}\,dx$

Take $u=-3x$ with $du=-3\,dx$, so $(-1/3)\,du=dx$.
\pause
The first way is to
do the indefinite integral,
\begin{equation*}
  \int e^{-3x}\,dx=\int e^u\cdot \frac{-1}{3}\,du
         =\frac{-1}{3}\cdot\int e^u\,du=\frac{-1}{3}\cdot e^{-3x}+C
\end{equation*}
giving this for the definite integral.
\begin{equation*}
  \int_{x=0}^2e^{-3x}\,dx=\biggl[ \frac{-1}{3}\cdot e^{-3x}\biggr]_{x=0}^2
    =\frac{-1}{3}\cdot(e^{-6}-1)
\end{equation*}
\pause
The second way is to change the limits of integration.
\begin{multline*}
  \int_{x=0}^2 e^{-3x}\,dx=\int_{u=0}^{-6} e^u\cdot \frac{-1}{3}\,du
         =\frac{-1}{3}\cdot \int_{u=0}^{-6} e^u\,du  \\   
         =\frac{-1}{3}\cdot \biggl[ e^{u}\biggr]_{u=0}^{-6}
        =\frac{-1}{3}\cdot(e^{-6}-1)
\end{multline*}
\end{frame}

\begin{frame}
This describes the second way, which is how we will usually work.

\Tm
$\displaystyle \int_{x=a}^bf(u(x))\cdot u'(x)\,dx
  =\int_{u=u(a)}^{u(b)}f(u)\,du $ 

\Ex $\displaystyle \int_{x=0}^2x^2\sqrt{x^3+1}\,dx$

\pause
Take $u=x^3+1$ so that $du=3x^2\,dx$.
As $x$ varies from $0$ to~$2$, we get that $u$ varies from $1$ to~$9$.
\begin{multline*}
  \int_{x=0}^2x^2\sqrt{x^3+1}\,dx
  =\int_{u=1}^9\sqrt{u}\cdot\frac{1}{3}\,du
  =\frac{1}{3}\int_{u=1}^9u^{1/2}\,du               \\
  =\biggl[ \frac{1}{3}\cdot\frac{2}{3}u^{3/2} \biggr]_{u=1}^{9}
  =\frac{52}{9}
\end{multline*}
\end{frame}


\begin{frame}{Practice}
Evaluate.
\begin{enumerate}
\item $\displaystyle \int_{t=0}^1 (3t-1)^{50}\,dt$ 

\pause
Take $u=3t-1$ so $du=3\,dt$.
That leads to $\int_{u=-1}^{2}u^{50}(1/3)\,du$.
The Fundamental Theorem gives
$(1/3)[u^{51}/51]_{-1}^2$.

\item $\displaystyle \int_0^4 \frac{x}{x^2+1}\,dx$

\pause
With $u=x^2+1$ we have $du=2x\,dx$, giving
$\int_{u=1}^{17}u^{-1}(1/2)\,dx$.
Take the antiderivative to get
$(1/2)[\ln|u|]_{1}^{17}$.
\end{enumerate}

  
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
