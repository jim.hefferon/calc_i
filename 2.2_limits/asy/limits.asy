// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "limits%03d";

import graph;

// void draw_graphpaper(picture pic, real xleft, real xright, real ytop, real ybot) {
//   xaxis(pic,axis=YEquals(ytop+0.2),
// 	xmin=xleft-0.5, xmax=xright+0.5,
// 	p=nullpen,
// 	ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
//   xaxis(pic,axis=YEquals(ybot-0.2),
// 	xmin=xleft-0.5, xmax=xright+0.5,
// 	p=nullpen,
// 	ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
//   yaxis(pic,axis=XEquals(xleft-0.2),
// 	ymin=ybot-0.5, ymax=ytop+0.5,
// 	p=nullpen,
// 	ticks=LeftTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
//   yaxis(pic,axis=XEquals(xright+0.2),
// 	ymin=ybot-0.5, ymax=ytop+0.5,
// 	p=nullpen,
// 	ticks=LeftTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));  
// }


// ================ 5-(1/2)*(x-3)^2 =======
real f0(real x) {return 5-(1/2)*(x-3)^2;}
pair anchor_pt = (1,3);
real[] delta_x = {1, 1, 0.1, 0.01};

for(int i; i<=3; ++i) {
  picture pic;
  int picnum = 0+i;
  size(pic,5cm,0,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=5;

  path f = graph(f0, -0.1, xright+0.1);
  draw(pic, f, FCNPEN);

  if (i>0) { 
    real remote_x = anchor_pt.x+delta_x[i];
    pair remote_pt = (remote_x, f0(remote_x));
    real secant_line(real x) {
      return ((remote_pt.y-anchor_pt.y)/(remote_x-anchor_pt.x))*(x-anchor_pt.x)+anchor_pt.y;
    }
    draw(pic, graph(secant_line,-0.10,2.15), FCNPEN_NOCOLOR+highlight_color);
    filldraw(pic, circle(remote_pt,0.05), white, black);
    label(pic,  format("$(a+%f,",delta_x[i])+format("f(a+%f))$",delta_x[i]), remote_pt, 3*E, filltype=Fill(white));
  }
  // put in anchor point
  filldraw(pic, circle(anchor_pt,0.05), white, black);
  label(pic,  "$(1,3)$", anchor_pt, 2*W, filltype=Fill(white));
  
// Axes making graph paper
// pen GRAPHPAPERPEN=(0.25*light_color+0.75*white)
//   +squarecap; // For graph paper lines
  draw_graphpaper(pic, xleft, xright, ytop, ybot);
  xaxis(pic, L="",  // label
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+.75,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  yaxis(pic, L="",  // label
	axis=XZero,
	ymin=ybot-0.75, ymax=ytop+0.75,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}

// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(3)
// 5
// sage: f(1)
// 3
// sage: f(2)
// 9/2




// ================ Initial example of limits =======
real f4(real x) {return x^2;}

picture pic;
int picnum = 4;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=6;

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
path f = graph(f4, xleft-0.1, 2.5);
draw(pic, f, FCNPEN);
// sage: n(sqrt(6))
// 2.44948974278318

filldraw(pic, circle((2,4),0.05), white, black);
label(pic,  "$(2,4)$", (2,4), SE, filltype=Fill(white));

draw_graphpaper(pic, xleft, xright, ytop, ybot);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ......................
picture pic;
int picnum = 5;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=6;

path f = graph(f4,xleft-0.1,2.5);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,f4(2)),0.05));
label(pic, "$(a,L)$", (2,f4(2)), W);

// tolerances
draw(pic, (1.732,0)--(1.732,3),dashed+highlight_color);
  draw(pic, (1.732,3)--(0,3),dashed+highlight_color);
draw(pic, (2.236,0)--(2.236,5),dashed+highlight_color);
  draw(pic, (2.236,5)--(0,5),dashed+highlight_color);

// draw(pic, Label("Out tolerance",align=Center, filltype=Fill(white)),
//      (-0.2,2.7)--(-0.2,5.3),
//      black,Arrows,
//      Bars);
draw(pic,(-0.2,5.8)--(-0.2,5.3),Arrow(ARROWSIZE),Bar);
draw(pic,(-0.2,2.2)--(-0.2,2.7),Arrow(ARROWSIZE),Bar);
label(pic,"\begin{tabular}{c}Out tolerance\\$\varepsilon$\end{tabular}",
          (-0.2,4.0), W);
// draw(pic, Label("In tolerance",align=S, filltype=UnFill),
//      (1.732,-0.2)--(2.236,-.2),
//      black,Arrows,
//      Bars);
draw(pic,(1.2,-0.2)--(1.732,-0.2),Arrow(ARROWSIZE),Bar);
draw(pic,(2.7,-0.2)--(2.236,-0.2),Arrow(ARROWSIZE),Bar);
label(pic,"In tolerance $\delta$", (2,-0.2), 3*S);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Second example of limits =======
real f1(real x) {return sqrt(x+7);}

picture pic;
int picnum = 6;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=4;

// Draw graph paper
draw_graphpaper(pic, xleft, xright, ytop, ybot);

// Label L_fcn = Label("$f(x)=\sqrt{x+7}$", align=3*N, position=Relative(0.2),
// 		    highlight_color,
// 		    filltype=Fill(white));
path f = graph(f1,xleft-0.1,xright+0.2);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,3),0.05));
label(pic,  "$(2,3)$", (2,3), SE);

xaxis(pic, L="", 
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="", 
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ One-sided limits =======
real f7(real x) {return sqrt(x-2);}

picture pic;
int picnum = 7
  ;
size(pic,4.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f7,2,xright+0.2);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,0),0.075));
label(pic,  "$(2,0)$", (2,0), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Vertical asymptotes =======
real f3(real x) {return 1/(x-2)^2;}

picture pic;
int picnum = 8;
size(pic,0,6cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=10;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)=\sqrt{x+7}$", align=3*N, position=Relative(0.2),
// 		    highlight_color,
// 		    filltype=Fill(white));
draw(pic, graph(f3,xleft-0.1,1.684), // L=L_fcn,
     highlight_color);
draw(pic, graph(f3,2.316,xright+0.1), // L=L_fcn,
     highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .....
real f4(real x) {return 1/(x-2);}

picture pic;
int picnum = 9;
size(pic,0,6cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-5; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f4,xleft-0.1,1.80), // L=L_fcn,
     highlight_color);
draw(pic, graph(f4,2.20,xright+0.1), // L=L_fcn,
     highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... tan x ...
real f10(real x) {return tan(x);}

picture pic;
int picnum = 10;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4.25;
ybot=-5; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f10,xleft-0.1,(pi/2)-0.2), FCNPEN);
draw(pic, graph(f10,(pi/2)+0.2,xright+0.1), FCNPEN);

draw(pic, (pi/2,ybot-0.1)--(pi/2,ytop+0.1),dashed+highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .... greatest integer ...
real f11(real x) {return tan(x);}

picture pic;
int picnum = 11;
size(pic,4.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

filldraw(pic, circle((0,0),0.05));
draw(pic,(0,0)--(0.95,0), FCNPEN);
  filldraw(pic, circle((1,0),0.05), white, black);
filldraw(pic, circle((1,1),0.05));
draw(pic,(1,1)--(1.95,1), FCNPEN);
  filldraw(pic, circle((2,1),0.05), white, black);
filldraw(pic, circle((2,2),0.05));
draw(pic,(2,2)--(2.95,2), FCNPEN);
filldraw(pic, circle((3,2),0.05), white, black);
filldraw(pic, circle((3,3),0.05));
draw(pic,(3,3)--(xright+0.25,3), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Second example of limits =======
real f12(real x) {return (x)/(3*(x-2));}

picture pic;
int picnum = 12;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-3; ytop=4;

// Draw graph paper
draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f12,xleft-0.3,2-0.20), FCNPEN);
draw(pic, graph(f12,2+0.18,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ sin x / x =======
real f13(real x) {return sin(x)/x;}

picture pic;
int picnum = 13;
size(pic,0,3.25cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f13,xleft-0.25,0-.01), FCNPEN);
draw(pic, graph(f13,0+0.01,xright+0.25), FCNPEN);

filldraw(pic, circle((0,1),0.05), white, black);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Random graph =======
picture pic;
int picnum = 14;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-2; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic,(xleft-0.2,1)--(1,1), FCNPEN);
filldraw(pic, circle((1,1),0.05), white, black);
// label(pic,  "$(1,1)$", (1,1), NW);
filldraw(pic, circle((1,2),0.05));
draw(pic,(1,2)..{right}(2,3)::{down}(2.9,-2), FCNPEN);
draw(pic,(3.1,-2){up}::(5,4), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ y=x^2 precise defn of limit =======
real f15(real x) {return x^2;}

picture pic;
int picnum = 15;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

// Draw graph paper
draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f15,xleft-0.1,xright+.01);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,4),0.1));
label(pic, "$(2,4)$", (2,4), 2*E, filltype=Fill(white));

draw(pic, Label("$\varepsilon=0.75$",align=2*W, filltype=Fill(white)),
     (-0.2,3.25)--(-0.2,4.75),
     black,Arrows,
     Bars);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .....................................
picture pic;
int picnum = 16;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f15,xleft-0.1,xright+.01);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,4),0.1));

draw(pic, (1.85,0)--(1.85,3.4225)--(0,3.4225), highlight_color+dashed);
draw(pic, (2.15,0)--(2.15,4.6225)--(0,4.6225), highlight_color+dashed);

draw(pic, Label("$\varepsilon=0.75$",align=2*W, filltype=Fill(white)),
     (-0.2,3.25)--(-0.2,4.75),
     black,Arrows,
     Bars);
draw(pic, Label("$\delta=0.15$",align=1.5*S, filltype=UnFill),
     (1.85,-0.2)--(2.15,-.2),
     black,Arrows,
     Bars);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Limit of infinity =======
real f13(real x) {return (x)/(3*(x-2)^2);}

picture pic;
int picnum = 17;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=4;
ybot=-1; ytop=6;

// Draw graph paper
draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f13,xleft-0.3,1.693), FCNPEN);
draw(pic, graph(f13,2.362,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
// sage: round(-1/36*sqrt(145)+73/36, ndigits=3)
// 1.693
// sage: round(1/36*sqrt(145)+73/36, ndigits=3)
// 2.362
// sage: solve(6==x/(3*(x-2)^2), x)
// [x == -1/36*sqrt(145) + 73/36, x == 1/36*sqrt(145) + 73/36]





// ================ Hole in the graph =======
real f18(real x) {return (x^2-4)/(x-2);}

picture pic;
int picnum = 18;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=6;

// Draw graph paper
draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f18,xleft-0.2,1.99), FCNPEN);
draw(pic, graph(f18,2.01,xright+0.2), FCNPEN);

filldraw(pic, circle((2,4),0.075), white, black);
label(pic, "$(2,4)$", (2,4), 2*SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ======== More from precise definition of a limit =======
real f19(real x) {  return x^2; }

picture pic;
int picnum = 19;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=6;

path f = graph(f19,xleft-0.1,2.5);
draw(pic, f, FCNPEN);

filldraw(pic, circle((2,f19(2)),0.05));
label(pic, "$(2,4)$", (2,f19(2)), W);

// tolerances
draw(pic, (1.732,0)--(1.732,3),dashed+highlight_color);
  draw(pic, (1.732,3)--(0,3),dashed+highlight_color);
draw(pic, (2.236,0)--(2.236,5),dashed+highlight_color);
  draw(pic, (2.236,5)--(0,5),dashed+highlight_color);

draw(pic,(-0.2,5.5)--(-0.2,5),Arrow(ARROWSIZE),Bar);
label(pic,"$4+\varepsilon$",(-0.2,5), 2*W);
draw(pic,(-0.2,2.5)--(-0.2,3),Arrow(ARROWSIZE),Bar);
label(pic,"$4-\varepsilon$", (-0.2,3), 2*W);

// Input tolerance
draw(pic,(1.2,-0.2)--(1.732,-0.2),Arrow(ARROWSIZE),Bar);
  label(pic,"\makebox[0pt][r]{$\sqrt{4-\varepsilon}$}", (1.732,-0.2), 3*S);
draw(pic,(2.7,-0.2)--(2.236,-0.2),Arrow(ARROWSIZE),Bar);
  label(pic,"\makebox[0pt][l]{$\sqrt{4+\varepsilon}$}", (2.236,-0.2), 3*S);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
