\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.2\ Limits}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Limits, intuitively}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Motivation: slope of a curve}
\begin{center}
  \only<1>{\includegraphics{asy/limits000.pdf}}%    
  \only<2>{\includegraphics{asy/limits001.pdf}}%    
  \only<3>{\includegraphics{asy/limits002.pdf}}%    
  \only<4->{\includegraphics{asy/limits003.pdf}}%    
% \end{center}
% \begin{center}
  \\
  \begin{tabular}{ccc|c}
    \multicolumn{2}{c}{\textit{Inputs}}
    \\ 
      \multicolumn{1}{c}{$\Delta x$}
      &\multicolumn{1}{c}{$x=1+\Delta x$}
      &\multicolumn{1}{c}{$x=1$}
      &\multicolumn{1}{c}{$\operatorname{slope}(\Delta x)=\Delta y/\Delta x$} \\ \hline
      \uncover<2->{$1$}    &\uncover<2->{$2$} &\uncover<2->{$1$}       
          &\uncover<2->{$(4.5-3)/(2-1)=1.5$} \\
      \uncover<3->{$0.1$}  &\uncover<3->{$1.1$} &\uncover<3->{$1$}     
          &\uncover<3->{$(3.195-3)/(1.1-1)=1.95$} \\
      \uncover<4->{$0.01$} &\uncover<4->{$1.01$} &\uncover<4->{$1$}    
          &\uncover<4->{$(3.0019995-3)(1.01-1)=1.999499$} \\
    % $1.001$ &$1$   &\uncover<4->{$(3.000199995-3)(1.001-1)=1.999950$} \\
    \uncover<4->{$0$} &\uncover<4->{$1$} &\uncover<4->{$1$}       
         &\uncover<4->{? $2$ ?}
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
The table's final line is fuzzy because  at $\Delta x=0$ the slope formula
gives nonsense, since it has a division by zero.
\begin{equation*}
  \frac{\Delta y}{\Delta x}
  =\frac{y_2-y_1}{x_2-x_1}
  =\frac{f(1+\Delta x)-f(1)}{(1+\Delta x)-1}
\end{equation*}
Nonetheless, it sure looks like the answer should be~$2$.
And if you take out a ruler and measure the slope then you get~$2$.

\pause
\fbox{
\parbox{\textwidth}{\color{red}
The main technical problem of this course is to make sense of,
``to find the value of this expression at some fixed number $c$
\begin{equation*}
  \frac{f(c+\Delta x)-f(c)}{(c+\Delta x)-c}
\end{equation*}
use the trend as $\Delta x$ gets close to zero.''}}
\end{frame}


\begin{frame}{Limit definition}
It turns out that the ``trend'' idea is very powerful.
We will use it in many ways.
So the following definition isn't limited to the 
rate of change $\operatorname{slope}$ function.   

\Df
Given a function~$\map{f}{\R}{\R}$ and an anchor point~$a$,
\alert{the limit of $f$ as $x$ approaches $a$ is $L$} if whenever
input $x$'s are near the number~$a$ then the output $f(x)$'s are near~$L$.
\begin{equation*}
  \alert{\lim_{x\to a}f(x)=L}
  \quad\text{or}\quad
  \alert{\textstyle\lim_{x\to a}f(x)=L}  
  \quad\text{or}\quad
  \text{\alert{as $x\to a$ then $f(x)\to L$}}
\end{equation*}
Restated, as the input difference $|x-a|$ becomes zero, 
the resulting output difference $|f(x)-L|$ also becomes zero. 
\end{frame}

\begin{frame}
Our motivating example used this function.
\begin{equation*}
  \operatorname{slope}(\Delta x)
  =\frac{f(2+\Delta x)-f(2)}{(2+\Delta x)-2}
  \qquad
  \text{where $\displaystyle f(x)=5-\frac{(x-3)^2}{2}$}
\end{equation*}
We got these numbers.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Input $\Delta x$}}
      &\multicolumn{1}{c}{\textit{Output $\operatorname{slope}(\Delta x)$}} \\ \hline
      $-0.1$     &$2.05000\ldots$ \\
      $-0.01$    &$2.00499\ldots$ \\
      $-0.001$   &$2.00049\ldots$ \\
      $0.$       &\textit{--Undefined--} \\ 
      $0.001$    &$1.99949\ldots$ \\
      $0.01$     &$1.99499\ldots$ \\
      $0.1$      &$1.95000\ldots$ \\
  \end{tabular}
\end{center}
We write 
$\lim_{\Delta x\to 0}\operatorname{slope}(\Delta x)=2$.

\pause
Again: the definition does not plug zero in for $\Delta x$.
It looks at the trend.
\end{frame}
% sage: def f(x):
% ....:     return 5-( ((x-3)^2)/(2) )
% ....: 
% sage: def slope(delta_x):
% ....:     return( (f(1+delta_x)-f(1))/delta_x )
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("delta_x=",-i," slope(delta_x)=", slope(-i) )
% ....: 
% delta_x= -0.100000000000000  slope(delta_x)= 2.05000000000000
% delta_x= -0.0100000000000000  slope(delta_x)= 2.00499999999995
% delta_x= -0.00100000000000000  slope(delta_x)= 2.00049999999985
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("delta_x=",i," slope(delta_x)=", slope(i) )
% ....: 
% delta_x= 0.100000000000000  slope(delta_x)= 1.95000000000000
% delta_x= 0.0100000000000000  slope(delta_x)= 1.99499999999997
% delta_x= 0.00100000000000000  slope(delta_x)= 1.99949999999971


\section{Catalog of what can happen with limits}

\begin{frame}{If there is nothing happening then the limit is the natural value}
Let $f(x)=\sqrt{x+7}$ and look near the anchor $a=2$.
Near there the expression $\sqrt{x+7}$ has no divided by zero, 
or square root of a negative, or any other pathology.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits006.pdf}}  
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $1.900$ &\uncover<2->{$2.98328\ldots$}  \\
    $1.990$ &\uncover<2->{$2.99833\ldots$}  \\
    $1.999$ &\uncover<2->{$2.99983\ldots$}  \\
    $2.$    &\uncover<2->{$3.$}  \\
    $2.001$ &\uncover<2->{$3.00016\ldots$}  \\
    $2.010$ &\uncover<2->{$3.00166\ldots$}  \\
    $2.100$ &\uncover<2->{$3.01662\ldots$}  \\
  \end{tabular}
\end{center}
% sage: for i in [0, 0.1, 0.01, 0.001]:
% ....:     print("x=",2+i," f(x)=",sqrt(2+i+7))
% ....: 
% x= 2  f(x)= 3
% x= 2.10000000000000  f(x)= 3.01662062579967
% x= 2.01000000000000  f(x)= 3.00166620396073
% x= 2.00100000000000  f(x)= 3.00016666203729
% sage: for i in [0, 0.1, 0.01, 0.001]:
% ....:     print("x=",2-i," f(x)=",sqrt(2-i+7))
% ....: 
% ....: 
% x= 2  f(x)= 3
% x= 1.90000000000000  f(x)= 2.98328677803526
% x= 1.99000000000000  f(x)= 2.99833287011299
% x= 1.99900000000000  f(x)= 2.99983332870345
\uncover<2->{We write $\lim_{x\to 2}f(x)=\lim_{x\to 2}\sqrt{x+7}=3$.} 
\end{frame}


\begin{frame}{If there is an obvious answer, the limit is that
answer}
Consider $g(t)=(t^2-4)/(t-2)$ near $a=2$.
As with the $\operatorname{slope}$ function, 
$g$ is not defined at the anchor number.
Notice that $g(t)=(t-2)(t+2)/(t-2)$ and so $g(t)=t+2$ everwhere except
at $t=2$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits018.pdf}}  
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$t$} &\multicolumn{1}{c}{$g(t)$}  \\
    \hline
    $1.9$ &\uncover<2->{$3.90000 \ldots$}  \\
    $1.99$ &\uncover<2->{$3.98999 \ldots$}  \\
    $1.999$ &\uncover<2->{$3.99899 \ldots$}  \\
    $2.$   &\text{\textit{--Undefined--}}  \\
    $2.001$ &\uncover<2->{$4.00100 \ldots$}  \\
    $2.01$ &\uncover<2->{$4.00999 \ldots$}  \\
    $2.1$ &\uncover<2->{$4.10000 \ldots$}  \\
  \end{tabular}
\end{center}
\only<2->{So $\lim_{t\to 2}g(t)=4$.
We don't directly plug in the anchor, we use the trend near it.}
\end{frame}
% sage: def g(t):
% ....:     return (t^2-4)/(t-2)
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("t=",2+i," g(t)=", g(2+i) )
% ....: 
% t= 2.10000000000000  g(t)= 4.10000000000000
% t= 2.01000000000000  g(t)= 4.00999999999998
% t= 2.00100000000000  g(t)= 4.00100000000014
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("t=",2-i," g(t)=", g(2-i) )
% ....: 
% ....: 
% t= 1.90000000000000  g(t)= 3.90000000000000
% t= 1.99000000000000  g(t)= 3.98999999999998
% t= 1.99900000000000  g(t)= 3.99899999999986


\begin{frame}
\Ex
Let $f(x)=\sin(x)/x$ and find $\lim_{x\to 0}f(x)$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits013.pdf}}
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $-0.100$ &\uncover<2->{$0.998\ldots$}  \\
    $-0.010$ &\uncover<2->{$0.999\ldots$}  \\
    $-0.001$ &\uncover<2->{$1.000\ldots$}  \\
    $0.$     &\uncover<2->{\textit{--Undefined--}}  \\
    $0.001$ &\uncover<2->{$1.000\ldots$}  \\
    $0.010$ &\uncover<2->{$0.999\ldots$}  \\
    $0.100$ &\uncover<2->{$0.998\ldots$}  \\
  \end{tabular}
\end{center}
\uncover<2->{Write $\displaystyle \lim_{x\to 0}\frac{\sin x}{x}=1$.}
\end{frame}



\begin{frame}{A limit can be infinite}
  Consider
    $\displaystyle f(x)=\frac{x}{3(x-2)^2}$ near the anchor point $a=2$.
  \begin{center}
  \vcenteredhbox{\includegraphics{asy/limits017.pdf}}
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $2.100$ &\uncover<2>{$69.99999\ldots$}  \\
    $2.010$ &\uncover<2>{$6700.00000\ldots$}  \\
    $2.001$ &\uncover<2>{$667000.00000\ldots$}  \\
    $2.$    &\textit{--Undefined--}  \\
    $1.999$ &\uncover<2>{$666333.33333\ldots$}  \\
    $1.990$ &\uncover<2>{$6633.33333\ldots$}  \\
    $1.900$ &\uncover<2>{$63.33333\ldots$}  \\
  \end{tabular}
\end{center}
\uncover<2->{We have
  $\displaystyle \lim_{x\to 2}\frac{x}{3(x-2)^2}=\infty$.
}
\end{frame}
% sage: def f(x):
% ....:     return x/(3*(x-2)^2)
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print(2+i, f(2+i))
% ....: 
% 2.10000000000000 69.9999999999999
% 2.01000000000000 6700.00000000028
% 2.00100000000000 667000.000000147
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print(2-i, f(2-i))
% ....: 
% ....: 
% 1.90000000000000 63.3333333333332
% 1.99000000000000 6633.33333333332
% 1.99900000000000 666333.333333480



\begin{frame}{A limit can fail to exist}
Let $f(x)=x/(3(x-2))$ and find $\lim_{x\to 2}f(x)$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits012.pdf}}
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $1.900$ &\uncover<2>{$-6.33333\ldots$}  \\
    $1.990$ &\uncover<2>{$-66.33333\ldots$}  \\
    $1.999$ &\uncover<2>{$-666.33333\ldots$}  \\
    $2.$    &\textit{--Undefined--} \\
    $2.001$ &\uncover<2>{$667.00000\ldots$}  \\
    $2.010$ &\uncover<2>{$67.00000\ldots$}  \\
    $2.001$ &\uncover<2>{$6.99999\ldots$}  \\
  \end{tabular}
\end{center}
\uncover<2->{
We say that 
  $\displaystyle \lim_{x\to 2}\frac{x}{3(x-2)}$
does not exist (DNE).
}
\end{frame}
% sage: def f(x):
% ....:     return( x/(3*(x-2)) )
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("x=",2-i," f(x)=", f(2-i) )
% ....: 
% x= 1.90000000000000  f(x)= -6.33333333333333
% x= 1.99000000000000  f(x)= -66.3333333333333
% x= 1.99900000000000  f(x)= -666.333333333407
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("x=",2+i," f(x)=", f(2+i) )
% ....: 
% ....: 
% x= 2.10000000000000  f(x)= 6.99999999999999
% x= 2.01000000000000  f(x)= 67.0000000000014
% x= 2.00100000000000  f(x)= 667.000000000073



\begin{frame}
  \frametitle{One-sided limits}
This function $f(x)=\sqrt{x-2}$ has no value for inputs less than~$a=2$.
Consequently, the limit at $2$ does not exist.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits007.pdf}}
  \qquad
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    % $2.$    &\only<2->{$0.$}  \\
    $2.001$ &\uncover<2->{$0.03162\ldots$}  \\
    $2.010$ &\uncover<2->{$0.09999\ldots$}  \\
    $2.100$ &\uncover<2->{$0.03162\ldots$}  \\
  \end{tabular}
\end{center}
\only<3->{
The \alert{one-sided limit from above of a function $f$ at~$a$ is $L$}
\begin{equation*}
  \alert{\lim_{x\to a^+}f(x)=L}
\end{equation*}
if as inputs~$x$ approach~$a$ from above then the output values $f(x)$
approach~$L$.}
\only<4->{
The \alert{one-sided limit from below $\lim_{x\to a^-}f(x)$ is $L$} if as~$x$
approaches~$a$ from below, the values $f(x)$ approach~$L$.
\begin{equation*}
  \text{$\lim_{x\to 2^-} \sqrt{x-2}$ \ does not exist}
  \qquad
  \lim_{x\to 2^+} \sqrt{x-2}=0
\end{equation*}
} 
\end{frame}
% sage: def f(x):
% ....:     return( sqrt(x-2) )
% ....: 
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("x=",2+i," f(x)=", f(2+i) )
% ....: 
% x= 2.10000000000000  f(x)= 0.316227766016838
% x= 2.01000000000000  f(x)= 0.0999999999999989
% x= 2.00100000000000  f(x)= 0.0316227766016820


\begin{frame}{Vertical asymptote}
A \alert{vertical asymptote} is a line $x=a$ satisfying any of these
\begin{equation*}
  \lim_{x\to a}f(x)=\infty
  \qquad
  \lim_{x\to a^-}f(x)=\infty
  \qquad
  \lim_{x\to a^+}f(x)=\infty
\end{equation*}
or any of these.
\begin{equation*}
  \lim_{x\to a}f(x)=-\infty
  \qquad
  \lim_{x\to a^-}f(x)=-\infty
  \qquad
  \lim_{x\to a^+}f(x)=-\infty
\end{equation*}
\pause
\begin{center}
  $\displaystyle f(x)=\tan x=\frac{\sin x}{\cos x}$
  \qquad
  \vcenteredhbox{\includegraphics{asy/limits010.pdf}}
  \qquad
  asymptote $x=\pi/2$
  \qquad
\end{center}
\end{frame}



\begin{frame}{Relationship between one-sided limits and the two-sided limit}
  We can take one-sided limits even when the function is defined on both sides.
  The two-sided limit exists and equals~$L$
  if and only if both one-sided limits exist, and both equal~$L$.
\end{frame}


\begin{frame}{Another way that limits may fail to exist}
  This is the \alert{greatest integer} function $g(x)$, whose value is the
  greatest integer less than or equal to~$x$.
  For instance, $g(1.25)=1$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits011.pdf}}
\end{center}
Notice that $\lim_{x\to 2}g(x)$ does not exist.
There is a limit from the left and one from the right,
but they  don't agree:
$\lim_{x\to 2^-}g(x)=1$
but
$\lim_{x\to 2^+}g(x)=2$.
\end{frame}



\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits014.pdf}}
\end{center}
For each $a$, give $\lim_{x\to a^-}f(x)$,  
$\lim_{x\to a^+}f(x)$,
and
$\lim_{x\to a}f(x)$.
\begin{enumerate}
\item $a=0$,
\pause\item $a=1$,
\pause\item $a=2$,
\pause\item $a=3$,
\pause\item $a=4$,
\pause\item $a=5$.
\end{enumerate}
Any vertical asymptotes?
\end{frame}


\begin{frame}
Determine each infinite limit.
\begin{enumerate}
\item $\displaystyle\lim_{x\to 5^-}\frac{x+1}{x-5}$
\pause\item $\displaystyle\lim_{x\to 3^-}\frac{\sqrt{x}}{(x-3)^5}$
\end{enumerate}
\end{frame}


\section{Precise definition}

\begin{frame}
Let $f$ be defined for $x$'s near to the anchor number~$a$.
Then \alert{the function's limit as $x\to a$ is $L$} 
\begin{equation*}
  \alert{\lim_{x\to a}f(x)=L}
\end{equation*}
when:
\begin{itemize}
\item
   for any required tolerance in the outputs 
\item
  we can find an input tolerance so that $x$ being within the input tolerance
of~$a$ forces $f(x)$ to be within the output tolerance of~$L$.
\end{itemize}
The next slide illustrates.
\end{frame}

\begin{frame}{It is a game}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits005.pdf}}
  \quad
  \begin{minipage}{2in}\RaggedRight
We are challenged with an output tolerance on the left, 
traditionally called $\varepsilon$.
We must respond with an input tolerance, 
traditionally called $\delta$, 
ensuring that if $x$ is within $\delta$ of~$a$
then $f(x)$ is within $\varepsilon$ of~$L$.
  \end{minipage}    
\end{center}
\end{frame}


\begin{frame}[fragile]
\lstinputlisting{bin/epsilondelta.sage}
\end{frame}
\begin{frame}[fragile]
\begin{lstlisting}
jim@millstone:~/teach/calc_i/2.2_limits/bin$ sage
┌────────────────────────────────────────────────────────────────────┐
│ SageMath version 9.5, Release Date: 2022-01-30                     │
│ Using Python 3.10.12. Type "help()" for help.                      │
└────────────────────────────────────────────────────────────────────┘
sage: attach("epsilondelta.sage")
sage: def sqr(x):
....:     return x^2
....: 
sage: epsdel(sqr, 2, 0.75, 0.5)
That delta is not small enough.
sage: epsdel(sqr, 2, 0.75, 0.4)
That delta is not small enough.
sage: epsdel(sqr, 2, 0.75, 0.25)
That delta is not small enough.
sage: epsdel(sqr, 2, 0.75, 0.15)
That delta works.
sage: 
sage: epsdel(sin, 0, 0.1, 0.2)
That delta is not small enough.
sage: epsdel(sin, 0, 0.1, 0.1)
That delta works.
\end{lstlisting}
\end{frame}  
% $


\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits019.pdf}}
  \quad
  \begin{minipage}{2in}\RaggedRight
    Let $f(x)=x^2$ and look at the anchor number $a=2$, 
    and show that the limit is $L=4$.
    We are challenged with an output tolerance $\varepsilon$.
    We must respond with an input tolerance $\delta$ 
    small enough that it suffices.
  \end{minipage}
\end{center}
Given $\varepsilon$, take $\delta$ to be the minimum of
$\sqrt{4-\varepsilon}$ and $\sqrt{4+\varepsilon}$.
With that, if $x$ is within $\delta$ of~$a=2$
\begin{equation*}
  2-\sqrt{4-\varepsilon}<x-2<2+\sqrt{4+\varepsilon}
\end{equation*}
then adding $2$ to all terms and squaring gives 
\begin{equation*}
  4-\varepsilon<x^2<4+\varepsilon
\end{equation*}
as required.
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
