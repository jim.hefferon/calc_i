# epsilondelta.sage
# Illustrate epsilons and deltas.
#
# License: Public Domain
#
# 2024-Dec-21 Jim Hefferon  Create.

TEST_NUMS = 100   # number of points in the interval to test
def epsdel(f, anchor, eps, delta):
    """Given a real-output function and an anchor point, along with an 
epsilon and delta, check whether that delta works.
    """
    L = f(anchor)
    works_flag = True
    first_test_input = anchor-delta
    input_subinterval_width = 2*delta/(TEST_NUMS-1)  
    for i in range(TEST_NUMS):
        test_input = first_test_input+i*input_subinterval_width 
        if abs(f(test_input)-L) >= eps:
            print("That delta is not small enough.")
            works_flag =  False
            break
    if works_flag:
        print("That delta works.")
    