// areas_between_curves.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "areas_between_curves%03d";
real PI = acos(-1);

import graph;


// ==== general function ====
path general_fcn0 = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ====== Area between curves =========
path general_fcn1 = (-0.25,0)..(1,0.-0.10)..(2,0.10)..(3,0.20)..(4,0)..(5.25,-0.5);
real a = 1; real b = 4;

// .... area below larger ..........
picture pic;
int picnum = 0;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f0 = shift(0,2.5)*general_fcn0;
draw(pic, f0, FCNPEN);
// path f1 = shift(0,0.75)*general_fcn1;
// draw(pic, f1, black);

// region to be integrated  
real f0_a_time = intersect(f0, (a,ybot)--(a,ytop))[0];
real f0_b_time = intersect(f0, (b,ybot)--(b,ytop))[0];
// real f1_a_time = intersect(f1, (a,ybot)--(a,ytop))[0];
// real f1_b_time = intersect(f1, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f0,f0_a_time)&subpath(f0, f0_a_time, f0_b_time)&point(f0,f0_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, background_color+opacity(0.6), highlight_color);

real[] T13 = {a,b};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// .... area below smaller ..........
picture pic;
int picnum = 1;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

// path f0 = shift(0,2.5)*general_fcn0;
// draw(pic, f0, black);
path f1 = shift(0,0.75)*general_fcn1;
draw(pic, f1, FCNPEN);

// region to be integrated  
// real f0_a_time = intersect(f0, (a,ybot)--(a,ytop))[0];
// real f0_b_time = intersect(f0, (b,ybot)--(b,ytop))[0];
real f1_a_time = intersect(f1, (a,ybot)--(a,ytop))[0];
real f1_b_time = intersect(f1, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f1,f1_a_time)&subpath(f1, f1_a_time, f1_b_time)&point(f1,f1_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, background_color+opacity(0.6), highlight_color);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

real[] T13 = {a,b};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// .... area between ..........
picture pic;
int picnum = 2;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f0 = shift(0,2.5)*general_fcn0;
path f1 = shift(0,0.75)*general_fcn1;

// region to be integrated  
real f0_a_time = intersect(f0, (a,ybot)--(a,ytop))[0];
real f0_b_time = intersect(f0, (b,ybot)--(b,ytop))[0];
real f1_a_time = intersect(f1, (a,ybot)--(a,ytop))[0];
real f1_b_time = intersect(f1, (b,ybot)--(b,ytop))[0];
path region = ( point(f1,f1_a_time)--point(f0,f0_a_time)&subpath(f0, f0_a_time, f0_b_time)&point(f0,f0_b_time)--point(f1,f1_b_time)&subpath(f1, f1_b_time, f1_a_time))--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic,point(f1,f1_a_time)--point(f0,f0_a_time));
draw(pic,point(f0,f0_b_time)--point(f1,f1_b_time));
draw(pic, f0, FCNPEN);
draw(pic, f1, FCNPEN);

real[] T13 = {a,b};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// === fcn that is negative =====
real f3(real x) {return -(1/2)*x^2;}

picture pic;
int picnum = 3;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-8; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

real a = 1; real b = 4;

path f = graph(f3, a, b);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, background_color+opacity(0.6), black);
draw(pic, f, FCNPEN);

real x = 2.3;
path slice = (x,0)--(x,f3(x));
draw(pic,slice,highlight_color);

real[] T13 = {a,b};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a, N);
labelx(pic, "$4$", b, N);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// === area between -(x-1)^2+5 and (1/3)x =====
real f4(real x) {return -(x-1)^2+5;}
real f4a(real x) {return (1/3)*x;}

real a = 0; real b = 3;

// ......... curve ..........

picture pic;
int picnum = 4;
size(pic,3.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path ftop = graph(f4, a-0.1, b+0.1);
draw(pic, ftop, FCNPEN);

// real[] T4 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
// labelx(pic, "$0$", a);
// labelx(pic, "$3$", b);
// labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// .................. line ...............
picture pic;
int picnum = 5;
size(pic,3.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path fbot = graph(f4a, a, b);
draw(pic, fbot, FCNPEN);

real[] T4 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%",T4,Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$0$", a);
// labelx(pic, "$3$", b);
// labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ........ both ................
picture pic;
int picnum = 6;
size(pic,3.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path ftop = graph(f4, a, b);
path fbot = graph(f4a, a, b);

// region to be integrated  
real ftop_a_time = intersect(ftop, (a,ybot)--(a,ytop))[0];
real ftop_b_time = intersect(ftop, (b,ybot)--(b,ytop))[0];
real fbot_a_time = intersect(fbot, (a,ybot)--(a,ytop))[0];
real fbot_b_time = intersect(fbot, (b,ybot)--(b,ytop))[0];
path region = ( point(fbot,fbot_a_time)--point(ftop,ftop_a_time)&subpath(ftop, ftop_a_time, ftop_b_time)&point(ftop,ftop_b_time)--point(fbot,fbot_b_time)--point(fbot,fbot_a_time) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, ftop, FCNPEN);
draw(pic, fbot, FCNPEN);

real x = 1.8;
path slice = (x,f4a(x))--(x,f4(x));
draw(pic,slice,highlight_color);
dotfactor = 4;
dot(pic, (x,f4(x)), highlight_color);
label(pic, "$(x,-(x-1)^2+5)$", (x,f4(x)), NE, filltype=Fill(white));
dot(pic, (x,f4a(x)), highlight_color);
label(pic, "$(x,x/3)$", (x,f4a(x)), SE, filltype=Fill(white));

real[] T4 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T4,Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$0$", a);
// labelx(pic, "$3$", b);
labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// === area between x+4 and 3-(x/2) =====
real f7(real x) {return x+4;}
real f7a(real x) {return 3-(x/2);}

real a = 1; real b = 4;

// ......... top line ..........

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path ftop = graph(f7, a-0.1, b+0.1);
// path fbot = graph(f4a, a, b);
// draw(pic, fbot, black);

// region to be integrated  
real ftop_a_time = intersect(ftop, (a,ybot)--(a,ytop))[0];
real ftop_b_time = intersect(ftop, (b,ybot)--(b,ytop))[0];
// real fbot_a_time = intersect(fbot, (a,ybot)--(a,ytop))[0];
// real fbot_b_time = intersect(fbot, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(ftop,ftop_a_time)&subpath(ftop, ftop_a_time, ftop_b_time)&point(ftop,ftop_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic,(a,0)--point(ftop,ftop_a_time));
draw(pic,point(ftop,ftop_b_time)--(b,0));
draw(pic, ftop, FCNPEN);

// real x = 1.8;
// path slice = (x,f4a(x))--(x,f4(x));
// draw(pic,slice,highlight_color);

label(pic, "$y=x+4$", (1,8), filltype=Fill(white));

// real[] T7 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);
labelx(pic, "$4$", b);
// labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// .................. bottom line ...............
picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

// path ftop = graph(f4, a, b);
// draw(pic, ftop, black);
path fbot = graph(f7a, a-0.1, b+0.1);

// region to be integrated  
// real ftop_a_time = intersect(ftop, (a,ybot)--(a,ytop))[0];
// real ftop_b_time = intersect(ftop, (b,ybot)--(b,ytop))[0];
real fbot_a_time = intersect(fbot, (a,ybot)--(a,ytop))[0];
real fbot_b_time = intersect(fbot, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(fbot,fbot_a_time)&subpath(fbot, fbot_a_time, fbot_b_time)&point(fbot,fbot_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, background_color+opacity(0.6), black);
draw(pic, fbot, FCNPEN);

// real x = 1.8;
// path slice = (x,f4a(x))--(x,f4(x));
// draw(pic,slice,highlight_color);

label(pic, "$y=3-(x/2)$", (2.5,4), filltype=Fill(white));

// real[] T4 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%",T4,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);
labelx(pic, "$4$", b);
// labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ........ both ................
picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=9;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path ftop = graph(f7, a-0.1, b+0.1);
path fbot = graph(f7a, a-0.1, b+0.1);

// region to be integrated  
real ftop_a_time = intersect(ftop, (a,ybot)--(a,ytop))[0];
real ftop_b_time = intersect(ftop, (b,ybot)--(b,ytop))[0];
real fbot_a_time = intersect(fbot, (a,ybot)--(a,ytop))[0];
real fbot_b_time = intersect(fbot, (b,ybot)--(b,ytop))[0];
path region = ( point(fbot,fbot_a_time)--point(ftop,ftop_a_time)&subpath(ftop, ftop_a_time, ftop_b_time)&point(ftop,ftop_b_time)--point(fbot,fbot_b_time)--point(fbot,fbot_a_time) )--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic,point(fbot,fbot_a_time)--point(ftop,ftop_a_time));
draw(pic,point(ftop,ftop_b_time)--point(fbot,fbot_b_time));
draw(pic, ftop, FCNPEN);
draw(pic, fbot, FCNPEN);

real x = a+(0.618*(b-a));
path slice = (x,f7a(x))--(x,f7(x));
draw(pic,slice,highlight_color);
dotfactor = 4;
dot(pic, (x,f7(x)), highlight_color);
label(pic,"$(x,x+4)$", (x,f7(x)), NW, filltype=Fill(white));
dot(pic, (x,f7a(x)), highlight_color);
label(pic,"$(x,3-x/2)$", (x,f7a(x)), SW, filltype=Fill(white));

real[] T9 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T9,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);
labelx(pic, "$4$", b);
labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== sin x and cos x ===============
real f10(real x) {return sin(x);}
real f10a(real x) {return cos(x);}

real a = 0; real b = pi;

picture pic;
int picnum = 10;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path sine = graph(f10, a-0.1, b+0.1);
path cosine = graph(f10a, a-0.1, b+0.1);

real c = pi/4;

// region to be integrated  
real sine_a_time = intersect(sine, (a,ybot)--(a,ytop))[0];
real sine_b_time = intersect(sine, (b,ybot)--(b,ytop))[0];
real sine_c_time = intersect(sine, (c,ybot)--(c,ytop))[0];
real cosine_a_time = intersect(cosine, (a,ybot)--(a,ytop))[0];
real cosine_b_time = intersect(cosine, (b,ybot)--(b,ytop))[0];
real cosine_c_time = intersect(cosine, (c,ybot)--(c,ytop))[0];
path region0 = ( point(sine,sine_a_time)--point(cosine,cosine_a_time)&subpath(cosine, cosine_a_time, cosine_c_time)&point(sine,sine_c_time)&subpath(sine, sine_c_time, sine_a_time)&point(sine,sine_a_time) )--cycle;
filldraw(pic, region0, background_color+opacity(0.6), black);
path region1 = ( point(sine,sine_c_time)&subpath(sine, sine_c_time, sine_b_time)&point(sine,sine_b_time)--point(cosine,cosine_b_time)&subpath(cosine, cosine_b_time, cosine_c_time)&point(cosine,cosine_c_time) )--cycle;
fill(pic, region1, background_color+opacity(0.6));
draw(pic,point(sine,sine_b_time)--point(cosine,cosine_b_time));
draw(pic, sine, FCNPEN);
draw(pic, cosine, FCNPEN);

real x0 = a+(0.1*(b-a));
real x1 = a+(0.75*(b-a));
path slice0 = (x0,f10a(x0))--(x0,f10(x0));
path slice1 = (x1,f10(x1))--(x1,f10a(x1));
draw(pic,slice0,highlight_color);
draw(pic,slice1,highlight_color);

real[] T10 = {x0, x1};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$1$", a);
// labelx(pic, "$4$", b);
labelx(pic,"$x_0$", x0);
labelx(pic,"$x_1$", x1, SE);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== what we are computing ===============

real a = 1; real b = 4;

picture pic;
int picnum = 11;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn0;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic,(a,0)--point(f,f_a_time));
draw(pic,point(f,f_b_time)--(b,0));

real x = a+(0.618*(b-a));
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
path slice = (x,0)--point(f,f_x_time);
draw(pic,slice,highlight_color);

draw(pic, f, FCNPEN);

real[] T10 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);
labelx(pic,"$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... partially below x axis .............
picture pic;
int picnum = 12;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-1; ytop=1;

path f = shift(0,0)*general_fcn0;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, background_color+opacity(0.6));
draw(pic, (a,0)--point(f,f_a_time));

real x = a+(0.618*(b-a));
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
path slice = (x,0)--point(f,f_x_time);
draw(pic,slice,highlight_color);

draw(pic, f, FCNPEN);

real[] T10 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a, S);
labelx(pic, "$b$", b, S);
labelx(pic,"$x$", x, N);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== split a region ===============

// ... area between 8x, 8/x^2, and x ...
real f13(real x) {return 8x;}
real f13a(real x) {return 8/x^2;}
real f13b(real x) {return x;}

real a = 0; real b = 2; real c = 1;

// ......... curve ..........

picture pic;
int picnum = 13;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f13, a-0.05, c+0.1);
path fa = graph(f13a, c-0.05, b+0.1);
path fb = graph(f13b, a-0.1, b+0.1);

// // region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
// real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
real f_c_time = intersect(f, (c,ybot)--(c,ytop))[0];
// real fa_a_time = intersect(fa, (a,ybot)--(a,ytop))[0];
real fa_b_time = intersect(fa, (b,ybot)--(b,ytop))[0];
real fa_c_time = intersect(fa, (c,ybot)--(c,ytop+1))[0];
real fb_a_time = intersect(fb, (a,ybot)--(a,ytop))[0];
real fb_b_time = intersect(fb, (b,ybot)--(b,ytop))[0];
// real fb_c_time = intersect(fb, (c,ybot)--(c,ytop))[0];
path region = ( (0,0)--point(fa, fa_c_time)&subpath(fa, fa_c_time, fa_b_time)&point(fa, fa_b_time)--(0,0) )--cycle;

fill(pic, region, background_color+opacity(0.6));

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);
draw(pic, fb, FCNPEN);

real x_left = 0.5;
real x_right = 1.4;
path slice_left = (x_left,f13b(x_left))--(x_left,f13(x_left));
path slice_right = (x_right,f13b(x_right))--(x_right,f13a(x_right));
draw(pic,slice_left,highlight_color);
dotfactor = 4;
dot(pic, (x_left,f13(x_left)), highlight_color);
label(pic,"$(x_0,8x_0)$", (x_left,f13(x_left)), NW, filltype=Fill(white));
dot(pic, (x_left,f13b(x_left)), highlight_color);
label(pic,"$(x_0,x_0)$", (x_left,f13b(x_left)), SE, filltype=Fill(white));
draw(pic,slice_right,highlight_color);
dot(pic, (x_right,f13a(x_right)), highlight_color);
label(pic,"$(x_1,8/x_1^2)$", (x_right,f13a(x_right)), NE, filltype=Fill(white));
dot(pic, (x_right,f13b(x_right)), highlight_color);
label(pic,"$(x_1,x_1)$", (x_right,f13b(x_right)), SE, filltype=Fill(white));

real[] T13 = {x_left, x_right};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T13, Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$0$", a);
// labelx(pic, "$x_0$", x_left);
// labelx(pic,"$x_1$", x_right);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== integrate dy ===============

// ... area between x=y+1 and x=y^2/2-3 ...
real f14(real x) {return x+1;}
real f14a(real x) {return (x^2/2)-3;}

picture pic;
int picnum = 14;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=5;
ybot=-2; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = reflect((-1,0),(1,0))*rotate(-90)*graph(f14, -2-0.1, 4+0.1);
path fa = reflect((-1,0),(1,0))*rotate(-90)*graph(f14a, -2-0.1, 4+0.1);

// // region to be integrated  
real a = -1; real b = 5;
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
real fa_a_time = intersect(fa, (a,ybot-1)--(a,ytop+1))[0];
real fa_b_time = intersect(fa, (b,ybot-1)--(b,ytop+1))[0];
path region = ( subpath(f, f_a_time, f_b_time)&subpath(fa, fa_b_time, fa_a_time) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, f, FCNPEN);
draw(pic, fa,  FCNPEN);

real x = 0.5;
path slice = reflect((-1,0),(1,0))*rotate(-90)*( (x,f14a(x))--(x,f14(x)) );
draw(pic,slice,highlight_color);
dotfactor = 4;
dot(pic, point(slice,0), highlight_color);
label(pic,"$(y^2/2-3,y)$", point(slice,0), 2*N, filltype=Fill(white));
dot(pic, point(slice,1), highlight_color);
label(pic,"$(y+1,y)$", point(slice,1), SE, filltype=Fill(white));

real[] T14 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
// labelx(pic, "$0$", a);
// labelx(pic, "$x_0$", x_left);
// labelx(pic,"$x_1$", x_right);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T14, Size=2pt),
      arrow=Arrows(TeXHead));
// labely(pic,"$y$", x, NW);
  
shipout(format(OUTPUT_FN,picnum),pic);


// ... area between x=y+1 and x=y^2/2-3 ...
real f15(real x) {return (x^2)^(1/3);}
real f15a(real x) {return x-4;}

picture pic;
int picnum = 15;
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=8;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f15, xleft, xright+0.1);
path fa = graph(f15a, 4-0.1, 8+0.1);

// // region to be integrated  
real a = 0; real b = 8;
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path region = ( subpath(f, f_a_time, f_b_time)&(8,4)--(4,0)--(0,0) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, f, FCNPEN);
draw(pic, fa,  FCNPEN);

real x = 1.5;
path slice = (x,f15(x))--(f15(x)+4,f15(x));
draw(pic,slice,highlight_color);
dotfactor = 4;
dot(pic, (x,f15(x)), highlight_color);
label(pic,"$(y^{3/2},y)$", (x,f15(x)), 2*N, filltype=Fill(white));
dot(pic, (f15(x)+4,f15(x)), highlight_color);
label(pic,"$(y+4,y)$", (f15(x)+4,f15(x)), SE, filltype=Fill(white));

real[] T15 = {f15(x)};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T15, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic,"$y$", f15(x), W);
  
shipout(format(OUTPUT_FN,picnum),pic);



// ====== area between x^2 and x^4-12 =====
real f16(real x) {return x^2;}
real f16a(real x) {return x^4-12;}

picture pic;
int picnum = 16;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-12; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f16, xleft-0.1, xright+0.1);
path fa = graph(f16a, xleft-0.01, xright+0.01);

// // region to be integrated  
real a = -2; real b = 2;
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
real fa_a_time = intersect(fa, (a,ybot-1)--(a,ytop+1))[0];
real fa_b_time = intersect(fa, (b,ybot-1)--(b,ytop+1))[0];
path region = ( subpath(f, f_a_time, f_b_time)&subpath(fa, f_b_time, f_a_time) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real x = 0.8;
path slice = (x,f16a(x))--(x,f16(x));
draw(pic,slice,highlight_color);
dotfactor = 4;
dot(pic, (x,f16(x)), highlight_color);
label(pic,"$(x,x^2)$", (x,f16(x)), NE, filltype=Fill(white));
dot(pic, (x,f16a(x)), highlight_color);
label(pic,"$(x,x^4-12)$", (x,f16a(x)), SE, filltype=Fill(white));

real[] T16 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=LeftTicks("%", T16, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x, SE);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ====== density fcn =====
real f17(real x) {return (1/2)*(sin(x^2)+1);}

picture pic;
int picnum = 17;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;

pen p = linewidth(1.25pt);
int maxi = 100;
real subinterval_left = 0;
real subinterval_right = 0;
draw(pic, (0,0)--(-0.20,0), p+gray(f17(1-0)), Arrow(TeXHead));
for(int i=0; i < 100; ++i) {
  subinterval_left = xleft+(xright-xleft)*(i/maxi);
  subinterval_right = xleft+(xright-xleft)*((i+1)/maxi);
  draw(pic, (subinterval_left,0)--(subinterval_right,0), p+gray(1-f17(subinterval_left)));
}
draw(pic, (subinterval_right,0)--(subinterval_right+0.20,0), p+gray(1-f17(subinterval_left)), Arrow(TeXHead));
label(pic,"density at $x$ is $f(x)$", ((xright-xleft)/2,0.5));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... in graph form ...........
picture pic;
int picnum = 18;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f17, xleft-0.1, xright+0.1);

// // region to be integrated  
real a = 0; real b = 5.1;
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path region = ( (0,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f, f_b_time)--(b,0)--(0,0) )--cycle;
fill(pic, region, background_color+opacity(0.6));

draw(pic, f, FCNPEN);

// real x = 1.4;
// path slice = (x,0)--(x,f17(x));
// draw(pic,slice,highlight_color);

real[] T18 = {x};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks
      );
// labelx(pic, "$x$", x);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot, ymax=ytop,
      p=currentpen,
      ticks=NoTicks
      );
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

