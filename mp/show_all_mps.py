#!/usr/bin/env python
import sys,subprocess
import glob

def figure_index_from_fn(fn):
    i=fn.find('-')
    j=fn.find('.')
    try:
        dex=int(fn[i:j])
    except:
        print "fn[i:j]=",fn[i:j]
        dex=0
    return dex

file_list=glob.glob("*.mps")
file_list.sort(key=figure_index_from_fn,reverse=True)
popenargs=['tex','mproof']+[" ".join(file_list)]
try:
    subprocess.call(popenargs)
except Exception, err:
    print "trouble",str(err)
    sys.exit(1)
print "done"
