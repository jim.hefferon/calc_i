// trigs.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "trigs%03d";

import graph;



// ================ sin(x) =======
real f0(real x) {return sin(x);}

picture pic;
int picnum = 0;

size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f0,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

real[] T={pi/2, pi, 3*pi/2, 2*pi};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%", T, Size=2pt),
  arrow=Arrows(TeXHead));
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
labelx(pic,"{\tiny $\pi$}",pi,0.5*S);
labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,N);
labelx(pic,"{\tiny$ 2\pi$}",2*pi,0.5*S);
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ......... cos(x) ...........
real f1(real x) {return cos(x);}

picture pic;
int picnum = 1;

size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f1,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

real[] T={pi/2, pi, 3*pi/2, 2*pi};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
      ticks=RightTicks("%", T, Size=2pt),
  arrow=Arrows(TeXHead));
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
labelx(pic,"{\tiny $\pi$}",pi,0.5*S);
labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,S);
labelx(pic,"{\tiny$ 2\pi$}",2*pi,0.5*S);
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ......... sin(h)/h near h=0 ...........
real f2(real x) {return sin(x)/x;}

picture pic;
int picnum = 2;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f2,xleft-0.1,-0.1), FCNPEN);
draw(pic, graph(f2,0.1,xright+0.1), FCNPEN);
filldraw(pic, circle((0,1),0.075), white, FCNPEN_COLOR);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ......... (cos(h)-1)/h near h=0 ...........
real f3(real x) {return (cos(x)-1)/x;}

picture pic;
int picnum = 3;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f3,xleft-0.2,-0.05), FCNPEN);
draw(pic, graph(f3,0.05,xright+0.2), FCNPEN);
filldraw(pic, circle((0,0),0.05), white, FCNPEN_COLOR);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ============= Proof (sin theta)/theta = 1 ==========


picture pic;
int picnum = 4;

unitsize(pic,3cm);

pair origin=(0,0);
pair A = (1,0);
pair C = (1,0.8);

real theta = atan(C.y/C.x); // radians
real theta_deg = degrees(theta);

path base = origin--A;
path rhs = A--C;
path hyp = C--origin;

draw(pic, base);
draw(pic, rhs);
draw(pic, hyp);

filldraw(pic, circle(origin,0.015), black, black);
label(pic,"$O$",origin,S);
filldraw(pic, circle(A,0.015), black, black);
label(pic,"$A$",A,S);
filldraw(pic, circle(C,0.015), black, black);
label(pic,"$C$",C,N);

label(pic,"$\theta$",origin,7*(cos(theta/2),sin(theta/2)));
draw(pic,arc(origin, 0.2, 0, theta_deg), Arrow);

path circle_arc = arc(origin, 1, 0, degrees(atan(C.y/C.x)));
draw(pic,circle_arc, red);

pair B = point(circle_arc, 1);
filldraw(pic, circle(B,0.015), highlight_color, highlight_color);
label(pic,"$B$",B,NW);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

real line_y = -0.2;
draw(pic, "$1$", (0,line_y)--(A.x,line_y),Bars);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .....................................
picture pic;
int picnum = 5;

unitsize(pic,3cm);

pair origin=(0,0);
pair A = (1,0);
pair C = (1,0.8);

real theta = atan(C.y/C.x); // radians
real theta_deg = degrees(theta);

path base = origin--A;
path rhs = A--C;
path hyp = C--origin;

draw(pic, base);
draw(pic, rhs);
draw(pic, hyp);

// filldraw(pic, circle(origin,0.02), black, black);
label(pic,"$O$",origin,S);
// filldraw(pic, circle(A,0.02), black, black);
label(pic,"$A$",A,S);
// filldraw(pic, circle(C,0.02), black, black);
label(pic,"$C$",C,N);

label(pic,"$\theta$",origin,7*(cos(theta/2),sin(theta/2)));
// draw(pic,arc(origin, 0.2, 0, theta_deg), Arrow);

path circle_arc = arc(origin, 1, 0, theta_deg);
draw(pic,circle_arc, black);

pair B = point(circle_arc, 1);
// filldraw(pic, circle(B,0.02), black, black);
label(pic,"$B$",B,NW);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

real line_y = -0.2;
draw(pic, "$1$", (0,line_y)--(A.x,line_y),Bars);

path sin_perp = (B.x,0)--B;
draw(pic, "$\sin\theta$", sin_perp, align=W, highlight_color);

real line_x = 0.15;
path tan_perp = (A.x+line_x,0)--(A.x+line_x,C.y);
draw(pic, "$\tan\theta$", tan_perp, align=E, bar=Bars, highlight_color);

draw(pic, A--B, highlight_color);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= Spring from JB ==============
pair coilpoint(real lambda, real r, real t)
{
  return (2.0*lambda*t+r*cos(t),r*sin(t));
}

guide coil(guide g=nullpath, real lambda, real r, real a, real b, int n)
{
  real width=(b-a)/n;
  for(int i=0; i <= n; ++i) {
    real t=a+width*i;
    g=g..coilpoint(lambda,r,t);
  }
  return g;
}

path fullspring(real x) {
  real r=8;
  real t1=-pi;
  real t2=10*pi;
  real lambda=(t2-t1+x)/(t2-t1);
  pair b=coilpoint(lambda,r,t1);
  pair c=coilpoint(lambda,r,t2);
  pair a=b-20;
  pair d=c+20;
  
  path r = a--b
    &coil(lambda,r,t1,t2,100)
    &c--d;
  return r;
}

// ... simple harmmonic motion ........
picture pic;
int picnum = 6;

size(pic,0,3cm);

// spring coil
draw(pic, rotate(90,(0,0))*fullspring(1));

// top
for (int i=0; i<6; ++i) {
  draw(pic, (15*(-3+i),92.5)--(15*(-2+i),105), GRAPHPAPERPEN);
}
draw(pic,(15*(-3),92.5)--(15*(3),92.5));

// weight on the bottom
path wgt = (-20,-35)--(20,-35)--(20,-50)--(-20,-50)--cycle;
filldraw(pic,wgt,bold_color,black);
// dot(pic,(0,-35),red);


shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ tan(x) =======
real f7(real x) {return tan(x);}
real tline7(real x) {return 2*(x-pi/4)+1;};

picture pic;
int picnum = 7;

size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-1; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f7,-pi/4-0.1,pi/2-0.24), FCNPEN);
draw(pic, graph(tline7,pi/4-0.5,pi/4+0.5), highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

