\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.3\ \ Trigonometric Funcions}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................




\begin{frame}{Trigonometric functions}
\fbox{\parbox{\textwidth}{\color{red}We will always work in radians.
      Calculus in degrees is painful.}}

These are the inputs and outputs that you should know.
\begin{center}
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{$\theta$} 
       &\multicolumn{1}{c}{$\sin \theta$} 
       &\multicolumn{1}{c}{$\cos \theta$} \\ \hline
    \rule{0pt}{11pt}$0$     &$\sqrt{0}/2$ &$\sqrt{4}/2$ \\
    $\pi/6$ &$\sqrt{1}/2$ &$\sqrt{3}/2$ \\
    $\pi/4$ &$\sqrt{2}/2$ &$\sqrt{2}/2$ \\
    $\pi/3$ &$\sqrt{3}/2$ &$\sqrt{1}/2$ \\
    $\pi/2$ &$\sqrt{4}/2$ &$\sqrt{0}/2$ 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}{Derivative of $\sin x$}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trigs000.pdf}}
  \quad
  \begin{tabular}{c|c}
   \multicolumn{1}{c}{$x$}
     &\multicolumn{1}{c}{\textit{Estimate of $\frac{d\,\sin(x)}{dx}$}} \\[1ex]
   \hline
   $0$      &\uncover<2->{$1$}   \\
   $\pi/2$  &\uncover<2->{$0$}   \\
   $\pi$    &\uncover<2->{$-1$}   \\
   $3\pi/2$ &\uncover<2->{$0$}   \\
   $2\pi$   &\uncover<2->{$1$}   
  \end{tabular}
\end{center}

\pause It looks like the derivative of $\sin x$ is $\cos x$. 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trigs001.pdf}}
\end{center}
\end{frame}

\begin{frame}{Argument for derivative of $\sin x$}
Invoke the trig formula for the sine of the sum of two angles.
\begin{equation*}
  \lim_{h\to 0}\frac{\sin(x+h)-\sin(x)}{h}    
  =\lim_{h\to 0}\frac{\sin(x)\cos(h)+\cos(x)\sin(h)-\sin(x)}{h}    
\end{equation*}
\pause Rearrange and apply the Limit Laws.
\begin{align*}
  &=\lim_{h\to 0}\left[\frac{\sin(x)\cos(h)-\sin(x)}{h}+\frac{\cos(x)\sin(h)}{h}\right]       \\    
  &=\lim_{h\to 0}\left[\sin(x)\cdot\frac{\cos(h)-1}{h}+\cos(x)\cdot\frac{\sin(h)}{h}\right]   \\
  &=\left(\lim_{h\to 0}\sin(x)\right)\cdot \left(\lim_{h\to 0}\frac{\cos(h)-1}{h}\right)
     +\left(\lim_{h\to 0}\cos(x)\right)\cdot \left(\lim_{h\to 0}\frac{\sin(h)}{h}\right) 
\end{align*}
\pause
Two are easy: there is no $h$ in $\sin(x)$ or $\cos(x)$ so we have this.
\begin{equation*}
  \lim_{h\to 0}\sin(x)=\sin(x)
  \qquad
  \lim_{h\to 0}\cos(x)=\cos(x)
\end{equation*}
We can prove these for the other two.
\begin{equation*}
  \lim_{h\to 0}\frac{\cos(h)-1}{h}=0
  \qquad
  \lim_{h\to 0}\frac{\sin(h)}{h}=1
\end{equation*}
\end{frame}

\begin{frame}
This is $\sin(h)/h$ and $(\cos(h)-1)/h$, both in the neighborhood 
of $h=0$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trigs002.pdf}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/trigs003.pdf}}
\end{center}
Certainly the graphs are convincing.

Because this is a key result that we will use all the time, we will 
do more than just look at a picture.
We will make the case that you can also algebraically argue the first one, 
$\lim_{h\to 0}\sin x/x=1$.  
(We will only give the argument for inputs
between $0$ and $\pi/2$, leaving negative angles for the book.)
\end{frame}


\begin{frame}{Optional: proof}
\hspace*{0.3\textwidth}\only<1>{\includegraphics{asy/trigs004.pdf}}%    
  \only<2->{\includegraphics{asy/trigs005.pdf}} 

\only<1>{This red arc is part of a unit circle.}%
\only<2>{The left line is $\sin\theta$ because it is the height of
  a triangle with a unit hypoteneuse.
  The right line has length $\tan\theta$ because that's the definition of
  the tangent function. 
  (If you are used to $\sin\theta/\cos\theta$ then rescale the unit hypoteneuse
  triangle by a factor of $1/\cos\theta$).} 
\only<3->{
The area of triangle $0AB$ is $(1/2)\sin\theta$, 
the area of wedge $OAB$ is $\theta/2$ (in a unit circle the wedge area is half 
the number of radians),
and the area of triangle $OAC$ is $(1/2)\tan\theta$.
\begin{equation*}
  0<\frac{1}{2}\sin\theta<\frac{1}{2}\theta<\frac{1}{2}\tan\theta
\end{equation*}
Multiply by $2/\sin\theta$ to get
$
  1<\theta/\sin\theta<1/\cos\theta
$,
and then take reciprocals (reversing the inequalities).
\begin{equation*}
  \cos\theta<\frac{\sin\theta}{\theta}<1
\end{equation*}
As $\theta\to 0$ the left and right sides go to~$1$.
Therefore so also does the middle expression.
}
\end{frame}


\begin{frame}{Derivatives of Trigonometric Functions}
\begin{center}
\begin{tabular}{ll}
  $\displaystyle\frac{d\,\sin x}{dx}=\cos x$       
    &$\displaystyle\frac{d\,\cos x}{dx}=-\sin x$      \\[2ex]
  $\displaystyle\frac{d\,\tan x}{dx}=\sec^2 x$     
    &$\displaystyle\frac{d\,\cot x}{dx}=-\csc^2 x$      \\[2ex]
  $\displaystyle\frac{d\,\sec x}{dx}=\sec x\tan x$ 
    &$\displaystyle\frac{d\,\csc x}{dx}=-\csc x\cot x$  
\end{tabular}
\end{center}
\pause
\textsc{Example}
Find the derivative of $f(x)=x\cdot \cos x$.

\pause
Apply the Product Rule.
\begin{equation*}
  x\cdot \frac{d\,\cos x}{dx}+\cos x\frac{d\,x}{dx}
  =-x\sin x+\cos x
\end{equation*}
\end{frame}

\begin{frame}
\textsc{Example}
Find $y'(0)$ when $y(x)=e^x\cos x$.
\pause
Apply the Product Rule.
\begin{equation*}
  e^x\cdot\frac{d\,\cos x}{dx}+\cos x\cdot\frac{d\,e^x}{dx}
  =e^x(\cos x-\sin x)
\end{equation*}
So $y'(0)=e^0\cdot(1-0)=1$.

\pause\medskip
\textsc{Example}
Find the equation of the line tangent to $f(x)=\tan x$ at $x=\pi/4$.
\pause
The derivative is $f'(x)=\sec^2 x$.
Since $\cos(\pi/4)=\sqrt{2}/2$, we have $\sec x=2/\sqrt{2}$ and 
so $\sec^2 x=4/2=2$.
This is the equation of the tangent line.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trigs007.pdf}}
  \quad
  \begin{minipage}{0.4\linewidth}
    \begin{equation*}
      y-1=2\cdot(x-\frac{\pi}{4})
    \end{equation*}
  \end{minipage}
\end{center}
\end{frame}


% \begin{frame}{Simple harmonic motion}
% Hang a mass on a spring, then push it up $3$~cm and let go.
% Its vertical location at time~$t$ will be $s(t)=3\cos t$.
% \begin{center}
%   \includegraphics{asy/trigs006.pdf}
% \end{center}
% Find its velocity, the first derivative.
% Also find its acceleration, the 
% derivative of the velocity.
% \pause
% \begin{equation*}
%   v(t)=\frac{ds}{dt}=-3\sin t
%   \qquad
%   a(t)=\frac{d^2s}{dt^2}=-3\cos t
% \end{equation*}
% It oscillates from $s=-3$ to $s=3$ with a period of $2\pi$.
% Its speed $|v(t)|=3\cdot|\sin t|$ is greatest when it passes through $s=0$.
% The acceleration is largest at $s=3$ and $s=-3$.
% \end{frame}


\begin{frame}
\textsc{Example}
Find all the derivatives of $f(x)=\sin x$.
\pause
\begin{center}
  \begin{tabular}{r|*{6}{c}}
    $n$                         &$0$ &$1$ &$2$ &$3$ &$4$ &$5$  \\
    \cline{1-7} 
    \textit{$n$-th derivative}  &$\sin x$ &$\cos x$ &$-\sin x$ &$-\cos x$ &$\sin x$ &\ldots \\ 
  \end{tabular}
\end{center}
  
\textsc{Example}
Find $f''(\pi/4)$ where $f(x)=\sec x$.

\pause
The first derivative is $f'(x)=\sec x\cdot\tan x$.
So for the second derivative we need the Product Rule.
\begin{align*}
  \sec x\cdot\frac{d\, \tan x}{dx}+\tan x\cdot \frac{d\,\sec x}{dx}
  &=\sec x\cdot\sec^2 x+\tan x\cdot(\sec x\tan x)               \\
  &=\sec^3 x+\sec x\tan^2 x 
\end{align*}
Plugging in $\pi/4$ gives $2\sqrt{2}+\sqrt{2}=3\sqrt{2}$.
\end{frame}


\begin{frame}
\textsc{Example}
Find the derivative of $\sin x/(1-\cos x)$.
\pause The Quotient Rule gives
\begin{equation*}
  \frac{\displaystyle(1-\cos x)\cdot\frac{d\,\sin x}{dx}-\sin x\cdot\frac{d\,(1-\cos x)}{dx}}{(1-\cos x)^2}
\end{equation*}
which is
\begin{align*}
  =\frac{(1-\cos x)\cdot \cos x-\sin x\cdot\sin x}{(1-\cos x)^2}
  &=\frac{\cos x-\cos^2 x-\sin^2 x}{(1-\cos x)^2}  \\
  &=\frac{\cos x-1}{(1-\cos x)^2}
  =\frac{1}{\cos x-1}
\end{align*}

\pause

\textsc{Example}
Find all points on the graph of $f(x)=x+\sin x$ where the tangent line
is horizontal.

\pause
The slope of the tangent line is $f'(x)=1+\cos x$.
A horizontal line has a slope of zero and $\cos x=-1$ when $x$ is an 
odd multiple of $\pi$, that is $x=(2k+1)\cdot \pi$ where $k$ is an integer.
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
