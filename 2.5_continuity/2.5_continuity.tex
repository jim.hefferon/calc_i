\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.5\ \ Continuity}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Continuity}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}
Continuity is an important principle in science, engineering, and mathematics.
Many things have to be continuous.

This is a graph of the temperature at various heights above the earth.
\begin{center}
  \includegraphics[height=0.5\textheight]{pix/atmosphere_1962.png}
\end{center}
Imagine that it had a discontinuity.
You'd say that can't be right.
\end{frame}


\begin{frame}
This illustrates continuity in Mathematics.
\begin{center}
  \includegraphics{asy/continuity000.pdf}
\end{center}
These high-wire walkers each want to get to the other's end.
It doesn't matter if one walks fast and the other slow, 
or even if one stops from time to time\Dash
because of continuity they will meet.
It cannot happen that, say, the walker on the left somehow
jumps from $x=2$ to $x=3$.
\end{frame}

\begin{frame}
We can define continuity with limits.
This will illustrate that limits are useful in places outside of rates of change,
and also give us practice with limits.
\end{frame}



\section{Definition}

\begin{frame}
We want to avoid a dicontinuity like this one.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity001.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}
    \begin{equation*}
      f(x)=
      \begin{cases}
        x^2  &\text{if $x\neq 1$}  \\
        2    &\text{if $x=1$}
      \end{cases}
    \end{equation*}
  \end{minipage}
\end{center}
\pause
The function \alert{$f$ is continuous at $x=a$} if
\begin{equation*}
  \lim_{x\to a}f(x)=f(a)
\end{equation*}
where $f$ is defined on an open interval containing~$a$.
A function is continuous on an interval if it is continuous at each point 
in that interval.
\end{frame}


\begin{frame}{Example}
The function $g(x)=\cos x$ is continuous at $a=\pi$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity002.pdf}}
\end{center}
From the graph it is clear that $\lim_{x\to \pi}g(x)=-1$, and
$g(\pi)=-1$.

In fact, this function is continuous on the entire real line.
\end{frame}


\begin{frame}
Find the inputs (if any) where each function is discontinuous.
\begin{enumerate}
\item $f(x)=2/(x-1)$
\pause\item $x/(x^2-1)$
\pause\item $\displaystyle\frac{x+1}{x^2-2x-3}$
\pause\item $\sqrt{x-3}$
\pause\item $x^3-2x$
\pause\item $\sqrt{(x-1)(x-3)}$
\pause\item $e^x$
\pause\item $\displaystyle f(x)=
\begin{cases}
  x-1  &\text{if $x<2$} \\
  x^2  &\text{if $x\geq 2$}
\end{cases}$
\end{enumerate}
\end{frame}



\begin{frame}{Kinds of discontinuities}
This function 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity001.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}
    \begin{equation*}
      f(x)=
      \begin{cases}
        x^2  &\text{if $x\neq 1$}  \\
        2    &\text{if $x=1$}
      \end{cases}
    \end{equation*}
  \end{minipage}
\end{center}
has a \alert{removable discontinuity}.
We can make it continuous by changing the definition of $f(1)$ to~$1$.
\end{frame}


\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity003.pdf}}
\end{center}
Is this function continuous at:
$x=0$?
\pause $x=1$?
\pause $x=2$?
\pause $x=3$?
\pause $x=4$?
\pause $x=5$?

\only<6->{
At $x=1$ the function has a \alert{jump discontinuity} while at 
$x=3$ it has an \alert{infinite discontinuity}.}
\end{frame}


\begin{frame}{Continuity at an endpoint}
This is the function of $x$ whose value is the greatest integer less than or
equal to~$x$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity004.pdf}}
\end{center}

A function $f$ is \alert{continuous from the right} at~$a$ if 
$\lim_{x\to a^+}f(x)=f(a)$.
It is \alert{continuous from the left} at~$a$ if 
$\lim_{x\to a^-}f(x)=f(a)$.
\end{frame}


\begin{frame}
At $a=2$ the function $f(x)=\sqrt{x-2}$
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity005.pdf}}
\end{center}
is continuous from the right but not from the left.
\end{frame}


\begin{frame}{Facts about continuity}
\begin{enumerate}
\item
Each of these types of functions is continuous at every point where it is
defined:
  polynomial function, 
  rational function,
  root function,
  trigonometric function,
  inverse trigonometric function,
  exponential function,
  logarithmic function.
\pause\item
If $f$ and~$g$ are continuous at~$a$ and $c$ is a constant 
then these functions are continuous at $a$: 
$c\cdot f$, $f+g$, $f-g$, $f\cdot g$,
$f/g$ (where $g(a)\neq 0$).
\pause\item
As to function composition, if $g$ is continuous at~$a$ and $f$ is continuous at
$f(a)$ then the composition function $f\circ g\, (x)=f(g(x))$ is continuous 
at $a$.
\end{enumerate}

For example, $\sin(3x)$ is continuous on the entire real line because
it is the composition of $f(x)=\sin(x)$ and $g(x)=3x$. 
Similarly, $h(x)=e^{5x}$ is also continuous on the entire real line.
And $\sqrt{1+x}$ is continuous where $x>-1$, although it is not
continuous at $x=-1$.

\end{frame}

\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
