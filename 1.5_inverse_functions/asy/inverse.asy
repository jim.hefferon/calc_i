// inverse.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "inverse%03d";

import graph;


// Draw a set bean
path setbean(real h=1, real v=1) {
  path p;
  p = (0,0)..(0,v)..(h,v)..tension(1.2)..(h,-v)..(0,-v)..cycle;
  return p;
}
pen BEANCOLOR = white;
pen ARROWCOLOR = highlight_color;

real h, v;  // horizontal and vertical units for beans
h = 0.70; v = h; 
real DOMAINTOCODOMAIN = 2.5*h;


// ================ bean diagrams =======
picture pic;
int picnum = 0;

size(pic,0,3cm,keepAspect=true);

// Establish all the stuff to draw
path domain, codomain;
domain = setbean(h,v);
codomain = shift(DOMAINTOCODOMAIN,0)*setbean(h,v);
pair[] domainpoint, codomainpoint;
for (int i; i<3; ++i) {
  domainpoint[i] = (0.5*h,((-1/2)+0.1)*v+(i/2)*(1)*v);
  dot(pic,domainpoint[i],black);
  label(pic,format("%d",i),domainpoint[i], 2*W, black);
}
for (int i; i<3; ++i) {
  codomainpoint[i] = shift(DOMAINTOCODOMAIN,0)*(0.5*h,((-2/3)+0.1)*v+(i/3)*(4/3)*v);
  dot(pic,codomainpoint[i],DARKPEN+black);
  label(pic,format("%d",10+i),codomainpoint[i], 2*E, black);
}
path[] maparrow;
maparrow[2] = domainpoint[2]{dir(15)}..codomainpoint[2];
maparrow[1] = domainpoint[1]{dir(15)}..codomainpoint[1];
maparrow[0] = domainpoint[0]{dir(15)}..codomainpoint[0];
// now draw; want white area around map arrow thru the border
// First the outlines
draw(pic,domain,black);
draw(pic,codomain,black);
// Next the arrow, wider, in white
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,white);
}
// Now fill, draw dots, and draw arrows narrow
fill(pic,domain,BEANCOLOR);
fill(pic,codomain,BEANCOLOR);
for (int i; i<3; ++i) {
  dot(pic,domainpoint[i],black);
}
for (int i; i<3; ++i) {
  dot(pic,codomainpoint[i],black);
}
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,highlight_color);
}

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............. not one-to-one
picture pic;
int picnum = 1;

size(pic,0,3cm,keepAspect=true);


// Establish all the stuff to draw
path domain, codomain;
domain = setbean(h,v);
codomain = shift(DOMAINTOCODOMAIN,0)*setbean(h,v);
pair[] domainpoint, codomainpoint;
for (int i; i<3; ++i) {
  domainpoint[i] = (0.5*h,((-1/2)+0.1)*v+(i/2)*(1)*v);
  dot(pic,domainpoint[i],black);
  label(pic,format("%d",i),domainpoint[i], 2*W, black);
}
for (int i; i<4; ++i) {
  codomainpoint[i] = shift(DOMAINTOCODOMAIN,0)*(0.5*h,((-2/3)+0.1)*v+(i/3)*(4/3)*v);
  dot(pic,codomainpoint[i],DARKPEN+black);
  label(pic,format("%d",10+i),codomainpoint[i], 2*E, black);
}
path[] maparrow;
maparrow[2] = domainpoint[2]{dir(15)}..codomainpoint[3];
maparrow[1] = domainpoint[1]{dir(15)}..codomainpoint[3];
maparrow[0] = domainpoint[0]{dir(15)}..codomainpoint[1];
// now draw; want white area around map arrow thru the border
// First the outlines
draw(pic,domain,black);
draw(pic,codomain,black);
// Next the arrow, wider, in white
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,white);
}
// Now fill, draw dots, and draw arrows narrow
fill(pic,domain,BEANCOLOR);
fill(pic,codomain,BEANCOLOR);
for (int i; i<3; ++i) {
  dot(pic,domainpoint[i],black);
}
for (int i; i<4; ++i) {
  dot(pic,codomainpoint[i],black);
}
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,highlight_color);
}

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ y=x^2 =======
real f2(real x) {return x^2;}

picture pic;
int picnum = 2;

size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-1; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,xright+0.1), black);
draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ y=x^3 =======
real f3(real x) {return x^3;}

picture pic;
int picnum = 3;

size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-1; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f3,xleft-0.1,xright), black);
// draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ one-to-one function going back =======
picture pic;
int picnum = 4;

size(pic,0,3cm,keepAspect=true);

// Establish all the stuff to draw
path domain, codomain;
domain = setbean(h,v);
codomain = shift(DOMAINTOCODOMAIN,0)*setbean(h,v);
pair[] domainpoint, codomainpoint;
for (int i; i<3; ++i) {
  domainpoint[i] = (0.5*h,((-1/2)+0.1)*v+(i/2)*(1)*v);
  dot(pic,domainpoint[i],black);
  label(pic,format("%d",i),domainpoint[i], 2*W, black);
}
for (int i; i<3; ++i) {
  codomainpoint[i] = shift(DOMAINTOCODOMAIN,0)*(0.5*h,((-2/3)+0.1)*v+(i/3)*(4/3)*v);
  dot(pic,codomainpoint[i],DARKPEN+black);
  label(pic,format("%d",10+i),codomainpoint[i], 2*E, black);
}
path[] maparrow;
maparrow[2] = codomainpoint[2]{dir(165)}..domainpoint[2];
maparrow[1] = codomainpoint[1]{dir(165)}..domainpoint[1];
maparrow[0] = codomainpoint[0]{dir(165)}..domainpoint[0];
// now draw; want white area around map arrow thru the border
// First the outlines
draw(pic,domain,black);
draw(pic,codomain,black);
// Next the arrow, wider, in white
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,white);
}
// Now fill, draw dots, and draw arrows narrow
fill(pic,domain,BEANCOLOR);
fill(pic,codomain,BEANCOLOR);
for (int i; i<3; ++i) {
  dot(pic,domainpoint[i],black);
}
for (int i; i<3; ++i) {
  dot(pic,codomainpoint[i],black);
}
for (int i; i<3; ++i) {
  draw(pic,subpath(maparrow[i],0.08,0.92),bar=BeginBar(2),arrow=EndArrow,highlight_color);
}

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ increasing function =======
picture pic;
int picnum = 5;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path increasing_fcn = (xleft-0.1,0.5){NE}..(1,3)..(2.35,4)..{NE}(xright+0.1,ytop+0.1);
draw(pic, increasing_fcn, highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........ decreasing fcn ........
picture pic;
int picnum = 6;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path increasing_fcn = (xleft-0.1,ytop){SE}..(1,3)..(2.35,2)..{SE}(xright+0.1,0.5);
draw(pic, increasing_fcn, highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


real PI = 3.14159;
// ================ arcsin =======
real f7(real x) {return sin(x);}

picture pic;
int picnum = 7;

size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f7,-1*PI/2,PI/2), highlight_color);
// draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

real[] T7 = {-1*PI/2,PI/2};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$-\frac{\pi}{2}$",-1*PI/2,S);
labelx(pic,"$\frac{\pi}{2}$",PI/2,S);
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ arcsin =======
real f8(real x) {return asin(x);}

picture pic;
int picnum = 8;

size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f8,-1,1), highlight_color);
// draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));

real[] T8 = {-1*PI/2,PI/2};

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T8, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic,"$-\frac{\pi}{2}$",-1*PI/2,W);
labely(pic,"$\frac{\pi}{2}$",PI/2,W);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ value of arctan =======
picture pic;
int picnum = 9;

size(pic,0,3.5cm,keepAspect=true);

pair origin = (0,0);
real x = 3;
real y = 4;
pair z = (x,y);
pair a = (x,0);

path triangle = origin--a--z--cycle;
draw(pic, triangle, black);
label(pic, "(x,y)", z, E);

path c = circle(origin, 0.8);
real t = intersect(c,origin--z)[0];

draw(pic, "$\theta$", subpath(c,0,t), 2*E, arrow=Arrow);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ y=e^x =======
real f10(real x) {return exp(x);}

picture pic;
int picnum = 10;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f10,xleft-0.1,1.61), highlight_color);
// sage: n(ln(5))
// 1.60943791243410


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ y=ln x =======
real f11(real x) {return log(x);}

picture pic;
int picnum = 11;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-3; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f11,0.05,xright+0.1), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======= device to do addition =======
picture pic;
int picnum = 12;

size(pic,6cm,0,keepAspect=true);

real edge = 0.7;
real thick = 1.0;

path top = (0-edge,0)--(9+edge,0)--(9+edge,thick)--(0-edge,thick)--cycle;
path bot = (1-edge,0)--(10+edge,0)--(10+edge,-thick)--(1-edge,-thick)--cycle;

draw(pic, top, black);
draw(pic, bot, black);

real tic_size = 0.2;
// Draw tics
for(int i=0; i <= 9; ++i) { 
  draw(pic,(i,0)--(i,tic_size));
  label(pic,format("%d",i), (i,tic_size), N);
}
for(int i=0; i <= 9; ++i) { 
  draw(pic,(i+1,0)--(i+1,-tic_size), highlight_color);
  label(pic,format("%d",i), (i+1,-tic_size), S, highlight_color);
}
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ Graph of a function and its inverse =======
real f13(real x) {return exp(x);}
real f13a(real x) {return log(x);}

picture pic;
int picnum = 13;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f13,xleft-0.1,log(3)+0.1), black);
draw(pic, graph(f13a,0.18,xright+0.2), black);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), highlight_color, highlight_color);
label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), highlight_color, highlight_color);
label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

// draw(pic, (xleft,xleft)--(xright,xright), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add line y=x .............
picture pic;
int picnum = 14;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f13,xleft-0.1,log(3)+0.1), black);
draw(pic, graph(f13a,0.18,xright+0.2), black);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), highlight_color, highlight_color);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), highlight_color, highlight_color);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add tangent lines .............
picture pic;
int picnum = 15;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path fcn =  graph(f13,xleft-0.1,log(3)+0.1);
path fcn_inv = graph(f13a,0.18,xright+0.2); 
draw(pic,fcn,black);
draw(pic,fcn_inv,black);

real t = 0.3;
pair fcn_pt = (t,exp(t));
pair fcn_inv_pt = (fcn_pt.y,fcn_pt.x);

filldraw(pic, circle(fcn_pt, 0.05), highlight_color, highlight_color);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle(fcn_inv_pt, 0.05), highlight_color, highlight_color);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

// draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);
real fcn_pt_time = times(fcn, fcn_pt)[0];
pair fcn_tangent_dir = 1.5*dir(fcn, fcn_pt_time);
path fcn_tangent = (fcn_pt-fcn_tangent_dir)--(fcn_pt+fcn_tangent_dir);
draw(pic, fcn_tangent, highlight_color);

real fcn_inv_pt_time = times(fcn_inv, fcn_inv_pt)[0];
pair fcn_inv_tangent_dir = 1.5*dir(fcn_inv, fcn_inv_pt_time);
path fcn_inv_tangent = (fcn_inv_pt-fcn_inv_tangent_dir)--(fcn_inv_pt+fcn_inv_tangent_dir); 
draw(pic, fcn_inv_tangent, highlight_color);

real p = 0.6; // length of fcn_tangent_slope
path fcn_tangent_slope_path = fcn_pt--(fcn_pt+(p,0))--point(fcn_tangent,times(fcn_tangent,fcn_pt.x+p)[0]);
draw(pic, fcn_tangent_slope_path,highlight_color+dotted);
draw(pic, reflect((0,0),(1,1))*fcn_tangent_slope_path,highlight_color+dotted);



xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



