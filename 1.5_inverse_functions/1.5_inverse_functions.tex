\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 1.5\ \ Review: inverse functions, inverse trig functions, logarithms}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Functions that cancel}
In beginning algebra we learn that to solve an equation we
should find what it takes to cancel.
Thus, given $3x+2=14$ we do this.
\begin{align*}
  \frac{1}{3}\cdot 14 &=\frac{1}{3}\cdot(3x+2)  \\
  \frac{14}{3} &=x+\frac{2}{3}  \\
  \frac{14}{3}-\frac{2}{3} &=x+\frac{2}{3}-\frac{2}{3}  \\
  \frac{12}{3}  &=x=4
\end{align*}

\pause
The inverse of a function is the function that reverses its effect.
The inverse of $f(x)=3x$ is the function $f^{-1}(x)=(1/3)x$.
The inverse of $g(x)=x+(2/3)$ is the function $g^{-1}=x-(2/3)$.
\end{frame}


\begin{frame}{Definition of inverse function}
Let $f$ be a function with domain~$A$ and range~$B$.
The \alert{inverse function $f^{-1}\!$}, if it exists,
has domain~$B$ and range~$A$ and is given by 
\begin{equation*}
  f^{-1}(y)=x
  \qquad\text{where}\quad f(x)=y
\end{equation*}
so that their composition is the identity function:
$f(\,f^{-1}(x)\,)=x$ and $f^{-1}(\,f(x)\,)=x$. 

The inverse may well not exist; we will say more below.
A function that has an inverse is \alert{invertible}.

\pause\medskip
Note that the superscript does not mean that $f^{-1}(x)$ is $1/f(x)$.
It denotes the function that cancels~$f$\Dash we need the function that
cancels more often than we need the reciprocal.
\end{frame}



\begin{frame}{Finding the inverse function, if it exists}
\Ex Consider $f(x)=2x-18$.
Find the inverse function by writing $y=f(x)$ and solving to express~$x$
in terms of~$y$. 
\begin{align*}
  y &=2x-18   \\
  y+18 &=2x \\
  \frac{1}{2}y+9 &= x
\end{align*}
The inverse function is $f^{-1}(y)=(1/2)y+9$.

\pause Here is the check that they cancel.
\begin{equation*}
  f^{-1}(\,f(x)\,)
  =f^{-1}(2x-18)
  =\frac{1}{2}\cdot(2x-18)+9
  =(x-9)+9=x
\end{equation*}
\begin{equation*}
  f(\,f^{-1}(y)\,)
  =f(\frac{1}{2}y+9)
  =2\cdot(\frac{1}{2}y+9)-18
  =(y+18)-18=y
\end{equation*}

\pause\medskip
Note: another way to write the function ~$f^{-1}(y)=(1/2)y+9$ 
is as 
$f^{-1}(x)=(1/2)x+9$.
\end{frame}


\begin{frame}
\Ex Find the inverse of $f(x)=(3x+2)/(5x-1)$.
\pause
\begin{align*}
  y &=\frac{3x+2}{5x-1}  \\
  y(5x-1) &=3x+2  \\
  5xy-y   &=3x+2  \\
  5xy-3x &=y+2 \\
  x(5y-3) &=y+2 \\
  x &=\frac{y+2}{5y-3}
\end{align*}

(For $f$ the domain is all real numbers except $1/5$, and the range is all
real numbers except $3/5$.
For $f^{-1}$ these two are swapped.)

Find the inverses.
\begin{enumerate}
\item $f(x)=3x-4$
\item $f(x)=(x+4)/(2x-3)$
\end{enumerate}
\end{frame}



\begin{frame}{Which functions have an inverse?}
Consider these two.
When can we go from output back to input?
\begin{center}\small
  \begin{tabular}[t]{r|l}
    \multicolumn{1}{c}{\textit{Input~$x$}}
      &\multicolumn{1}{c}{\textit{Output~$f(x)$}}  \\ \hline
    $0$  &$10$  \\
    $1$  &$11$  \\
    $2$  &$12$  \\
  \end{tabular}
  \qquad
  \begin{tabular}[t]{r|l}
    \multicolumn{1}{c}{\textit{Input~$x$}}
      &\multicolumn{1}{c}{\textit{Output~$g(x)$}}  \\ \hline
    $0$  &$10$  \\
    $1$  &$12$  \\
    $2$  &$12$  \\
  \end{tabular}
\end{center}
\pause
With~$f$ on the left we can invert the relationship:~given an output we
can produce the associated input.
With $g$ we cannot.
\begin{center}\small
  \begin{tabular}[t]{r|l}
    \multicolumn{1}{c}{\textit{Input~$y$}}
      &\multicolumn{1}{c}{\textit{Output~$f^{-1}(y)$}}  \\ \hline
    $10$  &$0$  \\
    $11$  &$1$  \\
    $12$  &$2$  \\
  \end{tabular}
\end{center}
The problem for $g$ is that there are two inputs for the output~$12$.
That is, $f$ has that 
for every output there is at most one associated input, and $g$ does not.
\end{frame}

\begin{frame}{In pictures}
The function~$f$ is on the left and its inverse~$f^{-1}$ on the right.
\begin{center}
  \includegraphics{asy/inverse000.pdf}
  \qquad
  \includegraphics{asy/inverse004.pdf}
\end{center}
\end{frame}



\begin{frame}{Functions that have inverses are one-to-one functions}
A function is \alert{one-to-one} provided that if $x_1\neq x_2$ then
$f(x_1)\neq f(x_2)$.
Said another way, $f(x_1)=f(x_2)$ implies that $x_1=x_2$.  

\pause
So $f(x)=x^3$ is one-to-one  
but $g(x)=x^2$ is not, because $g(-1)=g(1)$.  
\begin{center}
   \vcenteredhbox{\includegraphics{asy/inverse003.pdf}}
   \qquad
   \vcenteredhbox{\includegraphics{asy/inverse002.pdf}}
\end{center}
\end{frame}


\begin{frame}{Verifying that a function is one-to-one}
To show that a function is not one-to-one we can produce an output~$y$
with more than one associated input~$x$.
Conversely, to show that a function is one-to-one we must show that there
is no such~$y$.

The \alert{horizontal line test} is that
a function is one-to-one if and only if no horizontal line intersects the
graph in more than one place.

For instance, functions that are always increasing are one-to-one.
So are functions that are always decreasing.
\begin{center}
   \vcenteredhbox{\includegraphics{asy/inverse005.pdf}}
   \qquad
   \vcenteredhbox{\includegraphics{asy/inverse006.pdf}}
\end{center}
\end{frame}



\begin{frame}{Graph of a function and its inverse}\vspace*{-1ex}
This is the relationship between a 
function and its inverse.
\begin{center}\small
  \begin{tabular}{c|c}
    \multicolumn{2}{c}{\textit{Function}}  \\
    \multicolumn{1}{c}{\textit{Input}}
      &\multicolumn{1}{c}{\textit{Output}}  \\ \hline
    $\vdotswithin{a}$ &$\vdotswithin{b}$ \\
    $a$  &$b$ \\
    $\vdotswithin{a}$ &$\vdotswithin{b}$ \\
  \end{tabular}
  \qquad
  \begin{tabular}{c|c}
    \multicolumn{2}{c}{\textit{Inverse}}  \\
    \multicolumn{1}{c}{\textit{Input}}
      &\multicolumn{1}{c}{\textit{Output}}  \\ \hline
    $\vdotswithin{b}$ &$\vdotswithin{a}$ \\
    $b$  &$a$ \\
    $\vdotswithin{b}$ &$\vdotswithin{a}$ \\
  \end{tabular}
\end{center}
From the graph of~$f$,
get the graph of $f^{-1}$ by reflecting across $y=x$. 
\begin{center}
  \only<1>{\includegraphics{asy/inverse013.pdf}}%    
  \only<2->{\includegraphics{asy/inverse014.pdf}}%    
\end{center}
\end{frame}




\section{Review of inverse trigonometric functions}
\begin{frame}{$\sin(x)$ and $\sin^{-1}(x)$}
The \alert{arcsine function $\sin^{-1}(x)$}
is the unique angle~$\theta$ in the interval $\closed{-\pi/2}{\pi/2}$
such that $\sin\theta = x$.

\begin{center}
   \vcenteredhbox{\includegraphics{asy/inverse007.pdf}}
   \qquad
   \vcenteredhbox{\includegraphics{asy/inverse008.pdf}}
\end{center}
We have to restrict the angle range or the inverse would not be a function.
\end{frame}

\begin{frame}{Angle ranges}
These are the angle ranges for the most often-used inverse trigonometric
functions.
\begin{center}
  \begin{tabular}[t]{r|ll}
    \multicolumn{1}{c}{\textit{Function}}
      &\multicolumn{1}{c}{\textit{Inverse}}
      &\multicolumn{1}{c}{\textit{Angle range}} \\ \hline
    $\sin \theta$ &$\sin^{-1} x$ &$\theta\in\closed{-\pi/2}{\pi/2}$\rule{0pt}{11pt} \\
    $\cos\theta$  &$\cos^{-1} x$ &$\theta\in\closed{0}{\pi}$ \\
    $\tan\theta$  &$\tan^{-1} x$ &$\theta\in\open{-\pi/2}{\pi/2}$ 
  \end{tabular}
\end{center}
The importance of the arctangent function is that in the picture below,
$\theta=\tan^{-1}(y/x)$.
\begin{center}
   \vcenteredhbox{\includegraphics{asy/inverse009.pdf}}  
\end{center}
\end{frame}


\section{Review of logarithmic functions}

\begin{frame}{Definition}
The inverse of the exponential function $f(x)=b^x$
is the \alert{base~$b$ logarithm $\log_b(x)$}.
\begin{equation*}
  b^x=y \quad\text{if and only if}\quad \log_b(y)=x
\end{equation*}
On the left is the exponential function for
the base $e\approx 2.718$.
On the right is its inverse, the 
\alert{natural logarithm $\ln(x)$}. 
\begin{center}
   \vcenteredhbox{\includegraphics{asy/inverse010.pdf}}
   \qquad
   \vcenteredhbox{\includegraphics{asy/inverse011.pdf}}
\end{center}
\end{frame}


\begin{frame}{Most important formulas}
  
The \alert{change of base}  formula
\begin{equation*}
  \log_b(x)=\frac{\ln x}{\ln b}
\end{equation*}
means that we can do all our work in the natural log. 
So the formulas below are expressed in $\ln$, although they work
for all $\log_b$.
\begin{itemize}
\item $\ln 1=0$
\item The logarithm of a product is the sum of the logarithms, 
  $\displaystyle \ln(x\cdot y)=\ln x+\ln y $
\item As a consequence, the log of a quotient is the difference of the
  logs, $\ln(x/y)=\ln x-\ln y $,
  which has the special case that the log of the reciprocal is the negative of 
  the log, $\displaystyle \ln(1/x)=-\ln x $.
  And, the log of a power is the power times the log, 
  $\ln(x^n)=n\cdot \ln x $.
\end{itemize}

\pause
Practice
\begin{enumerate}
\item $\log_3(27)$
\item $\ln(1/\sqrt{e})$
\item Solve for the unknown: $100=7e^{5t}$
\end{enumerate}
\end{frame}


\begin{frame}{Logarithms for computation}
By hand, multiplications are much harder than additions.
The formula $\displaystyle \ln(x\cdot y)=\ln x+\ln y$ means that we can 
do a multiplication as an addition.

Logarithms were invented in 1614 by J~Napier, who produced a set of tables
of trigonometric functions and their natural logarithms. 
These greatly simplified calculations in spherical trigonometry, which is 
central to navigation, and which typically include 
products of sines, cosines and other functions. 

In finance, computation of interest also requires multiplication.
Here is Scrooge and Bob Cratchit.
The book on Bob's desk is a table of logarithms.
\begin{center}
  \includegraphics[height=0.3\textheight]{pix/Bob-Cratchit.jpg}    
\end{center}
\end{frame}

\begin{frame}{Slide Rule}
It is easy to make a device to do addition.
The ruler on top is fixed and the one on the bottom slides.
To add $1+4$, 
start at the fixed~$1$ and slide until the red $0$ lines up with it.
Then read the answer above the red~$4$. 
\begin{center}
  \includegraphics{asy/inverse012.pdf}
\end{center}

\pause
Logarithms turn multiplication into addition,
$\ln(a\cdot b)=\ln(a)+\ln(b)$.
So change the markings on the above rulers by replacing the~$0$ with~$1$, 
because $\ln(0)=1$,
and mark $2$ at the location where $\ln(x)=2$, etc.
\begin{center}
  \includegraphics[height=0.25\textheight]{pix/slide_rule.jpg}
\end{center}
Here the C and D lines show that $2\cdot 3=6$.
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
