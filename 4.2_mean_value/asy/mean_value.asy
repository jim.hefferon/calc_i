// mean_value.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "mean_value%03d";

import graph;


// ==== MVT ====
picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

draw(pic, A--B, highlight_color);
path grph = A..(2.5,3)..(5,3.5)..B;
draw(pic, grph, FCNPEN);

filldraw(pic, circle(A,0.05), FCNPEN_COLOR, highlight_color);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), FCNPEN_COLOR, highlight_color);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== Tightrope ====
real f1(real x) {return 2+(1/10)*(x-2.5)^2;}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f1,xleft-0.1,xright+0.1), FCNPEN);

label(pic,graphic("tightrope1.png","height=0.50cm,angle=-20"),(0.5,2.7),
      filltype=NoFill);
filldraw(pic, circle((0.5,f1(0.5)),0.05), highlight_color, highlight_color);
label(pic, "A", (0.5,f1(0.5)), 2*S, highlight_color, filltype=Fill(white));
label(pic,graphic("tightrope.png","height=0.50cm,angle=30"),(4.1,2.54),
      filltype=NoFill);
filldraw(pic, circle((4.1,f1(4.1)),0.04), highlight_color, highlight_color);
label(pic, "B", (4.1,f1(4.1)), 2*S, highlight_color, filltype=Fill(white));


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ===== IVT ====
picture pic;
int picnum = 2;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=-4; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

filldraw(pic, circle((0.5,3),0.05), highlight_color, highlight_color);
label(pic, "$(a,f(a))$", (0.5,3), NE, black, filltype=Fill(white));
filldraw(pic, circle((5.5,-3),0.05), highlight_color, highlight_color);
label(pic, "$(b,f(b))$", (5.5,-3), SW, black, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .. with graph ...
picture pic;
int picnum = 3;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=-4; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);
path grf = (0.25,3){SE}..(2.5,-0.25){E}..(3.50,0.25){E}..{SE}(5.75,-3);
draw(pic, grf, highlight_color);

filldraw(pic, circle((0.25,3),0.05), highlight_color, highlight_color);
label(pic, "$(a,f(a))$", (0.25,3), NE, black, filltype=Fill(white));
filldraw(pic, circle((5.75,-3),0.05), highlight_color, highlight_color);
label(pic, "$(b,f(b))$", (5.75,-3), SW, black, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ==== MVT ====

// ... Just the two points and the slope ...
picture pic;
int picnum = 4;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

draw(pic, A--B, gray(0.7));
// path grph = A..(2.5,3)..(5,3.5)..B;
// draw(pic, grph, black);

filldraw(pic, circle(A,0.05), black,  black);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), black,  black);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic);


// .. Just the two points and the slope ..
picture pic;
int picnum = 5;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

draw(pic, A--B,  gray(0.7));
path grph = A..(2.5,1.95)..(5,1.75)..(B.x,1.25);
draw(pic, grph, highlight_color);

filldraw(pic, circle(A,0.05), black,  black);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), black,  black);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic);


// ... Fcn passes through both ...
picture pic;
int picnum = 6;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

draw(pic, A--B, gray(0.7));
path grph = A..(2.5,3)..(5,3.5)..B;
draw(pic, grph, highlight_color);

filldraw(pic, circle(A,0.05), highlight_color,  highlight_color);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), highlight_color,  highlight_color);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

path note1 = point(grph, 0.2)..(1,4);
draw(pic, note1, bold_color);
label(pic, "$f'(c_1)\geq m$", (1,4), (0.15,0.25), bold_color);
path note2 = point(grph, 1.4)..(4,4.5);
draw(pic, note2, bold_color);
label(pic, "$f'(c_2)\leq m$", (4,4.5), (0.15,0.25), bold_color);

label(pic,"$\displaystyle m=\frac{f(b)-f(a)}{b-a}$", (4.5,1.25), gray(0.7));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic);


// ... Fcn passes through both, showing c ...
picture pic;
int picnum = 7;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

draw(pic, A--B, gray(0.7));
path grph = A..(2.5,3)..(5,3.5)..B;
draw(pic, grph, black);

filldraw(pic, circle(A,0.05), black,  black);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), black,  black);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real c_time = dirtime(grph,dir(A--B));
pair c_pt = point(grph,c_time);
// pen Dots(pen p=currentpen) {return linetype(new real[] {0,2})+0.15*linewidth(p);}
// pen Dots=Dots();

// draw(pic,c_pt--(c_pt+dir(A--B)),Dots+highlight_color+linewidth(1.1));
// draw(pic,c_pt--(c_pt-dir(A--B)),Dots+highlight_color+linewidth(1.1));
draw(pic,c_pt--(c_pt+dir(A--B)),highlight_color);
draw(pic,c_pt--(c_pt-dir(A--B)),highlight_color);
filldraw(pic, circle(c_pt,0.05), highlight_color,  highlight_color);
// label(pic,  "$(c,f(c))$", c_pt, 3*N, filltype=Fill(white));

real[] T0 = {A.x, B.x, c_pt.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);
labelx(pic, "$x^*$", c_pt.x, highlight_color);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic);




// ===== Find intervals of increasing/decreasing ====
real f8(real x) {return x^2-2*x-3;}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=4;
ybot=-4; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f8,xleft-0.1,xright+0.1), FCNPEN);

// draw(pic, (1,ybot-0.25)--(1,ytop+0.25), dashed);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// == Rolle's Thm ========
picture pic;
int picnum = 9;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=4;

pair A = (1,3.5);
pair B = (6,3.5);

draw(pic, (0,A.y)--(B.x+0.75,B.y), highlight_color+dotted);
path grph = A{SE}..(2.5,1.5)..(3.5,2.5)..(5,3.75)..B;
draw(pic, grph, FCNPEN);

filldraw(pic, circle(A,0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)=f(b)$", A.y);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== MVT continued ====

// ... Just the two points and the slope ...
picture pic;
int picnum = 10;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=0; ytop=5;

pair A = (1,1.5);
pair B = (6,4.5);

// draw(pic, A--B, highlight_color);
// path grph = A..(2.5,3)..(5,3.5)..B;
// draw(pic, grph, black);

filldraw(pic, circle(A,0.05), highlight_color,  highlight_color);
// label(pic,  "$(a,f(a))$", A, 2*N, filltype=Fill(white));
filldraw(pic, circle(B,0.05), highlight_color,  highlight_color);
// label(pic,  "$(b,f(b))$", B, 2*N, filltype=Fill(white));

real[] T0 = {A.x, B.x};
real[] T0y = {A.y, B.y};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, 2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", A.x);
labelx(pic, "$b$", B.x);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0y, 2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$f(a)$", A.y);
labely(pic, "$f(b)$", B.y);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== Find intervals of increasing/decreasing ====
real f11(real x) {return x^3-3*x^2-9*x-1;}

picture pic;
int picnum = 11;
size(pic,0,4cm,keepAspect=true);
scale(pic,Linear(5),Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=4;
ybot=-32; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot, yStep=10);

draw(pic, graph(pic,f11,xleft-0.1,xright+0.1), FCNPEN);

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1.25pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ============= worksheet ===========


picture pic;
int picnum = 12;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,1);
pair b = (5.5, 4);
path f = a {NE}..(2.5,3.0)..{SE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, SE, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// .........................................
picture pic;
int picnum = 13;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,1);
pair b = (5.5, 4);
path f = a {NE}..(2.5,3.0)..(4,2.75)..{NE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, SE, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);





// .........................................
picture pic;
int picnum = 14;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,1);
pair b = (5.5, 4);
path f = a {SE}..(3.5,0.5).. {NE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, N, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);





// .........................................
picture pic;
int picnum = 15;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,4.5);
pair b = (5.5, 2);
path f = a {SE} ..(3.5,1.5).. {NE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, N, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// .........................................
picture pic;
int picnum = 16;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,4.5);
pair b = (5.5, 1);
path f = a {SE} ..(3,1.5)..(4,2.5).. {SE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, N, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// .........................................
picture pic;
int picnum = 17;
size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair a = (0.25,2.5);
pair b = (5.5, 2.5);
path f = a {SE} ..(1.5,2){NE}::(3,4)::{SE}(4,2).. {NE} b;
draw(pic, f, FCNPEN);
draw(pic, a--b, gray(0.5));

dotfactor = 6;
dot(pic, a, FCNPEN);
label(pic, "$(a,f(a))$", a, N, filltype=Fill(white));
dot(pic, b, FCNPEN);
label(pic, "$(b,f(b))$", b, N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);







// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

