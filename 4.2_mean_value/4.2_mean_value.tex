\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.2\ \ Mean Value Theorem}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}{Motivation: increasing and decreasing functions}
The prior section talks about what happens when $f'(c)=0$.
We call this a stationary point and 
we understand that for an instant there, the function is neither increasing nor
decreasing.

We want to extend to show that if $f'(x)>0$ then the function is increasing
while if $f'(x)<0$ then the function is decreasing.
To prove this we need the Mean Value Theorem.
\end{frame}


\begin{frame}{About continuity}
Recall this picture of continuity.
The high-wire walkers want to each get to the other's end before a minute 
passes.
By continuity, they must meet.
\begin{center}
  \includegraphics{asy/mean_value001.pdf}
\end{center}
This is a pure existence result, giving that there is a~$x$ without
saying what is the value of $x$, or giving a way to compute that value.

% A mathematical approach is to let $f(t)$ be the difference between
% $B$'s $x$-coordinate and $A$'s $x$-coordinate, $f(t)=b(t)-a(t)$.
% At $t=0$ we have $f(t)=5$.
% And at $t=1$ we have $f(t)=-5$.
% So there is a $c$ with $f(c)=0$.
\end{frame}

% \begin{frame}
% More generally, consider a continuous~$f\!$.
% Imagine we have inputs $a$ and~$b$ with $f(a)\geq f(b)$.
% The Intermediate Value Theorem says that
% for any output value~$y$ intermediate between $f(a)$ and
% $f(b)$ there is at least one input~$c$ between $a$ and~$b$ with $y=f(c)$.
% \begin{center}
%   \only<1>{\includegraphics{asy/mean_value002.pdf}}%
%   \only<2->{\includegraphics{asy/mean_value003.pdf}}
% \end{center}
% \only<2->{For instance take $y=0$. There are a number of $c$'s where
%   $f(c)=0$.}
% \end{frame}

% \begin{frame}
% Next consider a function whose graph passes through $(a,f(a))$ and 
% $(b,f(b))$.
% \begin{center}
%   \only<1>{\includegraphics{asy/mean_value010.pdf}}%
%   \only<2>{\includegraphics{asy/mean_value004.pdf}}%
%   \only<3->{\includegraphics{asy/mean_value005.pdf}}
% \end{center}
% \only<2>{We know the slope.
%   \begin{equation*}
%     m=\frac{f(b)-f(a)}{b-a}
%   \end{equation*}
% }
% \only<3->{If the function's derivative stays less 
%   than the slope of the line
%   then $f$'s graph cannot reach the second point.
%   Likewise $f$'s graph cannot reach the second point if the 
%   derivative is always more than the slope of the line.  
%   }
% \end{frame}


% \begin{frame}
% Therefore, if the graph connects the endpoints  
% \begin{center}
%   \includegraphics{asy/mean_value006.pdf}
% \end{center}
% then there must be a place where the derivative is greater than 
% or equal to the line's slope, and a place where the derivative is less
% than or equal to that slope.

% So, taking the derivative to be continuous there is a 
% \alert{mean value}, an input~$c$ 
% where the derivative equals that slope.
% \end{frame}


\begin{frame}{Mean Value Theorem}
Consider the average slope between the points.
\begin{equation*}
  \frac{\Delta y}{\Delta x}
  =\frac{f(b)-f(a)}{b-a}
\end{equation*}
Give a $x^*$ where $f'(x^*)=\Delta y/\Delta x$.
\begin{center}
  \includegraphics{asy/mean_value012.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/mean_value013.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/mean_value014.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/mean_value015.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/mean_value016.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/mean_value017.pdf}
\end{center}
\end{frame}




\begin{frame}{Mean Value Theorem}
\Tm If the function $f$ is continuous on $\closed{a}{b}$ and differentiable on 
$\open{a}{b}$ then there is an input $x^*$ with $a<x^*<b$ where this holds.
\begin{equation*}
  f'(x^*)=\frac{f(b)-f(a)}{b-a}
\end{equation*}
% Restated, we have $f(b)-f(a)=f'(c)\cdot(b-a)$.
Restated, if the average slope is $m$ then any function
between the two points must at some point have slope~$m$.
Here is one such input.
\begin{center}
  \includegraphics{asy/mean_value007.pdf}
\end{center}
It says there is such a $x^*$ but not how to find it.
\end{frame}

\begin{frame}\vspace*{-1ex}
To prove this we first prove a special case,
Rolle's Theorem.

\Tm Let $f$ be continuous on $\closed{a}{b}$ and differentiable on 
$\open{a}{b}$. 
If $f(a)=f(b)$ then there is a~$x^*$ with $a<x^*<b$ so that
$f'(x^*)=0$.
\begin{center}
  \includegraphics{asy/mean_value009.pdf}
\end{center}
\Pf
Since $\closed{a}{b}$ is closed, the continuous function~$f$ attains its
maximum and minimum on that interval.
If an optima happens at some $x^*$ with $a<x^*<b$ then Fermat's Theorem
gives that $f'(x^*)=0$.
Otherwise both optima happen at the ends, but $f(a)=f(b)$ so $f$ must be 
a constant function, so the difference quotient is zero for all 
$x^*$ with $a<x^*<b$, and so the derivative is zero everywhere. 
\end{frame}


\begin{frame}{Proof of the MVT}
\Tm If the function $f$ is continuous on $\closed{a}{b}$ and differentiable on 
$\open{a}{b}$ then there is an input $x^*$ with $a<x^*<b$ so that this holds.
\begin{equation*}
  f'(x^*)=\frac{f(b)-f(a)}{b-a}
\end{equation*}

\Pf
Take the equation of the line between the two points to be $y=mx+r$ where
\begin{equation*}
  m=\frac{f(b)-f(a)}{b-a}
\end{equation*}
and consider the difference between the curve and the line, 
$g(x)=f(x)-(mx+r)$.
This function satisfies the conditions of Rolle's Theorem 
because $g(a)=0$ and $g(b)=0$.
Thus there is a~$x^*$ with $a<x^*<b$ so that $g'(x^*)=0$.
But $g'(x)=f'(x)-m$ and so $f'(x^*)=m$.
\end{frame}


\section{Using the Mean Value Theorem}
\begin{frame}
\Co If $f'(x)=0$ for all inputs~$x$ in an interval $\open{a}{b}$
then $f(x)$ is a constant function on the interval: $f(x)=C$ for some
real number~$C$.

\Pf
Pick two numbers $x_1, x_2$ in the interval with $x_1<x_2$.
The Mean Value Theorem gives that there is a number~$x^*$ between them
so that
\begin{equation*}
  0=f'(x^*)=\frac{f(x_2)-f(x_1)}{x_2-x_1}
\end{equation*}
A fraction equals zero only when the numerator is zero so $f(x_2)=f(x_1)$.
Since this holds for any two numbers in the interval, $f$ is a constant function.

\pause\medskip
\Co If $f'(x)=g'(x)$ for all inputs~$x$ in an interval $\open{a}{b}$
then $f-g$ is constant on that interval: $f(x)=g(x)+C$ for some real number~$C$.

\Pf Apply the above theorem to $F(x)=f(x)-g(x)$.
\end{frame}


\begin{frame}{Sign of the Derivative}
\Tm
Let $f$ be differentiable on $\open{a}{b}$.
If $f'(x)>0$ for all inputs~$x$ in the interval then $f$ is an increasing
function on that interval.
If $f'(x)<0$ for all~$x$ in the interval then $f$ is decreasing
on that interval.

\Pf
We will do the $f'(x)>0$ case; the other case is similar.
Pick two numbers $x_1, x_2$ from the interval with $x_1<x_2$.
The MVT gives a~$x^*$ between them so that this holds.
\begin{equation*}
  f'(x^*)=\frac{f(x_2)-f(x_1)}{x_2-x_1}>0
\end{equation*}
Since the fraction's denominator is positive, its numerator must be 
positive also, so $f(x_2)>f(x_1)$.
Thus the function is increasing.
\end{frame}

\begin{frame}{Using those results}
\Ex
Let $f(x)=x^2-2x-3$.
Find the intervals on which it is
increasing and the intervals
on which it is decreasing.

\pause
The derivative is $f'(x)=2x-2=2(x-1)$.
% It is negative for inputs in the
% interval $\open{-\infty}{1}$, and
% positive for inputs in the interval $\open{1}{\infty}$.  
\begin{center}
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1}$  &$f'(0)=-2$  &$f$ is decreasing  \\
    $\open{1}{\infty}$   &$f'(2)=2$   &$f$ is increasing  
  \end{tabular}
\end{center}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/mean_value008.pdf}}
  % \begin{minipage}{0.45\textwidth}
  %   \RaggedRight
  %   The function is decreasing on $\open{-\infty}{1}$,
  %   and increasing on $\open{1}{\infty}$.
  % \end{minipage}
\end{center}
\end{frame}

\begin{frame}{Practice}
Let
$f(x)=x^3-3x^2-9x-1$.
Find the intervals where the function is increasing and where it is decreasing.

\pause
The derivative is $3(x-3)(x+1)$.
The stationary numbers are $-1$ and $3$, and we have three intervals.
\begin{center}\small
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial value}}   
      &\multicolumn{1}{c}{\textit{Conclusion}}   \\ \hline
    $\open{-\infty}{-1}$ &$f'(-2)=15$  &$f$ increasing \\
    $\open{-1}{3}$       &$f'(0)=-9$   &$f$ decreasing \\
    $\open{3}{\infty}$   &$f'(4)=15$   &$f$ increasing \\
  \end{tabular}
\end{center}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/mean_value011.pdf}}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 3*(x-3)*(x+1)
% ....: 
% sage: f_prime(-2)
% 15
% sage: f_prime(0)
% -9
% sage: f_prime(4)
% 15



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
