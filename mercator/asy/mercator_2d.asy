// volumes.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="pdf";
settings.render=0;
import fontsize;

import graph;

string OUTPUT_FN = "mercator_2d%03d";

real PI = acos(-1);



// ==== secant and cosine =========
real f0(real x) {return cos(x);}
real f0a(real x) {return 1/cos(x);}

picture pic;
int picnum = 0;
scale(pic, Linear(3), Linear);
size(pic,0,4cm);

real xleft=0; real xright=3.5;
real ybot=-5; real ytop=5;

path f = graph(pic, f0, xleft-0.1, PI+0.5);
draw(pic, f, gray(0.8));
path fa = graph(pic, f0a, xleft-0.1, PI/2-0.20);
draw(pic, fa, highlight_color);
path fa_mid = graph(pic, f0a, PI/2+0.20, PI+0.5);
draw(pic, fa_mid, highlight_color);
// path fa_rt = graph(pic,f0a, 3*PI/2+0.25, xright+0.1);
// draw(pic, fa_mid);

// vert asymptote at pi/2
draw(pic, Scale(pic,(PI/2,ybot))--Scale(pic,(PI/2,ytop)), dashed);

real[] T0 = {PI/2, PI}; 
xaxis(pic,"$x$",
      xmin=xleft-0.1,xmax=xright+0.1,
      ticks=RightTicks("%", T0, Size=2pt),
      Arrow(TeXHead));
labelx(pic, "$\pi/2$", PI/2, NE);
labelx(pic, "$\pi$", PI, NE);

Label L = Label("");
yaxis(pic,L,
      ymin=ybot-0.5, ymax=ytop+0.5,
      ticks=LeftTicks(Step=1,OmitTick(0), Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// .... integral secant ......
picture pic;
int picnum = 1;
scale(pic, Linear(4), Linear);
size(pic,0,5cm);

real xleft=0; real xright=PI/2+0.2;
real ybot=-0.45; real ytop=4;

path fa = graph(pic, f0a, xleft-0.1, PI/2-0.25);
draw(pic, fa, black);
path secant = graph(pic, f0a, 0, PI/2-0.25);
// path fa_mid = graph(pic, f0a, PI/2+0.25, PI+0.5);
// draw(pic, fa_mid, highlight_color);
// path fa_rt = graph(pic,f0a, 3*PI/2+0.25, xright+0.1);
// draw(pic, fa_mid);

real region_time = 70;
path region = ( (0,0)--point(secant,0)&subpath(secant,0,region_time)&point(secant,region_time)--(point(secant,region_time).x,0)--(0,0) )--cycle;
filldraw(pic, region, gray(0.9), highlight_color);
label(pic,"$D(\phi)$", (2,0.6), filltype=Fill(white));

// vert asymptote at x=pi/2
draw(pic, Scale(pic,(PI/2,ybot))--Scale(pic,(PI/2,ytop)), dashed);

real[] T0 = {PI/2}; 
xaxis(pic,"$s$",
      xmin=xleft-0.1,xmax=xright+0.1,
      ticks=RightTicks("%", T0, Size=2pt),
      Arrow(TeXHead));
labelx(pic, "$\pi/2$", PI/2, NE);
labelx(pic, "$\phi$", 0.6*PI/2, S);

yaxis(pic,Label(""),
      ymin=ybot-0.25, ymax=ytop+0.5,
      ticks=LeftTicks(Step=1,OmitTick(0), Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// .... horizontal scales ......
real sec(real x) {return 1/cos(x);}

picture pic;
int picnum = 2;
scale(pic, Linear(2), Linear);
size(pic,0,3cm);

real xleft=0; real xright=2.5;

real deg;
real rad;
for(int lat=0; lat < 9; ++lat) {
  rad = 10*lat*(PI/180);
  label(pic,format("\scriptsize\makebox[0pt][r]{%d\ }",lat*10), Scale(pic,(0,2*(lat+1))));
  draw(pic, Scale(pic,(0,2*(lat+1)))--Scale(pic,(sec(rad),2*(lat+1))), squarecap);
}
dot(pic,(-3,0),white);
 
xaxis(pic,"",
      xmin=0,xmax=sec(10*8*(PI/180))+1,
      // ticks=RightTicks("%", T0, Size=2pt),
      ticks=RightTicks(Step=1, Size=2pt),
      Arrow(TeXHead));
// yaxis(pic,"",
//       ymin=0,ymax=85,
//       ticks=RightTicks(Step=10, OmitTick(0), Size=2pt),
//       Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ======== flat map ==========
picture pic;
int picnum = 3;
// scale(pic, Linear(2), Linear);
size(pic,0,4cm);

real xleft = 0; real xright= 1;
real ybot = 0; real ytop= 1;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// equator
draw(pic, (xleft,0.5*ytop)--(xright,0.5*ytop), black);
// latitude line
draw(pic, (xleft,0.8*ytop)--(xright,0.8*ytop), highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);



// ======== preserve angles ==========

// .....  square on globe .....
 picture pic;
int picnum = 4;
// scale(pic, Linear(2), Linear);
unitsize(pic,1.5cm);

real xleft = 0; real xright= 0.8;
real ybot = 0; real ytop= 1;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, light_color+white, black);
// latitude line
draw(pic, (xleft,0.5)--(xright,0.5), black);
// path
draw(pic, (xleft,ybot)--(xright,ytop), highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);


// ...... too short on map .........
picture pic;
int picnum = 5;
// scale(pic, Linear(2), Linear);
unitsize(pic,1.5cm);

real xleft = 0; real xright= 0.8;
real ybot = 0; real ytop= 0.5;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// latitude line
draw(pic, (xleft,0.5*ytop)--(xright,0.5*ytop), black);
// path
draw(pic, (xleft,ybot)--(xright,ytop), highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);


// ...... too tall on map .........
picture pic;
int picnum = 6;
// scale(pic, Linear(2), Linear);
unitsize(pic,1.5cm);

real xleft = 0; real xright= 0.8;
real ybot = 0; real ytop= 1.5;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// latitude line
draw(pic, (xleft,0.5*ytop)--(xright,0.5*ytop), black);
// path
draw(pic, (xleft,ybot)--(xright,ytop), highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);


// ...... square on map .........
picture pic;
int picnum = 7;
// scale(pic, Linear(2), Linear);
unitsize(pic,1.5cm);

real xleft = 0; real xright= 0.8;
real ybot = 0; real ytop= 1.0;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// latitude line
draw(pic, (xleft,0.5*ytop)--(xright,0.5*ytop), black);
// path
draw(pic, (xleft,ybot)--(xright,ytop), highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);




// ======== Mercator's approximation ==========
picture pic;
int picnum = 8;
// scale(pic, Linear(2), Linear);
size(pic,0,4cm);

real xleft = 0; real xright= 1;
real ybot = 0; real ytop= 1;

// map
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// equator
real eq = 0.5*ytop;
draw(pic, (xleft,eq)--(xright,eq), black);
// latitude line
draw(pic, (xleft,0.8*ytop)--(xright,0.8*ytop), highlight_color);

real start_x = xright*(0.1);
real delta_phi = ytop*(0.05);
real delta_x = xright*(0.05);
path box0 = (start_x,eq)--(start_x+delta_x,eq)--(start_x+delta_x,eq+delta_phi)--(start_x,eq+delta_phi)--(start_x,eq);
transform box_t = shift(0,delta_phi);
draw(pic,box0);
draw(pic,box_t*box0);
label(pic, "$\vdots$", (start_x+(1/2)*delta_x, eq+4*delta_phi));
draw(pic,shift(0,(0.8*ytop-eq-delta_phi))*box0);

// label the heights, with curve connecting box to label
label(pic, "\makebox[0em][l]{$(\sec 0)\cdot 1/90$}", (start_x+0.15*xright, eq-0.85*delta_phi));
  draw(pic, (start_x+delta_x,eq+delta_phi/2){E}..{E}(start_x+0.15*xright, eq-0.85*delta_phi));
label(pic, "\makebox[0em][l]{$(\sec 1)\cdot 1/90$}", (start_x+0.15*xright, eq+0.85*delta_phi));
  draw(pic, (start_x+delta_x,eq+3*delta_phi/2){E}..{E}(start_x+0.15*xright, eq+0.85*delta_phi));
label(pic, "\makebox[0em][l]{$(\sec (\phi-1))\cdot 1/90$}", (start_x+0.15*xright, 0.8*ytop-0.85*delta_phi));
draw(pic, (start_x+delta_x,(0.8*ytop-(1/2)*delta_phi)){E}..{E}(start_x+0.15*xright,0.8*ytop-delta_phi));

shipout(format(OUTPUT_FN,picnum),pic);





// .... ln(sec x+tan x) ......
real deg_rad(real x) {return x*PI/180;}
real f9(real x) {return log((1/cos(deg_rad(x)))+tan(deg_rad(x)));}

picture pic;
int picnum = 9;
scale(pic, Linear, Linear(12));
size(pic,4cm,0);

real xleft=0; real xright=85;

// real deg;
// real rad;
// for(int lat=0; lat < 9; ++lat) {
//   rad = deg_rad(10*lat);
//   // label(pic,format("\scriptsize\makebox[0pt][r]{%d\ }",lat*10), Scale(pic,(0,2*(lat+1))));
//   draw(pic, Scale(pic,(0,2*(lat+1)))--Scale(pic,(sec(rad),2*(lat+1))), squarecap);
// }
// dot(pic,(-3,0),white);


path f = graph(pic, f9, xleft, xright);
draw(pic, f, highlight_color);
  
xaxis(pic,"",
      xmin=0,xmax=xright+2.5,
      // ticks=RightTicks("%", T0, Size=2pt),
      ticks=RightTicks(Step=10, step=5, OmitTick(0), Size=2pt, size=1pt),
      Arrow(TeXHead));
yaxis(pic,"",
      ymin=0,ymax=3.25,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// .... mercator bars ....
picture pic;
int picnum = 10;
scale(pic, Linear, Linear);
size(pic,0,5cm);

real xleft=0; real xright=1;
real ybot=0, ytop=1; 

// map
real eq = 0.5*ytop;
path edge = (xleft,ybot)--(xright,ybot)--(xright,ytop)--(xleft,ytop)--(xleft,ybot)--cycle;
filldraw(pic, edge, background_color, black);
// equator
draw(pic, (xleft,eq)--(xright,eq), black);
// latitude line
// draw(pic, (xleft,0.8*ytop)--(xright,0.8*ytop), highlight_color);

// latitude bars
real lat;
real max_lat = f9(85);
real vert_factor = ytop/(2*max_lat);
for(int i=0; i < 6; ++i) {
  lat = 15*i; 
  rad = lat*(PI/180);
  label(pic,format("\tiny\makebox[0pt][r]{%d\ }",15*i), Scale(pic,(0,vert_factor*f9(lat)+eq)));
  draw(pic, Scale(pic,(xleft,vert_factor*f9(lat)+eq))--Scale(pic,(xright,vert_factor*f9(lat)+eq)), highlight_color+squarecap);
}
dotfactor = 1;
dot(pic,(-0.07,0.1),white);
 
// xaxis(pic,"",
//       xmin=0,xmax=xright,
//       ticks=NoTicks,
//       Arrow(TeXHead));
// yaxis(pic,"",
//       ymin=0,ymax=85,
//       ticks=RightTicks(Step=10, OmitTick(0), Size=2pt),
//       Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// ==== integral secant again, with reimann boxes ===
picture pic;
int picnum = 11;
scale(pic, Linear(4), Linear);
size(pic,0,5cm);

real xleft=0; real xright=PI/2+0.2;
real ybot=-0.45; real ytop=4;

path fa = graph(pic, f0a, xleft-0.1, PI/2-0.25);
draw(pic, fa, black);
path secant = graph(pic, f0a, 0, PI/2-0.25);
// path fa_mid = graph(pic, f0a, PI/2+0.25, PI+0.5);
// draw(pic, fa_mid, highlight_color);
// path fa_rt = graph(pic,f0a, 3*PI/2+0.25, xright+0.1);
// draw(pic, fa_mid);

real region_time = 70;
path region = ( (0,0)--point(secant,0)&subpath(secant,0,region_time)&point(secant,region_time)--(point(secant,region_time).x,0)--(0,0) )--cycle;
filldraw(pic, region, gray(0.9), highlight_color);

// vert asymptote at x=pi/2
draw(pic, Scale(pic,(PI/2,ybot))--Scale(pic,(PI/2,ytop)), dashed);


// reimann boxes
for (int boxno=0; boxno<5; ++boxno) {
  real x = boxno*PI/90;
  draw(pic, Scale(pic,(x,0))--Scale(pic,(x,f0a(x)))--Scale(pic,(x+pi/90,f0a(x+PI/90)))--Scale(pic,(x+PI/90,0))--cycle, bold_color);
}

real[] T0 = {PI/2}; 
xaxis(pic,"$s$",
      xmin=xleft-0.1,xmax=xright+0.1,
      ticks=RightTicks("%", T0, Size=2pt),
      Arrow(TeXHead));
labelx(pic, "$\pi/2$", PI/2, NE);
labelx(pic, "$\phi$", 0.6*PI/2, S);

yaxis(pic,Label(""),
      ymin=ybot-0.25, ymax=ytop+0.5,
      ticks=LeftTicks(Step=1,OmitTick(0), Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




