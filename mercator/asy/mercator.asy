// volumes.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="png";
settings.render=16;
import fontsize;

import graph3; import solids;
currentprojection = orthographic(5,2,1.5,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "mercator%03d";

material figure_material = material(diffusepen=light_color+opacity(0.60),
				 emissivepen=light_color,
				 specularpen=white+light_color);
material slice_material = material(diffusepen=highlight_color+white+opacity(0.5),
				   emissivepen=highlight_color+white,
				   specularpen=highlight_color+white);
material cylinder_material = material(diffusepen=background_color+opacity(0.40),
				      emissivepen=background_color,
				      specularpen=background_color);


currentlight = nolight;



// ==== sphere =========
picture pic;
int picnum = 0;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);


draw(pic, unitsphere, surfacepen=figure_material);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
// add a circular edge
draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


//  ......... get radius of latitude circle ........
picture pic;
int picnum = 1;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

draw(pic, unitsphere, surfacepen=figure_material);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

path3 radius_path = O--(0,r,z); 
path3 angle_mark_circle = rotate(90, Y)*scale(0.3, 0.3, 1)*unitcircle3;
// draw(pic, angle_mark_circle);
real angle_mark_circle_axis_time = intersect(angle_mark_circle, O--Y)[0];
real angle_mark_circle_radius_path_time = intersect(angle_mark_circle, radius_path)[0];
draw(pic,"$\phi$",
     subpath(angle_mark_circle,
	     angle_mark_circle_axis_time,
	     angle_mark_circle_radius_path_time),
     highlight_color,
     Arrow3(size=4)); 
draw(pic, radius_path--(0,0,z)--O--cycle, highlight_color);
dotfactor = 4;
dot(pic, "$(0,\cos\phi, z)$", (0,r,z), highlight_color);

transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
// add a circular edge
draw(pic, slice_t*unitcircle3, black);

// draw(pic,(0,0,0)--(0,0,z), highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.25, black, Arrow3(TeXHead2));
  
shipout(format(OUTPUT_FN,picnum),pic);





// ==== sphere and cylinder =========
real map_height = 2.5;

picture pic;
int picnum = 2;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

draw(pic, unitsphere, surfacepen=figure_material);
draw(pic, shift(0,0,-map_height/2)*scale(1,1,map_height)*unitcylinder, surfacepen=cylinder_material);
draw(pic, unitcircle3);  // equator

// transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // add a circular edge
// draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.75, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



//... include lines of latitude .......
picture pic;
int picnum = 3;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

draw(pic, unitsphere, surfacepen=figure_material);
// draw(pic, shift(0,0,-map_height/2)*scale(1,1,map_height)*unitcylinder, surfacepen=cylinder_material);
// draw(pic, shift(0,0,z*1.5)*unitcircle3, highlight_color);

transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// add a circular edge
draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);





//... include lines of latitude .......
picture pic;
int picnum = 4;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// Draw the latitude slice
real z0 = 2/3;
real r0 = sqrt(1-z0^2);
real z1 = z0+.1;
real r1 = sqrt(1-z1^2);

draw(pic, unitsphere, surfacepen=figure_material);

transform3 slice_t0 = shift((0,0,z0))*scale(r0,r0,1); 
surface slice0 = slice_t0*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// add a circular edge
path3 lat0 = slice_t0*unitcircle3; 
draw(pic, lat0, gray(0.9));
transform3 slice_t1 = shift((0,0,z1))*scale(r1,r1,1); 
surface slice1 = slice_t1*unitdisk;
path3 lat1 = slice_t1*unitcircle3; 
draw(pic, lat1, gray(0.9));

transform3 long0_t = rotate(55, Z)*rotate(90, X); 
path3 long0 = long0_t*unitcircle3;
transform3 long1_t = rotate(65, Z)*rotate(90, X); 
path3 long1 = long1_t*unitcircle3; 
draw(pic, long0, gray(0.9));
draw(pic, long1, gray(0.9));

real long_time_00 = intersections(long0, slice0)[0][0];
real long_time_01 = intersections(long0, slice1)[0][0];
real long_time_11 = intersections(long1, slice1)[0][0];
real long_time_10 = intersections(long1, slice0)[0][0];

real lat_time_00 = intersections(lat0,surface(scale(2,2,2)*long0))[0][0];
real lat_time_01 = intersections(lat0,surface(scale(2,2,2)*long1))[0][0];
real lat_time_11 = intersections(lat1,surface(scale(2,2,2)*long1))[0][0];
real lat_time_10 = intersections(lat1,surface(scale(2,2,2)*long0))[0][0];

// dot(pic, point(lat0, lat_time_00));
// dot(pic, point(lat0, lat_time_01));
// dot(pic, point(lat1, lat_time_11));
// dot(pic, point(lat1, lat_time_10));

path3 sq = ( subpath(long0,long_time_00,long_time_01) &
  subpath(lat1,lat_time_10,lat_time_11) &
  subpath(long1,long_time_11,long_time_10) &
	     subpath(lat0,lat_time_01,lat_time_00) ) -- cycle;
draw(pic, surface(sq), slice_material);
draw(pic, sq, highlight_color);


xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ==== radial projection =========
real map_height = 2.5;

picture pic;
int picnum = 5;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

draw(pic, unitsphere, surfacepen=figure_material);
draw(pic, shift(0,0,-map_height/2)*scale(1,1,map_height)*unitcylinder, surfacepen=cylinder_material);
draw(pic, unitcircle3);  // equator

draw(pic, O--((1,2,2)*0.8), highlight_color);
dotfactor = 4;
dot(pic, (1/3,2/3,2/3));
dot(pic, (1/sqrt(5),2/sqrt(5),2/sqrt(5)), highlight_color);

// transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // add a circular edge
// draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.75, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ==== mercator projection =========
real map_height = 2;

picture pic;
int picnum = 6;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// Draw the latitude slice
real z = 2/3;
real r = sqrt(1-z^2);

draw(pic, unitsphere, surfacepen=figure_material);
draw(pic, shift(0,0,-map_height/2)*scale(1,1,map_height)*unitcylinder, surfacepen=cylinder_material);
//draw(pic, unitcircle3);  // equator

// draw(pic, O--((1,2,2)*0.8), highlight_color);
// dotfactor = 4;
// dot(pic, (1/3,2/3,2/3));
// dot(pic, (1/sqrt(5),2/sqrt(5),2/sqrt(5)), highlight_color);

transform3 slice_t = shift((0,0,z))*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// add a circular edge
path3 sphere_lat = slice_t*unitcircle3; 
draw(pic, sphere_lat, black);
path3 map_lat = shift((0,0,z))*unitcircle3; 
draw(pic, map_lat, black);
real lat_time = 0.85;
draw(pic, point(sphere_lat, lat_time)--point(map_lat, lat_time), highlight_color);
dotfactor = 4;
dot(pic, point(sphere_lat, lat_time), black);
dot(pic, point(map_lat, lat_time), highlight_color);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
       0,1.75, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


















// // .... slice of sphere ........
// real f3(real x) {return sqrt(1-x^2);}

// picture pic;
// int picnum = 3;
// size(pic,0,5cm);
// size3(pic,0,5cm,0,keepAspect=true);


// // draw(pic, unitsphere, surfacepen=figure_material);


// real y = 2/3;
// real r = sqrt(1-y^2);
// // draw outline of the sphere
// path sphere_pth = graph(pic, f3, 0, 1);
// draw(pic, rotate(-90, Y)*path3(sphere_pth), gray(0.80));
// dot(pic, "$(0,y,\sqrt{1-y^2})$", (0,y,r), NE);
// // draw slice 
// transform3 slice_t = shift((0,y,0))*rotate(90,X)*scale(r,r,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // draw(pic, slice, surfacepen=highlight_color+white+opacity(0.75));
// draw(pic, slice_t*unitcircle3, highlight_color);
// // draw radius
// draw(pic, (0,y,0)--(0,y,r));

// dotfactor=4;
// dot(pic, "$y$",(0,y,0),NE);

// xaxis3(pic,Label("$x$",position=EndPoint, align=W),
//        0,3, black, Arrow3(TeXHead2));
// yaxis3(pic,Label(""),
//        0,1.5, black, Arrow3(TeXHead2));
// zaxis3(pic,Label("$z$"),\
//        0,1.5, black, Arrow3(TeXHead2));

  
// shipout(format(OUTPUT_FN,picnum),pic);

