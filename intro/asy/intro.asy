// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "intro%03d";

import graph;


// ===== Orbit of earth is an ellipse ====
picture pic;
int picnum = 0;
size(pic,7.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-4; ytop=4;

path orbit = scale(4,3)*unitcircle;
draw(pic,orbit,highlight_color);

pair earth_point = point(orbit,1.15);

real focus_dist = 2.25;
pair focus_sun = (focus_dist,0); 
pair focus_other = (-1*focus_dist,0); 

draw(pic,focus_sun--earth_point--focus_other,light_color);


label(pic,graphic("sun.png","width=0.50cm"),focus_sun,
      filltype=Fill(white));
filldraw(pic, circle(focus_other,0.05), white, black);
label(pic,graphic("earth.png","width=0.20cm"), earth_point,
      filltype=Fill(white));

xaxis(pic,axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
xaxis(pic,axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic,axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));
yaxis(pic,axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN));  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Kepler's Second Law ====
picture pic;
int picnum = 1;
size(pic,5.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-4; ytop=4;


real focus_dist = 2.25;
pair focus_sun = (focus_dist,0); 
pair focus_other = (-1*focus_dist,0); 

path orbit = scale(4,3)*unitcircle;
real[] orbit_times = {0, 0.45, 0.75, 
  1.1, 1.45, 1.75,
  2, 2.25, 2.55,
  2.9, 3.20, 3.55  };   // times for monthly pts on orbit
orbit_times.cyclic = true;  
// for (real t : orbit_times) {
for (int dex=0; dex<orbit_times.length; ++dex) {
  real orbit_time = orbit_times[dex];
  real orbit_time_prior = orbit_times[dex-1];
  pair orbital_pt = point(orbit, orbit_time);
  pair orbital_pt_prior = point(orbit, orbit_time_prior);
  // for help drawing: label(pic, format("%f",orbit_time), orbital_pt,E);
  path swept_out = buildcycle(orbital_pt--focus_sun,
			      focus_sun--orbital_pt_prior,
			      subpath(orbit,orbit_time_prior,orbit_time));
  fill(pic, swept_out, cmyk(background_color)*(1-(dex/(orbit_times.length-1))));
  draw(pic,orbital_pt--focus_sun, highlight_color);
}
draw(pic,orbit,FCNPEN);
for (real t : orbit_times) {  // draw monthly circles
  pair orbital_pt = point(orbit, t);
  filldraw(pic, circle(orbital_pt, 0.05), white, black);  
}
// draw(pic,focus_sun--earth_point--focus_other,light_color);

// Draw the sun
label(pic,graphic("sun.png","width=0.50cm"),focus_sun, 
      filltype=Fill(white));  // letting transparency work looks cramped
// filldraw(pic, circle(focus_other,0.05), white, black);
pair earth_point = point(orbit,1.20);
label(pic,graphic("earth.png","width=0.20cm"), earth_point); 
//       filltype=Fill(white)); // transparency seems OK here

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
