\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}
% \usepackage{enumitem} % resume numbering of enumerated exercises 

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Derivatives] % (optional, use only with long paper titles)
{Derivatives}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Rates of growth}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}
\frametitle{Introduction}  
Calculus is the language of science because
it is the language that we use to express change.

To start understanding how functions change
we will first see how 
changes in their inputs result in
changes in their outputs.
\end{frame}


\section{Linear functions}

\begin{frame}
\frametitle{Growth rate of line functions}

A repair person charges \$$75$ to come, and \$$100$~per hour.
\pause
\begin{center} \small
  \begin{tabular}{r|r}  
   \multicolumn{1}{c}{\begin{tabular}{@{}c@{}}\textit{time} \\ 
                                         $t$ \end{tabular}}  
   &\multicolumn{1}{c}{\begin{tabular}{@{}c@{}} \textit{cost} \\ 
                                         $C(t)$ \end{tabular}}  
    \\ \hline
       0              &75     \\
       0.5            &125     \\
       1.0            &175     \\ 
       1.5            &225     \\ 
       2.0            &275     \\ 
       2.5            &325      
  \end{tabular}
  \hspace*{3em}
  \vcenteredhbox{\includegraphics[height=1.0in]{repair.jpg}}
\end{center}

In the first hour, this is the growth rate.
\pause 
\begin{equation*}
 \frac{175-75}{1.0-0}=100 
\end{equation*}
Between $t=1.5$ and $t=2.0$ this is the growth rate.
\pause
\begin{equation*}
 \frac{275-175}{2.0-1.5}=1000 
\end{equation*}
A line function such as 
$c(t)=100t+75$ has a fixed rate of change.
\end{frame}


\begin{frame}
In high school we call that fixed rate of change the slope.
Usually we plot the input on the horizontal axis and the output on
the vertical.
The slope compares the input to the output.
\begin{center}
  \includegraphics{asy/derivatives000.pdf}    
\end{center}
\end{frame}


\begin{frame}{Some slopes}
\begin{center}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives001.pdf} \\ $\text{slope}=0.25$
  \end{tabular}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives002.pdf} \\ $\text{slope}=0.50$
  \end{tabular}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives003.pdf} \\ $\text{slope}=0.75$
  \end{tabular}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives004.pdf} \\ $\text{slope}=1.0$
  \end{tabular}
\end{center}
\begin{center}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives005.pdf} \\ $\text{slope}=1.5$
  \end{tabular}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives006.pdf} \\ $\text{slope}=2.0$
  \end{tabular}
  \begin{tabular}{c}
    \includegraphics{asy/derivatives007.pdf} \\ $\text{slope}=3.0$
  \end{tabular}
\end{center}
\begin{center}  % sage: n(arctan(3.0)*180/pi)
  \begin{tabular}{c|*{7}{c}}
    \textit{Slope} &$0.25$ &$0.50$ &$0.75$ &$1.0$ &$1.5$ &$2.0$ &$3.0$ \\
    \cline{2-8}
    \textit{Degrees} &$14.0$ &$26.6$ &$30.9$ &$45.0$ &$56.3$ &$63.4$ &$71.6$ 
  \end{tabular}
\end{center}
\end{frame}







\begin{frame}
\frametitle{Growth rate of non-line functions}
Functions that don't graph as lines don't have fixed rates of change.  
We will have a number of ways to understand the rate of change.

\begin{enumerate}
\item Slope of the line tangent to the curve.
\item Speed of object whose motion is given by the function.
\item Spreading.
\end{enumerate}
\end{frame}


\begin{frame}{Rate of change as the slope of the tangent line}
\begin{center}
  \only<1>{\includegraphics{asy/derivatives009.pdf}}%    
  \only<2>{\includegraphics{asy/derivatives010.pdf}}%    
  \only<3>{\includegraphics{asy/derivatives011.pdf}}%    
  \only<4->{\includegraphics{asy/derivatives012.pdf}}%    
\end{center}
\begin{center}
  \begin{tabular}{lc|c}
    \multicolumn{2}{c}{\textit{Inputs}}
      &\multicolumn{1}{c}{\textit{Rate of change}} \\ \hline
    \uncover<2->{$2$ &$1$       &$\frac{4.5-3}{2-1}=1.5$} \\
    \uncover<3->{$1.1$ &$1$     &$\frac{3.195-3}{1.1-1}=1.95$} \\
    \uncover<4->{$1.01$ &$1$    &$\frac{3.0019995-3}{1.01-1}=1.999499$} \\
    \uncover<4->{$1.001$ &$1$   &$\frac{3.000199995-3}{1.001-1}=1.999950$} 
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Estimate the plane's rate of climb}
\begin{center}
  \includegraphics{asy/derivatives008.pdf}    
\end{center}
Estimate the slope at the given points.
Use these.
\only<2->{(Ans: $-1/4$, $2$).}
\begin{center}
  \begin{tabular}{*{7}{c}}
    $0.25$ &$0.5$ &$0.75$ &$1.0$ &$1.5$ &$2.0$ &$3.0$  \\
    \includegraphics[scale=0.55]{asy/derivatives001.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives002.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives003.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives004.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives005.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives006.pdf}
    &\includegraphics[scale=0.55]{asy/derivatives007.pdf}
  \end{tabular}
\end{center}
\end{frame}







\begin{frame}{The speed of a falling body}
Drop a ball from $100$~meters above the Earth's surface and its
height after $x$~seconds will be $y=100- 4.9 x^2$.
\begin{center}\small
  \vcenteredhbox{\includegraphics[height=0.75in]{pisa.png}}
  \hspace*{3em}
  \begin{tabular}{c|c}  
   \multicolumn{1}{c}{\begin{tabular}{@{}c@{}} 
                             \textit{time} \\ 
                             $x$ 
                      \end{tabular}}  
   &\multicolumn{1}{c}{\begin{tabular}{@{}c@{}}
                          \textit{height}  \\ 
                          $h(x)$
                       \end{tabular}}  
    \\ \hline
    $1$    &$95.1$   \\
    $2$    &$80.4$   \\
    $3$    &$55.9$   \\
    $4$    &$21.6$   
  \end{tabular}
\end{center}
Find the rate of change in the first second (in meters per second).
\pause
\begin{equation*}
  \frac{95.1-100}{1.0-0}=-4.9
\end{equation*}
\pause
Find the rate of change in the next second.
\pause
\begin{equation*}
  \frac{80.4-95.1}{2.0-1.0}=-14.7
\end{equation*}
This function does not have a constant rate of change.
The change changes.
\end{frame}




\begin{frame}{Application: finance}
This is the Dow Jones average over time.
\begin{center}
  \includegraphics[scale=0.4]{dowjones.png}
\end{center}
The scale is tricky since the units of thousands per week is
not natural but obviously the rate changes, and is of great interest.
% \only<2->{(Ans: $-0.0005$, $0.02$).}
% % sage: (33134.92-33152.55)/33152.55
% % -0.000531784131235898
% % sage: (21406.93-20956.19)/20956.19
% % 0.0215086807287012
% \begin{center}
%   \begin{tabular}{*{7}{c}}
%     $0.25$ &$0.5$ &$0.75$ &$1.0$ &$1.5$ &$2.0$ &$3.0$  \\
%     \includegraphics[scale=0.55]{asy/derivatives001.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives002.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives003.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives004.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives005.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives006.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives007.pdf}
%   \end{tabular}
% \end{center}
\end{frame}




\begin{frame}{Application: Public health}
  This is the CDC's age-height chart for boys.
  Do $x=3$ and $x=27$.
\begin{center}
  \includegraphics[scale=0.425]{cdcboys.png}
\end{center}
Again the units of the boxes are a little tricky, but again
the rate changes and is of great interest.
% \begin{center}
%   \begin{tabular}{*{7}{c}}
%     $0.25$ &$0.5$ &$0.75$ &$1.0$ &$1.5$ &$2.0$ &$3.0$  \\
%     \includegraphics[scale=0.55]{asy/derivatives001.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives002.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives003.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives004.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives005.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives006.pdf}
%     &\includegraphics[scale=0.55]{asy/derivatives007.pdf}
%   \end{tabular}
% \end{center}
\end{frame}


\begin{frame}{Summary}
  So our first impression of Calculus has two aspects.
  \begin{itemize}
  \item It is a way of computing rates of change.
  \item It is a way of thinking about the world that
    experience shows is very fruitful.
  \end{itemize}
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
