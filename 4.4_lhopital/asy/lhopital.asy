// lhopital.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "lhopital%03d";
real PI = acos(-1);

import graph;


// === x^2 racing ahead of x =========

// ... graph comparing two fcns
real f0(real x) {return x^2;}
real f0a(real x) {return x;}

picture pic;
int picnum = 0;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-0.01; xright=0.2;
ybot=-0.01; ytop=0.12;

draw(pic, graph(f0a,0,xright+0.02), FCNPEN);
draw(pic, graph(f0,0,xright+0.02), FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=0-0.02, xmax=xright+0.0491,
      p=currentpen,
      ticks=RightTicks(Step=0.1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-.01, ymax=ytop+0.1,
      p=currentpen,
      ticks=LeftTicks(Step=0.1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== ln(x) ====
real f1(real x) {return log(x);}

picture pic;
int picnum = 1;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-3; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f1,xleft+0.05,xright+0.1), FCNPEN);
label(pic,"$\ln(x)$", (2,-1.5),filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);





// picture pic;
// int picnum = 0;
// size(pic,3cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

// draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

