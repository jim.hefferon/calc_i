\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.4\ \ L'H\^{o}pital's Rule}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{How is $\lim_{x\to k}n(x)/d(x)$ determined by 
the two limits?}

Let's look at the cases for this limit. 
\begin{equation*}
  \lim_{x\to k}\frac{n(x)}{d(x)}
\end{equation*}
\pause
The easiest case is given by the Limit Rules.
\begin{equation*}
  \lim_{x\to 3}\frac{2x+4}{x^2-7}
  =
  \frac{\lim_{x\to 3}2x+4}{\lim_{x\to 3}x^2-7}
  =
  \frac{10}{2}
  =5
\end{equation*}
\pause
Here the limit of the numerator is $\infty$ while the limit of the
denominator is $7$.
\begin{equation*}
  \lim_{x\to \infty}\frac{x^2+x}{7+(1/x)}
  =+\infty
\end{equation*}
\pause
Here the limit of the numerator is $1$ while the limit of the
denominator is~$0$.
\begin{equation*}
  \lim_{x\to 0+}\frac{1}{x}=+\infty
  \qquad
  \lim_{x\to 0-}\frac{1}{x}=-\infty
\end{equation*}
\end{frame}

\begin{frame}
This is many of the cases for $\lim_{x\to k}n(x)/d(x)$, where
$a=\lim_{x\to k}n(x)$ and $b=\lim_{x\to k}d(x)$ (for
$k\in\R\cup\set{\infty,-\infty}$).
\begin{center}
\begin{tabular}{r|ccc}
  \multicolumn{1}{r}{\ }
    &\multicolumn{1}{c}{\tabulartext{$b\neq 0$ finite}}
    &\multicolumn{1}{c}{\tabulartext{$b= 0$}}
    &\multicolumn{1}{c}{\tabulartext{$b=\pm\infty$}}
    \\ \cline{2-4}
  \tabulartext{$a\neq 0$ finite}
    &$a/b$        &$\pm\infty$    &$0$  \\
  \tabulartext{$a= 0$}
    &$0$          &Indeterminate  &$0$  \\
  \tabulartext{$a=\pm\infty$}
    &$\pm\infty$  &$\pm\infty$    &Indeterminate  
\end{tabular}
\end{center}
In some of these cases we can have a different result for 
$x\to k^+$ than for $x\to k^-\!$.

`Indeterminate' means that anything can happen; see the next slide.
\end{frame}



\begin{frame}{With `Indeterminate' limits anything can happen}
There are $0/0$ limits that go to~$0$, 
there are $0/0$ limits that go to~$\infty$, 
and there are $0/0$ limits that go to any number in between.
\begin{center}
  \begin{tabular}{cc|c}
    \multicolumn{1}{c}{$n(x)$}
       &\multicolumn{1}{c}{$d(x)$}
       &\multicolumn{1}{c}{$\lim_{x\to 0}n(x)/d(x)$}  \\ \hline
    \rule{0pt}{10pt}
    $x^2$  &$x$  &$0$  \\ 
    $x$  &$x^2$  &$\infty$  \\ 
    $5x$  &$x$  &$5$  \\ 
  \end{tabular}
\end{center}
% In a fraction, $n(x)\to 0$ tends to bring the fraction as a whole
% to~$0$.
% And $d(x)\to 0$ tends to bring the fraction as a whole to~$\pm\infty$.

\pause
Similarly,
there are $\infty/\infty$ limits that go to~$0$, 
there are $\infty/\infty$ limits that go to~$\infty$, 
and there are $\infty/\infty$ limits that go to any number in between.
\begin{center}
  \begin{tabular}{cc|c}
    \multicolumn{1}{c}{$n(x)$}
       &\multicolumn{1}{c}{$d(x)$}
       &\multicolumn{1}{c}{$\lim_{x\to 0}n(x)/d(x)$}  \\ \hline
    \rule{0pt}{10pt}
    $1/x$  &$1/x^2$  &$0$  \\ 
    $1/x^2$  &$1/x$  &$\infty$  \\ 
    $1/x$  &$1/5x$  &$5$  \\ 
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Intuition: tension between the functions}
This will help us understand $0/0$ limits.
\begin{equation*}
  \lim_{x\to 0^+}\frac{x^2}{x}
\end{equation*}
\pause
We could cancel the $x$'s to get $\lim_{x\to 0+}x=0$. 
We instead pause to reflect on the relationship 
between the numerator and denominator.
In a fraction limit, $n(x)\to 0$ tends to make the fraction as a whole
go to zero.
And $d(x)\to 0$ tends to make the fraction go to infinity.
\pause
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$d(x)=x$}
      &\multicolumn{1}{c}{$n(x)=x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}%
    $0.1$   &$0.01$       &$1/10$ \\
    $0.01$  &$0.000\,1$   &$1/100$ \\
    $0.001$ &$0.000\,001$ &$1/1\,000$ \\
  \end{tabular}
  \qquad
  \vcenteredhbox{\includegraphics{asy/lhopital000.pdf}}
\end{center}
Overall this fraction goes to zero.
In this contest the numerator,~$x^2$, has a greater growth than the 
denominator, $x$.
Both go to zero but $x^2$ is racing there faster.
\end{frame}


\begin{frame}{Wait a minute professor \ldots}
\begin{center}
  \includegraphics[height=0.3\textheight]{pix/professor.jpg}    
\end{center}
``The table starts when $d(x)$ 
has a height of $0.1$.
How can $n(x)$ `race ahead'?
I don't there is room for anybody to race ahead.''


\pause
This is a place where the table of numbers tells the story better than the 
graph picture.
The table says:~first $x^2$ is at $1/10$-th the height.
Then it is at $1/100$-th the height, and
then $1/1000$-th.
They both go to zero but in a way such that the numerator
forever gains on the denominator. 
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$x$}
      &\multicolumn{1}{c}{$x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}
    $0.1$   &$0.01$       &$1/10$ \\
    $0.01$  &$0.000\,1$   &$1/100$ \\
    $0.001$ &$0.000\,001$ &$1/1\,000$ \\
  \end{tabular}
\end{center}
\end{frame}



\begin{frame}{Similar tension, now $\infty/\infty$}
Similarly we could cancel the $x$'s to get 
$\lim_{x\to \infty}x=\infty$.
\begin{equation*}
  \lim_{x\to \infty}\frac{x^2}{x}
\end{equation*}
Here also we will pause to reflect.
In a fraction limit, $n(x)\to \infty$ tends to make the fraction as a whole
go to infinity.
And $d(x)\to \infty$ tends to make the fraction go to zero.

\pause
The winner is the numerator, $x^2\!$.
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$d(x)=x$}
      &\multicolumn{1}{c}{$n(x)=x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}
    $10$     &$100$         &$10$ \\
    $100$    &$10\,000$     &$100$ \\
    $1\,000$ &$1\,000\,000$ &$1\,000$ \\
  \end{tabular}
\end{center}
Both go to infinity but $x^2$ is racing ahead of~$x$.
(The infinite limits context has the advantage that 
there is no unease, however mistaken, about `room'.)
\end{frame}




% \begin{frame}
% We have developed the intuition that these indeterminate forms are
% resolved by comparing the rates of growth of the two functions.  
% How to use the rate information, $n'(x)$ and $d'(x)$, to figure out the
% limits?

% \end{frame}


\begin{frame}{L'H\^{o}pital's Rule}
\Tm Suppose that $n(x)$ and $d(x)$ are differentiable in some neighborhood of
the number~$a$ (such that $d'(x)\neq 0$ in that interval
except possibly at~$a$).
If $\lim_{x\to a}n(x)=0$ and  $\lim_{x\to a}d(x)=0$ then 
\begin{equation*}
  \lim_{x\to a}\frac{n(x)}{d(x)}=\lim_{x\to a}\frac{n'(x)}{d'(x)}
  \tag{$*$}
\end{equation*}  
provided that the latter limit exists.
Similarly, if 
$\lim_{x\to a}n(x)=\infty$ and  $\lim_{x\to a}d(x)=\infty$ then ($*$) holds.
This result also holds for one-sided limits $x\to a^+$ or $x\to a^-$,
and for infinite limits $x\to\infty$ or $x\to-\infty$. 

\pause
Here is an argument for $0/0$, giving some sense of how derivatives
appear.  
The derivative gives a linear approximation,
$n(x)\approx n(a)+n'(x)\cdot (x-a)$
and $d(x)\approx n(a)+d'(x)\cdot (x-a)$.
Write the fraction and take the limit.
\begin{equation*}
  \lim_{x\to a}\frac{n(x)}{d(x)}
  =
  \lim_{x\to a}\frac{n(a)+n'(x)\cdot (x-a)}{d(a)+d'(x)\cdot (x-a)}
  =
  \lim_{x\to a}\frac{n'(x)(x-a)}{d'(x)(x-a)}
  =
  \lim_{x\to a}\frac{n'(x)}{d'(x)}
\end{equation*}
\end{frame}


\begin{frame}
\Ex This is a $0/0$-type limit. 
\begin{equation*}
  \lim_{x\to 0}\frac{1-\cos x}{x}
  =
  \lim_{x\to 0}\frac{\sin x}{1}
  =0
\end{equation*}
\pause
\Ex This is another $0/0$-type limit. 
Note that it is $x\to 1$.
\begin{equation*}
  \lim_{x\to 1}\frac{\ln x}{x-1}
  =
  \lim_{x\to 1}\frac{1/x}{1}
  =
  \lim_{x\to 1}\frac{1}{x}=1
\end{equation*}
\pause
\Ex This is an $\infty/\infty$-type limit. 
\begin{equation*}
  \lim_{x\to \infty}\frac{e^x}{x^2}
  =
  \lim_{x\to \infty}\frac{e^x}{2x}
\end{equation*}
That is also an $\infty/\infty$ limit, so we iterate.
\begin{equation*}
  \lim_{x\to \infty}\frac{e^x}{2x}
  =
  \lim_{x\to \infty}\frac{e^x}{2}=\infty
\end{equation*}
\end{frame}


\begin{frame}{Warning!  Check the limit type}
This is wrong:
\begin{equation*}
  \lim_{x\to \pi^-}\frac{\sin x}{1-\cos x}
  =
  \lim_{x\to \pi^-}\frac{\cos x}{\sin x}
\end{equation*}
The initial limit is of type $0/2$ so L'H\^{o}pital's Rule does not apply.  
Instead the answer by the Limit Laws is $0$.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \lim_{x\to -2} \frac{x^3+8}{x+2}$ 

\pause
\item $\displaystyle \lim_{x\to 0} \frac{\tan 3x}{\sin 2x}$ 

\pause
\item $\displaystyle \lim_{x\to \infty} \frac{x+x^2}{1-2x^2}$ 

\pause
\item $\displaystyle \lim_{x\to 1} \frac{\sin(\pi x)}{\ln x}$ 

\pause
\item $\displaystyle \lim_{x\to 0} \frac{\sin x-x}{x^2}$ 

\pause
\item $\displaystyle \lim_{x\to \infty} \frac{\ln(\sqrt{x})}{x^2}$ 

\end{enumerate}
\end{frame}



\begin{frame}{Other indeterminate forms}
\Ex As written, L'H\^{o}pital's Rule does not apply because 
this isn't a fraction.
\begin{equation*}
  \lim_{x\to 0^+}x\cdot \ln x
\end{equation*}
But as $x\to 0^+$
the~$x$ factor goes to zero, while
the $\ln x$ part goes to negative infinity.
\begin{center}
  \includegraphics{asy/lhopital001.pdf}    
\end{center}
In this $0\cdot \infty$-type limit
there is a tension between the parts
that makes it feel like a L'H\^{o}pital situation.
\end{frame}

\begin{frame}
Some algebra translates to a form where
the rule applies.
\begin{equation*}
  \lim_{x\to 0^+}x\cdot \ln x
  =\lim_{x\to 0^+}\frac{\ln x}{(1/x)}
\end{equation*}
That is an $\infty/\infty$-type limit.
\begin{equation*}
  \lim_{x\to 0^+}\frac{\ln x}{(1/x)}
  =\lim_{x\to 0^+}\frac{1/x}{-1/x^2}
  =\lim_{x\to 0^+}-x=0
\end{equation*}

\pause\medskip
\Rm A little more about `tension':
there are $0\cdot \infty$ limits that go to~$0$, 
there are $0\cdot \infty$ limits that go to~$\infty$, 
and there are $0\cdot \infty$ limits that go to any number in between.
\begin{center}
  \begin{tabular}{cc|c}
    \multicolumn{1}{c}{$f(x)$}
       &\multicolumn{1}{c}{$g(x)$}
       &\multicolumn{1}{c}{$\lim_{x\to 0}f(x)\cdot g(x)$}  \\ \hline
    \rule{0pt}{10pt}
    $x^2$  &$1/x$  &$0$  \\ 
    $x$  &$1/x^2$  &$\infty$  \\ 
    $5x$  &$1/x$  &$5$  \\ 
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}
\Ex As $x\to 1^+$, both $1/\ln x$ and $1/(x-1)$ go to infinity. 
So here also there is L'H\^{o}pital-type tension.
\begin{equation*}
  \lim_{x\to 1^+}\frac{1}{\ln x}-\frac{1}{x-1}
\end{equation*}

\pause
Get a common denominator.
\begin{equation*}
  \lim_{x\to 1^+}\frac{(x-1)-\ln(x)}{(x-1)\ln x}
\end{equation*}
That's a $0/0$-type.
The first application of L'H\^{o}pital's Rule gives this.
\begin{equation*}
  \lim_{x\to 1^+}\frac{1-(1/x)}{(x-1)\cdot (1/x)+(\ln x)\cdot 1}  
  =\lim_{x\to 1^+}\frac{x-1}{x-1+ x\ln x}  
\end{equation*}
That's another $0/0$-type.
One more iteration finishes the job. 
\begin{equation*}
  \lim_{x\to 1^+}\frac{1}{1 x(1/x)+(\ln x)\cdot 1}  
  =\lim_{x\to 1^+}\frac{1}{2+\ln x}
  =1/2  
\end{equation*}
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
