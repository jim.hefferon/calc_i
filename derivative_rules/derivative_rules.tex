\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{present}
% \usepackage{enumitem} % resume numbering of enumerated exercises 

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Derivative Rules] % (optional, use only with long paper titles)
{Derivative Rules}

\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu}
}
\date{}

\subject{Derivative Rules}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%=================================================
\section{Differentiation}
\begin{frame}
Fix a function and we can find its rate of change at various inputs.
For instance here is $f(x)=x^2$.
\begin{center}
  \begin{tabular}{c|c}
    $x$  &rate of change of $f$ at $x$ \\ \hline
    $1$    &$2$   \\
    $2$    &$4$   \\
    $3$    &$6$   \\
    $4$    &$8$
  \end{tabular}
\end{center}
\pause
We hypothesize that the rate of change at input~$x$ is always twice~$x$.
\begin{equation*}
  f(x)=x^2
  \hspace*{3em}
  f^\prime(x)=2x
  \quad\text{or}\quad
  \frac{d\,f}{dx}=2x
\end{equation*}
We say the second function is \textit{derived} from the first,
or that it is the \textit{derivative} of the first.

\pause
We have an operation, finding the derivative.
It inputs functions and outputs functions.
\begin{equation*}
  f(x)=x^2\quad\mapsunder{D}\quad f^\prime(x)=2x
\end{equation*}
We next work out rules for this $D$ operator.
\end{frame}




%=================================================
\section{Polynomial functions}
\begin{frame}
\frametitle{Power Rule} 

We saw why the function derived from $f(x)=x^2$ is $df/dx=2x$.
\begin{equation*}
  (x+\Delta x)^2=x^2+2x\cdot\Delta x+(\Delta x)^2
\end{equation*}
The $x^2$ doesn't change.
For small $\Delta x$ the square term $(\Delta x)^2$ is much smaller
than\Dash is dominated by\Dash the linear term $2x\cdot\Delta x$.

\pause
So near to $x$, the output change
$\Delta y$ compares with the input change $\Delta x$ as
approximately $2x$.
% \end{frame}

% \begin{frame}
The language of limits allows us to calculate the same thing in 
a more mechanical, less conceptual, way.
\begin{align*}
  \lim_{\Delta x\to 0}\frac{\text{change in $y$}}{\text{change in $x$}}
  &=
  \lim_{\Delta x\to 0}\frac{f(x+\Delta x)-f(x)}{(x+\Delta x)-x} \\
  &=
  \lim_{\Delta x\to 0}\frac{(x^2+2x\cdot\Delta x +(\Delta x)^2)-x^2}{\Delta x}\\
  &=
  \lim_{\Delta x\to 0}\frac{\Delta x\cdot(2x +\Delta x)}{\Delta x}
  =2x
\end{align*}
\end{frame}

\begin{frame}
Here are other power functions.
The expansions are done using the Binomial Theorem.
\begin{center} \small
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{$x^n$}  
       &\multicolumn{1}{c}{\textit{expansion $(x+\Delta x)^n$}}  \\ \hline
      $x^3$          &$x^3 +3x^2\cdot \Delta x 
                        +3x\cdot(\Delta x)^2 + (\Delta x)^3$ \\
      $x^4$          &$x^4 +4x^3\cdot \Delta x 
                        +6x^2\cdot(\Delta x)^2 + 4x\cdot (\Delta x)^3
                        +(\Delta x)^3$                      \\
      $x^5$          &$x^5 +5x^4\cdot \Delta x 
                        +10x^3\cdot(\Delta x)^2 + 10x^2\cdot (\Delta x)^3
                        +5x\cdot(\Delta x)^4+(\Delta x)^5$ 
  \end{tabular}
\end{center}
\pause
Therefore we have these.
\begin{center} \small
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{\textit{function}}  
       &\multicolumn{1}{c}{\textit{derivative}}  \\ \hline
      $f(x)=x^3$     &$df/dx = 3x^2$               \\
      $f(x)=x^4$     &$df/dx=4x^3$                 \\
      $f(x)=x^5$     &$df/dx=5x^4$ 
  \end{tabular}
\end{center}
\pause
\thm Let $f$ be the power function $f(x)=x^n$.
\begin{equation*}
  \frac{d\,f}{dx}=nx^{(n-1)}
\end{equation*}
\pause
This holds not just if the power~$n$ is a positive integer, but also if it is
negative, zero, fractional, or real.
\end{frame}




\begin{frame}
\frametitle{Sums and differences}  
\thm The derivative of a constant times a function equals that constant
times the derivative of the function.
The derivative of the sum of two functions is the sum of the derivatives.
The derivative of the difference of two functions is the difference of the
derivatives.

\pause
In symbols, let $f$ and~$g$ be functions, and let $c$ be a real number constant.
Then
\begin{equation*}
  \frac{d\,(c\cdot f)}{dx}=c\cdot \frac{d\,f}{dx}
\end{equation*}
and
\begin{equation*}
  \frac{d\,(f+g)}{dx}=\frac{d\,f}{dx}+\frac{d\,g}{dx}
\end{equation*}
and also (this rule follows from the above two).
\begin{equation*}
  \frac{d\,(f-g)}{dx}=\frac{d\,f}{dx}-\frac{d\,g}{dx}
\end{equation*}
\end{frame}




\begin{frame}
\frametitle{Polynomials}

Those rules give us the derivative of any polynomial function.
This includes powers that are fractional, negative, etc.  
\end{frame}





%=================================================
\section{Exponential functions}
\begin{frame}
\frametitle{Exponentials}  
Next we consider the derivative of the function $f(x)=a^x$ where $a$
is a positive real number.

\pause
To avoid technical details, consider the discrete case with
the exponential function $g(n)=2^n$.
\begin{center} \small
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{$n$}
      &\multicolumn{1}{c}{$2^n$}  \\ \hline
     $0$  &$1$  \\
     $1$  &$2$  \\
     $2$  &$4$  \\
     $3$  &$8$  \\[-.5ex]
     \raisebox{.5ex}{$\vdots$}  
       &\raisebox{.5ex}{$\vdots$}
  \end{tabular}
\end{center}
\pause
This function suits a collection where, every day at midnight, each element
reproduces.
\begin{center} \small
  \begin{tabular}{cc|c}
    \multicolumn{1}{c}{$n$}
      &\multicolumn{1}{c}{$2^n$}
      &\multicolumn{1}{c}{\textit{rate of change}}  \\ \hline
     \onslide<2->{$0$}  &\onslide<2->{$1$} &\onslide<4->{$1$} \\
     \onslide<3->{$1$}  &\onslide<3->{$2$} &\onslide<5->{$2$} \\
     \onslide<4->{$2$}  &\onslide<4->{$4$} &\onslide<6->{$4$} \\
     \onslide<5->{$3$}  &\onslide<5->{$8$} &\onslide<7->{$8$} 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
So, in this discrete case,
in terms the graph, at any $x$ the rate of change is the height
of the graph~$g(x)$.

\pause
In the continuous case the exponential model that is natural is $f(x)=e^x$.
\thm Where $f(x)=e^x$
\begin{equation*}
  \frac{d\,f}{dx}=e^x
\end{equation*}
and, more generally for $a>0$, where $f(x)=a^x$ we have this.
\begin{equation*}
  \frac{d\,f}{dx}=a^x\cdot \ln a
\end{equation*}
\pause
Here is the limit statement.
\begin{equation*}
  \lim_{\Delta x\to 0}\frac{a^{x+\Delta x}-a^x}{\Delta x}
  =
  \lim_{\Delta x\to 0}\frac{a^x\cdot(a^{\Delta x}-1)}{\Delta x}
  =
  a^x\cdot\lim_{\Delta x\to 0}\frac{a^{\Delta x}-1}{\Delta x}
\end{equation*}
(Recognize the fraction as the rate of change of $f$ at $x=0$.
We'll pass over the proof that it is the natural logarithm of $a$.)
\end{frame}




%=================================================
\section{Product and Quotient Rules}
\begin{frame}
\frametitle{Product rule}
In contrast to the rules for constants, sum, and difference, the rule for
product is not what you'd naively guess.
\begin{equation*}
  \frac{d\,x^2}{dx}\neq \frac{d\,x}{dx}\cdot\frac{d\,x}{dx}
\end{equation*}
\pause
\thm Let $f$ and~$g$ be functions.
\begin{equation*}
  \frac{d\,(f\cdot g)}{dx}
  =f\cdot\frac{d\,g}{dx}+g\cdot\frac{d\,f}{dx}
\end{equation*}
\end{frame}




\begin{frame}
\frametitle{Quotient rule}
\thm Let $f(x)$ and~$g(x)$ be functions.
\begin{equation*}
  \frac{d\,(f/g)}{dx}
  =
  \frac{g\cdot (df/dx)-f\cdot (dg/dx)}{(g(x))^2}
\end{equation*}
\end{frame}




%=================================================
\section{Trigonometric functions}
\begin{frame}
\frametitle{Sine and cosine}  
Recall the principle values of the sine and cosine functions.
\begin{center} \small
  \begin{tabular}{r|ll}
    \multicolumn{1}{c}{\textit{radians $\theta$}}
      &\multicolumn{1}{l}{$\sin\theta$}
      &\multicolumn{1}{c}{$\cos\theta$}  \\ \hline
    $0$      &$\sqrt{0}/2$               &$\sqrt{4}/2$  \\
    $\pi/6$  &$\sqrt{1}/2$               &$\sqrt{3}/2$  \\
    $\pi/4$  &$\sqrt{2}/2\approx 0.707$  &$\sqrt{2}/2$  \\
    $\pi/3$  &$\sqrt{3}/2\approx 0.866$  &$\sqrt{1}/2$  \\
    $\pi/2$  &$\sqrt{4}/2$               &$\sqrt{0}/2$      
  \end{tabular}
\end{center}

\pause
\begin{center}
  \includegraphics{asy/sincos.pdf}
\end{center}

\pause
\thm
\begin{equation*}
  \frac{d\,\sin(\theta)}{d\theta}=\cos(\theta)  
  \qquad
  \frac{d\,\cos(\theta)}{d\theta}=-\sin(\theta)  
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{All trig functions}  
\begin{center} \small
  \begin{tabular}{c|c}
    \multicolumn{1}{c}{\textit{function}}
      &\multicolumn{1}{c}{\textit{derivative}}  \\ \hline
    $\sin\theta$  &$\cos\theta$ \\
    $\cos\theta$  &$-\sin\theta$ \\[1ex]
    $\tan\theta$  &$\sec^2\theta$ \\
    $\cot\theta$  &$-\csc^2\theta$ \\[1ex]
    $\sec\theta$  &$\sec\theta\cdot\tan\theta$ \\
    $\csc\theta$  &$-\csc\theta\cdot\cot\theta$ 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Why Calculus works in radians}

In this graph the red curve is $\sin(\theta)$, in radians.
The green curve is for degrees, $\sin(\theta\cdot\pi/180)$.
It rises slowly because it takes $360$ to complete one cycle
while the red curve only takes about $6.28$.
\begin{center}
  \includegraphics{asy/sindegs.pdf}
\end{center}
\pause
The slope of the green curve at $\theta=0$ is not $\cos(0)=1$.
The derivative of the degree-sine function is not the degree-cosine
function.
(In the next section we will see it is the degree-cosine function
multiplied by 
$\pi/180\approx 0.017$.)

\pause
Calculus works in radians because they make the derivative of sine be cosine.
\end{frame}



%=================================================
\section{Compositions of functions}
\begin{frame}
\frametitle{Function composition}  
Recall the operation of composition of function:
$\sin(x^2)$ is the sine function performed on the
result of the square function.
For instance, with input~$2$ we have
$2\mapsto 4\mapsto \sin(4)$.
\begin{equation*}
  u=g(x)=x^2\quad y=f(u)=\sin(u)
  \qquad
  y(x)=\composed{f}{g}(x)=\sin(x^2)
\end{equation*}
\pause
We want the formula for the derivative of a composition. 
\end{frame}

\begin{frame}
\frametitle{Chain rule}  
Recall the mapping interpretation of the rate of change.
\centergraphic{derivatives-2.mps}
It leads to the formula for the derivative of a composition.

\pause
\thm Let $y=f(u)$ and~$u=g(x)$.
\begin{equation*}
  \frac{d\,y}{dx} = \frac{d\,y}{du}\cdot\frac{d\,u}{dx}
\end{equation*}
\end{frame}




%=================================================
\section{Implicit differentiation}
\begin{frame}
\frametitle{Implicit definition}
We have seen a good array of examples of taking derivatives $dy/dx$ 
where $y$ is the output and $x$ is the input.
But sometimes the variables are all mixed together and there is 
no obvious way to separate them.
\end{frame}




\begin{frame}
\frametitle{Implicit differentiation} 
\end{frame}




\begin{frame}
\frametitle{Inverse trigonometric functions}  
The definition of the arcsine function is: $\arcsin(x)=y$ if $\sin y=x$.
Express $y$ as a function of $x$ and differentiate, using the Chain Rule.
\begin{align*}
  \frac{d}{dx}[ sin\,y(x)] &= \frac{d}{dx}[ x ] \\
  \cos y(x)\cdot\frac{d\,y(x)}{dx} &= 1 
\end{align*}
Thus $dy/dx=1/\cos(y(x))$. 
Get the answer in terms of $x$.
\begin{equation*}
  \frac{d\,y}{dx}
  =\frac{1}{\cos(y(x))}
  =\frac{1}{\sqrt{1-\sin^2y}}  
  =\frac{1}{\sqrt{1-x^2}}  
\end{equation*}
\pause
\begin{center} \small
  \begin{tabular}{cc@{\hspace*{3em}}cc}
    \textit{function} 
      &\textit{derivative}  
      &\textit{function} 
      &\textit{derivative}  \\ \hline
    $\arcsin x$  &$\frac{1}{\sqrt{1-x^2}}$     
      &$\arccos x$  &-$\frac{1}{\sqrt{1-x^2}}$   \\[1ex]
    $\arctan x$  &$\frac{1}{1+x^2}$            
      &$\operatorname{arccot} x$  &-$\frac{1}{1+x^2}$           \\[1ex] 
    $\operatorname{arcsec} x$  &$\frac{1}{x\sqrt{x^2-1}}$   
      &$\operatorname{arccsc} x$  &$-\frac{1}{x\sqrt{x^2-1}}$   
  \end{tabular}
\end{center}
\end{frame}










% -----------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
