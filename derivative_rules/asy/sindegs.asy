/*<asyxml><html>One can see this graphe drawed with my package <a href="../modules/graph_pi/index.html#fig0005">HERE</a></html></asyxml>*/
import graph;
// import patterns;
usepackage("mathrsfs");

unitsize(.35inch);
real xmin=-1,xmax=2*pi+0.5;
real ymin=-1.5,ymax=1.5;

// Definition of functions f and g :
real f(real x) {return sin(x);}
real g(real x) {return sin(x*pi/180);}

// Trace the curves :
path Cf=graph(f,xmin,xmax,n=400);
path Cg=graph(g,xmin,xmax,n=400);
draw(Cf,linewidth(1bp)+red);
draw(Cg,linewidth(1bp)+green);
xlimits(xmin,xmax,Crop);
ylimits(ymin,ymax,Crop);

// The grid :
xaxis(BottomTop, xmin, xmax, Ticks("%", Step=1, step=0.5, extend=true, ptick=lightgrey));
yaxis(LeftRight, ymin, ymax, Ticks("%", Step=1, step=0.5, extend=true, ptick=lightgrey));
// The axis.
// xequals(Label("$y$",align=W),0,ymin=ymin-0.25, ymax=ymax+0.25,
//         Ticks(xaxisticks,pTick=nullpen, ptick=grey),
//         p=linewidth(1pt), Arrow(2mm));
xequals(0,
        // Ticks(NoZero,pTick=nullpen, ptick=grey),
        p=linewidth(1pt));
labely(Label("\tiny $1$"), 1, align=NW);
labely(Label("\tiny $-1$"), -1, align=SW);
yequals(0,
        //Ticks(xaxisticks,pTick=nullpen, ptick=grey),
        p=linewidth(1pt));
labelx(Label("\tiny $\pi/2$"), pi/2, align=0.25S);
labelx(Label("\tiny $\pi$"), pi, align=0.25S);
labelx(Label("\tiny $3\pi/2$"), 3*pi/2, align=0.25S);
labelx(Label("\tiny $2\pi$"), 2*pi, align=0.25S);
// labelx(Label("$O$",NoFill), 0, SW);
// draw(Label("$\vec{\imath}$",align=S,UnFill),
//      (0,0)--(1,0),scale(2)*currentpen,Arrow);
// draw(Label("$\vec{\jmath}$",align=W,UnFill),
//      (0,0)--(0,1),scale(2)*currentpen,Arrow);
// dot((0,0));

// label("$\mathscr{C}_f$",(2.25,f(2.25)),2N);
// label("$\mathscr{C}_f$",(2.25,g(2.25)),2S);

// Les hachures.
// path vline=(1,-1)--(1,5);
// add("hachure",hatch(3mm));
// fill(buildcycle(vline,graph(f,1,4),graph(g,1,4)),pattern("hachure")); 
