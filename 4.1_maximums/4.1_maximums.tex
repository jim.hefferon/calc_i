\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.1\ \ Maximums}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}{Optima}
\Df 
Where $f$ is a function, the number~$c$ is a \alert{local maximum} if 
$f(c)\geq f(x)$ for inputs $x$ near to $c$,
and it is a \alert{local minimum} if 
$f(c)\leq f(x)$ for inputs $x$ near to $c$
A number~$c$ is an \alert{absolute maximum}
if $f(c)\geq f(x)$ for all inputs~$x$ in $f$'s domain. 
and an  \alert{absolute maximum}
if $f(c)\leq f(x)$ for all inputs~$x$. 
\begin{center}
  \includegraphics{asy/maximums000.pdf}
\end{center}
\end{frame}


\begin{frame}
In the picture below the ball rises, for just an instant is neither rising nor 
falling, and then falls.
\begin{center}
  \includegraphics{asy/maximums013.pdf}    
\end{center}
So the maximum is associated with a vertical velocity of zero.
\end{frame}


\begin{frame}{Fermat's theorem}\vspace*{-1ex}
\Tm For a function~$f\!$, any optima must occur at $f(c)$'s where
either  $f'(c)=0$ or $f'(c)$ does not exist. 

The intuitive argument is that
a function is approximated by its derivative.
If the derivative at~$c$ is positive then the function is still climbing and 
so there is a nearby number~$\hat{c}$ where the value is higher,
$f(\hat{c})>f(c)$, and so it is not a maximum.
Likewise, if the derivative at~$c$ is negative then~$f$ does not 
have a minimum there.  

\Df An input $x$ such that $f'(x)=0$ is a \alert{stationary number}. 
An input~$x$ such that either $f'(x)=0$ or $f'(x)$ does not exist
is a \alert{critical number} 
(often instead called \alert{critical point}).
\begin{center}
  \includegraphics{asy/maximums001.pdf}
  \qquad
  \includegraphics{asy/maximums002.pdf}    
\end{center}
% \end{frame}
% \begin{frame}

\end{frame}


\begin{frame}{Critical numbers are candidates for optima}
If you find a critical number then there may be a local optima there.
\begin{center}
  \includegraphics{asy/maximums003.pdf}
  \qquad    
  \includegraphics{asy/maximums004.pdf}    
\end{center}
Or maybe not.
\begin{center}
  \includegraphics{asy/maximums005.pdf}
  \qquad    
  \includegraphics{asy/maximums006.pdf}    
\end{center}
\end{frame}



\begin{frame}{Critical numbers}
For each function find the critical numbers.
\begin{enumerate}
\item $f(x)=x^3/3-5x^2/2+4x$
\pause \\ The derivative is $f'(x)=x^2-5x+4=(x-1)(x-4)$.
The derivative is always defined and is zero at $x=1$ and $x=4$; those are
the critical points.

\item $g(x)=(x^2-1)^3$
\pause \\
The derivative is $g'(x)=3(x^2-1)^2\cdot 2x=6x(x^2-1)^2$.
The derivative always exists.
The critical points are the zeros of $g'\!$, namely $x=-1$, $x=0$, and $x=1$.

\item $\displaystyle h(v)=\frac{3v+1}{5v-1}$
\pause \\
The derivative is this.
\begin{equation*}
  \frac{(5v-1)\cdot 3-(3v+1)\cdot 5}{(5v-1)^2}
  =\frac{(15v-3)-(15v+5)}{(5v-1)^2}
  =\frac{-11}{(5v-1)^2}
\end{equation*}
The only critical point is $v=1/5$.
\end{enumerate}
\end{frame}



\section{Functions that never attain their optima}

\begin{frame}{$f(x)=x^3$}
This function has a critical point, but no
maximum or minimum.
\begin{center}
  \includegraphics{asy/maximums007.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=(x-1)\cdot x\cdot(x+1)=x^3-x$}
The derivative is $f'(x)=3x^2-1$ so the critical points are $x=\pm \sqrt{1/3}$.
The graph shows that this function has a local maximum and a local minimum
at those critical numbers.
But it has no absolute maximum or minimum.
\begin{center}
  \includegraphics{asy/maximums008.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=1/(x^2+1)$}
The function achieves its maximum at $x=0$.
It never achieves a minimum.
\begin{center}
  \includegraphics{asy/maximums010.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=1/x$ for $x>0$}
The function's values never reach infinity.
Also, its values never reach $y=0$.
\begin{center}
  \includegraphics{asy/maximums009.pdf}    
\end{center}
\end{frame}


\begin{frame}{$f(x)=1/x$ for the open interval $\open{1}{3}$}
This failure to have optima isn't just about infinity.
The interval $\open{1}{3}$ is bounded but on this interval also, 
the values of the function $f(x)=1/x$ do not quite get up to~$1$, 
nor do they get all the way down to~$1/3$.
\begin{center}
  \includegraphics{asy/maximums017.pdf}    
\end{center}
\end{frame}


\section{Ensuring that a function attains its optima}

\begin{frame}{There is something about intervals that are closed and bounded}
\Tm If a function is continuous 
on a closed and bounded interval $\closed{a}{b}$
then it attains its absolute
maximim and absolute minimum on that interval.

\medskip
\textit{Strategy for finding optima}\hspace{0.618em}
Given a function~$f$ that is continuous on $\closed{a}{b}$.
\begin{enumerate}
\item Find the critical points and evaluate the function at all of them.
  That is, compute $f(c)$ for all such~$c$.
\item Also evaluate the function at the interval's endpoints, $f(a)$ and $f(b)$.
\item The largest such value is the absolute maximum 
  and the smallest is the absolute minimum (ties are possible).
\end{enumerate}
\end{frame}


\begin{frame}
\Ex Optimize $-x^2+3x-2$ over $\closed{1}{3}$.

\pause
The derivative $-2x+3$
is always defined and is zero at $x=3/2$.
So there is a single critical number. 
With the endpoints that is three numbers to check.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End number $1$         &$f(1)=0$ \\
    End number $3$         &$f(3)=-2$ \\
    Critical number $3/2$ &$f(3/2)=1/4$ \\
  \end{tabular}
\end{center}
\begin{center}
  \includegraphics{asy/maximums011.pdf}    
\end{center}
\end{frame}



\begin{frame}
\Ex Optimize $x^2-3x^{2/3}$ over $\closed{0}{2}$.

\pause
The derivative is $x^2-2x^{-1/3}=(2x^{4/3}-2)/x^{1/3}$.
It is not defined at $x=0$. 
It is zero when $0=2x^{4/3}-2$, so at $x=\pm 1$.
We needn't evaluate $-1$ because it is not in the interval.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End and critical number $0$         &$f(0)=0$ \\
    End number $2$         &$f(2)=4-3(2)^{2/3}\approx -0.762$ \\
    Critical number $1$    &$f(1)=-2$ \\
  \end{tabular}
\end{center}
\begin{center}
  \includegraphics{asy/maximums012.pdf}    
\end{center}
\end{frame}



\begin{frame}{Practice}
\Ex Optimize $f(x)=2x^3-15x^2+24x+7$ on $\closed{0}{6}$.

\pause The derivative is 
$f'(x)=6x^2-30x+24$.
Setting it equal to zero gives that the critical numbers are $x=1$ and $x=4$.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End number $0$         &$f(0)=7$ \\
    End number $6$         &$f(6)=43$ \\
    Critical number $1$    &$f(1)=18$ \\
    Critical number $4$    &$f(4)=-9$ \\
  \end{tabular}
\end{center}
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tell gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
