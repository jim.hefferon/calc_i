// maximums.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "maximums%03d";

import graph;


// ==== local and absolute max ====
picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
..(4,ytop-1.5){E}..(xright+0.1,2){SE};


draw(pic, grph, FCNPEN);

filldraw(pic, circle((2,ytop),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
filldraw(pic, circle((3.5,ytop-2),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
filldraw(pic, circle((4,ytop-1.5),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ==== parabola ====
real f1(real x) {return x^2;}

picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f1,xleft-0.1,xright+0.1), FCNPEN);

filldraw(pic, circle((0,0),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead),
      above=true);
  
shipout(format(OUTPUT_FN,picnum),pic);


// ==== absolute value ====
real f2(real x) {return abs(x);}

picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f2,xleft-0.1,xright+0.1), FCNPEN);

filldraw(pic, circle((0,0),0.05), FCNPEN_COLOR,  FCNPEN_COLOR);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== non-optima at critical point ====

// ....... max ............
real f3(real x) { return(-3*(x-1)^2+1.5); }

picture pic;
int picnum = 3;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path f = graph(pic, f3, 0.5,1.5);
draw(pic, f, FCNPEN);

filldraw(pic, circle((1,1.5),0.025), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "max", (1,1.5), 2*N, filltype=Fill(white));

real[] T3 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T3, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$c$",1);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... undefined derivative ............
picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,0.5)..(1,1.5){NE}&(1,1.5)::{SE}(1.5,0.5);

draw(pic, grf, FCNPEN);

filldraw(pic, circle((1,1.5),0.025), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "max", (1,1.5), 2*N, filltype=Fill(white));

real[] T3 = {1};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=RightTicks("%", T3, Size=2pt),
  arrow=Arrows(TeXHead));
labelx(pic,"$c$",1);

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... inflection ............
real f5(real x) { return( 1.5(x-1)^3+1 );}

picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// path grf = (0.5,0.5){NE}..(1,1){E}..{NE}(1.5,1.5);
path grf = graph(pic, f5, 0.25, 1.75);
draw(pic, grf, FCNPEN);

filldraw(pic, circle((1,1),0.02), FCNPEN_COLOR, FCNPEN_COLOR);
draw(pic,(0.5,1)--(1.5,1),highlight_color+dotted);
label(pic,  "inflection", (1,1), 2*NW, filltype=Fill(white));

real[] T3 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T3, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$c$",1);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... f'(c) undefined ............
picture pic;
int picnum = 6;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,0.5){E}..(1,1){N}..{E}(1.5,1.5);

draw(pic, grf, FCNPEN);

filldraw(pic, circle((1,1),0.02), FCNPEN_COLOR, FCNPEN_COLOR);
draw(pic,(1,0.5)--(1,1.5), highlight_color+dotted);
// label(pic,  "$f'(c)$ undefined", (1,1), 2*W, filltype=Fill(white));

real[] T3 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T3, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$c$",1);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== x^3 ====
real f7(real x) {return x^3;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-8; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f7,xleft-0.02,xright+0.02), FCNPEN);

filldraw(pic, circle((0,0),0.1), FCNPEN_COLOR, FCNPEN_COLOR);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== (x-1)x(x+1) ====
real f8(real x) {return (x-1)*x*(x+1);}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-6; ytop=6;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f8,xleft-0.02,xright+0.02), FCNPEN);

filldraw(pic, circle((0,0),0.10), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== 1/x ====
real f9(real x) {return 1/x;}

picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=8;
ybot=0; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f9, 0.125, xright+0.02), FCNPEN);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== 1/(x^2+1) ====
real f10(real x) {return 1/(x^2+1);}

picture pic;
int picnum = 10;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f10, xleft-0.1, xright+0.1), FCNPEN);

filldraw(pic, circle((0,1),0.075), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "max", (0,1), N, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== optimize -x^2+3x-2 ====
real f11(real x) {return -x^2+3*x-2;}

picture pic;
int picnum = 11;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-2; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f11,1,3), FCNPEN);

filldraw(pic, circle((1,0),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
filldraw(pic, circle((3,-2),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
filldraw(pic, circle((3/2,1/4),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
// label(pic,  "max", (1,0), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== optimize -x^2+3x-2 ====
real f12(real x) {return x^2-3*x^(2/3);}

picture pic;
int picnum = 12;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=-2; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f12,0,2), FCNPEN);

filldraw(pic, circle((0,0),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
filldraw(pic, circle((2,f12(2)),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
filldraw(pic, circle((1,-2),0.05), FCNPEN_COLOR, FCNPEN_COLOR);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// === baseball ======

picture pic;
int picnum = 13;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

label(pic,graphic("batter.eps","scale=0.1"), (0.5,0));
label(pic,graphic("fielder.eps","scale=0.1"), (9.5,0));

// dot(pic,(1,0.75),red);
path grph = (1,0.75)..(4,4)..(5,4.4){E}..(6,4)..(9,0.75);
draw(pic, grph, FCNPEN);

filldraw(pic, circle((5,4.4),0.05), FCNPEN_COLOR, FCNPEN_COLOR);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// ==== 1/x  on open interval (1..3) ====
real f17(real x) {return 1/x;}

picture pic;
int picnum = 17;
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f17,1,3), FCNPEN);

filldraw(pic, circle((1,1),0.05), white,  FCNPEN_COLOR);
filldraw(pic, circle((3,1/3),0.05), white,  FCNPEN_COLOR);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
labelx(pic,"$1$",1);
labelx(pic,"$3$",3);
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// // ==== picture of linear approx and differentials ====
// real f1(real x) {return (1/3)*(5-(x-2)^2);}
// real tangent1(real x) {return (2/3)*(x-1)+(4/3);}

// picture pic;
// int picnum = 1;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// draw(pic, graph(f1,xleft+0.25,xright+0.1), black);
// draw(pic, graph(tangent1,0.5,2), highlight_color);
// filldraw(pic, circle((1,4/3),0.025), highlight_color,  highlight_color);
// label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));

// real[] T1 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.75,
//   p=currentpen,
//       ticks=RightTicks("%",T1),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$a$", 1);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
