# Makefile for calc_i slides
#
# 2023-Oct-15 Jim Hefferon Public Domain

SHELL=/bin/bash

LATEX:=lualatex
ASYMPTOTE:=asy

INTRO := intro
DERIVATIVES := derivatives
INVERSE_FCN := 1.5_inverse_functions
LIMITS := 2.2_limits
LIMIT_LAWS := 2.3_limit_laws
CONTINUITY := 2.5_continuity
LIMITS_AT_INFINITY := 2.6_limits_at_infinity
RATES_OF_CHANGE := 2.7_rates_of_change
DERIVATIVE_AS_FUNCTION := 2.8_derivative_as_function
DERIVATIVES_OF_POLYS := 3.1_derivatives_of_polys
PRODUCT_RULE := 3.2_product_rule
TRIGS := 3.3_trigs
CHAIN_RULE := 3.4_chain_rule
IMPLICIT := 3.5_implicit
LOGS := 3.6_logs 
SCIENCE := 3.7_science 
GROWTH := 3.8_growth
RELATED_RATES := 3.9_related_rates
LINEAR := 3.10_linear_approximation
HYPERBOLIC := 3.11_hyperbolic
DERIV_PRACTICE := 3_derivative_practice
NO_CALCULUS := 4.0_no_calculus
MAXIMUMS := 4.1_maximums
MEAN_VALUE := 4.2_mean_value
GRAPH_SHAPE := 4.3_graph_shape
LHOPITAL := 4.4_lhopital
BIGO := 4.4a_bigo
CURVE_SKETCH := 4.5_curve_sketching
OPTIMIZATION := 4.7_optimization
AREAS := 5.1_areas
FTC := 5.3_ftc
SUBSTITUTION := 5.5_substitution


all: ${INTRO}/${INTRO}.pdf \
     ${DERIVATIVES}/${DERIVATIVES}.pdf \
     ${INVERSE_FCN}/${INVERSE_FCN}.pdf \
     ${LIMITS}/${LIMITS}.pdf \
     ${LIMIT_LAWS}/${LIMIT_LAWS}.pdf \
     ${LIMITS_AT_INFINITY}/${LIMITS_AT_INFINITY}.pdf \
     ${RATES_OF_CHANGE}/${RATES_OF_CHANGE}.pdf \
     ${DERIVATIVE_AS_FUNCTION}/${DERIVATIVE_AS_FUNCTION}.pdf \
     ${DERIVATIVES_OF_POLYS}/${DERIVATIVES_OF_POLYS}.pdf \
     ${PRODUCT_RULE}/${PRODUCT_RULE}.pdf \
     ${TRIGS}/${TRIGS}.pdf \
     ${CHAIN_RULE}/${CHAIN_RULE}.pdf \
     ${IMPLICIT}/${IMPLICIT}.pdf \
     ${LOGS}/${LOGS}.pdf \
     ${SCIENCE}/${SCIENCE}.pdf \
     ${GROWTH}/${GROWTH}.pdf \
     ${RELATED_RATES}/${RELATED_RATES}.pdf \
     ${LINEAR}/${LINEAR}.pdf \
     ${HYPERBOLIC}/${HYPERBOLIC}.pdf \
     ${DERIV_PRACTICE}/${DERIV_PRACTICE}.pdf \
     ${NO_CALCULUS}/${NO_CALCULUS}.pdf \
     ${NO_CALCULUS}/sketch_homework.pdf \
     ${MAXIMUMS}/${MAXIMUMS}.pdf \
     ${MEAN_VALUE}/${MEAN_VALUE}.pdf \
     ${GRAPH_SHAPE}/${GRAPH_SHAPE}.pdf \
     ${LHOPITAL}/${LHOPITAL}.pdf \
     ${BIGO}/${BIGO}.pdf \
     ${CURVE_SKETCH}/${CURVE_SKETCH}.pdf \
     ${OPTIMIZATION}/${OPTIMIZATION}.pdf \
     ${AREAS}/${AREAS}.pdf \
     ${FTC}/${FTC}.pdf \
     ${SUBSTITUTION}/${SUBSTITUTION}.pdf \


${INTRO}/asy/intro000.pdf: ${INTRO}/asy/intro.asy
	cd ${INTRO}/asy; ${ASYMPTOTE} intro

${INTRO}/${INTRO}.pdf: ${INTRO}/asy/intro000.pdf \
                     ${INTRO}/${INTRO}.tex
	cd ${INTRO}; ${LATEX} ${INTRO}; ${LATEX} ${INTRO}


${DERIVATIVES}/asy/derivatives000.pdf: ${DERIVATIVES}/asy/derivatives.asy
	cd ${DERIVATIVES}/asy; ${ASYMPTOTE} derivatives

${DERIVATIVES}/${DERIVATIVES}.pdf: ${DERIVATIVES}/asy/derivatives000.pdf \
                     ${DERIVATIVES}/${DERIVATIVES}.tex
	cd ${DERIVATIVES}; ${LATEX} ${DERIVATIVES}; ${LATEX} $}{


${INVERSE_FCN}/asy/inverse000.pdf: ${INVERSE_FCN}/asy/inverse.asy
	cd ${INVERSE_FCN}/asy; ${ASYMPTOTE} inverse

${INVERSE_FCN}/${INVERSE_FCN}.pdf: ${INVERSE_FCN}/asy/inverse000.pdf \
                     ${INVERSE_FCN}/${INVERSE_FCN}.tex
	cd ${INVERSE_FCN}; ${LATEX} ${INVERSE_FCN}; ${LATEX} ${INVERSE_FCN}


${LIMITS}/asy/limits000.pdf: ${LIMITS}/asy/limits.asy
	cd ${LIMITS}/asy; ${ASYMPTOTE} limits

${LIMITS}/${LIMITS}.pdf: ${LIMITS}/asy/limits000.pdf \
                     ${LIMITS}/${LIMITS}.tex
	cd ${LIMITS}; ${LATEX} ${LIMITS}; ${LATEX} ${LIMITS}


${LIMIT_LAWS}/asy/limits000.pdf: ${LIMIT_LAWS}/asy/limit_laws.asy
	cd ${LIMIT_LAWS}/asy; ${ASYMPTOTE} limit_laws

${LIMIT_LAWS}/${LIMIT_LAWS}.pdf: ${LIMIT_LAWS}/asy/limit_laws000.pdf \
                     ${LIMIT_LAWS}/${LIMIT_LAWS}.tex
	cd ${LIMIT_LAWS}; ${LATEX} ${LIMIT_LAWS}; ${LATEX} ${LIMIT_LAWS}


${CONTINUITY}/asy/continuity000.pdf: ${CONTINUITY}/asy/continuity.asy
	cd ${CONTINUITY}/asy; ${ASYMPTOTE} continuity

${CONTINUITY}/${CONTINUITY}.pdf: ${CONTINUITY}/asy/continuity000.pdf \
                     ${CONTINUITY}/${CONTINUITY}.tex
	cd ${CONTINUITY}; ${LATEX} ${CONTINUITY}; ${LATEX} ${CONTINUITY}


${LIMITS_AT_INFINITY}/asy/infinite_limits000.pdf: ${LIMITS_AT_INFINITY}/asy/infinite_limits.asy
	cd ${LIMITS_AT_INFINITY}/asy; ${ASYMPTOTE} infinite_limits

${LIMITS_AT_INFINITY}/${LIMITS_AT_INFINITY}.pdf: ${LIMITS_AT_INFINITY}/asy/infinite_limits000.pdf \
                     ${LIMITS_AT_INFINITY}/${LIMITS_AT_INFINITY}.tex
	cd ${LIMITS_AT_INFINITY}; ${LATEX} ${LIMITS_AT_INFINITY}; ${LATEX} ${LIMITS_AT_INFINITY}


${RATES_OF_CHANGE}/asy/rates_of_change000.pdf: ${RATES_OF_CHANGE}/asy/rates_of_change.asy
	cd ${RATES_OF_CHANGE}/asy; ${ASYMPTOTE} rates_of_change

${RATES_OF_CHANGE}/${RATES_OF_CHANGE}.pdf: ${RATES_OF_CHANGE}/asy/rates_of_change000.pdf \
                     ${RATES_OF_CHANGE}/${RATES_OF_CHANGE}.tex
	cd ${RATES_OF_CHANGE}; ${LATEX} ${RATES_OF_CHANGE}; ${LATEX} ${RATES_OF_CHANGE}


${DERIVATIVE_AS_FUNCTION}/asy/derivative_function000.pdf: ${DERIVATIVE_AS_FUNCTION}/asy/derivative_function.asy
	cd ${DERIVATIVE_AS_FUNCTION}/asy; ${ASYMPTOTE} derivative_function

${DERIVATIVE_AS_FUNCTION}/${DERIVATIVE_AS_FUNCTION}.pdf: ${DERIVATIVE_AS_FUNCTION}/asy/derivative_function000.pdf \
                     ${DERIVATIVE_AS_FUNCTION}/${DERIVATIVE_AS_FUNCTION}.tex
	cd ${DERIVATIVE_AS_FUNCTION}; ${LATEX} ${DERIVATIVE_AS_FUNCTION}; ${LATEX} ${DERIVATIVE_AS_FUNCTION}


# No asy here
${DERIVATIVES_OF_POLYS}/${DERIVATIVES_OF_POLYS}.pdf: \
                     ${DERIVATIVES_OF_POLYS}/${DERIVATIVES_OF_POLYS}.tex
	cd ${DERIVATIVES_OF_POLYS}; ${LATEX} ${DERIVATIVES_OF_POLYS}; ${LATEX} ${DERIVATIVES_OF_POLYS}


${PRODUCT_RULE}/asy/product_rule000.pdf: ${PRODUCT_RULE}/asy/product_rule.asy
	cd ${PRODUCT_RULE}/asy; ${ASYMPTOTE} product_rule

${PRODUCT_RULE}/${PRODUCT_RULE}.pdf: ${PRODUCT_RULE}/asy/product_rule000.pdf \
                     ${PRODUCT_RULE}/${PRODUCT_RULE}.tex
	cd ${PRODUCT_RULE}; ${LATEX} ${PRODUCT_RULE}; ${LATEX} ${PRODUCT_RULE}


${TRIGS}/asy/trigs000.pdf: ${TRIGS}/asy/trigs.asy
	cd ${TRIGS}/asy; ${ASYMPTOTE} trigs

${TRIGS}/${TRIGS}.pdf: ${TRIGS}/asy/trigs000.pdf \
                     ${TRIGS}/${TRIGS}.tex
	cd ${TRIGS}; ${LATEX} ${TRIGS}; ${LATEX} ${TRIGS}


${CHAIN_RULE}/asy/chain_rule000.pdf: ${CHAIN_RULE}/asy/chain_rule.asy
	cd ${CHAIN_RULE}/asy; ${ASYMPTOTE} chain_rule

${CHAIN_RULE}/${CHAIN_RULE}.pdf: ${CHAIN_RULE}/asy/chain_rule000.pdf \
                     ${CHAIN_RULE}/${CHAIN_RULE}.tex
	cd ${CHAIN_RULE}; ${LATEX} ${CHAIN_RULE}; ${LATEX} ${CHAIN_RULE}


${IMPLICIT}/asy/implicit000.pdf: ${IMPLICIT}/asy/implicit.asy
	cd ${IMPLICIT}/asy; ${ASYMPTOTE} implicit

${IMPLICIT}/${IMPLICIT}.pdf: ${IMPLICIT}/asy/implicit000.pdf \
                     ${IMPLICIT}/${IMPLICIT}.tex
	cd ${IMPLICIT}; ${LATEX} ${IMPLICIT}; ${LATEX} ${IMPLICIT}


${LOGS}/asy/logs000.pdf: ${IMPLICIT}/asy/logs.asy
	cd ${LOGS}/asy; ${ASYMPTOTE} logs

${LOGS}/${LOGS}.pdf: ${LOGS}/asy/logs000.pdf \
                     ${LOGS}/${LOGS}.tex
	cd ${LOGS}; ${LATEX} ${LOGS}; ${LATEX} ${LOGS}


${SCIENCE}/asy/science000.pdf: ${SCIENCE}/asy/science.asy
	cd ${SCIENCE}/asy; ${ASYMPTOTE} science

${SCIENCE}/${SCIENCE}.pdf: ${SCIENCE}/asy/science000.pdf \
                     ${SCIENCE}/${SCIENCE}.tex
	cd ${SCIENCE}; ${LATEX} ${SCIENCE}; ${LATEX} ${SCIENCE}


${GROWTH}/asy/growth000.pdf: ${GROWTH}/asy/growth.asy
	cd ${GROWTH}/asy; ${ASYMPTOTE} growth

${GROWTH}/${GROWTH}.pdf: ${GROWTH}/asy/growth000.pdf \
                     ${GROWTH}/${GROWTH}.tex
	cd ${GROWTH}; ${LATEX} ${GROWTH}; ${LATEX} ${GROWTH}


${RELATED_RATES}/asy/related_rates000.pdf: ${RELATED_RATES}/asy/related_rates.asy
	cd ${RELATED_RATES}/asy; ${ASYMPTOTE} related_rates

${RELATED_RATES}/${RELATED_RATES}.pdf: ${RELATED_RATES}/asy/related_rates000.pdf \
                     ${RELATED_RATES}/${RELATED_RATES}.tex
	cd ${RELATED_RATES}; ${LATEX} ${RELATED_RATES}; ${LATEX} ${RELATED_RATES}


${LINEAR}/asy/linear_approximation000.pdf: ${LINEAR}/asy/linear_approximation.asy
	cd ${LINEAR}/asy; ${ASYMPTOTE} linear_approximation

${LINEAR}/${LINEAR}.pdf: ${LINEAR}/asy/linear_approximation000.pdf \
                     ${LINEAR}/${LINEAR}.tex
	cd ${LINEAR}; ${LATEX} ${LINEAR}; ${LATEX} ${LINEAR}


${HYPERBOLIC}/asy/hyperbolic000.pdf: ${HYPERBOLIC}/asy/hyperbolic.asy
	cd ${HYPERBOLIC}/asy; ${ASYMPTOTE} hyperbolic

${HYPERBOLIC}/${HYPERBOLIC}.pdf: ${HYPERBOLIC}/asy/hyperbolic000.pdf \
                     ${HYPERBOLIC}/${HYPERBOLIC}.tex
	cd ${HYPERBOLIC}; ${LATEX} ${HYPERBOLIC}; ${LATEX} ${HYPERBOLIC}


# No asy
${DERIV_PRACTICE}/${DERIV_PRACTICE}.pdf:  \
                     ${DERIV_PRACTICE}/${DERIV_PRACTICE}.tex
	cd ${DERIV_PRACTICE}; ${LATEX} ${DERIV_PRACTICE}; ${LATEX} ${DERIV_PRACTICE}


${HYPERBOLIC}/asy/hyperbolic000.pdf: ${HYPERBOLIC}/asy/hyperbolic.asy
	cd ${HYPERBOLIC}/asy; ${ASYMPTOTE} hyperbolic

${HYPERBOLIC}/${HYPERBOLIC}.pdf: ${HYPERBOLIC}/asy/hyperbolic000.pdf \
                     ${HYPERBOLIC}/${HYPERBOLIC}.tex
	cd ${HYPERBOLIC}; ${LATEX} ${HYPERBOLIC}; ${LATEX} ${HYPERBOLIC}


${NO_CALCULUS}/asy/no_calculus000.pdf: ${NO_CALCULUS}/asy/no_calculus.asy
	cd ${NO_CALCULUS}/asy; ${ASYMPTOTE} no_calculus

${NO_CALCULUS}/sketch_homework.pdf: ${NO_CALCULUS}/sketch_homework.tex 
	cd ${NO_CALCULUS}; ${LATEX} sketch_homework; ${LATEX} sketch_homework
${NO_CALCULUS}/${NO_CALCULUS}.pdf: ${NO_CALCULUS}/asy/no_calculus000.pdf \
                     ${NO_CALCULUS}/${NO_CALCULUS}.tex
	cd ${NO_CALCULUS}; ${LATEX} ${NO_CALCULUS}; ${LATEX} ${NO_CALCULUS}


${MAXIMUMS}/asy/maximums000.pdf: ${MAXIMUMS}/asy/maximums.asy
	cd ${MAXIMUMS}/asy; ${ASYMPTOTE} maximums

${MAXIMUMS}/${MAXIMUMS}.pdf: ${MAXIMUMS}/asy/maximums000.pdf \
                     ${MAXIMUMS}/${MAXIMUMS}.tex
	cd ${MAXIMUMS}; ${LATEX} ${MAXIMUMS}; ${LATEX} ${MAXIMUMS}


${MEAN_VALUE}/asy/mean_value000.pdf: ${MEAN_VALUE}/asy/mean_value.asy
	cd ${MEAN_VALUE}/asy; ${ASYMPTOTE} mean_value

${MEAN_VALUE}/${MEAN_VALUE}.pdf: ${MEAN_VALUE}/asy/mean_value000.pdf \
                     ${MEAN_VALUE}/${MEAN_VALUE}.tex
	cd ${MEAN_VALUE}; ${LATEX} ${MEAN_VALUE}; ${LATEX} ${MEAN_VALUE}


${GRAPH_SHAPE}/asy/graph_shape000.pdf: ${GRAPH_SHAPE}/asy/graph_shape.asy
	cd ${GRAPH_SHAPE}/asy; ${ASYMPTOTE} graph_shape

${GRAPH_SHAPE}/${GRAPH_SHAPE}.pdf: ${GRAPH_SHAPE}/asy/graph_shape000.pdf \
                     ${GRAPH_SHAPE}/${GRAPH_SHAPE}.tex
	cd ${GRAPH_SHAPE}; ${LATEX} ${GRAPH_SHAPE}; ${LATEX} ${GRAPH_SHAPE}


${LHOPITAL}/asy/lhopital000.pdf: ${LHOPITAL}/asy/lhopital.asy
	cd ${LHOPITAL}/asy; ${ASYMPTOTE} lhopital

${LHOPITAL}/${LHOPITAL}.pdf: ${LHOPITAL}/asy/lhopital000.pdf \
                     ${LHOPITAL}/${LHOPITAL}.tex
	cd ${LHOPITAL}; ${LATEX} ${LHOPITAL}; ${LATEX} ${LHOPITAL}


${BIGO}/asy/bigo000.pdf: ${BIGO}/asy/bigo.asy
	cd ${BIGO}/asy; ${ASYMPTOTE} bigo

${BIGO}/${BIGO}.pdf: ${BIGO}/asy/bigo000.pdf \
                     ${BIGO}/${BIGO}.tex
	cd ${BIGO}; ${LATEX} ${BIGO}; ${LATEX} ${BIGO}


${CURVE_SKETCH}/asy/curve_sketching000.pdf: ${CURVE_SKETCH}/asy/curve_sketching.asy
	cd ${CURVE_SKETCH}/asy; ${ASYMPTOTE} curve_sketching

${CURVE_SKETCH}/${CURVE_SKETCH}.pdf: ${CURVE_SKETCH}/asy/curve_sketch000.pdf \
                     ${CURVE_SKETCH}/${CURVE_SKETCH}.tex
	cd ${CURVE_SKETCH}; ${LATEX} ${CURVE_SKETCH}; ${LATEX} ${CURVE_SKETCH}


${OPTIMIZATION}/asy/optimization000.pdf: ${OPTIMIZATION}/asy/optimization.asy
	cd ${OPTIMIZATION}/asy; ${ASYMPTOTE} optimization

${OPTIMIZATION}/${OPTIMIZATION}.pdf: ${OPTIMIZATION}/asy/optimization000.pdf \
                     ${OPTIMIZATION}/${OPTIMIZATION}.tex
	cd ${OPTIMIZATION}; ${LATEX} ${OPTIMIZATION}; ${LATEX} ${OPTIMIZATION}


${AREAS}/asy/areas000.pdf: ${AREAS}/asy/areas.asy
	cd ${AREAS}/asy; ${ASYMPTOTE} areas

${AREAS}/${AREAS}.pdf: ${AREAS}/asy/areas000.pdf \
                     ${AREAS}/${AREAS}.tex
	cd ${AREAS}; ${LATEX} ${AREAS}; ${LATEX} ${AREAS}


${FTC}/asy/ftc000.pdf: ${FTC}/asy/ftc.asy
	cd ${FTC}/asy; ${ASYMPTOTE} ftc

${FTC}/${FTC}.pdf: ${FTC}/asy/ftc000.pdf \
                     ${FTC}/${FTC}.tex
	cd ${FTC}; ${LATEX} ${FTC}; ${LATEX} ${FTC}


${SUBSTITUTION}/asy/substitution000.pdf: ${SUBSTITUTION}/asy/substitution.asy
	cd ${SUBSTITUTION}/asy; ${ASYMPTOTE} substitution

${SUBSTITUTION}/${SUBSTITUTION}.pdf: ${SUBSTITUTION}/asy/substitution000.pdf \
                     ${SUBSTITUTION}/${SUBSTITUTION}.tex
	cd ${SUBSTITUTION}; ${LATEX} ${SUBSTITUTION}; ${LATEX} ${SUBSTITUTION}
