// shell_2d.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = false;
settings.tex="lualatex";  // for graphic command
settings.outformat="pdf";
settings.render=0;
import fontsize;

import graph;

string OUTPUT_FN = "shells_2d%03d";
real PI = acos(-1);


// ==== surface of revolution =========

// ......... between (x+1)^2 and x axis ........
real f0(real x) {return 1/(1+x^2);}
 
picture pic;
int picnum = 0;
size(pic,0,4cm);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f0,a-0.05, b+0.1);

real a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,a_time)&subpath(f,a_time,b_time)&point(f,b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color);
draw(pic,point(f,b_time)--(b,0));
draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, "$(1,1/2)$", (1,1/2), NE);

// path a_line = (a,0)--(a,f0(a)); 
// path b_line = (b,0)--(b,f0(b)); 
// draw(pic, b_line);

real c0 = 0.25;
draw(pic, (a,c0)--(b,c0), highlight_color);
real c1 = 2/3;
draw(pic, (a,c1)--(sqrt((1/c1)-1),c1), highlight_color);


xaxis(pic,Label("$x$",position=EndPoint, align=S),
       0-0.1,1.5, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$y$"),
       0-0.1,1+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ......... dx thick slice ............
picture pic;
int picnum = 1;
size(pic,0,4cm);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f0,a-0.05, b+0.1);

real a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,a_time)&subpath(f,a_time,b_time)&point(f,b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color+opacity(0.6));
draw(pic, point(f,b_time)--(b,0));
draw(pic, f, FCNPEN);

// path a_line = (a,0)--(a,f0(a)); 
// path b_line = (b,0)--(b,f0(b)); 
// draw(pic, b_line);

real c = 0.618;
draw(pic, (c,0)--(c,f0(c)), highlight_color);

xaxis(pic,Label("$x$",position=EndPoint, align=S),
       0-0.1,1.+0.25, black,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));
yaxis(pic,Label("$y$"),
       0-0.1,1+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// =========== between sin(x^2) and x axis ........
real f2(real x) {return sin(x^2);}
 
picture pic;
int picnum = 2;
size(pic,0,3cm);

real a = 0;
real b = sqrt(pi/2);
real ybot = -1;
real ytop = 2;

path f = graph(pic,f2,a-0.05, b+0.1);

real a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ( (a,0)--point(f,a_time)&subpath(f,a_time,b_time)&point(f,b_time)--(b,0)--(a,0) )--cycle; 
fill(pic, region, background_color);
draw(pic,point(f,b_time)--(b,0));
draw(pic, f, FCNPEN);

real c = 0.75;
draw(pic, (c,0)--(c,f2(c)), highlight_color);

real[] T2 = {1, b}; 

xaxis(pic,Label("$x$",position=EndPoint, align=S),
       0-0.1,1.75, black,
      ticks=RightTicks("%", T2, Size=2pt),
      Arrow(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$\sqrt{\pi/2}$", b);

yaxis(pic,Label("$y$"),
       0-0.1,1+0.25, black,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      Arrow(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


