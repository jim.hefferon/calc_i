// shells.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.prc = true;
settings.render=0;
import fontsize;

import graph3; import solids;
currentprojection = orthographic(5,2,1.5,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "shells_3d%03d";
real PI = acos(-1);

material figure_material = material(diffusepen=bold_color+opacity(0.40),
				 emissivepen=bold_color,
				 specularpen=white+bold_color);
material slice_material = material(diffusepen=highlight_color+white+opacity(0.5),
				   emissivepen=highlight_color+white,
				   specularpen=highlight_color+white);

currentlight = nolight;



// ......... between (x+1)^2 and x axis ........
real f0(real x) {return 1/(1+x^2);}
 
// ........ without discs ...........
picture pic;
int picnum = 0;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

currentprojection = orthographic(1,0.25,0.5,up=Z);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f0,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7));
draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7));

surface solid = surface(region_pth3, c=O, axis=Y);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// real c0 = 1/3;
// real c1 = 2/3;
// transform3 slice_t0 = shift((0,c0,0))*rotate(90, X); 
// surface slice0 = slice_t0*unitdisk;
// transform3 slice_t1 = shift((0,c1,0))*rotate(90, X)*scale(f0(c1),f0(c1),1); 
// surface slice1 = slice_t1*unitdisk;
// draw(pic, slice0, surfacepen=slice_material);
// draw(pic, slice_t0*unitcircle3, highlight_color);
// draw(pic, slice1, surfacepen=slice_material);
// draw(pic, slice_t1*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


 
// ........ with discs ...........
picture pic;
int picnum = 1;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// currentprojection = orthographic(1,0.25,0.25,up=Z);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f0,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
// draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7));
// draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7));

surface solid = surface(region_pth3, c=O, axis=Y);
draw(pic, solid, surfacepen=figure_material, light=nolight);

real c0 = 1/3;
real c1 = 2/3;
transform3 slice_t0 = shift((0,c0,0))*rotate(90, X); 
surface slice0 = slice_t0*unitdisk;
transform3 slice_t1 = shift((0,c1,0))*rotate(90, X)*scale(f0(c1),f0(c1),1); 
surface slice1 = slice_t1*unitdisk;
draw(pic, slice0, surfacepen=slice_material);
draw(pic, slice_t0*unitcircle3, highlight_color);
draw(pic, slice1, surfacepen=slice_material);
draw(pic, slice_t1*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


 
// ........ with shells ...........
picture pic;
int picnum = 2;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// currentprojection = orthographic(1,0.25,0.25,up=Z);

real a = 0;
real b = 1;
real ybot = -1;
real ytop = 2;

path f = graph(pic,f0,a, b);
real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
// path region_pth = subpath(f,f_a_time,f_b_time);
path3 region_pth3 = path3(region_pth);
draw(pic, surface(region_pth3), gray(0.8), light=nolight);

draw(pic, region_pth3, gray(0.5));
draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7));
draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7));

surface solid = surface(region_pth3, c=O, axis=Y);
draw(pic, solid, surfacepen=figure_material, light=nolight);

real c = 0.75;
transform3 slice_t = rotate(-90, X)*scale(c,c,f0(c)); 
surface slice = slice_t*unitcylinder;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);
draw(pic, shift(0,f0(c),0)*slice_t*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,1.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


 
// ........ shells fill the region ...........
int num_shells = 5;
for (int k=0; k<num_shells; ++k) {
  picture pic;
  int picnum = 2+k;
  size(pic,0,5cm);
  size3(pic,0,5cm,0,keepAspect=true);
  
  // currentprojection = orthographic(1,0.25,0.25,up=Z);
  
  real a = 0;
  real b = 1;
  real ybot = -1;
  real ytop = 2;

  path f = graph(pic,f0,a, b);
  real f_a_time = intersect(f, (a,0)--(a,ytop))[0];
  real f_b_time = intersect(f, (b,0)--(b,ytop))[0];
  path region_pth = ( (a,0)--point(f,f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle; 
  // path region_pth = subpath(f,f_a_time,f_b_time);
  path3 region_pth3 = path3(region_pth);
  draw(pic, surface(region_pth3), gray(0.8), light=nolight);

  draw(pic, region_pth3, gray(0.5));
  draw(pic, circle((0,1/2,0), 1, normal=Y), gray(0.7));
  draw(pic, circle((0,0,0), 1, normal=Y), gray(0.7));
  
  surface solid = surface(region_pth3, c=O, axis=Y);
  draw(pic, solid, surfacepen=figure_material, light=nolight);

  real c = a+((k+1)/(num_shells+1))*(b-a);
  transform3 slice_t = rotate(-90, X)*scale(c,c,f0(c)); 
  surface slice = slice_t*unitcylinder;
  draw(pic, slice, surfacepen=slice_material);
  draw(pic, slice_t*unitcircle3, highlight_color);
  draw(pic, shift(0,f0(c),0)*slice_t*unitcircle3, highlight_color);

  xaxis3(pic,Label("$x$",position=EndPoint, align=W),
	 0,1.75, black, Arrow3(TeXHead2));
  yaxis3(pic,Label("$y$"),
	 0,1.5, black, Arrow3(TeXHead2));
  zaxis3(pic,Label(""),				\
	 0,1.25, black, Arrow3(TeXHead2));

  shipout(format(OUTPUT_FN,picnum),pic);
}


// ........ shell cut flat ..........
picture pic;
int picnum = 7;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

real x = 0.681;
real length = 2*PI*x; 
path3 shell_pth = (0,0,0)--(length,0,0)--(length,f0(x),0)--(0,f0(x),0)--(0,0,0)--cycle;
surface shell = surface(shell_pth);
draw(pic, shell, surfacepen=slice_material);
draw(pic, shell_pth, highlight_color);

// xaxis3(pic,Label("$x$",position=EndPoint, align=W),
//        0,1.75, black, Arrow3(TeXHead2));
// yaxis3(pic,Label("$y$"),
//        0,1.5, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),				\
//        0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



 
// === dowel ============

// ... without shell ......
picture pic;
int picnum = 8;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

currentprojection = orthographic(2,0.75,0.5,up=Z);

real ybot = -1;
real ytop = 2;

real ang = 60;
real start_angle = 90-ang;
real end_angle = 90+ang;
path fourtyfive_pth = (0,0)--3*(Cos(start_angle),Sin(start_angle));
path minusfourtyfive_pth = (0,0)--3*(Cos(end_angle),Sin(end_angle));
real arc_a_time = intersect(unitcircle, fourtyfive_pth)[0];
real arc_b_time = intersect(unitcircle, minusfourtyfive_pth)[0];
path3 dowel_pth3 = path3(subpath(unitcircle,arc_b_time,arc_a_time)--point(unitcircle,arc_b_time)--cycle);

// draw(pic, surface(dowel_pth3), gray(0.8));
// draw(pic,dowel_pth3, gray(0.7));

path3 front_circle = shift(point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
path3 back_circle = shift(-point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
draw(pic, front_circle, gray(0.6));
draw(pic, back_circle, gray(0.6));

surface solid = surface(dowel_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw the shell
// real c = 0.75;
// transform3 slice_t = shift(sqrt(1-c^2),0,0)*rotate(-90, Y)*scale(c,c,2*sqrt(1-c^2)); 
// surface slice = slice_t*unitcylinder;
// draw(pic, slice, surfacepen=slice_material);
// draw(pic, slice_t*unitcircle3, highlight_color);
// draw(pic, shift(-2*sqrt(1-c^2),0,0)*slice_t*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ... with the shell ......
picture pic;
int picnum = 9;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

currentprojection = orthographic(2,0.75,0.5,up=Z);

real ybot = -1;
real ytop = 2;

real ang = 60;
real start_angle = 90-ang;
real end_angle = 90+ang;
path fourtyfive_pth = (0,0)--3*(Cos(start_angle),Sin(start_angle));
path minusfourtyfive_pth = (0,0)--3*(Cos(end_angle),Sin(end_angle));
real arc_a_time = intersect(unitcircle, fourtyfive_pth)[0];
real arc_b_time = intersect(unitcircle, minusfourtyfive_pth)[0];
path3 dowel_pth3 = path3(subpath(unitcircle,arc_b_time,arc_a_time)--point(unitcircle,arc_b_time)--cycle);

draw(pic, surface(dowel_pth3), gray(0.8));
draw(pic,dowel_pth3, gray(0.7));

path3 front_circle = shift(point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
path3 back_circle = shift(-point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
draw(pic, front_circle, gray(0.6));
draw(pic, back_circle, gray(0.6));

surface solid = surface(dowel_pth3, c=O, axis=X);
draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw the shell
real c = 0.75;
transform3 slice_t = shift(sqrt(1-c^2),0,0)*rotate(-90, Y)*scale(c,c,2*sqrt(1-c^2)); 
surface slice = slice_t*unitcylinder;
draw(pic, slice, surfacepen=slice_material);
draw(pic, slice_t*unitcircle3, highlight_color);
draw(pic, shift(-2*sqrt(1-c^2),0,0)*slice_t*unitcircle3, highlight_color);

// dotfactor=4;
// dot(pic, "$z$",(0,0,z),NE);

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ... with the shell and the region in the xy-plane ......
picture pic;
int picnum = 10;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

currentprojection = orthographic(2,0.75,0.5,up=Z);

real ybot = -1;
real ytop = 2;

real ang = 60;
real start_angle = 90-ang;
real end_angle = 90+ang;
path fourtyfive_pth = (0,0)--3*(Cos(start_angle),Sin(start_angle));
path minusfourtyfive_pth = (0,0)--3*(Cos(end_angle),Sin(end_angle));
real arc_a_time = intersect(unitcircle, fourtyfive_pth)[0];
real arc_b_time = intersect(unitcircle, minusfourtyfive_pth)[0];
path3 dowel_pth3 = path3(subpath(unitcircle,arc_b_time,arc_a_time)--point(unitcircle,arc_b_time)--cycle);

draw(pic, surface(dowel_pth3), gray(0.8));
draw(pic,dowel_pth3, gray(0.7));

path3 front_circle = shift(point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
path3 back_circle = shift(-point(unitcircle,arc_a_time).x,0,0)*scale3(point(unitcircle,arc_a_time).y)*rotate(90, Y)*unitcircle3;
// draw(pic, front_circle, gray(0.8)); // for hole
// draw(pic, back_circle, gray(0.8));

// surface solid = surface(dowel_pth3, c=O, axis=X);
// draw(pic, solid, surfacepen=figure_material, light=nolight);

// draw the shell
real c = 0.75;
transform3 slice_t = shift(sqrt(1-c^2),0,0)*rotate(-90, Y)*scale(c,c,2*sqrt(1-c^2)); 
surface slice = slice_t*unitcylinder;
draw(pic, slice, surfacepen=slice_material);
path3 shell_front = slice_t*unitcircle3;  // circle on front of dowel
path3 shell_rear = shift(-2*sqrt(1-c^2),0,0)*slice_t*unitcircle3;
draw(pic, shell_front, highlight_color);
draw(pic, shell_rear, highlight_color);

// slice in the xy plane
draw(pic, point(shell_front,1)--point(shell_rear,1));
dotfactor=3;
dot(pic, "$(\sqrt{R^2-y^2},y,0)$",point(shell_front,1),1.5*SE);
dot(pic,point(shell_rear,1));

xaxis3(pic,Label("$x$",position=EndPoint, align=W),
       0,2.75, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,1.25, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
       0,1.25, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




