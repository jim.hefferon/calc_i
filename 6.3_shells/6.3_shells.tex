\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\usepackage{etoolbox}
\newbool{threed}
\booltrue{threed}
%\boolfalse{threed}

\usepackage{graphicx}
\graphicspath{{asy/}}
\usepackage{asymptote}
\ifbool{threed}{
  \input asy/shells_3d.pre 
  \def\asydir{asy}
}{}

\title{Section 6.3:\ \ Method of Shells}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Shells instead  of discs}
In the section on discs
we rotated the region below $f(x)=1/(1+x^2)$ about the $y$~axis.
\begin{center}
\ifbool{threed}{
  \input asy/shells_3d000.tex %
}{
  \includegraphics{asy/shells_2d000.pdf}
}
\end{center}
The method of discs called on us to integrate~$dy$.
We needed two integrals because there are two types of slices:~one
covering $0\leq y\leq 1/2$ and 
the other covering $1/2< y\leq 1$.
\end{frame}

\begin{frame}
Here is the solid.
\uncover<2->{And here are the two kinds of discs.}
\begin{center}
\ifbool{threed}{
  % \input asy/shells_3d001.tex %
  \only<1>{\vcenteredhbox{\input asy/shells_3d000.tex }}%
  \only<2->{\vcenteredhbox{\input asy/shells_3d001.tex }}%

}{
  \only<1>{\vcenteredhbox{\includegraphics{asy/shells000.png}}}%
  \only<2->{\vcenteredhbox{\includegraphics{asy/shells001.png}}}%
}
\end{center}
\end{frame}

\begin{frame}
In this section we will instead integrate~$dx$.
The slices are not through the axis of rotation, they are parallel to it.
\begin{center}
\ifbool{threed}{
  \input asy/shells_3d001.tex %
}{
  \includegraphics{asy/shells_2d001.pdf}
}
\end{center}
\end{frame}

\begin{frame}
Rotating each such slice about the $y$~axis results in a \alert{shell}.
Just as the discs fill the solid so do the shells, for $0\leq x\leq 1$. 
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/shells002.png}}}%
  \only<2>{\vcenteredhbox{\includegraphics{asy/shells003.png}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/shells004.png}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/shells005.png}}}%
  \only<5->{\vcenteredhbox{\includegraphics{asy/shells006.png}}}
\end{center}
\end{frame}

\begin{frame}
\begin{center}
 \ifbool{threed}{
  \input asy/shells_3d005.tex %
}{
  \vcenteredhbox{\includegraphics{asy/shells005.png}}%
}
% \vcenteredhbox{\includegraphics{asy/shells005.png}}%
  \qquad
\ifbool{threed}{
  \input asy/shells_3d007.tex %
}{
  \vcenteredhbox{\includegraphics{asy/shells007.png}}%
}
%   \vcenteredhbox{\includegraphics{asy/shells007.png}}%
\end{center}
If we take the shell at~$x$, cut it, and lay it flat, then it
is  $2\pi x$ long (the circumference of its circle), $f(x)$~wide,
and $dx$~thick.
\begin{equation*}
  \text{Volume}=\int_{x=0}^1 2\pi x\cdot \frac{1}{1+x^2}\,dx
\end{equation*}
\end{frame}

\begin{frame} 
This is the integral.
\begin{equation*}
  2\pi\cdot \int_{x=0}^1 \frac{x}{1+x^2}\,dx
\end{equation*}
Use the substitution $u=1+x^2$, so that $du=2x\,dx$.
\begin{align*}
  2\pi\cdot \int_{u=1}^2 \frac{1}{u}\cdot \frac{1}{2}\,du
  &= \pi\cdot \biggl[ \ln(u) \biggr]_{u=1}^2  \\
  &= \pi\cdot [ \ln(2) -\ln(1) ]   \\
  &= \pi \ln(2)
\end{align*}
\end{frame}


\begin{frame}{Practice}\vspace*{-1ex}
\Ex Sketch the region between $y=\sin(x^2)$ and the $x$~axis from $a=0$ to
$b=\sqrt{\pi/2}$.
Rotate that region about the $y$~axis, and 
find the volume of the solid using shells, by integrating~$dx$.
\begin{center}
  \includegraphics{asy/shells_2d002.pdf}%
\end{center}
\pause
The integral is 
\begin{equation*}
  \int_{x=0}^{\sqrt{\pi/2}}2\pi x\cdot\sin(x^2)\,dx
\end{equation*}
and the substitution $u=x^2$ (so that $du=2x\,dx$) gives this.
\begin{equation*}
  \pi\cdot \int_{u=0}^{\pi/2}\sin u\,du
  =\pi\Bigl[ -\cos u\Bigr]_{u=0}^{\pi/2}
  =\pi
\end{equation*}
\end{frame}


\begin{frame}{A drilled sphere}
Start with a sphere, and drill a hole through it along the $x$~axis.
\begin{center}
  \only<1>{\ifbool{threed}{
  \input asy/shells_3d008.tex %
}{
  \vcenteredhbox{\includegraphics{asy/shells008.png}}%
}}
% \vcenteredhbox{\includegraphics{asy/shells008.png}}}%
  \only<2->{\ifbool{threed}{
  \input asy/shells_3d009.tex %
}{
  \vcenteredhbox{\includegraphics{asy/shells009.png}}%
}}
% \vcenteredhbox{\includegraphics{asy/shells009.png}}}%
\end{center}
How much material is left?
Use shells, that is, integrate~$dy$.
\end{frame}


\begin{frame}
Take the radius of the sphere to be $R$ and the radius of the drilled hole
to be~$r$.
The shown cap of a circle in the $xy$-plane is part of $x^2+y^2=R^2\!$.
\begin{center}
  \input asy/shells_3d010.tex %  
\end{center}
So front to back the shell is $2\,\sqrt{R^2-y^2}$.
\begin{equation*}
  \int_{y=r}^R 2\pi y\cdot 2\,\sqrt{R^2-y^2}\,dy
\end{equation*}
\end{frame}

\begin{frame}
Use the substitution $u=R^2-y^2$ so that $du=-2y\,dy$.
\begin{align*}
  \int_{y=r}^R 2\pi y\cdot 2\,\sqrt{R^2-y^2}\,dy
    &=-2\pi\int_{y=r}^R \sqrt{R^2-y^2}\cdot (-2y)\,dy     \\
    &=-2\pi\cdot \int_{u=R^2-r^2}^{0} \sqrt{u}\,du    \\
    &=-2\pi\,\Bigl[ \frac{2}{3}u^{3/2}\Bigr]_{u=R^2-r^2}^{0}  \\
    &=-\frac{4}{3}\pi\,\Bigl[ (0^{3/2})-(R^2-r^2)^{3/2}\Bigr]  \\
    &=\frac{4\pi}{3}(R^2-r^2)^{3/2}
\end{align*}
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item
Sketch the region between the graph of $x=6y-y^2$ and the $y$~axis.
Use shells to 
find the volume of the solid from rotating that region about the $x$~axis.

\pause
The answer is $\int_{y=0}^6 2\pi y\cdot(-y^2+6y)\,dy=216\pi$.

\item Sketch the region between $y=9-x$ and $y=9-3x$.
Use shells to find the volume of the solid that you get from rotating 
that region about the $y$~axis.

\pause
The answer is $\int_{x=0}^3 2\pi x\cdot (3x-x^2)\,dx=27\pi/2$.
\end{enumerate}
\end{frame}




\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
