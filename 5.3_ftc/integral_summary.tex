\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.3:\ \ Integral summary}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Accumulation of a continuous quantity}


\begin{frame}
\Ex A house with solar panels sometimes will take energy
from the power grid
and sometimes instead supplies energy to the grid.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/rooftop-solar-panels.jpg}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/ftc036.pdf}}
\end{center}
The graph's vertical axis is kilowatts and the horizontal axis is hours.
At the start it is night and the house takes in kilowatts, while later it is
day and the house sends back kilowatts.

\pause
Units on the horizontal axis are hours and units on the vertical axis
are kilowatts, so each box is a kilowatt-hour.
Boxes below the axis are negative while those above the axis are positive. 
What is the total?
\end{frame}




\begin{frame}{Sweep out the region}
Here we find the size $F(x)$ of the region below $f(t)$ and above the $t$-axis.
The region's left is at $t=0$.
Its right side is at $t=x$.
\begin{center}
  \includegraphics{asy/ftc037.pdf}    
\end{center}
We write this \alert{definite integral}.
\begin{equation*}
  F(x)=\int_{t=0}^xf(t)\,dt
\end{equation*}
Note that the upper limit of integration is the variable~$x$.

\pause
It says ``size.''
Sometimes we want the signed area and sometimes we want the
unsigned area, which many authors call the geomertic area and which the
text just calls ``area.''
\end{frame}



\begin{frame}{Fundamental Theorem of Calculus}
\Tm Let $f(t)$ be continuous on the interval $\closed{a}{b}$.
Then this function
\begin{equation*}
  F(x) = \int_{t=a}^{x}f(t)\,dt
  \qquad a\leq x\leq b
\end{equation*}
is an antiderivative of $f(x)$, so that $F'(x)=f(x)$.
(It is also continuous on $\closed{a}{b}$ and differentiable on
$\open{a}{b}$.)
Consequently, 
\begin{equation*}
  \int_{t=a}^bf(t)\,dt=F(b)-F(a)
\end{equation*}
for any antiderivative~$A\!$.

\medskip
We use a vertical bar notation or square brackets notation for the difference.
\begin{equation*}
  \Bigl. F(x)\Bigr|_{x=a}^b=
  \biggl[ F(x)\biggr]_{x=a}^b\!= 
  F(b)-F(a)
\end{equation*}
\end{frame}

\begin{frame}
\Pf To see why $F(x)=f(x)$, this shows $F(x+\Delta x)-F(x)$.
\begin{center}
  \includegraphics{asy/ftc017.pdf}
\end{center}
It suggests that
$F(x+\Delta x)-F(x)\approx f(x)\cdot \Delta x$.
Then $[F(x+\Delta x)-F(x)]/\Delta x\approx f(x)$
and we take the limit as $\Delta x\to 0$.

\pause
This makes clear the second half of the FTC.
\begin{center}
  \begin{tabular}{@{}c}
    \vcenteredhbox{\includegraphics{asy/ftc032.pdf}}%
    \\
    $\int_a^b f(t)\,dt$
  \end{tabular}  
  equals
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{asy/ftc033.pdf}}%
    \\
    $\int_0^b f(t)\,dt$
  \end{tabular}  
  minus
  \begin{tabular}{c@{}}
    \vcenteredhbox{\includegraphics{asy/ftc034.pdf}}%
    \\
    $\int_0^a f(t)\,dt$
  \end{tabular}  
\end{center}
\end{frame}

\begin{frame}{The problem of integration reduces to antidifferentiation}
\Ex Find the area under $f(x)=x^2$ between $x=0$ and~$x=1$.

\pause
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
  =\biggl[\frac{x^3}{3}\biggr]_{x=0}^{1}
  =\frac{1^3}{3}-\frac{0^3}{3}
  =\frac{1}{3}
\end{equation*}
\begin{center}
  \includegraphics{asy/ftc035.pdf}
\end{center}
\end{frame}


\begin{frame}
\Ex Find the area under $y=\sin\theta$ between $\theta=0$ and $\theta=\pi$.

\pause
We want this.
\begin{equation*}
  \int_{\theta=0}^{\pi}\sin\theta \,d\theta
\end{equation*}
An antiderivative is $A(\theta)=-\cos(\theta)$
(Usually when working with definite integrals we omit the `C', because
it cancels anyway.)
The FTC gives this.
\begin{equation*}
  \Bigl. -\cos\theta \bigr|_{\theta=0}^{\pi}
  =-\cos(\pi)-(-\cos(0))
  =-(-1)-1=2
\end{equation*}
\bigskip
\begin{center}
  \includegraphics{asy/ftc018.pdf}
\end{center}
\end{frame}



\begin{frame}{What is the intuitive meaning of $f$?}
In $\int_a^bf(x)\,dx$, what is the meaning of the integrand $f(x)$?
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc025.pdf}}%
\end{center}
Suppose that the integral computes the area of some 
region.
It cannot be that $f(x)$ is a number of square units associated with~$x$.
For instance, consider the area beneath $f(x)=x^2$, between $a=0$ and $b=3$.
Then $f(1)=1$ and $f(2)=4$, and that's only two of the inputs\Dash 
accumulating all of infinitely many inputs like that would give an 
infinite answer, while the area beneath $x^2$ is finite. 

Instead, $f(x)$ is the instantaneous rate at which we are accumulating stuff 
as we sweep from left to right, from $t=a$ to $t=b$.
\end{frame}



\begin{frame}
\begin{itemize}\setcounter{enumi}{1}
\item
Consider a column of air that is thousands of meters tall, 
over a base that is one meter square. 
Air near the bottom is denser because it 
is squeezed by the weight of the air above.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc028.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/ftc029.pdf}}
\end{center}
The \alert{density function}~$\rho$ expresses this.
Where there is a lot of air, the graph of $\rho$ is high.
Where there is less air, the graph  of $\rho$ is low. 
The total mass of air below the height of Everest is this.
\begin{equation*}
  \int_{x=0}^{8848}\rho(x)\, dx 
\end{equation*}
The integrand gives the rate at which we are accumulating air mass at the
instant~$x$, when we sweep from $0$ to $8848$. 
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize}
\item
Consider electrons in a wire.
In some places there are more
and in other places fewer. 
Instead of us sweeping along the wire, they flow past us as electric current.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc030.pdf}}
\end{center}
The function~$f(t)$ gives the rate of flow of electrons at time~$t$.
\begin{center}
  \includegraphics{asy/ftc031.pdf}
\end{center}
We accumulate the total between $a$ and~$b$ with this.
\begin{equation*}
  \int_{t=a}^{b} f(t)\, dt 
\end{equation*}
The meaning of $f(t)$ is that it is the rate at which we are accumulating
electrons at the instant~$t$, where $a\leq t\leq b$.  
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize} \setcounter{enumi}{2}
\item
In the US the height of adult women is well approximated by a Normal 
curve with a mean of $\mu=63.6$~inches and $\sigma=2.5$.
If we measure a hundred women we might get this.  
\begin{center}
  \includegraphics{asy/ftc026.pdf}
\end{center}
The dots are densely packed where there are more women and loosely packed
where there are fewer.
The function~$f$ expresses this.
\begin{center}
  \includegraphics{asy/ftc027.pdf}
\end{center}
Then the percentage of women with height less than~$x$ is this.
\begin{equation*}
  \int_{t=-\infty}^{x}f(t)\,dt
\end{equation*}
(The $-\infty$ just means to cover all cases to the left of $x$.)
\end{itemize}
\end{frame}


\begin{frame}
So $f(x)$ is the density of stuff associated with the location~$x$,
the rate at which we accumulate stuff as we sweep past,
whether the stuff is air, or electricity, or probability, etc.

Imagine that we have a string.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/string.png}}%
\end{center}
Some of the string is make of candy floss, which is not very dense,
and some is lead.
Find the mass of the string by taking the integral of the string's density,
from the beginning to the end.

At first we will accumulate area, and volume.
In this case, the integrand $f$ is the instantaneous rate at which are 
accumulating geometric content.
But integration is more general than that, and applies to many
different kinds of stuff.
Integration is like addition, except that it is the accumulation
of a continuous quantity. 
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
