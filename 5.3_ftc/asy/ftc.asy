// ftc.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "ftc%03d";
real PI = acos(-1);

import graph;
import stats;


// ==== general function ====

path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;

for(int i=0; i < 10; ++i) {
  picture pic;
  int picnum = 0+i;
  size(pic,4cm,0,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=3;

  path f = shift(0,2)*general_fcn;

  // region to be integrated  
  real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
  real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
  path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
  fill(pic, region, background_color);
  draw(pic, (a,ybot)--point(f,f_a_time));
  draw(pic, point(f,f_b_time)--(b,ybot));
  // draw(pic, subpath(f, f_a_time, f_b_time), green);

  // sweep left to right
  real x = a+(b-a)*(i/9);
  real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
  path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
  fill(pic, region_done, bold_color+opacity(0.6));
  draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

  draw(pic, f, FCNPEN);

  real[] T13 = {a,b};
  xaxis(pic, L="\tiny $x$",  
	axis=YZero,
	xmin=xleft-0.25, xmax=xright+.25,
	p=currentpen,
	ticks=RightTicks("%",T13,Size=2pt),
	arrow=Arrow(TeXHead));
  labelx(pic, "a", a);
  labelx(pic, "b", b);
  
  yaxis(pic, L="\tiny $y$",  
	axis=XZero,
	ymin=ybot-0.2, ymax=ytop,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrow(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic);
}


// ===== usual integration picture ======
picture pic;
int picnum = 10;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, background_color);
draw(pic, (a,ybot)--point(f,f_a_time));
draw(pic, point(f,f_b_time)--(b,ybot));

draw(pic, f, FCNPEN);

real[] T10 = {a,b};
xaxis(pic, L="\tiny $x$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrow(TeXHead));
labelx(pic, "a", a);
labelx(pic, "b", b);

yaxis(pic, L="\tiny $y$",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== given velocity, find position ====
real f11(real x) {return (1/2)*x+2;}


for (int t=0; t<6; ++t) {
  picture pic;
  int picnum = 11+t;
  size(pic,4cm,0,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=5;

  draw_graphpaper(pic, xleft, xright, ytop, ybot);

  if (t == 0) {
    label(pic, "$v(t)=t/2+2$", (1,1.75), E, filltype=Fill(white));
  }
  path f = graph(f11,xleft-0.1,xright+0.1);
  draw(pic, f, black);
  
  real f_0_time = intersect(f, (0,ybot)--(0,ytop))[0];
  real f_t_time = intersect(f, (t,ybot)--(t,ytop))[0];
  path region = ((0,ybot)--point(f,f_0_time)&subpath(f, f_0_time, f_t_time)&point(f,f_t_time)--(t,ybot)--(0,ybot))--cycle;
  filldraw(pic, region, light_color, black);

  real[] T11 = {t};
  xaxis(pic, L="sec",  // label
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+1.5,
	p=currentpen,
	ticks=RightTicks("%", T11, Size=2pt),
	arrow=Arrows(TeXHead));
  if (t > 0) {
    labelx(pic,format("$%d$",t),t);
  }
  
  yaxis(pic, L="m/s",  // label
	axis=XZero,
	ymin=ybot-0.75, ymax=ytop+0.25,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}



// ===== Argument for FTC ======
picture pic;
int picnum = 17;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

path f = shift(0,1.5)*general_fcn;

// region to be integrated  
real a=3.5; real b=3.65;
real f_0_time = intersect(f, (0,ybot)--(0,ytop))[0];
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((0,ybot)--point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(0,ybot))--cycle;
filldraw(pic, region, background_color, black);
draw(pic, f, FCNPEN);
path reimann = ((a,ybot)--point(f,f_a_time)--(b,point(f,f_a_time).y)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, reimann, white, highlight_color);


real[] T17 = {a,b};
xaxis(pic, L="\tiny $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T17,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "\makebox[0pt][r]{$x$}", a);
labelx(pic, "\makebox[0pt][l]{$x+\Delta x$}", b);

yaxis(pic, L="\tiny $y$",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== practice definite integrals ====
real f18(real x) {return sin(x);}

picture pic;
int picnum = 18;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(pic, f18, xleft-0.1, xright+0.1);
draw(pic, f, black);
  
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_pi_time = intersect(f, (PI,ybot-1)--(PI,ytop+1))[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_pi_time)&point(f,f_pi_time)--(PI,0)--(0,0))--cycle;
filldraw(pic, region, light_color+opacity(0.5), black);

real[] T18 = {PI/2, PI};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T18, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\pi/2$", PI/2);
labelx(pic, "$\pi$", PI);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .............. y=x^2 .............
real f19(real x) {return x^3+x;}

picture pic;
int picnum = 19;
scale(pic,Linear,Linear(0.2));
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=35;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(pic, f19, xleft-0.1,  xright+0.1);
draw(pic, f, black);
  
real f_0_time = intersect(f, (1,ybot-1)--(1,ytop+1))[0];
real f_1_time = intersect(f, (3,ybot-1)--(3,ytop+1))[0];
path region = ((1,0)--point(f,f_0_time)&subpath(f, f_0_time, f_1_time)&point(f,f_1_time)--(3,0)--(1,0))--cycle;
filldraw(pic, region, light_color+opacity(0.5), black);

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-2.5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0,35),Size=3pt,size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======== Area fcn ===============

// ............. rectangle .............
real f20(real x) {return 2;}

picture pic;
int picnum = 20;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2.5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(pic, f20, xleft-0.1,  xright+0.1);

real x = 3.25; 
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_x_time = intersect(f, (x,ybot-1)--(x,ytop+1))[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_x_time)&point(f,f_x_time)--(x,0)--(0,0))--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

draw(pic, f, FCNPEN);

label(pic, "$A(x)$", (x/2,f20(x/2)/2), filltype=Fill(white));

real[] T20 = {x};
xaxis(pic, L="\tiny $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T20, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);

Label yLabel = Label("\tiny $f(t)$");
yaxis(pic, L=rotate(0)*yLabel,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............. triangle up .............
real f21(real x) {return x/2;}

picture pic;
int picnum = 21;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2.5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(pic, f21, xleft-0.1,  xright+0.1);

real x = 3.5; 
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_x_time = intersect(f, (x,ybot-1)--(x,ytop+1))[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_x_time)&point(f,f_x_time)--(x,0)--(0,0))--cycle;
fill(pic, region, background_color+opacity(0.6));
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

draw(pic, f, FCNPEN);

label(pic, "$A(x)$", (3*x/4,f21(3*x/4)/2), filltype=Fill(white));

real[] T21 = {x};
xaxis(pic, L="\tiny $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T21, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);

Label yLabel = Label("\tiny $f(t)$");
yaxis(pic, L=rotate(0)*yLabel,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............. triangle down .............
real f22(real x) {return -x+2;}

// ...... left region only .........
picture pic;
int picnum = 22;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f22, xleft-0.1,  xright+0.1);
draw(pic, f, black);

real x = 1.75; 
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_x_time = intersect(f, (x,ybot-1)--(x,ytop+1))[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_x_time)&point(f,f_x_time)--(x,0)--(0,0))--cycle;
filldraw(pic, region, light_color+opacity(0.5), black);
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

label(pic, "$F(x)$", (0.4*x,f22(0.4*x)/3), filltype=Fill(white));

real[] T22 = {x};
xaxis(pic, L="$t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T22, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);

Label yLabel = Label("$f(t)$");
yaxis(pic, L=rotate(0)*yLabel,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ...... left and right region .........
picture pic;
int picnum = 23;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f22, xleft-0.1,  xright+0.1);
draw(pic, f, black);

path left_region = (0,0)--(0,2)--(2,0)--(0,0)--cycle;
filldraw(pic, left_region, light_color+opacity(0.5), black);

real x = 3.75; 
real f_x_time = intersect(f, (x,ybot-1)--(x,ytop+1))[0];
path right_region = (2,0)--(x,0)--(x,f22(x))--(2,0)--cycle;
filldraw(pic, right_region, light_color+opacity(0.5), black);
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

label(pic, "$F(x)$", (2+0.6*(x-2),f22(2+0.6*(x-2))/3), filltype=Fill(white));

real[] T22 = {x};
xaxis(pic, L="$t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T22, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x, align=N);

Label yLabel = Label("$f(t)$");
yaxis(pic, L=rotate(0)*yLabel,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== usual integration picture ======
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 2; real b = 4;

picture pic;
int picnum = 24;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes
for(int i=0; i < 9; ++i) {
  real x_i = a+(b-a)*(i/9);
  real x_iplus = a+(b-a)*((i+1)/9);
  real f_x_i_time = intersect(f, (x_i,ybot)--(x_i,ytop))[0];
  real f_x_iplus_time = intersect(f, (x_iplus,ybot)--(x_iplus,ytop))[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color, highlight_color);
}

real[] T23 = {a,b};
xaxis(pic, L="$x$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T23,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="$y$",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== usual integration picture ======
path area_fcn = (0,0)..(1,1.35)..(2,1.5)..(3,2.00)..(4,2.25)..(5.25,3.0);
real a = 0; real b = 4;

picture pic;
int picnum = 25;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,0)*area_fcn;

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, background_color, black);
draw(pic, f, FCNPEN);

real c = a+ 0.681*(b-a);
real f_c_time = intersect(f, (c,ybot)--(c,ytop))[0];
draw(pic, (c,0)--point(f,f_c_time), highlight_color);

// Reimann boxes
// for(int i=0; i < 9; ++i) {
//   real x_i = a+(b-a)*(i/9);
//   real x_iplus = a+(b-a)*((i+1)/9);
//   real f_x_i_time = intersect(f, (x_i,ybot)--(x_i,ytop))[0];
//   real f_x_iplus_time = intersect(f, (x_iplus,ybot)--(x_iplus,ytop))[0];
//   path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
//   filldraw(pic, reimann_box, light_color, highlight_color);
// }

real[] T25 = {c, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T25,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", c);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ====== women's heghts ========

picture pic;
int picnum = 26;
size(pic,5cm,0,keepAspect=true);

real mean = 63.6; // women's heights
real std_dev = 2.5; // in inches

real xleft, xright, ybot, ytop; // limits of graph
xleft=mean-2.5*std_dev; xright=mean+2.5*std_dev;
ybot=0; ytop=1;

dotfactor = 2;
for (int i =0; i<100; ++i) {
  pair point = (mean+std_dev*Gaussrand(), unitrand());
  dot(pic, point, highlight_color);
}
dot(pic,(mean,1.25), white); // add some breathing room at the top

real[] T26 = {mean, mean-std_dev, mean+std_dev};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T26,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\mu$", mean);
labelx(pic, "$\mu-\sigma$", mean-std_dev);
labelx(pic, "$\mu+\sigma$", mean+std_dev);

// yaxis(pic, L="",  
//       axis=XZero,
//       ymin=ybot-0.75, ymax=ytop+0.75,
//       p=currentpen,
//       ticks=NoTicks,
//       arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .... as gaussian curve ..........

picture pic;
int picnum = 27;
scale(pic,Linear,Linear(5));
size(pic,5cm,0,keepAspect=true);

real mean = 63.6; // women's heights
real std_dev = 2.5; // in inches
real f27(real x) {return Gaussian(x-mean,std_dev);}

real xleft, xright, ybot, ytop; // limits of graph
xleft=mean-3*std_dev; xright=mean+2.5*std_dev;
ybot=0; ytop=1;

f = graph(pic, f27, xleft, xright);

real a = mean-2.5*std_dev;
real b = mean+2.5*std_dev;
real f_a_time = intersect(f, Scale(pic,(a,ybot-1))-- Scale(pic,(a,ytop+1)))[0];
real f_b_time = intersect(f, Scale(pic,(b,ybot-1))-- Scale(pic,(b,ytop+1)))[0];
path region_pth = ( Scale(pic,(a,ybot))--point(f, f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)) )--cycle;
fill(pic, region_pth, background_color);
draw(pic, f, FCNPEN);

real c = mean+0.4*std_dev;
real f_c_time = intersect(f, Scale(pic,(c,ybot-1))-- Scale(pic,(c,ytop+1)))[0];
path slice = Scale(pic,(c,ybot))--point(f,f_c_time);
draw(pic, slice, highlight_color);

real[] T27 = {mean, mean-std_dev, mean+std_dev, c};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T26,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", c);
// labelx(pic, "$\mu-\sigma$", mean-std_dev);
// labelx(pic, "$\mu+\sigma$", mean+std_dev);

yaxis(pic, L="",  
      axis=XEquals(mean-3*std_dev),
      ymin=ybot-0.15, ymax=ytop+0.15,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ====== air in a column ========
srand(321);

picture pic;
int picnum = 28;
scale(pic,Linear(1000),Linear);
size(pic,0,4cm,keepAspect=true);

real mean = 0; // ground
real std_dev = 3000; // in meters

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=mean; ytop=mean+2.5*std_dev;

dotfactor = 2;
for (int i =0; i<200; ++i) {
  pair point = (unitrand(),mean+std_dev*Gaussrand());
  if (point.y >= 0) {
    dot(pic, Scale(pic,point), highlight_color);
  }
}
// dot(pic,(mean,1.25), white); // add some breathing room at the top

// real[] T26 = {mean, mean-std_dev, mean+std_dev};
xaxis(pic, L="",  
      axis=YZero,
      xmin=0, xmax=1,
      p=currentpen,
      ticks=NoTicks,
      arrow=None);

yaxis(pic, L="",  
      axis=XZero,
      ymin=0, ymax=10000,
      p=currentpen,
      ticks=LeftTicks(Step=5000,step=1000,OmitTick(0,10000),Size=2pt,size=1pt),
      arrow=Arrow(TeXHead));
labely(pic,"$10000$", 10000);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... pressure above sea level ..........

real f29(real x) {return 101.325*(1-(0.0000225577)*x)^(5.25588);}  // in kiloPascals

picture pic;
int picnum = 29;
scale(pic,Linear,Linear(25));
size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10000;
ybot=0; ytop=100;

f = graph(pic, f29, xleft, xright);

real a = 0;
real b = 8848;  // hgt Everest, meters
real f_a_time = intersect(f, Scale(pic,(a,ybot-1))-- Scale(pic,(a,ytop+10)))[0];
real f_b_time = intersect(f, Scale(pic,(b,ybot-1))-- Scale(pic,(b,ytop+10)))[0];
path region_pth = ( Scale(pic,(a,ybot))--point(f, f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)) )--cycle;
fill(pic, region_pth, background_color);
draw(pic,point(f,f_b_time)--Scale(pic,(b,ybot)));
draw(pic, f, FCNPEN);

real c = a+0.25*(b-a);
real f_c_time = intersect(f, Scale(pic,(c,ybot-1))-- Scale(pic,(c,ytop+1)))[0];
path slice = Scale(pic,(c,ybot))--point(f,f_c_time);
draw(pic, slice, highlight_color);

real[] T29 = {c, 5000, 10000, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+100,
      p=currentpen,
      ticks=RightTicks("%", T29, Size=2pt),
      arrow=Arrow(TeXHead));
labelx(pic, "$x$", c);
labelx(pic, "$5000$", 5000);
labelx(pic, "\makebox[0.25cm][r]{Everest}", b);
labelx(pic, "$10\,000$", 10000);

yaxis(pic, L="",  
      axis=XZero,
      ymin=0, ymax=ytop+8.5,
      p=currentpen,
      ticks=LeftTicks(Step=50, step=10, OmitTick(0), Size=2pt, size=1pt),
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ====== electrons in a wire ========
srand(321);
real f30() {return 6+1*Gaussrand();}  // two peaks
real f30a() {return 1.5+1*Gaussrand();}  // two peaks

picture pic;
int picnum = 30;
scale(pic,Linear,Linear);
size(pic,5cm,0,keepAspect=true);

real mean = 0; // ground
real std_dev = 3; // in meters

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=8;
ybot=0; ytop=1;

dotfactor = 2;
for (int i =0; i<500; ++i) {
  pair pt = (f30(),unitrand());
  if ((0 <= pt.x) && (pt.x <= xright)) {
    dot(pic, Scale(pic,pt), highlight_color);
  }
  pair pta = (f30a(),unitrand());
  if ((0 <= pta.x) && (pta.x <= xright)) {
    dot(pic, Scale(pic,pta), highlight_color);
  }
}

// dot(pic,(mean,1.25), white); // add some breathing room at the top

// real[] T26 = {mean, mean-std_dev, mean+std_dev};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.1, xmax=xright+0.1,
      p=currentpen,
      ticks=RightTicks(Step=1,Size=2pt),
      arrow=None);
xaxis(pic, L="",  
      axis=YEquals(1),
      xmin=xleft-0.1, xmax=xright+0.1,
      p=currentpen,
      ticks=LeftTicks("%", Step=1,Size=2pt),
      arrow=None);

// yaxis(pic, L="",  
//       axis=XZero,
//       ymin=0, ymax=10000,
//       p=currentpen,
//       ticks=LeftTicks(Step=5000,step=1000,OmitTick(0,10000),Size=2pt,size=1pt),
//       arrow=Arrow(TeXHead));
// labely(pic,"$10000$", 10000);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... electrons in a wire ..........

picture pic;
int picnum = 31;
scale(pic,Linear,Linear);
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=8;
ybot=0; ytop=1;

path two_peaks = (0,0.3)..(1.5,0.8)..(4,0.2)..(6,0.75)..(8,0.25);
path f = shift(0,2)*two_peaks;

real a = 1;
real b = 6.5;  // 
real f_a_time = intersect(f, Scale(pic,(a,ybot-1))-- Scale(pic,(a,ytop+10)))[0];
real f_b_time = intersect(f, Scale(pic,(b,ybot-1))-- Scale(pic,(b,ytop+10)))[0];
path region_pth = ( Scale(pic,(a,ybot))--point(f, f_a_time)&subpath(f,f_a_time,f_b_time)&point(f,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)) )--cycle;
filldraw(pic, region_pth, background_color,black);
draw(pic, f, FCNPEN);

// real c = mean+0.4*std_dev;
// real f_c_time = intersect(f, Scale(pic,(c,ybot-1))-- Scale(pic,(c,ytop+1)))[0];
// path slice = Scale(pic,(c,ybot))--point(f,f_c_time);
// draw(pic, slice, highlight_color);

real[] T30 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.1, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T30, Size=2pt),
      arrow=Arrow(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=0, ymax=3+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ===== More for the argument for FTC ======

path shorter_general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0);
real b = 3.5;
real a = 0.5;

// ..... a to b ............
picture pic;
int picnum = 32;
size(pic,2.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=1.5;

path f = shift(0,1.25)*shorter_general_fcn;

// region to be integrated  
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path ab_region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, ab_region, background_color, black);
draw(pic, f, FCNPEN);

real[] T32 = {a,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T32,Size=2pt),
      arrow=None);
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=None);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... 0 to b ............
picture pic;
int picnum = 33;
size(pic,2.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=1.5;

path f = shift(0,1.25)*shorter_general_fcn;

// region to be integrated  
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path zb_region = ((0,ybot)--point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(0,ybot))--cycle;
filldraw(pic, zb_region, background_color, black);
draw(pic, f, FCNPEN);

real[] T33 = {b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T33,Size=2pt),
      arrow=None);
// labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=None);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ..... 0 to a ............
picture pic;
int picnum = 34;
size(pic,2.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=1.5;

path f = shift(0,1.25)*shorter_general_fcn;

// region to be integrated  
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path za_region = ((0,ybot)--point(f,f_0_time)&subpath(f, f_0_time, f_a_time)&point(f,f_a_time)--(a,ybot)--(0,ybot))--cycle;
filldraw(pic, za_region, background_color, black);
draw(pic, f, FCNPEN);

real[] T34 = {a};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T34,Size=2pt),
      arrow=None);
labelx(pic, "$a$", a);
// labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=None);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ==== one more practice definite integrals ====
real f35(real x) {return x^2;}

picture pic;
int picnum = 35;
size(pic,0,2.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f35, xleft-0.2, xright+0.1);
draw(pic, f, black);
  
real f_0_time = intersect(f, (0,ybot-1)--(0,ytop+1))[0];
real f_1_time = intersect(f, (1,ybot-1)--(1,ytop+1))[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_1_time)&point(f,f_1_time)--(1,0)--(0,0))--cycle;
filldraw(pic, region, light_color, black);

real[] T35 = {1};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T35, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// for integral_summary.tex

// ==== solar panel ====

path panel_fcn = (0,-2)..(1,-0.5)..(2,1)..(3,2.25)..(4,2.0)..(5.25,0.25);

picture pic;
int picnum = 36;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-1; ytop=3;

real a = 0; real b = 4.6;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = shift(0,1)*panel_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_0_time = intersect(f, (0,ybot)--(0,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path left_region = ( point(f,f_a_time)&subpath(f, f_a_time, f_0_time)&point(f,f_0_time)--(0,0)--point(f,f_a_time) )--cycle;
path right_region = ( point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,0)--(0,0) )--cycle;
filldraw(pic, left_region, gray(0.8)+opacity(0.60), black);
filldraw(pic, right_region, gray(0.8)+opacity(0.60), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// // sweep left to right
// real x = a+(b-a)*(i/9);
// real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
// path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
// fill(pic, region_done, light_color);
// draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T10 = {b};
xaxis(pic, L="\scriptsize $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", b);
// labelx(pic, "$b$", b);

Label L = Label("\scriptsize kw",EndPoint,W);
yaxis(pic, L=rotate(0)*L,  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .... area function ......
real a = 0; real b = 4.2;

picture pic;
int picnum = 37;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, gray(0.85)+opacity(0.5), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);
draw(pic, (b,0)--point(f,f_b_time), highlight_color);

real[] T37 = {b};
xaxis(pic, L="$t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T37,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "x", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

