\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.3:\ \ Fundamental Theorem of Calculus}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Area function}

\begin{frame}
We want the area of the region below the function's graph and above the axis,
between $x=a$ and $x=b$.
We sweep right to left,
\alert{integrating} the columns to make the whole.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/ftc010.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{asy/ftc000.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/ftc001.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/ftc002.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{asy/ftc003.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{asy/ftc004.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{asy/ftc005.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{asy/ftc006.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{asy/ftc007.pdf}}}%
  \only<10>{\vcenteredhbox{\includegraphics{asy/ftc008.pdf}}}%
  \only<11->{\vcenteredhbox{\includegraphics{asy/ftc009.pdf}}}%
\end{center}
\pause
\only<11->{%
We denote this with the \alert{definite integral}.
\begin{equation*}
  \text{Area}=\int_{x=a}^{b}f(x)\,dx
\end{equation*}
}
\end{frame}


\begin{frame}{Area function $A(x)$}
Here we find the area $A(x)$ below $f(t)=2$ and above the $t$-axis.
The region's left is at $t=0$.
Its right side is the variable, so it is at $t=x$.
\begin{center}
  \includegraphics{asy/ftc020.pdf}    
\end{center}
We compute this function of~$x$.
Note that the upper limit of integration is the variable~$x$.
\begin{equation*}
  A(x)=\int_{t=0}^xf(t)\,dt=2x
\end{equation*}
\end{frame}


\begin{frame}
This is the area below $f(t)=t/2$ between $0$ and~$x$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc021.pdf}}%    
\end{center}
\begin{equation*}
  A(x)=\int_{t=0}^x f(t)\,dt=\frac{1}{2}\cdot x\cdot (x/2)=x^2/4
\end{equation*}
\end{frame}



\begin{frame}{Fundamental Theorem of Calculus}
\Tm Let $f(t)$ be continuous on the interval $\closed{a}{b}$.
Then this function
\begin{equation*}
  A(x) = \int_{t=a}^{x}f(t)\,dt
  \qquad a\leq x\leq b
\end{equation*}
is an antiderivative of $f(x)$, so that $A'(x)=f(x)$.
(It is also continuous on $\closed{a}{b}$ and differentiable on
$\open{a}{b}$.)
Consequently, 
\begin{equation*}
  \int_{t=a}^bf(t)\,dt=A(b)-A(a)
\end{equation*}
for any antiderivative~$A\!$.

\medskip
We use a vertical bar notation or square brackets notation for the difference.
\begin{equation*}
  \Bigl. A(x)\Bigr|_{x=a}^b=
  \biggl[ A(x)\biggr]_{x=a}^b\!= 
  A(b)-A(a)
\end{equation*}
\end{frame}

\begin{frame}
\Pf To see why $A'(x)=f(x)$, this shows $A(x+\Delta x)-A(x)$.
\begin{center}
  \includegraphics{asy/ftc017.pdf}
\end{center}
It suggests that
$A(x+\Delta x)-A(x)\approx f(x)\cdot \Delta x$.
Then $[A(x+\Delta x)-A(x)]/\Delta x\approx f(x)$
and we take the limit as $\Delta x\to 0$.

\pause
For the second half of the FTC, this makes it clear.
\begin{center}
  \begin{tabular}{@{}c}
    \vcenteredhbox{\includegraphics{asy/ftc032.pdf}}%
    \\
    $\int_a^b f(t)\,dt$
  \end{tabular}  
  equals
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{asy/ftc033.pdf}}%
    \\
    $\int_0^b f(t)\,dt$
  \end{tabular}  
  minus
  \begin{tabular}{c@{}}
    \vcenteredhbox{\includegraphics{asy/ftc034.pdf}}%
    \\
    $\int_0^a f(t)\,dt$
  \end{tabular}  
\end{center}
\end{frame}

\begin{frame}
\Ex Find the area under $f(x)=x^2$ between $x=0$ and~$x=1$.

\pause
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
  =\biggl[\frac{x^3}{3}\biggr]_{x=0}^{1}
  =\frac{1^3}{3}-\frac{0^3}{3}
  =\frac{1}{3}
\end{equation*}
\begin{center}
  \includegraphics{asy/ftc035.pdf}
\end{center}
\end{frame}

\begin{frame}{What if we use a different antiderivative?}
Consider this integral again.
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
\end{equation*}
The Fundamental Theorem says that $A(x)=\int_a^xf(t)\,dt$
is an antiderivative of $f$.
But $(x^3/3)+7$ also has the derivative of $f(x)=x^2\!$.
Does it matter which antiderivative we use?
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
  =\biggl[\frac{x^3}{3}+7\biggr]_{x=0}^{1}
  \!\!=\bigr(\frac{1^3}{3}+7\bigr)-\bigl(\frac{0^3}{3}+7\bigr)
  =\frac{1}{3}
\end{equation*}
No difference.
You can use the simplest antiderivative.
\end{frame}


\begin{frame}
\Ex Find the area under $y=\sin\theta$ between $\theta=0$ and $\theta=\pi$.

\pause
We want this.
\begin{equation*}
  \int_{\theta=0}^{\pi}\sin\theta \,d\theta
\end{equation*}
An antiderivative is $A(\theta)=-\cos(\theta)$.
The FTC gives this.
\begin{equation*}
  \Bigl. -\cos\theta \bigr|_{\theta=0}^{\pi}
  =-\cos(\pi)-(-\cos(0))
  =-(-1)-1=2
\end{equation*}
\bigskip
\begin{center}
  \includegraphics{asy/ftc018.pdf}
\end{center}
\end{frame}


\begin{frame}
\Ex Find the area under $f(x)=x^3+x$ between $x=1$ and~$x=3$.

\pause
\begin{align*}
  \int_{x=1}^{3}x^3+x\,dx
  &=\Bigl[\frac{1}{4}\cdot x^4+\frac{1}{2}x^2\Bigr]_{x=1}^{3}   \\
  &=\bigl(\frac{1}{4}(3)^4+\frac{1}{2}(3)^2\bigr)-\bigl(\frac{1}{4}(1)^4+\frac{1}{2}(1)^2\bigr)
  =24
\end{align*}
\bigskip
\begin{center}
  \includegraphics{asy/ftc019.pdf}
\end{center}
\end{frame}


\begin{frame}{Definite integration formulas}
The Fundamental Theorem says that the antidifferention formulas apply.
\begin{center}\small
  \begin{tabular}{@{}ll@{}}
    $\displaystyle \int_a^b k\,dx=\biggl[ kx\biggr]_a^b$ 
     &$\displaystyle \int_a^b x^n\,dx=\biggl[ \frac{1}{n+1}x^{n+1}\biggr]_a^b \quad n\neq 1$ \\[3ex]
    $\displaystyle \int_a^b \cos x\,dx=\biggl[ \sin x\biggr]_a^b$ 
     &$\displaystyle \int_a^b \sin x\,dx=\biggl[ -\cos x\biggr]_a^b$ \\[3ex]
    $\displaystyle \int_a^b e^x\,dx=\biggl[ e^x\biggr]_a^b$ 
     &$\displaystyle \int_a^b \frac{1}{x}\,dx=\biggl[ \ln |x|\biggr]_a^b$ 
  \end{tabular}
\end{center}
Definite integrals play nice with constant multiplication, as
well as with addition and subtraction.
\begin{itemize}
\item 
${\displaystyle
  \int_a^b k\cdot f(x)\,dx=k\cdot\int_a^b f(x)\,dx
}$

\item  
${\displaystyle
  \int_a^b f(x)\pm g(x)\,dx=\int_a^b f(x)\,dx \pm\int_a^b g(x)\,dx
}$ 
\end{itemize}

% \begin{equation*}
%   \int c\cdot f(x)\,dx=c\cdot\int f(x)\,dx
%   \qquad \int f(x)\pm g(x)\,dx=\int f(x)\,dx \pm\int g(x)\,dx
% \end{equation*} 
\end{frame}



\begin{frame}{Practice}
Interpret each definite integral
as an area by drawing a graph.
Then evaluate using the Fundamental Theorem.
\begin{enumerate}
\item $\displaystyle \int_{x=0}^3 x^2\,dx$
\pause
\item $\displaystyle \int_{-1}^2 8-x^3\,dx$
% \pause
% \item $\displaystyle \int_{0}^5 -x+6\,dx$
\pause
\item $\displaystyle \int_{3\pi/2}^{2\pi} \cos(x) \,dx$
% \pause
% \item $\displaystyle \int_{0}^{-3} x^2 \,dx$
\end{enumerate}
\end{frame}




\begin{frame}{What we compute}\vspace*{-1ex}
Usually we will want that if $f(t)$ is negative
then the slice contributes a negative.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc022.pdf}}%
  \qquad    
  \vcenteredhbox{\includegraphics{asy/ftc023.pdf}}%    
\end{center}\small
\begin{equation*}
  F(x)=\int_{t=0}^x 2-t\,dt=\Bigl[ 2t-\frac{1}{2}t^2\Bigr]_{t=0}^x
                         =2x-\frac{1}{2}x^2
  \qquad\text{\begin{tabular}{c|c}
  \multicolumn{1}{c}{\textit{$x$}}    
    &\multicolumn{1}{c}{\textit{$F(x)$}} \\ \hline
  $1$ &$1.5$  \\    
  $2$ &$2$  \\    
  $3$ &$1.5$  \\    
  $4$ &$0$  \\    
  $5$ &$-2.5$  \\    
\end{tabular}}
\end{equation*}
% \begin{center}\small
% \begin{tabular}{c|c}
%   \multicolumn{1}{c}{\textit{$x$}}    
%     &\multicolumn{1}{c}{\textit{$A(x)$}} \\ \hline
%   $1$ &$1.5$  \\    
%   $2$ &$2$  \\    
%   $3$ &$1.5$  \\    
%   $4$ &$0$  \\    
%   $5$ &$-2.5$  \\    
% \end{tabular}
% \end{center}
But sometimes we want only positives.
Which we want depends on the application.
(Some authors call the first `signed areas' and the second `geometric areas'.)  
\end{frame}




\begin{frame}{Intuitive meaning of $f$?}
In $\int_a^bf(x)\,dx$, what is the meaning of the integrand $f(x)$?
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc025.pdf}}%
\end{center}
Suppose that the integral computes the area of some 
region.
It cannot be that $f(x)$ is a number of square units associated with~$x$.
For instance, consider the area beneath $f(x)=x^2$, between $a=0$ and $b=3$.
Then $f(1)=1$ and $f(2)=4$, and that's only two of the inputs\Dash 
accumulating all of infinitely many inputs like that would give an 
infinite answer, while the area beneath $x^2$ is finite. 

Instead, $f(x)$ is the instantaneous rate at which we are accumulating stuff 
as we sweep from left to right, from $x=a$ to $x=b$.
\end{frame}



\begin{frame}
\begin{itemize}\setcounter{enumi}{1}
\item
Consider a column of air that is thousands of meters tall, 
over a base that is one meter square. 
Air near the bottom is denser because it 
is squeezed by the weight of the air above.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc028.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/ftc029.pdf}}
\end{center}
The \alert{density function}~$\rho$ expresses this.
Where there is a lot of air, the graph of $\rho$ is high.
Where there is less air, the graph  of $\rho$ is low. 
The total mass of air below the height of Everest is this.
\begin{equation*}
  \int_{x=0}^{8848}\rho(x)\, dx 
\end{equation*}
The integrand gives the rate at which we are accumulating air mass at the
instant~$x$, when we sweep from $0$ to $8848$. 
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize}
\item
Consider electrons in a wire.
In some places there are more
and in other places fewer. 
Instead of us sweeping from left to right while they stand still, 
here the electrons 
flow past us as electric current.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/ftc030.pdf}}
\end{center}
The function~$f(t)$ gives the rate of flow of electrons at time~$t$.
\begin{center}
  \includegraphics{asy/ftc031.pdf}
\end{center}
We accumulate the total between $a$ and~$b$ with this.
\begin{equation*}
  \int_{t=a}^{b} f(t)\, dt 
\end{equation*}
The meaning of $f(t)$ is that it is the rate at which we are accumulating
electrons at the instant~$t$, where $a\leq t\leq b$.  
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize} \setcounter{enumi}{2}
\item
In the US the height of adult women is well approximated by a Normal 
curve with a mean of $\mu=63.6$~inches and $\sigma=2.5$.
If we measure a hundred women we might get this.  
\begin{center}
  \includegraphics{asy/ftc026.pdf}
\end{center}
The dots are densely packed where there are more women and loosely packed
where there are fewer.
The function~$f$ expresses this.
\begin{center}
  \includegraphics{asy/ftc027.pdf}
\end{center}
Then the percentage of women with height less than~$x$ is this.
\begin{equation*}
  \int_{t=-\infty}^{x}f(t)\,dt
\end{equation*}
(The $-\infty$ just means to cover all cases to the left of $x$.)
\end{itemize}
\end{frame}


\begin{frame}
So $f(x)$ is the density of stuff associated with the location~$x$,
the rate at which we accumulate stuff as we sweep past,
whether the stuff is air, or electricity, or probability, etc.

Imagine that we have a string.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/string.png}}%
\end{center}
Some of the string is make of candy floss, which is not very dense,
and some is lead.
Find the mass of the string by taking the integral of the string's density,
from the beginning to the end.

\textbf{Summary:}
To learn integration, at first we will accumulate area and volume.
In this case, the integrand $f$ is the instantaneous rate at which are 
accumulating geometric content.
But integration is more general than that, and applies to many
different kinds of stuff.
Integration is like addition, except that it is the accumulation
of a continuous quantity. 
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
