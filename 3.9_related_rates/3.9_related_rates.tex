\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}


\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.9\ \ Related rates}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}






\begin{frame}
\frametitle{Introduction}
Understanding 
derivatives is about understanding rates of change.
Here we will see that if two variables are
related then their
rates of change are also related.

\pause
First, the basic idea.
On this axis Person~$y$ is always located 
at three times the position of Person~$x$. 
\begin{center}
  \includegraphics{asy/related_rates000.pdf}
\end{center}
This relationship between their positions 
implies that their rates are also related\Dash when 
Person~$x$ moves then Person~$y$ must move three times as fast.
\end{frame}

\begin{frame}
% \exm
Suppose that two quantities are related by $y=3x$.
\begin{center}
  \includegraphics{asy/related_rates001.pdf}  
\end{center}
Suppose further that the two change with time: $y=y(t)$ and $x=x(t)$.
How to find the relationship between the rates $dy/dt$ and $dx/dt$? 

\pause\smallskip
Differentiate (implicitly) both sides.
\begin{equation*}
  \frac{d}{dt}\;y(t)  =  \frac{d}{dt}\;3\cdot x(t) 
  \qquad\Rightarrow\qquad
  \frac{dy}{dt}       =  3\cdot \frac{dx}{dt}
\end{equation*}
\end{frame}



\begin{frame}\vspace*{-1ex}
\Ex Consider $y=100-4.9x^2\!$.
\begin{center}
   \includegraphics{asy/related_rates002.pdf}  
\end{center}
Suppose that $x=x(t)$ and $y=y(t)$.
If we know the rate of change $dx/dt$ then what is the rate of 
change $dy/dt$? 

\pause\smallskip
Differentiate, using the Chain Rule.
\begin{equation*}
  \frac{d\,y(t)}{dt}  =\frac{d\,(\,100-4.9\cdot x(t)^2\,)}{dt} 
                      =-4.9\cdot 2x\cdot\frac{d\,x}{dt}   
\end{equation*}
For instance, 
if $x=4t$ then what is $dy/dt$ at $t=2$? 
Plugging in $x(t)=4t$ and $dx/dt=4$ gives
$dy/dt=-156.8t$.
When $t=2$ we have $dy/dt=-313.6$.
\end{frame}




\begin{frame}
  \frametitle{Outline of the steps}

\begin{enumerate}
\item Find an equation that relates the variables.
\item Differentiate both sides
  (often you need the Chain Rule).
\item Plug in the given quantities and solve for the desired rate. 
\end{enumerate}
\end{frame}





\begin{frame}
\Ex 
Blow up a balloon by adding $2$~$\text{cm}^3$ of air per second.
At this instant the balloon holds $5$~$\text{cm}^3$ of air. 
At what rate is the radius increasing?
% From: http://www.clipartpal.com/clipart_pd/holiday/birthday/birthdayballoon_10041.html
% Described as free, 2011-Aug-07
\begin{center}
  \includegraphics[height=.15\textheight]{balloon.png}
\end{center}
Remember that a sphere's volume is related to its radius by 
$V=(4/3)\pi r^3\!$.

\pause\smallskip
Start with $V(t)=(4/3)\pi\cdot r(t)^3\!$.
Differentiate with respect to~$t$.
\begin{equation*}
  \frac{d\,V(t)}{dt}
  =(4/3)\pi\cdot\underbracket{3r(t)^2}\underbracket{\frac{dr(t)}{dt}}
  =4\pi r^2\frac{d\,r}{dt}
  \tag{$*$}
\end{equation*}
We are given $dV/dt=2$ and $V=5$.
Get~$r$ by solving $5=(4/3)\pi r^3\!$, giving $r=\sqrt[3]{15/4\pi}$. 
Plugging into ($*$) gives this.
\begin{equation*}
  2=4\pi(15/4\pi)^{2/3}\cdot\frac{dr}{dt}
\end{equation*}
(I get $dr/dt\approx 0.057$.)
% sage: 2/(4*pi*(15/(4*pi)^(2/3))).n()
% 0.0573502675885400
\end{frame}



\begin{frame}
\Ex
A ship leaves port traveling east at $400$~$\text{km}/\text{day}$ while
another ship leaves traveling north at $300$~$\text{km}/\text{day}$.
\begin{center}
  \includegraphics{asy/related_rates003.pdf}  
\end{center}
How quickly are they separating three hours later?
%(Ignore the earth's curvature.)

\pause
The relationship is $x(t)^2+y(t)^2=z(t)^2\!$.
Differentiate.
\begin{equation*}
  \underbracket{2x}\underbracket{\frac{dx}{dt}}
     +\underbracket{2y}\underbracket{\frac{dy}{dt}}
     =\underbracket{2z}\underbracket{\frac{dz}{dt}}
\end{equation*}
Cancel the $2$'s.
We are given $dx/dt=400$, $dy/dt=300$, and these.
\begin{equation*}
  % \frac{dx}{dt}=400
  % \quad
  % \frac{dy}{dt}=300
  % \quad
  x(3/24)=50
  \quad
  y(3/24)=75/2
  \quad
  z(3/24)=\sqrt{x^2+y^2}=125/2
\end{equation*}
Putting it together,
$50\cdot 400+(75/2)\cdot 300=125/2\cdot dz/dt$,
giving $dz/dt=500$.
% sage: (3/24)*400
% 50
% sage: (3/24)*300
% 75/2
% sage: (3/24)*300.n()
% 37.5000000000000
% sage: sqrt(50^2+(75/2)^2)
% 125/2
% sage: (50*400+(75/2)*300)*2/125
% 500
\end{frame}



\begin{frame}
\Ex
An observer watches a rocket being launched straight up
from a base that is $5$~$\text{km}$ away.
\begin{center}
  \includegraphics{asy/related_rates004.pdf}  
\end{center}
What is the rate of change of the angle of elevation $\theta$ when the 
rocket is $3$~$\text{km}$ high and is rising at a rate of 
$0.7$~$\text{km}/\text{sec}$?

\pause\smallskip
The relationship is $\tan\theta(t)=y(t)/5$.
Differentiate.
\begin{equation*}
  \underbracket{\sec^2\theta}\cdot\underbracket{\frac{d\theta}{dt}} 
  =(1/5)\cdot \frac{dy}{dt}
\end{equation*}
We are given $dy/dt=0.7$.
Also, the $3$~$\text{km}$ height of the rocket at this instant 
gives that the hypotenuse is $\sqrt{5^2+3^2}=\sqrt{34}$.
So $\sec^2\theta=(1/\cos\theta)^2=(1/(5/\sqrt{34}))^2=34/25$.
We get $d\theta/dt=(1/5)\cdot 0.7\cdot 25/34\approx 0.103$ radians.
% sage: (1/5)*(0.7)*(25/34)
% 0.102941176470588
\end{frame}


% \begin{frame}
% \exm
% What is the rate of change of the distance from the observer to the rocket
% at that moment?
% \centergraphic{rr-03.mps}
% Remember that the 
% rocket is $3$~$\text{km}$ high and is rising at a rate of 
% $0.7$~$\text{km}/\text{sec}$.

% \pause
% The relationship between the variables is $5^2+[y(t)]^2=[z(t)]^2$.
% \begin{equation*}
%   2y(t)\frac{dy}{dt}=2z(t)\frac{dz}{dt}
% \end{equation*}
% We are given that $y(t)=3$ and $dy/dt=0.7$.
% With $y=3$ the Pythagorean Theorem gives
% $z^2=5^2+3^2=34$ and so $z=\sqrt{34}$.
% Calculation shows that 
% $dz/dt=3\cdot 0.7/\sqrt{34}\approx 0.062$.
% \end{frame}



\begin{frame}
\Ex
A $1.5$~meter tall woman walks away from a $6$~meter streetlight.
\begin{center}
  \includegraphics{asy/related_rates005.pdf}  
\end{center}
How fast is the tip of her shadow moving when she is 10~meters
from the light and walking away at $1$~$\text{m}/\text{s}$?

\pause\smallskip
There are similar triangles here.
Let the distance from the streetlight base to the woman be~$a(t)$ and let the 
distance from the streetlight to the shadow's tip be~$b(t)$.
Then
\begin{equation*}
  \frac{6}{1.5}=\frac{b(t)}{b(t)-a(t)}
  \quad
  \Rightarrow
  \quad
  4(b(t)-a(t))=b(t)
\end{equation*}
Thus the relationship is $3\cdot b(t)=4\cdot a(t)$.
Differentiate: 
$3\cdot db/dt=4\cdot da/dt$.
Since $da/dt$ is~$1$, we have $db/dt=4/3$.
Since the woman is walking, the total shadow tip rate is $1+(4/3)$.
\end{frame}




\begin{frame}
\Ex % http://en.wikipedia.org/wiki/Related_rates 2011-Aug-07
A $10$ meter long ladder is leaning against the wall.
\begin{center}
  \includegraphics{asy/related_rates006.pdf}  
  % \includegraphics{rr-04.mps}  
\end{center}
The base of the ladder is sliding away from the wall, at 
$3$~$\text{m}/\text{sec}$. 
That makes the ladder top slide down the wall.
How fast is it sliding at the instant
when the base is $6$ meters from the wall? 

\pause\smallskip
The relationship is $x(t)^2+y(t)^2=10^2\!$.
\begin{equation*}
  \underbracket{2x}\underbracket{\frac{dx}{dt}}
    +\underbracket{2y}\underbracket{\frac{dy}{dt}}=0
\end{equation*}
We are given $dx/dt=3$ and $x=6$ and we want $dy/dt$.
Using $x=6$ gives $6^2+y^2=10^2$, and so $y=8$.
Plugging in gives
$6\cdot 3+8\cdot dy/dt=0$, and so 
$dy/dt=-6\cdot 3/8=-2.25$.
\end{frame}



% \begin{frame}
% \exm
% A conical coffee filter is $9$~cm tall, with a base radius of $6$~cm.
% Coffee comes out at $0.25$~$\text{cm}^3/sec$.
% At what rate is the radius of the liquid in the filter dropping
% when the height is $3$~cm?

% \pause
% \exm
% A Ferris wheel with a $20$~meter radius revolves once every two minutes.
% How quickly is a passenger going up when they are $25$~meters up? 

% \pause

% \end{frame}



% \begin{frame}
% \frametitle{Topic: \textit{On Growth and Form}}
% The nature of the equations for volume, surface area, etc., limit what
% is possible.

% \exm
% Find the volume-to-surface-area ratio for
% a sphere as a function of the radius.
% Find the formula for the rate of change.

% \exm
% Do the same calculation for a cube.
% % and for a cylinder with a height equal to the radius.  

% % \pause
% % \exm
% % A spherical mothball evaporates at a rate proportional to its surface area.
% % Show that the rate of decrease of the radius is constant.
% \end{frame}



% \begin{frame}
% \frametitle{}  
% \end{frame}

\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
