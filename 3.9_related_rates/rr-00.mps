%!PS
%%BoundingBox: -1 -1 56 148 
%%HiResBoundingBox: -0.19925 -0.19925 55.19923 147.98187 
%%Creator: MetaPost 1.208
%%CreationDate: 2011.08.07:1229
%%Pages: 1
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.6 0.89961 1 setrgbcolor 0.09962
 0 dtransform exch truncate exch idtransform pop setlinewidth [] 0 setdash
 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 32.08333 0 moveto
32.08333 146.66663 lineto stroke
newpath 50.41666 0 moveto
50.41666 146.66663 lineto stroke
 0 0.09962 dtransform truncate idtransform setlinewidth pop
newpath 0 13.75 moveto
54.99998 13.75 lineto stroke
newpath 0 32.08333 moveto
54.99998 32.08333 lineto stroke
newpath 0 50.41666 moveto
54.99998 50.41666 lineto stroke
newpath 0 68.74998 moveto
54.99998 68.74998 lineto stroke
newpath 0 87.08331 moveto
54.99998 87.08331 lineto stroke
newpath 0 105.41664 moveto
54.99998 105.41664 lineto stroke
newpath 0 123.74997 moveto
54.99998 123.74997 lineto stroke
newpath 0 142.0833 moveto
54.99998 142.0833 lineto stroke
 0.6 0.6 0.6 setrgbcolor
newpath -0.19925 32.28258 moveto
-0.19925 31.88408 lineto
0.19925 31.88408 lineto
55.19923 31.88408 lineto
55.19923 32.28258 lineto
54.80074 32.28258 lineto
 closepath fill
newpath 13.55075 -0.19925 moveto
13.94925 -0.19925 lineto
13.94925 0.19925 lineto
13.94925 146.86588 lineto
13.55075 146.86588 lineto
13.55075 146.46738 lineto
 closepath fill
 0 0.50197 0 setrgbcolor 0 0.79701
 dtransform truncate idtransform setlinewidth pop 0 setlinecap
newpath 4.58333 4.58333 moveto
52.2501 147.58336 lineto stroke
showpage
%%EOF
