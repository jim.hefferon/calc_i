// related_rates.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1pt);

string OUTPUT_FN = "related_rates%03d";

import graph;

// some parameters
real axis_arrow_size = 0.35mm;
real axis_tick_size = 0.75mm;

// pen graphpaperpen=(0.4*rgb("00BFFF")+0.6*white)
//   +linewidth(0.8pt)
//   +squarecap;

// ===== Stick figures
picture pic;
int picnum = 0;
size(pic,5cm);

picture runner;
label(runner,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("running-person.eps","scale=0.5"), (0,0));

picture walker;
label(pic,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("man-walking.eps","scale=0.6"), (0,0));

for(int i=0; i <= 23; ++i) {
  draw(pic,(i-3,0)--(i-3.6,-1),GRAPHPAPERPEN);
}

add(pic,runner,(15,0));
add(pic,walker,(0,0));

// Add the labels
object Box=draw(pic,"\tiny Person x",box,(0,-1),0.5mm,white,Fill);
object Box=draw(pic,"\tiny Person y",box,(15,-1),0.5mm,white,Fill);

path base=(-3.6,0)--(20.6,0);
draw(pic,base,GRAPHPAPERPEN); 

shipout(format(OUTPUT_FN,picnum),pic);


// ========= y=3x ==============
picture pic;
int picnum = 1;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

real f1(real x) {
  return( 3*x );
}

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f1, xleft-0.2, xright+0.2);
draw(pic, f, FCNPEN);

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ========= 100-4.9x^2 ==============
picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);
scale(pic, Linear, Linear(1/10));

real f2(real x) {
  return( 100-4.9x^2 );
}

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=f2(xright+0.2); ytop=100;

draw_graphpaper(pic, xleft, xright, ytop, ybot, xStep=1.0, yStep=20.0);

path f = graph(pic, f2, xleft-0.2, xright+0.2);
draw(pic, f, FCNPEN);

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+5,
      p=currentpen,
      ticks=LeftTicks(Step=20,pTick=white),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ========= two ships ==============
picture pic;
int picnum = 3;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);
draw(pic, (0,0)--(0,3), FCNPEN_NOCOLOR+highlight_color);
filldraw(pic, circle((0,3), 0.075), white, highlight_color);
label(pic, "$\text{ship}_2$", (0,3), 2*E, filltype=Fill(white));
label(pic,graphic("../pix/ship2.png","scale=0.02"),(0,3), filltype=Fill(white));
draw(pic, (0,0)--(4,0), FCNPEN_NOCOLOR+highlight_color);
filldraw(pic, circle((4,0), 0.075), white, highlight_color);
label(pic, "$\text{ship}_1$", (4,0), 2*N, filltype=Fill(white));
label(pic,graphic("../pix/ship1.png","scale=0.02"),(4,0), filltype=Fill(white));

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ========= rocket launch ==============
picture pic;
int picnum = 4;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path triangle = (0,0)--(5,0)--(5,3)--cycle;
draw(pic, triangle, FCNPEN);
filldraw(pic, circle((5,0), 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{rocket}$", (5,3), 2*NW, filltype=Fill(white));
label(pic,graphic("../pix/rocket.png","scale=0.01"),(5,3), filltype=Fill(white));
filldraw(pic, circle((0,0), 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{observer}$", (0,0), 2*W, filltype=Fill(white));
label(pic,graphic("../pix/photographer.png","scale=0.01"),(0,-.1), filltype=Fill(white));

path anglearc = arc( (0,0), 1, 0, aTan(3/5));
draw(pic, "$\theta$", anglearc);

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ========= lamp ==============
picture pic;
int picnum = 5;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=15;
ybot=0; ytop=6;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path ray = (0,6)--(13,0);
draw(pic, ray, FCNPEN);
filldraw(pic, circle((0,6), 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{lamp}$", (0,6), 2*W, filltype=Fill(white));
label(pic,graphic("../pix/streetlamp.png","scale=0.024"),(0,3) );
filldraw(pic, circle((10,13/10), 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{woman}$", (10,13/10), NE, filltype=Fill(white));
label(pic,graphic("../pix/woman.png","scale=0.016"),(10,0.7));
filldraw(pic, circle((13,0), 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{shadow}$", (13,0), NE, filltype=Fill(white));
// label(pic,graphic("../pix/rocket.png","scale=0.01"),(5,3), filltype=Fill(white));
draw(pic, "$a$", (0,-1)--(10,-1), highlight_color, bar=Bars, arrow=Arrows);
// draw(pic, "$b-a$", (10,-1)--(13,-1), highlight_color, bar=Bars, arrow=Arrows);
draw(pic, "$b$", (0,-2.25)--(13,-2.25), highlight_color, bar=Bars, arrow=Arrows);

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);





// ========= lamp ==============
picture pic;
int picnum = 6;

size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=10;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

pair ladder_top = (0,8);
pair ladder_bot = (sqrt(10^2-(ladder_top.y)^2),0);
path ladder = ladder_top--ladder_bot;
draw(pic, ladder, FCNPEN);
filldraw(pic, circle(ladder_top, 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{ladder top}$", ladder_top, 2*W, filltype=Fill(white));
label(pic,graphic("../pix/ladder.png","scale=0.16,angle=21"),(3.8,4.7) );
filldraw(pic, circle(ladder_bot, 0.075), white, FCNPEN_COLOR);
label(pic, "$\text{ladder base}$", ladder_bot, NE, filltype=Fill(white));

xaxis(pic, L="",
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);
