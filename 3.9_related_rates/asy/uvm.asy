// uvm.asy

import settings;
settings.outformat="pdf";
settings.render=0;

unitsize(1pt);

// Set LaTeX defaults
texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "uvm%03d";

import graph;

// some parameters
real axis_arrow_size = 0.35mm;
real axis_tick_size = 0.75mm;

pen graphpaperpen=(0.4*rgb("00BFFF")+0.6*white)
  +linewidth(0.8pt)
  +squarecap;

// ===== Stick figures
picture pic;
int picnum = 0;
size(pic,5cm);

picture runner;
label(runner,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("running-person.eps","scale=0.5"), (0,0));


picture walker;
label(pic,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("man-walking.eps","scale=0.6"), (0,0));


for(int i=0; i <= 23; ++i) {
  draw(pic,(i-3,0)--(i-3.6,-1),graphpaperpen);
}

add(pic,runner,(15,0));
add(pic,walker,(0,0));

// Add the labels
object Box=draw(pic,"\tiny Person x",box,(0,-1),0.5mm,white,Fill);
object Box=draw(pic,"\tiny Person y",box,(15,-1),0.5mm,white,Fill);

path base=(-3.6,0)--(20.6,0);
draw(pic,base,graphpaperpen); 

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
