\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.10\ \ Linear approximation}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}
What is the square root of $4.2$?

\pause
Let $f(x)=\sqrt{x}=x^{1/2}\!$.
The derivative is $f'(x)=(1/2)x^{-1/2}=1/(2\sqrt{x})$.
At the point $(4,2)$ the tangent line is 
$y-2=(1/4)(x-4)$.
\begin{center}
  \includegraphics{asy/linear_approximation000.pdf}
\end{center}
In the interval from $3$ to $5$ we can hardly tell the two apart.
That is, an excellent approximation for $\sqrt{4.2}$ 
is $2+(1/4)(4.2-4)$.
(The values are
$\sqrt{4.02}\approx 2.049\,390$ 
and $2+(1/4)(4.2-4)=2.05$.)
\end{frame}
% sage: n((1/4)*(0.2)+2)
% 2.05000000000000
% sage: sqrt(4.2)
% 2.04939015319192


\begin{frame}
\Df Given~$f$, at $x=a$ the equation of the tangent line 
is the \alert{tangent line approximation}
or  \alert{linear approximation}
or \alert{linearization}
or \alert{first order approximation}.
\begin{equation*}
  L(x)=f(a)+f'(a)\cdot (x-a)
\end{equation*}

\pause
\begin{columns}
\begin{column}{0.75\textwidth}
\Ex
We will find the first order approximation to $f(x)=x^2$ at $a=5$.

\pause
We have $f'(x)=2x$ so $f'(a)=10$.
\begin{equation*}
  L(x)=25+10\cdot (x-5)
      =10x-25
\end{equation*}
This is an excellent approximation for $x$'s near to~$a$.
For instance, $L(4.95)=24.5000$ while $4.95^2=24.5025$. 
\end{column}
\begin{column}{0.25\textwidth}
  \begin{center}
  \includegraphics{asy/linear_approximation007.pdf}
  \end{center}  
\end{column}
\end{columns}
\end{frame}
% sage: def L(x):
% ....:     return 10*x-25
% ....: 
% sage: L(4.95)
% 24.5000000000000
% sage: 4.95^2
% 24.5025000000000


\begin{frame}{History: difference between $\Delta y$ and $dy$}
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.3\textheight]{pix/Johann_Bernoulli.jpg}}
  \qquad
  \begin{minipage}{0.4\textwidth}\small\RaggedRight
    Johann Bernoulli 1667--1748, known for his contributions to 
    calculus and for being a teacher of Leonhard Euler.     
  \end{minipage}
\end{center}
When limits were not yet developed,
people thought:~for small $\Delta x$ we have
\begin{equation*}
  \frac{\Delta y}{\Delta x}\approx f'(x)
\end{equation*}
and when the numbers become so small that they are
``infinitesimal'' then the approximation
becomes an equality.
\begin{equation*}
  \frac{dy}{dx}=f'(x)
\end{equation*}
Here $dy$ and $dx$ are \alert{differentials}. 
Think of
$dy$ as being a variable that depends on $x$ and~$dx$.
% \begin{equation*}
%   f(x)\approx f(a)+f'(a)\cdot (x-a)
%   \quad\text{is the same as}\quad
%   f(a+dx)=f(a)+dy
% \end{equation*}
\end{frame}

\begin{frame}
On the left is the usual secant line picture.
On the right we have zoomed in super close, so that the curve
is essentially a straight line, the tangent line.
\begin{center} 
  \vcenteredhbox{\includegraphics{asy/linear_approximation008.pdf}}%
  \qquad    
  \vcenteredhbox{\includegraphics{asy/linear_approximation009.pdf}}%    
\end{center}
The relationship
  $f(a+\Delta x)=f(a)+\Delta y$
becomes, when close up,
$f(a+dx)=f(a)+dy$.

\pause
Summary:
(1)~the $\Delta y/\Delta x$ ratio is from a secant line while the 
$dy/dx$ ratio is from a tangent line,
(2)~the connotation is that $dx$ is very small.
\end{frame}


\section{Using the linear approximation}
\begin{frame}
\Ex Use the linear approximation to estimate $\ln(1.04)$.
The actual value is about $0.0392$.  

\pause
We can find the tangent line at the nearby input $a=0$:
the value is $\ln(0)=1$, and 
the derivative of $\ln$ is $1/x$, so at $(1,0)$ the slope is~$1/1=1$.
\begin{equation*}
  L(x)=f(a)+f'(a)\cdot(x-a)=0+1\cdot(x-1)=x-1.
\end{equation*}
When $x=1.04$ our estimate is $L(1.04)=0.04$.

\begin{center}
  \includegraphics{asy/linear_approximation005.pdf}    
\end{center}
\end{frame}
% sage: ln(1.04)
% 0.0392207131532813


\begin{frame}
\Ex 
Use the linear approximation and differentials to estimate $1/4.005$, 
whose correct value is $0.249\,687\,890\,137\,328$

\pause
Let $f(x)=1/x$ and $a=4$.
Then $f(a)=1/4$, and $f'(x)=-x^{-2}$ so $f'(a)=-1/16$.
\begin{equation*}
  L(x)=f(a)+f'(a)\cdot(x-a)=\frac{1}{4}+\frac{-1}{16}\cdot(x-4)
\end{equation*}
Taking $x=4.005$ gives the desired answer.
\begin{center}
  \includegraphics{asy/linear_approximation006.pdf}    
\end{center}
Restated, with $dx=0.005$ we get $dy=-(1/16)\cdot 0.005\approx -0.000313$.  
Our approximation is $0.25-0.000313=0.249\,687\,5$.
\end{frame}
% sage: n(0.005*-1/16)
% -0.000312500000000000
% sage: (1/4)+n(0.005*-1/16)
% 0.249687500000000

\begin{frame}{Practice}
Give the linear approximation to each.
\begin{enumerate}
\item $\displaystyle \sqrt[3]{8.1}$
\item $\displaystyle \sin \theta$ where $\theta$ is $44^\circ$.
  \textit{Hint:} convert to radians.
\end{enumerate}
\end{frame}


\section{Remark: second order change, etc.}

\begin{frame}{Recall this picture of $f(x)=x^2$ for input $x+\Delta x$}
% \begin{align*}
%   \frac{df}{dx}
%   =\lim_{\Delta x\to 0}\frac{f(x+\Delta x)-f(x)}{\Delta x}
%   &=\lim_{\Delta x\to 0}\frac{(x+\Delta x)^2-x^2}{\Delta x}      \\
%   &=\lim_{\Delta x\to 0}\frac{x^2+2x\Delta x +(\Delta x^2)-x^2}{\Delta x}
% \end{align*}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/linear_approximation004.pdf}}
  \quad
  \begin{minipage}{0.4\textwidth}
    \begin{equation*}
      (x+\Delta x)^2
        =x^2+2\cdot x\Delta x+(\Delta x)^2
    \end{equation*}
  \end{minipage}
\end{center}
The area that is quadratic in $\Delta x$ is much smaller than the
area linear in $\Delta x$.
\end{frame}
\begin{frame}
As a numerical example, fix the anchor point $a=2$, and so $f(a)=4$.
\begin{center}\small
    \begin{tabular}{l|lll}
      \multicolumn{1}{c}{$\Delta x$} 
          &\multicolumn{1}{c}{$f(a+\Delta x)$} 
          &\multicolumn{1}{c}{$2a \Delta x$} 
          &\multicolumn{1}{c}{$(\Delta x)^2$} \\ \hline
      $0.1$   &$4.41$     &$0.4$   &$0.01$  \\
      $0.01$  &$4.0401$   &$0.04$  &$0.001$  \\
      $0.001$ &$4.004001$ &$0.004$ &$0.000001$  \\
    \end{tabular}
\end{center}
As $\Delta x\to 0$ the second order change term $(\Delta x)^2$
drops toward zero much more quickly than does 
the first order change term $2a\Delta x$.
The derivative $f'(x)=2x$ sticks to the rate of change
that is linear in~$\Delta x$.

% Near the anchor point~$a$ the linear change is the most important change. 
% Often it is greater than the second order change by orders of magnitude, 
% as above.
\end{frame}
% sage: def f(x):
% ....:     return x^2
% ....: 
% sage: f(5)
% 25
% sage: for deltax in [0.1, 0.01, 0.001]:
% ....:     print("2+deltax=",2+deltax," f(2+deltax)=",f(2+deltax))
% ....: 
% 2+deltax= 2.10000000000000  f(2+deltax)= 4.41000000000000
% 2+deltax= 2.01000000000000  f(2+deltax)= 4.04010000000000
% 2+deltax= 2.00100000000000  f(2+deltax)= 4.00400100000000
% sage: for deltax in [0.1, 0.01, 0.001]:
% ....:     print("4*deltax=",4*deltax," (deltax)^2=",(deltax)^2)
% ....: 
% ....: 
% 4*deltax= 0.400000000000000  (deltax)^2= 0.0100000000000000
% 4*deltax= 0.0400000000000000  (deltax)^2= 0.000100000000000000
% 4*deltax= 0.00400000000000000  (deltax)^2= 1.00000000000000e-6



\begin{frame}{Looking forward: how can we improve the linear approximation?}
\begin{center}
  \includegraphics{asy/linear_approximation001.pdf}%    
\end{center}
The red line reflects the function's rate of change at $x=a$.
But as the input moves away from~$a$, the curve deviates from the line.

That's because the rate of change is not constant\Dash the change changes.
For instance
there is a second order change, as with $(\Delta x)^2\!$, which has only a small
effect near~$a$ but which becomes more important for points farther from~$a$.   
\end{frame}


\begin{frame}
We could look for an approximation function that 
not only has the right slope at $(a,f(a))$, the right first derivative,
but also has the right second derivative there.
That function is the 
\alert{second degree Taylor polynomial}.
There is also a third degree polynomial, etc.
You will see them in Calculus~II.
\end{frame}



\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
