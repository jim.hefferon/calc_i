// linear_approximation.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "linear_approximation%03d";

import graph;


// ==== y=sqrt(x) ====
real f0(real x) {return sqrt(x);}
real tangent0(real x) {return (1/4)*(x-4)+2;}

picture pic;
int picnum = 0;
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f0,xleft,xright+0.1), FCNPEN);
draw(pic, graph(tangent0,1,xright+0.1), highlight_color);

filldraw(pic, circle((4,2),0.05), highlight_color,  highlight_color);
label(pic,  "$(4,2)$", (4,2), 2*SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== picture of linear approx and differentials ====
real f1(real x) {return (1/3)*(5-(x-2)^2);}
real tangent1(real x) {return (2/3)*(x-1)+(4/3);}

picture pic;
int picnum = 1;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f1,xleft+0.25,xright+0.1), FCNPEN);
draw(pic, graph(tangent1,0.5,1.5), highlight_color);
filldraw(pic, circle((1,4/3),0.025), highlight_color,  highlight_color);
label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));

real[] T1 = {1};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",T1),
  arrow=Arrows(TeXHead));
labelx(pic,"$a$", 1);
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............ delta x and delta y ...........
picture pic;
int picnum = 2;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f1,xleft+0.25,xright+0.1), black);
// draw(pic, graph(tangent1,0.5,2), black);
filldraw(pic, circle((1,4/3),0.025), highlight_color,  highlight_color);
label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));
filldraw(pic, circle((1.75,f1(1.75)),0.025), highlight_color,  highlight_color);
label(pic,  "$(a+\Delta x,f(a+\Delta x))$", (1.75,f1(1.75)), 2*N);

// sides of triangle
path deltax_triangle = (1,f1(1))--(1.75,f1(1))--(1.75,f1(1.75));
draw(pic, deltax_triangle, highlight_color);

real offset=0.15;
draw(pic,"$\Delta x$" ,(1,f1(1)-offset)--(1.75,f1(1)-offset),
     highlight_color, bar=Bars(2));
draw(pic,"$\Delta y$" ,(1.75+offset,f1(1))--(1.75+offset,f1(1.75)),
     highlight_color, bar=Bars(2));

real[] T2 = {1, 1.75};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",T2),
  arrow=Arrows(TeXHead));
labelx(pic,"$a$", 1);
labelx(pic,"$a+\Delta x$", 1.75);

  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............ dx and dy ...........
picture pic;
int picnum = 3;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f1,xleft+0.25,xright+0.1), black);
draw(pic, graph(tangent1,0.5,2), highlight_color);
filldraw(pic, circle((1,4/3),0.025), highlight_color,  highlight_color);
// label(pic,  "$(x,f(x))$", (1,4/3), 2*NW, filltype=Fill(white));
filldraw(pic, circle((1.75,tangent1(1.75)),0.025), highlight_color,  highlight_color);
// label(pic,  "$(x+\Delta x,f(x+\Delta x))$", (1.75,f1(1.75)), 2*NE, filltype=Fill(white));

// sides of triangle
path dx_triangle = (1,f1(1))--(1.75,f1(1))--(1.75,tangent1(1.75));
draw(pic, dx_triangle, black);

real offset=0.15;
draw(pic,"$dx=\Delta x$" ,(1,f1(1)-offset)--(1.75,f1(1)-offset),
     highlight_color, bar=Bars(2));
draw(pic,"$dy$" ,(1.75+offset,f1(1))--(1.75+offset,tangent1(1.75)),
     highlight_color, bar=Bars(2));

real[] T2 = {1, 1.75};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",T2),
  arrow=Arrows(TeXHead));
labelx(pic,"$a$", 1);
labelx(pic,"$a+\Delta x$", 1.75);

  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================ rate of change of a square =======
picture pic;
int picnum = 4;
unitsize(pic,1cm);

real u=3;  // width
real v=u;  // height
real delta_u=0.85; 
real delta_v=delta_u; 

path main_rectangle=(0,0)--(u,0)--(u,v)--(0,v)--cycle;
path right_rectangle=(u,0)--(u+delta_u,0)--(u+delta_u,v)--(u,v)--cycle;
path top_rectangle=(0,v)--(u,v)--(u,v+delta_v)--(0,v+delta_v)--cycle;
path corner_rectangle=(u,v)--(u+delta_u,v)--(u+delta_u,v+delta_v)
                         --(u,v+delta_v)--cycle;

filldraw(pic,main_rectangle,white,black);
label(pic,"$x^2$",(u/2,u/2));

filldraw(pic,right_rectangle,light_color,black);
label(pic,"$x\Delta x$",(u+delta_u/2,u/2));

filldraw(pic,top_rectangle,background_color,black);
label(pic,"$x\Delta x$",(u/2,u+delta_u/2));

filldraw(pic,corner_rectangle,bold_color,black);
label(pic,"$(\Delta x)^2$",(u+delta_u/2,u+delta_u/2));

real LINE_OFFSET = 0.25;
// vertical label
draw(pic, L=Label("$x$",MidPoint,W),
     (-LINE_OFFSET,0)--(-LINE_OFFSET,v),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta x$",MidPoint,W),
     (-LINE_OFFSET,v)--(-LINE_OFFSET,v+delta_v),
     highlight_color,
     bar=Bars);
// horizontal label
draw(pic, L=Label("$x$",MidPoint,S),
     (0,-LINE_OFFSET)--(u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);
draw(pic, L=Label("$\Delta x$",MidPoint,S),
     (u,-LINE_OFFSET)--(u+delta_u,-LINE_OFFSET),
     highlight_color,
     bar=Bars);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== y=ln(x) ====
real f5(real x) {return log(x);}
real tangent5(real x) {return (x-1);}

picture pic;
int picnum = 5;
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=-1; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f5,0.325,xright+0.1), FCNPEN);
draw(pic, graph(tangent5,0.5,1.5), highlight_color);

filldraw(pic, circle((1,0),0.05), highlight_color,  highlight_color);
label(pic,  "$(1,0)$", (1,0), NW, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== y=1/x ====
real f6(real x) {return 1/x;}
real tangent6(real x) {return (-1/16)*(x-4)+(1/4);}

picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f6,0.45,xright+0.1), FCNPEN);
draw(pic, graph(tangent6,2.25,5.05), highlight_color);

filldraw(pic, circle((4,1/4),0.05), highlight_color,  highlight_color);
label(pic,  "$(4,1/4)$", (4,1/4), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== y=x^2 ====
real f7(real x) {return x^2;}
real tangent7(real x) {return 10*x-25;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=6;
ybot=0; ytop=36;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f7,xleft,xright), FCNPEN);
draw(pic, graph(tangent7,4,6), highlight_color);

filldraw(pic, circle((5,25),0.125), highlight_color,  highlight_color);
label(pic,  "$(5,25)$", (5,25), 2*SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.5, xmax=xright+.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.5, ymax=ytop+0.5,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrow(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// =========== delta x and delta y ============
picture pic;
int picnum = 8;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f1,xleft+0.5,xright+0.1), black);
dotfactor=5;
dot(pic, (1,4/3), highlight_color);
label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));
dot(pic, (1.75,f1(1.75)), highlight_color);
label(pic,  "$(a+\Delta x,f(a+\Delta x))$", (1.75,f1(1.75)), 2*N);

// sides of triangle
path deltax_triangle = (1,f1(1))--(1.75,f1(1))--(1.75,f1(1.75));
draw(pic, deltax_triangle, highlight_color);

real offset=0.15;
draw(pic,"$\Delta x$" ,(1,f1(1)-offset)--(1.75,f1(1)-offset),
     highlight_color, bar=Bars(2));
draw(pic,"$\Delta y$" ,(1.75+offset,f1(1))--(1.75+offset,f1(1.75)),
     highlight_color, bar=Bars(2));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ............ dx and dy ...........
picture pic;
int picnum = 9;
size(pic,0,1.25cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f1,0.85,1.15), black);
dotfactor=6;
dot(pic, (1,4/3), highlight_color);
label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));

// sides of triangle
path deltax_triangle = (1,f1(1))--(1.05,f1(1))--(1.05,f1(1.05));
draw(pic, deltax_triangle, highlight_color);

real offset=0.02;
draw(pic,"$dx$" ,(1,f1(1)-offset)--(1.05,f1(1)-offset),
     highlight_color, bar=Bars(2));
draw(pic,"$dy$" ,(1.05+offset,f1(1))--(1.05+offset,f1(1.05)),
     highlight_color, bar=Bars(2));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// // ================ y=e^x with tangent lines =======
// real f2(real x) {return exp(x);}
// real tangent_line2a(real x) {return exp(1)*(x-1)+exp(1);}
// real tangent_line2b(real x) {return exp(-2)*(x+2)+exp(-2);}

// picture pic;
// int picnum = 2;

// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-3; xright=2;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f2,xleft-0.1,1.61), black);
// // sage: n(ln(5))
// // 1.60943791243410
// draw(pic, graph(tangent_line2a,1-0.5,1+0.5), highlight_color);
// filldraw(pic,circle((1,exp(1)),0.05),highlight_color,highlight_color);
// draw(pic, graph(tangent_line2b,-2-1,-2+1), highlight_color);
// filldraw(pic,circle((-2,exp(-2)),0.05),highlight_color,highlight_color);

// xaxis(pic, L="",  // label
//       axis=YZero,
//       xmin=xleft-0.75, xmax=xright+.75,
//       p=currentpen,
//       ticks=NoTicks,
//       arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//       axis=XZero,
//       ymin=ybot-0.75, ymax=ytop+0.75,
//       p=currentpen,
//       ticks=NoTicks,
//       arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
