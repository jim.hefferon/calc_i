// curve_sketching.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "curve_sketching%03d";
real PI = acos(-1);

import graph;



// ==== inc/dec and concavity ====

// ....... inc and up ............
picture pic;
int picnum = 0;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

draw(pic, grf, FCNPEN);

shipout(format(OUTPUT_FN,picnum),pic);


// ....... inc and down ............
picture pic;
int picnum = 1;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf, FCNPEN);

shipout(format(OUTPUT_FN,picnum),pic);


// ....... dec and down ............
picture pic;
int picnum = 2;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,1.5)::(1,0.75)::(1.5,0.5);

draw(pic, grf, FCNPEN);

shipout(format(OUTPUT_FN,picnum),pic);



// ....... dec and up ............
picture pic;
int picnum = 3;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

path grf = (0.5,1.5)::(1,1.25)::(1.5,0.5);

draw(pic, grf, FCNPEN);

shipout(format(OUTPUT_FN,picnum),pic);



// ======= concave up ======
real f4(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f4, 0.5, 1.5), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);

real f5(real x) {return -2*(x-1)^2+1.5;}

// ......... concave down ...........
picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

draw(pic, graph(f5, 0.5, 1.5), FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ==== (1/3)x^3-(1/2)x^2-2x+3 ====

// ....... large scale behavior .......
picture pic;
int picnum = 6;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=5;

path grf_l = (-2,-3.5)..(-2.5,-4)..(-3,-5); 
path grf_r = (2,3.5)..(2.5,4)..(3,5); 

draw(pic, grf_l, highlight_color, Arrow(ARROWSIZE));
draw(pic, grf_r, highlight_color, Arrow(ARROWSIZE));

filldraw(pic, circle((0,3),0.07), highlight_color,  highlight_color);
label(pic,  "$(0,3)$",
        (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ....... axis crossing .......
picture pic;
int picnum = 7;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=5;

filldraw(pic, circle((0,3),0.07), highlight_color,  highlight_color);
label(pic,  "$(0,3)$",
        (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ....... graph .......
real f8(real x) {return (1/3)*x^3-(1/2)*x^2-2*x+3;}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2.65; xright=3;
ybot=-2; ytop=4;

draw(pic,(-1,ybot)--(-1,ytop), ASYMPTOTEPEN);
draw(pic,(1/2,ybot)--(1/2,ytop), ASYMPTOTEPEN);
draw(pic,(2,ybot)--(2,ytop), ASYMPTOTEPEN);

draw(pic, graph(f8,xleft-0.1,xright+0.1), highlight_color, Arrows(ARROWSIZE));

filldraw(pic, circle((-1,25/6),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((0,3),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((1/2,23/12),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((2,-1/3),0.05), highlight_color,  highlight_color);
//label(pic,  "$(0,3)$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks("%",Step=1),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== 1/(1+ sin x) =========

// ....... large scale behavior .......
real f9(real x) {return 1+sin(x);}

picture pic;
int picnum = 9;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2*PI;
ybot=-0; ytop=5;

path right_asymptote = (3*PI/2,ybot)--(3*PI/2,ytop);
draw(pic, right_asymptote, ASYMPTOTEPEN);

path f = graph(pic, f9, xleft-0.1, xright+0.1);
draw(pic, f, gray(0.75));

path grf0 = (3*PI/2-0.5,3)..(3*PI/2-0.25,4)..(3*PI/2-0.15,5); 
path grf0a = reflect((3*PI/2,ybot),(3*PI/2,ytop))*grf0;

draw(pic, grf0, highlight_color, Arrow(ARROWSIZE));
draw(pic, grf0a, highlight_color, Arrow(ARROWSIZE));

dotfactor = 4;
dot(pic, (0,1), highlight_color);

real[] T9 = {PI/2, PI, 3*PI/2, 2*PI};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T9, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\pi/2$", PI/2);
labelx(pic, "$\pi$", PI);
labelx(pic, "$3\pi/2$", 3*PI/2);
labelx(pic, "$2\pi$", 2*PI);

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks("%",Step=1,step=0,OmitTick(0)),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ....... graph .......
real f10(real x) {return 1/(1+sin(x));}

picture pic;
int picnum = 10;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2*PI;
ybot=-0; ytop=7;

dotfactor = 4;
dot(pic, (PI/2,1/2), highlight_color);
label(pic,  "$\displaystyle (\frac{\pi}{2},\frac{1}{2})$", (PI/2,0.5), N, filltype=Fill(white));

path right_asymptote = (3*PI/2,ybot)--(3*PI/2,ytop);
draw(pic, right_asymptote, ASYMPTOTEPEN);

draw(pic, graph(f10,xleft-0.5,(3*PI/2)-0.5), highlight_color, Arrows(ARROWSIZE));
draw(pic, graph(f10,(3*PI/2)+0.5,xright+0.5), highlight_color, Arrows(ARROWSIZE));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T9, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\pi/2$", PI/2);
labelx(pic, "$\pi$", PI);
labelx(pic, "$3\pi/2$", 3*PI/2);
labelx(pic, "$2\pi$", 2*PI);

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks("%",Step=1,step=0,OmitTick(0)),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ===== e^{-x^2} =========

// ....... large scale behavior .......
picture pic;
int picnum = 11;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=1;

filldraw(pic, circle((0,1),0.05), highlight_color,  highlight_color);
// label(pic,  "", (0,1.5), W, filltype=Fill(white));

path grf_left = (-2.5,0.3)..(-2.8,0.2)..(-3.5,0.1);
path grf_right = reflect((0,ybot),(0,ytop))*grf_left;

draw(pic, grf_left, highlight_color, Arrow(ARROWSIZE));
draw(pic, grf_right, highlight_color, Arrow(ARROWSIZE));
// draw(pic, grf1, highlight_color, Arrow);
// draw(pic, grf1a, highlight_color, Arrow);
// draw(pic, grf_r, highlight_color, Arrow);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ....... graph .......
real f12(real x) {return exp(-x^2);}

picture pic;
int picnum = 12;
size(pic,6cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=1;

filldraw(pic, circle((0,1),0.05), highlight_color,  highlight_color);
label(pic,  "$\displaystyle (0,1)$", (0,1), 2*N, filltype=Fill(white));

draw(pic, graph(f12,xleft-0.1,xright+0.1), highlight_color);
draw(pic,(-sqrt(2)/2,ybot)--(-sqrt(2)/2,ytop), ASYMPTOTEPEN);
draw(pic,(sqrt(2)/2,ybot)--(sqrt(2)/2,ytop), ASYMPTOTEPEN);

xaxis(pic, L="", 
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);





// ===== 10x^3/(x^2-1) =========

// ....... large scale behavior .......
picture pic;
int picnum = 13;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=10;

filldraw(pic, circle((0,0),0.075), highlight_color,  highlight_color);
// label(pic,  "", (0,1.5), W, filltype=Fill(white));

path grf_left = (-2.5,-8)..(-2.75,-9)..(-3,-10);
path grf_right = (2.5,8)..(2.75,9)..(3,10);

draw(pic, grf_left, highlight_color, Arrow(ARROWSIZE));
draw(pic, grf_right, highlight_color, Arrow(ARROWSIZE));

draw(pic,(-1,ybot)--(-1,ytop), ASYMPTOTEPEN);
draw(pic,(1,ybot)--(1,ytop), ASYMPTOTEPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


// ....... graph .......
real f14(real x) {return 10*x^3/(x^2-1);}

picture pic;
int picnum = 14;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-40; ytop=40;

draw(pic, graph(pic, f14, xleft-0.1,-1-0.2), highlight_color);
draw(pic, graph(pic, f14, -1+0.1, 1-0.1), highlight_color);
draw(pic, graph(pic, f14, 1+0.2, xright+0.1), highlight_color);

draw(pic,Scale(pic,(-1,ybot))--Scale(pic,(-1,ytop)), ASYMPTOTEPEN);
draw(pic,Scale(pic,(1,ybot))--Scale(pic,(1,ytop)), ASYMPTOTEPEN);

dotfactor = 4;
dot(pic,Scale(pic,(-sqrt(3),-15*sqrt(3))), highlight_color);
  label(pic, "$(-\sqrt{3},-15\sqrt{3})$", Scale(pic,(-sqrt(3),-15*sqrt(3))), NW);
dot(pic,Scale(pic,(sqrt(3),15*sqrt(3))), highlight_color);
  label(pic, "$(\sqrt{3},15\sqrt{3})$", Scale(pic,(sqrt(3),15*sqrt(3))), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ============= (2x-4)/(x+2) ============
real f16(real x) {return (2x-4)/(x+2);}

picture pic;
int picnum = 16;
size(pic,0,4cm,keepAspect=true);
// scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-4; ytop=4;

draw(pic, (-2,ybot)--(-2,ytop), ASYMPTOTEPEN);
label(pic, "$x=-2$", (-2,-1), W);

draw(pic, (xleft,2)--(xright,2), ASYMPTOTEPEN);
label(pic, "$y=2$", (3,2), N);

path grf_left = (-3,2.5)..(-3.25,2.25)..(-4,2.15); 
draw(pic, grf_left, highlight_color, Arrow(ARROWSIZE));
path grf_right = (3,1.5)..(3.25,1.75)..(4,1.85); 
draw(pic, grf_right, highlight_color, Arrow(ARROWSIZE));

path grf_top = (-2.5,3.5)..(-2.25,3.75)..(-2.1,4); 
draw(pic, grf_top, highlight_color, Arrow(ARROWSIZE));
path grf_bot = (-1.5,-3.5)..(-1.75,-3.75)..(-1.9,-4); 
draw(pic, grf_bot, highlight_color, Arrow(ARROWSIZE));

dotfactor = 4;
dot(pic, (2,0), highlight_color);
label(pic, "$(2,0)$", (2,0), S);
dot(pic, (0,-2), highlight_color);
label(pic, "$(0,-2)$", (0,-2), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


//  ..... graph ........
picture pic;
int picnum = 17;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-20; ytop=20;

draw(pic, Scale(pic,(-2,ybot))--Scale(pic,(-2,ytop)), ASYMPTOTEPEN);
draw(pic, Scale(pic,(xleft,2))--Scale(pic,(xright,2)), ASYMPTOTEPEN);
label(pic, "$y=2$", Scale(pic,(2.75,2)), NE);

draw(pic, graph(pic,f16, xleft-0.1, -2-0.4), highlight_color);
draw(pic, graph(pic,f16, -2+0.4, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(2,0)), highlight_color);
// label(pic, "$(2,0)$", Scale(pic,(2,0)), N);
dot(pic, Scale(pic,(0,-2)), highlight_color);
label(pic, "$(0,-2)$", Scale(pic,(0,-2)), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.75, ymax=ytop+2.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ============= x^5-5x ============
real f18(real x) {return (x^5)-(5*x);}

picture pic;
int picnum = 18;
size(pic,0,4cm,keepAspect=true);
// scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-4; ytop=4;

path grf_left = (-2.5,-2.75)..(-2.75,-3)..(-3,-3.75); 
draw(pic, grf_left, highlight_color, Arrow(ARROWSIZE));
path grf_right = (2.5,2.75)..(2.75,3)..(3,3.75); 
draw(pic, grf_right, highlight_color, Arrow(ARROWSIZE));

dotfactor = 4;
dot(pic, (0,0), highlight_color);
dot(pic, (-1.5,0), highlight_color);
dot(pic, (1.5,0), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


//  ..... graph ........
picture pic;
int picnum = 19;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(100), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-200; ytop=200;

draw(pic, graph(pic,f18, xleft+0.25, xright-0.25), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(-1,4)), highlight_color);
label(pic, "$(-1,4)$", Scale(pic,(-1,4)), N);
dot(pic, Scale(pic,(1,-4)), highlight_color);
label(pic, "$(1,-4)$", Scale(pic,(1,-4)), N);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-25, ymax=ytop+25,
      p=currentpen,
      ticks=LeftTicks(Step=100,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// ============= x*e^x ============
real f20(real x) {return x*exp(x);}

picture pic;
int picnum = 20;
size(pic,0,4cm,keepAspect=true);
// scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-1; ytop=4;

path grf_left = (-2.5,-0.25)..(-2.75,-0.1)..(-3,-0.05); 
draw(pic, grf_left, highlight_color, Arrow(ARROWSIZE));
path grf_right = (2.5,2.75)..(2.75,3)..(3,3.75); 
draw(pic, grf_right, highlight_color, Arrow(ARROWSIZE));

dotfactor = 4;
dot(pic, (0,0), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);


//  ..... graph ........
picture pic;
int picnum = 21;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(5), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=-2; ytop=20;

draw(pic, graph(pic,f20, xleft-0.1, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(-1,-0.37)), highlight_color);
label(pic, "$(-1,-e^{-1})$", Scale(pic,(-1,-0.37)), N);
// dot(pic, Scale(pic,(1,-4)), highlight_color);
// label(pic, "$(1,-4)$", Scale(pic,(1,-4)), N);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// =========== 2x^3-3x^2-12x+12 ========
real f22(real x) {return 2*x^3-3*x^2-12*x+12;}

picture pic;
int picnum = 22;
scale(pic, Linear(4), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=3;
ybot=-10; ytop=20;

draw(pic, graph(pic, f22, xleft-0.1, xright+0.1), highlight_color);

// draw(pic, Scale(pic,(-2,ybot-0.1))--Scale(pic,(-2,ytop+0.1)), dashed);
dotfactor = 4;
dot(pic, Scale(pic,(-1,19)), highlight_color);
dot(pic, Scale(pic,(2,-8)), highlight_color);

// dotfactor = 4;
// dot(pic, Scale(pic,(2,-1)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// =========== 1+(1/x)+(1/x^2) ========
real f23(real x) {return 1+(1/x)+1/x^2;}

picture pic;
int picnum = 23;
scale(pic, Linear(3), Linear);
size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=10;

draw(pic, Scale(pic,(xleft-0.1,1))--Scale(pic,(xright+0.1,1)), ASYMPTOTEPEN);

draw(pic, graph(pic, f23, xleft-0.2, 0-0.28), highlight_color);
draw(pic, graph(pic, f23, 0+0.40, xright+0.1), highlight_color);


// draw(pic, Scale(pic,(-2,ybot-0.1))--Scale(pic,(-2,ytop+0.1)), dashed);
dotfactor = 4;
dot(pic, Scale(pic,(-1,f23(-1))), highlight_color);
dot(pic, Scale(pic,(-2,f23(-2))), highlight_color);
// dot(pic, Scale(pic,(2,-8)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// =========== x\sqrt{x^2+1} ========
real f24(real x) {return x*sqrt(x^2+1);}

picture pic;
int picnum = 24;
scale(pic, Linear(3), Linear);
size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=10;

draw(pic, graph(pic, f24, xleft-0.1, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(0,f24(0))), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);



// =========== x^4-6x^2 ========
real f25(real x) {return x^4-6*x^2;}

picture pic;
int picnum = 25;
scale(pic, Linear(6), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=35;

draw(pic, graph(pic, f25, xleft-0.1, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(0,f25(0))), highlight_color);
dot(pic, Scale(pic,(-sqrt(3),f25(-sqrt(3)))), highlight_color);
dot(pic, Scale(pic,(sqrt(3),f25(sqrt(3)))), highlight_color);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// =========== ln(4-x^2) ========
real f26(real x) {return log(4-x^2);}

picture pic;
int picnum = 26;
scale(pic, Linear(2), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=2;

draw(pic, graph(pic, f26, -2+0.003, 2-0.003), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(0,f26(0))), highlight_color);
dot(pic, Scale(pic,(-sqrt(3),f26(-sqrt(3)))), highlight_color);
dot(pic, Scale(pic,(sqrt(3),f26(sqrt(3)))), highlight_color);

draw(pic, Scale(pic,(-2,ybot))--Scale(pic,(-2,ytop)), ASYMPTOTEPEN);
draw(pic, Scale(pic,(2,ybot))--Scale(pic,(2,ytop)), ASYMPTOTEPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);









// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

