\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

% \DeclareMathOperator*{\bigOh}{\mathcal{O}}
% \DeclareMathOperator*{\bigTheta}{\Theta}

\title{Section 4.5:\ \ Curve sketching}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}{Guidelines}
\begin{enumerate}
\item Start with the non-calculus techniques.
  This includes finding where the graph crosses the $y$~axis and
  the $x$~axis, asymptotes, and behavior at the infinities.
\item Find the intervals where the first derivative is positive or negative,
  and the same for the second derivative.
  Check the transitions between them, and apply the First Derivative test
  or Second Derivative test.
\item Fill in the spaces between with sketches of the arcs.
\end{enumerate}
\begin{center}\small
  \begin{tabular}{r|cc}
    \multicolumn{1}{c}{\ }
    &\multicolumn{1}{c}{\textit{Concave up}}
      &\multicolumn{1}{c}{\textit{Concave down}}  \\ \cline{2-3}
    \rule{0pt}{0.8cm}
    \textit{Increasing}
      &\vcenteredhbox{\includegraphics{asy/curve_sketching000.pdf}}  
      &\vcenteredhbox{\includegraphics{asy/curve_sketching001.pdf}}  \\
    \rule{0pt}{0.9cm}
    \textit{Decreasing}
      &\vcenteredhbox{\includegraphics{asy/curve_sketching002.pdf}}  
      &\vcenteredhbox{\includegraphics{asy/curve_sketching003.pdf}}  
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Polynomial $f(x)=(1/3)x^3-(1/2)x^2-2x+3$}
This is a cubic so we know its large scale behavior,
$\lim_{x\to\infty}f(x)=\infty$ and $\lim_{x\to-\infty}f(x)=-\infty$.
\begin{center}
  \includegraphics{asy/curve_sketching006.pdf}%
\end{center}
It crosses the $y$~axis at $(0,3)$.
  Finding $x$~axis crossings by solving $f(x)=0$ is not as easy.
\end{frame}

\begin{frame}
The derivatives are here.
\begin{align*}
  f'(x)  &= x^2-x-2=(x+1)(x-2)  \\
  f''(x) &= 2x-1
\end{align*}
\pause
The critical numbers are $c_1=-1$ and $c_2=2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)$ pos %=4$  
                          &$f$ is increasing  \\
    $\open{-1}{2}$        &$f'(0)$ neg %=-2$  
                          &$f$ is decreasing  \\
    $\open{2}{\infty}$    &$f'(3)$ pos %=4$   
                          &$f$ is increasing  
  \end{tabular}
\end{center}
\pause
There is one solution to $f''(x)=0$ so these are the intervals of concavity.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1/2}$  &$f''(0)$ neg %=-1$  
                           &$f$ is concave down  \\
    $\open{1/2}{\infty}$   &$f''(1)$ pos %=1$   
                           &$f$ is concave up  
  \end{tabular}
\end{center}

\pause
At $x=-1$ there is a local max, at $x=2$ there is a local min.
At $x=1/2$ there is an inflection point.
Here are the associated outputs.
\begin{equation*}
  f(-1)=25/6
  \quad
  f(1/2)=23/12
  \quad
  f(2)=-1/3
\end{equation*}
\end{frame}

\begin{frame}
\begin{center}\small
  \includegraphics{asy/curve_sketching008.pdf}  \\[1ex]
  $f(x)=(1/3)x^3-(1/2)x^2-2x+3$    
\end{center}
\end{frame}


% =========== 10x^3/(x^2-1)
\begin{frame}{Rational function $f(x)=10x^3/(x^2-1)$}
We can get its large scale behavior
by looking at the leading terms of the numerator and denominator,
namely that it acts like $y=10x$.
It crosses the $y$~axis at $(0,0)$.
It is undefined at $\pm 1$.
\begin{center}
  \includegraphics{asy/curve_sketching013.pdf}%
\end{center}
\end{frame}


\begin{frame}
These are the derivatives.
\begin{equation*}
  f'(x)=\frac{10x^2(x^2-3)}{(x^2-1)^2}
  \qquad
  f''(x)=\frac{20x(x^2+3)}{(x^2-1)^3}
\end{equation*}
\pause
Besides $x=\pm 1$, the critical numbers are $x=0$ and $x=\pm\sqrt{3}$.
There is only one inflection point, at $x=0$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-\sqrt{3}}$ &$f'(-2)$ pos %=40/9$     
                                &$f$ increasing  \\
    $\open{-\sqrt{3}}{-1}$      &$f'(-3/2)$ neg %=-54/4$  
                                &$f$ decreasing  \\
    $\open{-1}{0}$              &$f'(-1/2)$ neg %=-110/9$ 
                                &$f$ decreasing  \\
    $\open{0}{1}$               &$f'(1/2)$ neg %=-110/9$  
                                &$f$ decreasing  \\
    $\open{1}{\sqrt{3}}$        &$f'(3/2)$ neg %=-54/4$   
                                &$f$ decreasing  \\
    $\open{\sqrt{3}}{\infty}$   &$f'(2)$ pos %=40/9$      
                                &$f$ increasing  
  \end{tabular}
\end{center}
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f''(-2)$ pos %=280/27$      
                          &$f$ concave down  \\
    $\open{-1}{0}$        &$f''(-1/2)$ pos %=2080/27$   
                          &$f$ concave up  \\
    $\open{0}{1}$         &$f''(1/2)$ neg %=-2080/27$   
                          &$f$ concave down  \\
    $\open{1}{\infty}$    &$f''(2)$ pos %=280/27$       
                          &$f$ concave up  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 10*x^2*(x^2-3)/((x^2-1)^2)
% ....: 
% sage: f_prime(-2)
% 40/9
% sage: f_prime(-3/2)
% -54/5
% sage: f_prime(-1/2)
% -110/9
% sage: f_prime(1/2)
% -110/9
% sage: f_prime(3/2)
% -54/5
% sage: f_prime(2)
% 40/9
% sage: def f_double(x):
% ....:     return 20*x*(x^2+3)/((x^2-1)^3)
% ....: 
% sage: f_double(-2)
% -280/27
% sage: f_double(-1/2)
% 2080/27
% sage: f_double(1/2)
% -2080/27
% sage: f_double(2)
% 280/27

\begin{frame}
\begin{center}\small
  \includegraphics{asy/curve_sketching014.pdf}  \\[1ex]
  $f(x)=10x^3/(x^2-1)$  
\end{center}
\end{frame}



% ================================

\begin{frame}{Trigonometric $f(x)=1/(1+\sin x)$}
We know that $\sin x$ is periodic so $f$ will also periodically
repeat.
If we understand what happens between $x=0$ and $x=2\pi$ then we 
understand everything.

First, $1+\sin(x)\geq 0$ for all inputs.
The function~$f$ is undefined when $\sin x=-1$, so it has a
vertical asymptote at $3\pi/2$. 
The large-scale behavior is that the value of $1+\sin x$ is 
positive on both sides of this asymptote.  
\begin{center}\small
  \includegraphics{asy/curve_sketching009.pdf}  
\end{center}
As to crossing the $y$~axis, $f(0)=1$. 
\end{frame}

\begin{frame}
The derivatives are here.
\begin{align*}
  f'(x)  &= \frac{-\cos x}{(1+\sin x)^2}  \\
  f''(x) &= \frac{(1+\sin x)^2\cdot \sin x-(-\cos x)\cdot 2(1+\sin x)\cdot \cos x}{(1+\sin x)^4}       \\
         &= \frac{(1+\sin x)\cdot [-\sin x-\sin^2 x+2\cos^2 x]}{(1+\sin x)^4} \\
         &= \frac{\sin x+\sin^2 x+2\cos^2 x}{(1+\sin x)^3} 
         = \frac{\sin x\cdot (1+\sin x)+2\cos^2 x}{(1+\sin x)^3} 
\end{align*}
\pause
The zeros of $f'(x)$ are the places where
$\cos x=0$, namely $\pi/2$ and $3\pi/2$.
(Between $0$ and $2\pi$ the open intervals don't have a natural stopping point.)
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Sample interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{\text{left of\ }0}{\pi/2}$    &$f'(0)$ neg %=-1$     
                                         &$f$ decreasing  \\
    $\open{\pi/2}{3\pi/2}$               &$f'(\pi)$ pos %=1$     
                                         &$f$ increasing  \\
    $\open{3\pi/2}{\text{right of\ }2\pi/2}$ &$f'(2\pi)$ neg %=-1$  
                                         &$f$ decreasing  
  \end{tabular}
\end{center}
\pause
Finding the zeroes of $f''(x)$ looks hard.
% \begin{center}\small
%   \begin{tabular}[t]{c|cc}
%     \multicolumn{1}{c}{\textit{Sample interval}}
%       &\multicolumn{1}{c}{\textit{Test input}}
%       &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
%     $\open{-5\pi/2}{-\pi/2}$  &$f''(-\pi)=2$  &$f$ concave up  \\
%     $\open{-\pi/2}{3\pi/2}$   &$f''(0)=2$     &$f$ concave up  \\
%     $\open{3\pi/2}{7\pi/2}$   &$f''(2\pi)=2$  &$f$ concave up  
%   \end{tabular}
% \end{center}
\end{frame}

\begin{frame}
\begin{center}\small
  \includegraphics{asy/curve_sketching010.pdf}  \\[1ex]
  $f(x)=1/(1+\sin x)$  
\end{center}
\end{frame}
% sage: var('x')
% x
% sage: f(x) = 1/(1+sin(x))
% sage: derivative(f,x)
% x |--> -cos(x)/(sin(x) + 1)^2
% sage: f_prime = derivative(f,x)
% sage: derivative(f_prime)
% x |--> 2*cos(x)^2/(sin(x) + 1)^3 + sin(x)/(sin(x) + 1)^2
% sage: def f_prime(x):
% ....:     return(-cos(x)/((1+sin(x))^2))
% ....: 
% sage: def f_double(x):
% ....:     return((sin(x)*(1+sin(x))+2*(cos(x))^2)/((1+sin(x))^3))
% ....: 
% sage: f_prime(0)
% -1
% sage: f_prime(2*pi)
% -1
% sage: f_double(0)
% 2
% sage: f_double(2*pi)
% 2




\begin{frame}{Exponential $f(x)=e^{-x^2}$}
This function is important in Statistics.

\pause
The negative sign in the
exponent makes this the function $f(x)=1/e^{x^2}\!$.
We know the behaviors of $x^2$ and $e^x\!$, 
neither of which are ever negative.
So we know the large-scale behavior:
$\lim_{x\to\infty}f(x)=0$ and $\lim_{x\to-\infty}f(x)=0$
(both approach from above). 
\begin{center}\small
  \includegraphics{asy/curve_sketching011.pdf}  
\end{center}
It never crosses the $x$~axis because $e^x$ never does.
We know where it crosses the $y$~axis, at $f(0)=1$. 
\end{frame}

\begin{frame}
The derivatives are here.
\begin{align*}
  f'(x)  &= -2xe^{-x^2}  \\
  f''(x) &= 2(2x^2-1)e^{-x^2}
\end{align*}
\pause
The only critical number is $x=0$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{0}$  &$f'(-1)$ pos %=2e^{-1}$  
                         &$f$ increasing  \\
    $\open{0}{\infty}$   &$f'(1)$ neg %=-2e^{-1}$  
                         &$f$ decreasing  \\  
  \end{tabular}
\end{center}
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    \rule{0pt}{10pt}
    $\open{-\infty}{-\sqrt{2}/2}$      &$f''(-1)$ pos %=2e^{-1}$  
                                       &$f$ concave up  \\
    $\open{-\sqrt{2}/2}{\sqrt{2}/2}$   &$f''(0)$ neg %=-2$       
                                       &$f$ concave down  \\
    $\open{\sqrt{2}/2}{\infty}$        &$f''(1)$ pos %=2e^{-1}$   
                                       &$f$ concave up  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return -2*x*e^(-x^2)
% ....: 
% sage: f_prime(0)
% 0
% sage: f_prime(-1)
% 2*e^(-1)
% sage: f_prime(1)
% -2*e^(-1)
% sage: def f_double(x):
% ....:     return 2*(2*x^2-1)*e^(-x^2)
% ....: 
% sage: f_double(-1)
% 2*e^(-1)
% sage: f_double(0)
% -2
% sage: f_double(1)
% 2*e^(-1)

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching012.pdf}  \\[1ex]
  $f(x)=e^{-x^2}$   
\end{center}
\end{frame}






\begin{frame}{Practice: sketch $x^5-5x$}
\pause
It is a polynomial, with oblique asymptote $x^5$.
It is the difference of two squares so it factors as 
$f(x)=x(x^2-\sqrt{5})(x^2+\sqrt{5})$,
which equals $x(x-\sqrt[4]{5})(x+\sqrt[4]{5})(x^2+\sqrt{5})$.
so it has three odd roots: at $x=0$ and~$x=\pm\sqrt[4]{5}\approx \pm 1.50$.
\begin{center}
  \includegraphics{asy/curve_sketching018.pdf}
\end{center}
\end{frame}

\begin{frame}
Here are the derivatives.
\begin{align*}
  f'(x) &=5x^4-5=5(x^4-1)=5(x^2-1)(x^2+1)=5(x-1)(x+1)(x^2+1) \\
  f''(x) &=20x^3
\end{align*}
The critical numbers are $x=1$ and $x=-1$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)$ pos %=75$ 
                          &$f$ increasing  \\
    $\open{-1}{1}$        &$f'(0)$ neg %=-5$  
                          &$f$ decreasing  \\  
    $\open{1}{\infty}$    &$f'(2)$ pos % =75$  
                          &$f$ increasing  \\  
  \end{tabular}
\end{center}
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    \rule{0pt}{10pt}
    $\open{-\infty}{0}$ &$f''(-1)$ neg %=-20$  
                        &$f$ concave down  \\
    $\open{0}{\infty}$  &$f''(1)$ pos %=20$    
                        &$f$ concave up  \\
  \end{tabular}
\end{center}
There is a local maximum at $(-1,4)$ and a local minimum at $(1,-4)$.
\end{frame}
% sage: def f_prime(x):
% ....:     return 5*x^4-5
% ....: 
% sage: f_prime(-2)
% 75
% sage: f_prime(0)
% -5
% sage: f_prime(2)
% 75
% sage: def f_double(x):
% ....:     return 20*x^3
% ....: 
% sage: f_double(-1)
% -20
% sage: f_double(1)
% 20
% sage: n(5^(0.25))
% 1.49534878122122
% sage: n(5^(0.5))
% 2.23606797749979
% sage: def f(x):
% ....:     return x^5-5*x
% ....: 
% sage: f(-1)
% 4
% sage: f(1)
% -4

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching019.pdf} \\[2ex]
  $\displaystyle f(x)=x^5-5x$
\end{center}  
\end{frame}



\begin{frame}{Practice: sketch $2x^3-3x^2-12x+12$}
\pause
It is a polynomial.
The leading term is $2x^3$, so that gives the long-term behavior.
It crosses the $y$~axis at $(0,12)$.
It doesn't easily factor, so we skip looking for roots.
\pause
\begin{equation*}
  f'(x)=6x^2-6x-12
       =6(x+1)(x-2)
  \qquad
  f''(x)=12x-6=6(2x-1)  
\end{equation*}
The critical numbers are $-1$ and $2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)$ pos %=24$   
                          &$f$ increasing  \\
    $\open{-1}{2}$        &$f'(0)$ neg %=-12$   
                          &$f$ decreasing  \\
    $\open{2}{\infty}$    &$f'(3)$ pos %=24$    
                          &$f$ increasing  \\
  \end{tabular}
\end{center}
So there is a local maximum at $(-1,19)$ 
and a local minimum at $(2,-8)$.
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1/2}$  &$f''(0)$ neg %=-6$   
                           &$f$ concave down  \\
    $\open{1/2}{\infty}$   &$f''(1)$ pos %=6$    
                           &$f$ concave up  \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def f(x):
% ....:     return(2*x^3-3*x^2-12*x+1)
% ....: 
% sage: f(-1)
% 19
% sage: f(2)
% -8
% sage: def f_prime(x):
% ....:     return (6*(x+1)*(x-2))
% ....: 
% sage: f_prime(-2)
% 24
% sage: f_prime(0)
% -12
% sage: f_prime(3)
% 24
% sage: def f_double(x):
% ....:     return (6*(2*x-1))
% ....: 
% sage: f_double(0)
% -6
% sage: f_double(1)
% 6
\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching022.pdf}
\end{center}
\end{frame}




  
\begin{frame}{Practice: sketch $(2x-4)/(x+2)$}
It is a rational function, so we note that the numerator is zero at $x=2$
and that the denominator is zero at $x=-2$.
Further, $f(0)=-2$
We also note the horizontal asymptote $\lim_{x\to\pm\infty}f(x)$ of $y=2$. 
\begin{center}
  \includegraphics{asy/curve_sketching016.pdf}
\end{center}
\end{frame}

\begin{frame}
These are the derivatives.
\begin{align*}
  f'(x)
       &=\frac{(x+2)\cdot 2-(2x-4)\cdot 1}{(x+2)^2}
       =\frac{8}{(x+2)^2}=8(x+2)^{-2}                  \\
  f''(x)
      &=8\cdot (-2\cdot (x+2)^{-3}\cdot 1)
       =-16(x+2)^{-3}
\end{align*}

The only critical number is $-2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$  &$f'(-3)$ pos %=8$  
                          &$f$ is increasing  \\
    $\open{-2}{\infty}$   &$f'(0)$ pos %=2$   
                          &$f$ is increasing  
  \end{tabular}
\end{center}
The only solution to $f''(x)=0$ is also $-2$
so these are the intervals of concavity.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$  &$f''(-3)$ pos %=16$  
                          &$f$ is concave up  \\
    $\open{-2}{\infty}$   &$f''(0)$ neg %=-2$   
                          &$f$ is concave down  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 8*(x+2)^(-2)
% ....: 
% sage: f_prime(-1)
% 8
% sage: f_prime(0)
% 2
% sage: def f_double(x):
% ....:     return -16*(x+2)^(-3)
% ....: 
% sage: f_double(-3)
% 16
% sage: f_double(0)
% -2
% sage: f_prime(-3)
% 8

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching017.pdf} \\[2ex]
  $\displaystyle f(x)=\frac{2x-4}{x+2}$    
\end{center}
\end{frame}




\begin{frame}{Practice}
Sketch each curve.
\begin{enumerate}
\item $\displaystyle x^4-6x^2$
\pause\\
$x^4-6x^2=x^2(x^2-6)$ so the $x$~intercepts are $x=0,\pm\sqrt{6}$.
$f'(x)=4x^3-12x=4x(x^2-3)$,
so there are local mins at $(-\sqrt{3},-9)$ and~$(\sqrt{3},-9)$
and a local max at $(0,0)$
$f''(x)=12x^2-12=12(x^2-1)$ so it is concave up, then down, then up
and the inflection points are $(-1,5)$ and $(1,-5)$. 
\item $\displaystyle (x^2-1)^3$
\pause \\
$f(x)=[(x+1)(x-1)]^3$ so the $x$~intercepts are $x=-1,1$.
$f'(x)=6x(x^2-1)^2=6x(x+1)^2(x-1)^2$, and $f$ is decreasing, 
then decreasing again, then increasing, and then increasing again, 
and therefore has a local minimum at $(0,-1)$.
$f''(x)=6(x^3-1)(5x^2-1)$ and is first concave up, then down, then up,
then down and then up again, giving inflection points at
$(-1,0)$, $(-1/\sqrt{5},-64/125)$, $(1/\sqrt{5},-64/125)$, and $(1,0)$.
\end{enumerate}
\end{frame}
\begin{frame}
\begin{enumerate} \setcounter{enumi}{2}
\item $\displaystyle x\cdot \sqrt{x^2+1}$
\pause\\
The only $x$~intercept is $x=0$.
By the product rule $f'(x)=(2x^2+1)/\sqrt{x^2+1}$
and because $2x^2+1$ has no real roots there are no local optima.
As to the second derivative, 
\begin{equation*}
  f''(x)=\frac{2x^3+3x}{(x^2+1)\sqrt{x^2+1}}
\end{equation*}
and we get that $f''(x)=0$ for $x=0, -3/2$,
and the function is concave down, then down, then up, 
and so there is an inflection at $(0,0)$
\item $\displaystyle \frac{x}{(x-1)^2}$
\pause\\
There is one $x$~intercept, at $x=0$.
$f'(x)=(-x-1)/(x-1)^3$ so the critical points are $x=-1,0,1$,
and we get $f$ increasing, decreasing, and increasing again
so there is a local minimum at $(-1,-/4)$
$f''(x)=(2x+4)/(x-1)^4$ which has only one zero, namely $x=-2$,
but there are three intervals to check because of the vertical asymptote
at $x=1$.
We get that $f$ is concave down, up, and then down again,
so it has an inflection point at $(-2,-2/9)$. 
\end{enumerate}
\end{frame}


\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching025.pdf}   \\[1ex]
  $f(x)=x^4-6x^2$   
\end{center}
\end{frame}



\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching024.pdf}   \\[1ex]
  $f(x)=x\cdot \sqrt{x^2+1}$   
\end{center}
\end{frame}

  
\begin{frame}{Practice: sketch $xe^x$}
We spot that $f(0)=0$ and that $xe^x=0$ has only the solution $x=0$.
Note that $f(x)<0$ to the left of that root and $f(x)>0$ to its right.

Clearly $\lim_{x\to\infty}f(x)=\infty$.
In the other direction, $\lim_{x\to\-\infty}f(x)$ is a $-\infty\cdot 0$ limit.
We can rewrite it as the $0/0$~type limit $\lim_{x\to\infty}x/e^{-x}$
and apply L'H\^{o}pital's Rule to get 
$\lim_{x\to\-\infty}1/-e^{-x}=\lim_{x\to-\infty}-e^x=0$. 
\begin{center}
  \includegraphics{asy/curve_sketching020.pdf}
\end{center}
\end{frame}

\begin{frame}
Here are the derivatives.
\begin{align*}
  f'(x)
       &=x\cdot e^x+e^x\cdot 1=e^x(x+1)         \\
  f''(x)
      &=e^x\cdot 1+(x+1)\cdot e^x
       =e^x(x+2)
\end{align*}

\pause
The only critical number is $x=-1$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)$ neg %=-e^{-2}$  
                          &$f$ is decreasing  \\
    $\open{-1}{\infty}$    &$f'(0)$ pos %=1$       
                          &$f$ is increasing  
  \end{tabular}
\end{center}
The only solution to $f''(x)=0$ is $x=-2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$  &$f''(-3)$ neg %=-e^{-3}$  
                          &$f$ is concave down  \\
    $\open{-2}{\infty}$   &$f''(0)$ pos % =2$        
                          &$f$ is concave up  
  \end{tabular}
\end{center}
The function has a local minimum at $(-1,-e^{-1})\approx(-1,-0.37)$.
\end{frame}
% sage: def f_prime(x):
% ....:     return 8*(x+2)^(-2)
% ....: 
% sage: f_prime(-1)
% 8
% sage: f_prime(0)
% 2
% sage: def f_double(x):
% ....:     return -16*(x+2)^(-3)
% ....: 
% sage: f_double(-3)
% 16
% sage: f_double(0)
% -2
% sage: f_prime(-3)
% 8

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching021.pdf} \\[2ex]
  $\displaystyle f(x)=xe^x$    
\end{center}
\end{frame}


  
\begin{frame}{Practice: sketch $\ln(4-x^2)$}\vspace*{-1ex}
We spot that $f(0)=\ln(4)$ and that $\ln(4-x^2)=0$ gives $4-x^2=1$,
which has the solution $x=\pm\sqrt{3}$.
Note that $f(x)$ is not defined when $x\leq -2$ and when $x\geq 2$.

Here are the derivatives.
\begin{equation*}
  f'(x)
       =\frac{-2x}{4-x^2}
  \qquad
  f''(x)
      =\frac{-8-2x^2}{(4-x^2)^2}
\end{equation*}

\pause
The only stationary number is $x=0$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{0}$  &$f'(-1)$ pos %=2/3$    
                         &$f$ is increasing  \\
    $\open{0}{\infty}$   &$f'(1)$ neg % =-2/3$    
                         &$f$ is decreasing  
  \end{tabular}
\end{center}
As to the second derivative, on the interval where the function is defined,
the denominator is always positive and the numerator is always negative.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)$ neg %=-1/2$  
                              &$f$ is concave down   
  \end{tabular}
\end{center}
The function has a local maximum at $(0,\ln(4))\approx(-1,1.39)$.
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching026.pdf} \\[2ex]
  $\displaystyle f(x)=\ln(4-x^2)$    
\end{center}
\end{frame}



\begin{frame}{Practice}
Sketch.  
Label points of interest.
\begin{enumerate}
\item $2x^3-12x^2+18x$

\pause\item $x^4-8x^2+8$

\pause\item $(4-x^2)^5$

\pause\item $\displaystyle \frac{x^2+5x}{25-x^2}$

\pause\item $\displaystyle 1+\frac{1}{x}+\frac{1}{x^2}$

\pause\item $\displaystyle \frac{1}{x^2-4}$
\end{enumerate}
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching023.pdf}   \\[1ex]
  $f(x)=1+\frac{1}{x}+\frac{1}{x^2}$   
\end{center}
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
