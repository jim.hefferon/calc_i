// growth.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "growth%03d";

import graph;


// ==== y=e^x ====
real f0(real x) {return (2.71828)^x;}

picture pic;
int picnum = 0;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=10;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f0,xleft-0.1,2.31);
draw(pic, f, FCNPEN);

filldraw(pic, circle((0,1),0.075), FCNPEN_COLOR,  FCNPEN_COLOR);
label(pic,  "$(0,1)$", (0,1), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ==== y=ln(x) ====
real f1(real x) {return log(x);}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-3; ytop=2;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f1,0.05,xright+0.1);
draw(pic, f, FCNPEN);

filldraw(pic, circle((1,0),0.05), FCNPEN_COLOR, FCNPEN_COLOR);
label(pic,  "$(1,0)$", (1,0), NW, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ y=e^x with tangent lines =======
real f2(real x) {return exp(x);}
real tangent_line2a(real x) {return exp(1)*(x-1)+exp(1);}
real tangent_line2b(real x) {return exp(-2)*(x+2)+exp(-2);}

picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f2,xleft-0.1,1.61);
draw(pic, f, FCNPEN);
// sage: n(ln(5))
// 1.60943791243410
draw(pic, graph(tangent_line2a,1-0.5,1+0.5), highlight_color);
filldraw(pic,circle((1,exp(1)),0.05),highlight_color,highlight_color);
label(pic,  "$(1,2.718)$", (1,2.718), 2*E, filltype=Fill(white));
draw(pic, graph(tangent_line2b,-2-1,-2+1), highlight_color);
filldraw(pic,circle((-2,exp(-2)),0.05),highlight_color,highlight_color);
label(pic,  "$(-2,0.135)$", (-2,0.135), NW, filltype=Fill(white));
// sage: round(e^1, ndigits=3)
// 2.718
// sage: round(e^-2, ndigits=3)
// 0.135

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ (a^h-1)/h =======
real E = exp(1);
real f3(real h) {return (E^h-1)/h;}

picture pic;
int picnum = 3;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path fleft = graph(f3,xleft-0.1,-0.025);
draw(pic, fleft, FCNPEN);
path fright = graph(f3,0.05,xright+0.1);
draw(pic, fright, FCNPEN);
filldraw(pic,circle((0,1),0.05),white,highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
