\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.8\ \ Exponential growth and decay}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Interest}


\begin{frame}{Simple and compound interest}
Invest an initial principal of~$P_0$ for $t$ years at an annual rate of~$r$
percent per year then at the end the \alert{simple interest} value is this.
\begin{equation*}
  A=P[1+rt]
\end{equation*}

If the interest is assessed $n$ times a year then at the end the 
\alert{compound interest} amount is this.
\begin{equation*}
  A=P\left[1+\frac{r}{n}\right]^{n\cdot t}
\end{equation*}

\Ex Deposit $\$1000$ for $t=3$ years at an annual rate of 
$4.25\%$ (that's $r=0.0425$)
compounded quarterly, that is, $n=4$ times per year:
\begin{equation*}
  1000\left[1+\frac{0.0425}{4}\right]^{4\cdot3}=1135.22
\end{equation*}
(rounded to the nearest cent).
% sage: 1000*(1+0.0425/4)^(4*3)
% 1135.22108009586
\end{frame}


\begin{frame}{What happens when you compound more often?}
What happens for larger and larger $n$'s?
\pause
The simplest principal is $P=1$,
the simplest number of years is $t=1$, 
and the simplest rate is $r=1.00$.
\begin{center}
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{\textit{Num compoundings $n$}}
      &\multicolumn{1}{c}{\rule[-5pt]{0em}{12pt}\textit{Amount $A=1\cdot\left[1+\frac{1.00}{n}\right]^{n\cdot 1}$}}           \\
    \hline
    $1$         &$2.00000$  \\
    $4$         &$2.44141$  \\
    $12$        &$2.61304$  \\
    $365$       &$2.71457$  \\
    $1,000$     &$2.71692$  \\
    $1,000,000$ &$2.71828$  \\
  \end{tabular}
\end{center}
\pause
As $n$ grows the amount of money grows, but it is bounded.
It has a limit.
This limit is denoted \alert{$e$}.
\begin{equation*}
  e=\lim_{n\to\infty}\left[1+\frac{1}{n}\right]^n
   \approx 2.718
\end{equation*}
\end{frame}
% sage: def A(x):
% ....:     return 1*(1+(1/x))^(x*1)
% ....: 
% sage: for m in [1, 4, 12, 365, 1000, 1000000]:
% ....:     print("m=",m," A=",n(A(m)))
% ....: 
% m= 1  A= 2.00000000000000
% m= 4  A= 2.44140625000000
% m= 12  A= 2.61303529022468
% m= 365  A= 2.71456748202187
% m= 1000  A= 2.71692393223589
% m= 1000000  A= 2.71828046931938

\begin{frame}{Formula for continuously compounded interest}
Start with the discrete formula.
\begin{equation*}
    \lim_{n\to\infty}\left[1+\frac{r}{n}\right]^{nt}
\end{equation*}
Change variables to use that $n=mr$.
\begin{equation*}
    =\lim_{m\to\infty}\left[1+\frac{1}{m}\right]^{mrt}
\end{equation*}
The $r$ and~$t$ don't depend on $m$.
\begin{equation*}
    =\biggl(\lim_{m\to\infty}\left[1+\frac{1}{m}\right]^{m}\biggr)^{rt}
\end{equation*}
Recognize the limit.
\begin{equation*}
    =e^{rt}
\end{equation*}
So the formula is $Pe^{rt}\!$.
\end{frame}




\begin{frame}{Feedback: discrete or continuous}

With interest, at each compounding you get more money, and because you
have more money, the next time you gain even more.
This is feedback.
\begin{equation*}
  \text{Discrete feedback:}
  \qquad  A=P\left[1+\frac{r}{n}\right]^{nt}
\end{equation*}

\pause
Feedback is where the rate of change 
is related to the current value of the function.
We could imagine that the growth rate is ten percent of the current value
of the function $f'(x)=0.10\cdot f(x)$,
or the growth rate is the square root of the current value,
$f'(x)=\sqrt{f(x)}$.
But the simplest feedback case is $f'(x)=f(x)$.
In a continuous context,
this happens with $f(x)=e^x\!$.
\begin{equation*}
  \text{Continuous feedback:}
  \qquad
  A=Pe^{rt}
\end{equation*}
\end{frame}


% \begin{frame}{Exponential growth: graph of $f(x)=e^x$}
% \begin{center}
%   \includegraphics{asy/growth000.pdf}    
% \end{center}  
% \end{frame}



\begin{frame}{Feedback: the derivative}
We've expressed the intuition that the defining property of $f(x)=e^x$ is that
$f'(x)=f(x)$.
That is, the derivative of $f(x)=e^x$ is this.
\begin{equation*}
  \frac{d\,e^x}{dx}=e^x
  \qquad
   f'(x)=e^x
\end{equation*}

This is a graph of $f(x)=e^x\!$.
\begin{center}
  \includegraphics{asy/growth002.pdf}    
\end{center}
At $x=1$ the rate of change, the slope of the tangent line, 
is $e^1\approx 2.718$.
At $x=-2$ the rate of change is $e^{-2}\approx 0.135$.
\end{frame}


\begin{frame}{Derivative starting with the definition}
Let $f(x)=e^x$.
\begin{align*}
  f'(x)
  =\lim_{h\to 0}\frac{f(x+h)-f(x)}{h}    
  &=\lim_{h\to 0}\frac{e^{x+h}-e^x}{h}  \\    
  &=\lim_{h\to 0}\frac{e^x\cdot e^h-e^x}{h}
  =e^x\cdot\lim_{h\to 0}\frac{e^h-1}{h}    
\end{align*}
Not a proof but the picture makes it clear that limit on the right
equals~$1$.
\begin{equation*}
  y=\frac{e^h-1}{h}
  \qquad
  \vcenteredhbox{\includegraphics{asy/growth003.pdf}}
\end{equation*}
\end{frame}


\begin{frame}{Solving problems with exponents: review of logarithms}
% Recall that the natural logarithm $\ln$ is defined by the 
% property that $\ln(e^x)=x$.  
By definition, $\log_b(y)=x$ means exactly that $b^x=y$.
So the expression $\log_b(y)$ asks the question $b^{\text{what?}}=y$.

Formulas:
\begin{itemize}
\item By far the most important case is $b=e$ where
   $\ln(y)=x$ if and only if $e^x=y$.
\item Convert to bases other than~$e$ with this formula.
  \begin{equation*}
    \log_b x=\frac{\ln x}{\ln b}
  \end{equation*}
\item The logarithm of a product is the sum of the logarithms. 
  \begin{equation*}
    \ln(x\cdot y)=\ln x+\ln y
  \end{equation*}
\item Because of that, powers move out front, $\ln(x^n)=n\cdot \ln x $,
  reciprocals become negatives, $\ln(1/x)=-\ln x $, and 
  quotients become differences, $\ln(x/y)=\ln x-\ln y $.
\end{itemize}
\end{frame}



\section{Growth and decay}

\begin{frame}
\Ex A bacteria culture grows according to $f(x)=Pe^{rt}$.
At the start, $t=0$ hours, the population was $f(0)=400$.
After $t=4$ hours it was $f(4)=25\,600$.
\begin{enumerate}
\item Find $P$.
\pause \\ $\displaystyle 400=Pe^{r\cdot 0}=P\cdot 1$.
\item Find $r$.
\pause
  \begin{align*}
    25600  &=400\cdot e^{r\cdot 4}  \\
    25600/400 &= e^{4r}  \\
    64 &= e^{4r}  \\
    \ln(64) &=4r
  \end{align*}
  We get $\displaystyle r=\ln(64)/4\approx 1.04$.
\item Predict the population when $t=5$.
\pause
\begin{equation*}
  400\cdot e^{1.04\cdot 5}\approx 400\cdot 181.27\approx 72\, 509
\end{equation*}
\end{enumerate}

% One lesson is that in $P_0e^{rt}$, the $P_0$ is the initial amount, the 
% amount at time $t=0$.
\end{frame}
% sage: n(25600/400)
% 64.0000000000000
% sage: n(ln(64))
% 4.15888308335967
% sage: n(ln(64))/4
% 1.03972077083992
% sage: exp(1.04*5)
% 181.272241875151
% sage: exp(1.04*5)*400
% 72508.8967500605



\begin{frame}
\Ex A bacteria culture grows according to $f(x)=Pe^{rt}$.
After $t=3$ hours the population was $f(3)=190$.
After $t=5$ hours it was $f(5)=9\,100$.
\begin{enumerate}
\item Find $r$.
  \textit{Hint:} compare the two populations in ratio.
\pause
\begin{equation*}
  \frac{9\,100}{190}=\frac{Pe^{5r}}{Pe^{3r}}=e^{2r}
\end{equation*} % 9100/190\approx 47.89
Apply the logarithm, $2r\approx\ln(9100/190)$ to get $r\approx 1.93$.
\item Find $P$.
\pause 
\begin{equation*}
  190=Pe^{3\cdot 1.93}\approx Pe^{5.79} \approx P\cdot 327.01
\end{equation*}
Then divide, $190/327.01\approx 0.58$.
% \item Predict the population at $t=10$.
% \pause
% \begin{equation*}
%   327.01\cdot e^{1.93\cdot 10}\approx 327.01\cdot 2.41\approx 788.09
% \end{equation*}
\end{enumerate}
\end{frame}
% sage: n(9100/190)
% 47.8947368421053
% sage: ln(47.89)
% 3.86890671435452
% sage: ln(47.89)/2
% 1.93445335717726
% sage: 3*1.93
% 5.79000000000000
% sage: exp(5.79)
% 327.013024375971
% sage: 190/327.01
% 0.581021987095196
% sage: exp(19.3)
% 2.40925905951589e8
% sage: 327.01*2.41
% 788.094100000000


\begin{frame}{Doubling time}
Deposit money at an annual rate of $4\%$ compounded continuously.
How long until the money doubles?
\pause
\begin{align*}
  2P &= Pe^{0.04t}  \\
   2 &= e^{0.04t}  \\
  \ln(2) &= 0.04t \\
  0.693  &= 0.04t
\end{align*}
We get $t=0.693/0.04=17.33$ years.

\medskip
For exponential growth, the \alert{doubling time}
is independent of the amount.

\pause
The doubling time formula is \alert{$t=\ln(2)/r$}.
\end{frame}
% sage: n(ln(2))
% 0.693147180559945
% sage: 0.693/0.04
% 17.3250000000000
% sage: 0.693/0.051
% 13.5882352941176


% \begin{frame}
% \Ex Deposit at $5.1\%$ compounded continuously.
% How long until it doubles?  
% \pause
% \begin{align*}
%   2P_0 &= P_0e^{0.051t}  \\
%    2 &= e^{0.051t}  \\
%   \ln(2) &= 0.051t \\
%   0.693  &= 0.051t
% \end{align*}
% The doubling time is $0.693/0.051=13.59$ years.
% \end{frame}

% \begin{frame}{Use logarithms to solve discrete compounding problems}
% \Ex Deposit $\$3000$ at an annual rate of $5\%$ compounded $m=4$
% times a year.
% How long until you have $\$5000$?
% \begin{align*}
%   5000 &= 3000[1+\frac{0.05}{4}]^{4t}  \\
%   5000/3000 &= [1+\frac{0.05}{4}]^{4t}  \\
%   1.67 &= [1+\frac{0.05}{4}]^{4t}  \\
%   \ln(1.67) &= 4t\cdot \ln(1.0125)  
% \end{align*}
% We get $t=\ln(1.67)/(4\cdot\ln(1.0125))\approx 10.32$.
% Rounding up, you will exceed $\$5000$ after ten and a half years.
% \end{frame}
% % sage: n(5/3)
% % 1.66666666666667
% % sage: .05/4
% % 0.0125000000000000
% % sage: n(ln(1.67)/(4*ln(1.0125)))
% % 10.3204427621817



\begin{frame}{Exponential decay}
Radioactive material loses instead of gains.
It has a negative~$r$, and
instead of a doubling time it has a \alert{half life}.

\Ex Radium 226 has a half life of 1590 years.
Find~$r$.

\pause
\begin{align*}
  \frac{1}{2}P &= Pe^{r\cdot 1590}  \\
  1/2 &= e^{r\cdot 1590}  \\
  \ln(1/2) &= r\cdot 1590  \\
\end{align*}
So $r=\ln(1/2)/1590\approx -0.00044$.
It is the negative rate that signals decay instead of growth.  

\pause
The halving time formula is \alert{$t=\ln(1/2)/r$},
which for historical reasonss is sometimes written $-\ln(2)/r$.
\end{frame}
% sage: n(ln(1/2)/1590)
% -0.000435941622993676


\begin{frame}
\Ex Chromium-51 has a half life of $28$ days.
Find~$r$, and also find the amount left after three years if $P=0.23$.

\pause
The half life information gives this (working in years).
\begin{align*}
  \frac{1}{2}P_0 &=Pe^{r\cdot (28/365)}  \\
  \frac{1}{2} &=e^{r\cdot (28/365)}  \\
  \ln(1/2)   &=r\cdot (28/365)  \\
  \frac{ln(1/2)\cdot 365}{28} =r
\end{align*}
We get $r\approx -9.036$.

For the amount left, we have this.
\begin{equation*}
  A=(0.23)\cdot e^{-9.036\cdot 3}\approx 3.88\times 10^{-13}
\end{equation*}
\end{frame}
% sage: ln(1/2)*365/28
% 365/28*log(1/2)
% sage: n(ln(1/2)*365/28)
% -9.03566860372786
% sage: 0.23*exp(-9.036*3)
% 3.88036894822696e-13



\begin{frame}{Is $e$ special?}
\begin{itemize}
\item It is special: it is the base for the function satisfying $f'(x)=f(x)$.
\item It is not special:~if we consider an $e$-based exponential function such 
as  $f(x)=Pe^{0.06t}$, then we can rewrite as
\begin{equation*}
  Pe^{0.06t}=P\left(e^{0.06}\right)^t
            \approx P (1.062)^t
\end{equation*}
and so we are actually working with all kinds of bases.
\end{itemize}
\end{frame}

% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
