\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

% \DeclareMathOperator*{\bigOh}{\mathcal{O}}
% \DeclareMathOperator*{\bigTheta}{\Theta}

\title{Section 4.4 extra:\ \ Big $O$}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}{Motivation: speed of algorithms}
In programming we want a way to gauge the speed of an algorithm.

% An algorithm takes in a string of bits~$\alpha$ and then after some time,
% outputs another string of bits~$\beta$.

% For instance, we may want to compare two to see which is fastest.
Usually algorithms take more time for longer inputs.
The \alert{runtime function}  
takes in the length of the input, and outputs the number of
ticks that the algorithm takes to finish.
\end{frame}

\begin{frame}{We want to measure the long-run behavior}
Suppose that we compare two algorithms, 
one with a runtime function of $f(x)=\sqrt{x}$ and the other with
$g(x)=10\ln(x)$.
\begin{center}
  \only<1>{\includegraphics{asy/bigo000.pdf}}%
  \only<2->{\includegraphics{asy/bigo001.pdf}}%
\end{center}
For these inputs it looks like the $\sqrt{x}$ algorithm takes less time.
\uncover<2->{But for larger inputs the $10\ln(x)$ algorithm is faster.
So to rank algorithms we look at the runtime as $x\to\infty$.}
\end{frame}


% \begin{frame}{We want to compare the growth rate, not the values}
% In the limit of a fraction, $n(x)/d(x)$, if the numerator goes to infinity then 
% that tends to take the fraction as a whole to infinity.
% If the denominator goes to infinity then that takes the fraction as a whole
% to zero.
% So a $\infty/\infty$ limit is a contest between the two.

% \Ex Consider $\lim_{x\to\infty}x^2/x=\infty$.
% \begin{center}\small
%   \begin{tabular}{ll|r}
%     \multicolumn{1}{c}{$d(x)=x$}
%       &\multicolumn{1}{c}{$n(x)=x^2$}
%       &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
%     \rule{0em}{10pt}
%     $10$     &$100$         &$10$ \\
%     $100$    &$10\,000$     &$100$ \\
%     $1\,000$ &$1\,000\,000$ &$1\,000$ \\
%   \end{tabular}
% \end{center}
% Both numerator and denominator go to infinity 
% but $x^2$ is racing there ahead of~$x$.
% So $x^2$'s growth is faster
% than $x$'s.

% Here is a summary.
% \begin{center}
%   \begin{tabular}{l|l}
%     \multicolumn{1}{c}{\textit{Result}} 
%        &\multicolumn{1}{c}{$\lim_{x\to\infty}n(x)/d(x)=L$}   \\ \hline 
%     Denominator wins &$L=0$  \\
%     It is a tie      &$L$ is finite and greater than~$0$ \\
%     Numerator wins   &$L=\infty$  \\
%   \end{tabular}
% \end{center}
% \end{frame}

\begin{frame}{Big~$\bigOh$}
We restrict ourselves to functions that are differentiable 
for large enough numbers,
that is, over an interval $\open{N}{\infty}$ for some~$N$.

\Df  
We say that \alert{$f$ is $\bigOh(g)$},
or that \alert{$f$'s order of growth is at most~$g$},
or that \alert{$f=\bigOh(g)$},
or that  \alert{$f\in \bigOh(g)$},
if the limit
\begin{equation*}
  \lim_{x\to\infty} \frac{f(x)}{g(x)}
\end{equation*}
exists and is finite.

The \alert{Big~$\bigOh$} of $g$, written \alert{$\bigOh(g)$}, 
is the set of functions~$f$
So $\bigOh(g)$ is the set of functions~$f$ whose
growth rate is less than or equal to $g$'s rate.
(Of course, we usually prefer an algorithm 
with a smaller runtime.)

\Ex
With $f(x)=10\ln(x)$ and $g(x)=\sqrt{x}$ we have $f\in\bigOh(g)$.
Or we might skip the function names and write $10\ln(x)\in\bigOh(\sqrt{x})$.

\Ex
With $f(x)=x^2$ and $g(x)=x^3$ we have $f\in\bigOh(g)$.
\end{frame}



\begin{frame}
\Tm  
  Suppose that 
  $\lim_{x\to\infty}f(x)/g(x)$ exists and equals $L$, 
  which is a member of $\R\cup\set{\infty}$.
  \begin{itemize}
  \item If $L=0$ then $g$'s order of growth is greater than~$f$'s,
    that is, $f$ is~$\bigOh(g)$ but $g$ is not~$\bigOh(f)$.
  \item 
  If $L$ is between $0$ and~$\infty$   
  then the two functions have equivalent orders of growth,
    meaning that both $f$ is~$\bigOh(g)$ and $g$ is~$\bigOh(f)$.
  \item If $L=\infty$ then $f$'s order of growth is greater than~$g$'s,
    so that $g$ is~$\bigOh(f)$ but $f$ is not $\bigOh(g)$.
  \end{itemize}
\pause
\begin{center}
  \includegraphics{asy/bigo002.pdf}
  \hspace{3em}
  \includegraphics{asy/bigo003.pdf}  
\end{center}
On the left, $g$ appears to accelerate away,
suggesting that $g$'s order of growth is strictly greater than $f$'s, 
so that $f$ is $\bigOh(g)$ but $g$ is not $\bigOh(f)$. 
On the right the two seem to track together 
so that they have the same order of growth,
that is, both $f$ is $\bigOh(g)$ and also $g$ is $\bigOh(f)$.
\end{frame}



\begin{frame}{Remark: the notation}
The `\kern-1pt$f=\bigOh(g)$' notation is the most common, but awkward.

It is awkward in that it does not follow the usual rules
of equality, such as that 
$f=\bigOh(g)$ does not allow
us to write `$\bigOh(g)=f$\kern1pt'.
Another awkwardness is that 
$x=\bigOh(x^2)$ and $x^2=\bigOh(x^2)$ together do not
imply that $x=x^2\!$.
\end{frame}




\begin{frame}{Polynomials}
\Ex
  Let $f(x)=x^2+5x+6$ and $g(x)=x^3+2x+3$.
  Here we apply L'H\^{o}pital's Rule multiple times.
  \begin{equation*}
    \lim_{x\to\infty}\frac{f(x)}{g(x)}
    =\lim_{x\to\infty}\frac{x^2+5x+6}{x^3+2x+3}
    =\lim_{x\to\infty}\frac{2x+5}{3x^2+2}
    =\lim_{x\to\infty}\frac{2}{6x}
    =0
  \end{equation*}
  So $f$ is~$\bigOh(g)$ but $g$ is not $\bigOh(f)$.
  That is, $f$'s order of growth is strictly less than $g$'s.

\pause
\Ex
  Let $f(x)=4x^5+1$ and $g(x)=x^2+5$.
  Again we apply L'H\^{o}pital's Rule multiple times.
  \begin{equation*}
    \lim_{x\to\infty}\frac{f(x)}{g(x)}
    =\lim_{x\to\infty}\frac{4x^5+1}{x^2+5}
    =\lim_{x\to\infty}\frac{20x^4}{2x}
    =\lim_{x\to\infty}10x^3
    =\infty
  \end{equation*}
  So $f$'s order of growth is strictly greater than $g$'s;
  $g\in\bigOh(f)$ but $f$ is not in $\bigOh(g)$.
\end{frame}
\begin{frame}
\Ex
  Next consider $f(x)=3x^2+4x+5$ and $g(x)=x^2\!$. 
  \begin{equation*}
    \lim_{x\to\infty}\frac{3x^2+4x+5}{x^2}
    =\lim_{x\to\infty}\frac{6x+4}{2x}
    =\lim_{x\to\infty}\frac{6}{2}
    =3
  \end{equation*}
  So their growth rates are equivalent. 
  That is, both $f$ is~$\bigOh(g)$
  and $g$ is~$\bigOh(f)$.

\pause\medskip
Conclusion:~for polynomials,  compare growth rates by
comparing the degree of their leading terms.

\pause
With polynomials we use the power functions as a benchmark
so we would say ``$5x^3-x+1$ is $\bigOh(x^3)$'' to mean that 
there are a cluster of functions all with the same order of growth,
and that cluster is represented by $x^3\!$.  
We have the start of a hierarchy, a ranking of these clusters of
functions by their order of growth.
\begin{center}
  $x$ is less than $x^2$ is less than $x^3$ is less than $x^4$ \ldots 
\end{center}
(Most common are positive integer powers but we could talk about
something like $\bigOh(x^{1.1})$.)
In terms of algorithms, a runtime of $\bigOh(x)$ is strictly faster
than a runtime of $\bigOh(x^2)$. 
\end{frame}




\begin{frame}{Two important points}
\begin{enumerate}
\item
Don't confuse a function having smaller values with it 
having a smaller order of growth.
Take $g(x)=x^2$ and $f(x)=x^2+1$, so that $g(x)<f(x)$ for all~$x$.
But $g$'s order of growth is not smaller;~rather,
the limit of the ratio $g(x)/f(x)$ is~$1$ and so 
besides that $g$ is~$\bigOh(f)$ we also have that $f$ is~$\bigOh(g)$.

\pause\item
Some pairs of functions aren't comparable.
Let $g(x)=x^3$ and consider this function.
\begin{equation*}
  f(x)=\begin{cases}
         x^2  &\case{if the greatest integer of $x$ is even}  \\
         x^4  &\case{if it is odd}
       \end{cases}
\end{equation*}
(Recall that a positive number's greatest integer, or floor, is what you get
if you drop the decimal.
Thus the greatest integer of $3.2$ is $3$.)
Then $f$ is not~$\bigOh(g)$, because for input $x$'s where
the greatest integer is odd, the limit of $f(x)/g(x)$ is infinity.
And $g$ is not $\bigOh(f)$ because where
the greatest integer is even, the limit of $f(x)/g(x)$ is zero.
So there is no single limit of the ratio.
\end{enumerate}  
\end{frame}



\begin{frame}{Logarithms: all logs are equivalent}
Recall the change of base formula for logarithms.
\begin{equation*}
  \log_b(x)=\frac{\log_a(x)}{\log_a(b)}
\end{equation*}
In particular,
\begin{equation*}
  \log_b(x)=\frac{\ln(x)}{ln(b)}=\frac{1}{\ln(b)}\cdot \ln(x)
\end{equation*}
and so all logarithms differ from each other only by a constant.
So for any base~$b$, the two functions
$f(x)=\log_b(x)$ and $\ln(x)$
have the same order of growth.
\begin{equation*}
  \lim_{x\to\infty}\frac{\log_b(x)}{\ln(x)}
  =\lim_{x\to\infty}\frac{(1/\ln(b))\ln(x)}{\ln(x)}
  =1/\ln(b)
\end{equation*}
So we can state our results for $\ln(x)$ and they will apply to all
logarithmic functions.
\end{frame}


\begin{frame}
Logarithmic functions $f(x)=\ln(x)$ grow very slowly.
For instance, $\ln(x)$ is~$\bigOh(x)$.
\begin{equation*}
  \lim_{x\to\infty}\frac{\ln(x)}{x}
  =\lim_{x\to\infty}\frac{(1/x)}{1}
  =\lim_{x\to\infty}\frac{1}{x}
  =0
\end{equation*}
\pause
The function $\ln(x)$ is also $\bigOh(x^{0.1})$.
\begin{equation*}
  \lim_{x\to\infty}\frac{\ln(x)}{x^{0.1}}
  =\lim_{x\to\infty}\frac{(1/x)}{0.1\cdot x^{-0.9}}
  =\lim_{x\to\infty}\frac{1}{0.1\cdot x^{0.1}}
  =0
\end{equation*}
\pause
In general, for any $d>0$ no matter how small,
$\ln(x)$ is~$\bigOh(x^d)$ and $x^d$ is not~$\bigOh(\ln_b(x))$.
\begin{equation*}
  \lim_{x\to\infty}\frac{\ln(x)}{x^d}
  =\lim_{x\to\infty}\frac{(1/x)}{d\cdot x^{d-1}}
  =\lim_{x\to\infty}\frac{1}{d\cdot x^d}
  =0
\end{equation*}
\end{frame}

\begin{frame}
The difference in growth rates is even more marked than that.
L'H\^{o}pital's Rule
gives that $(\ln(x))^2$ is $\bigOh(x)$.
\begin{equation*}
\lim_{x\to\infty}\frac{(\ln(x))^2}{x}
=\lim_{x\to\infty}\frac{2\ln(x)/(x)}{1}      
=2\cdot\lim_{x\to\infty}\frac{\ln(x)}{x}
=2\cdot\lim_{x\to\infty}\frac{1/x}{1}=0
\end{equation*}
\pause
In the same way, every power~$k$ of a logarithm, no matter how large,
is $\bigOh(x)$. 
\pause
Even more, 
every power of a log grows more slowly than every polynomial:
for every~$k$, no matter how large, and for every~$d>0$, no matter how small, 
the function $(\ln(x))^k$ is $\bigOh(x^d)$.

Logarithmic growth is very, very much smaller than polynomial growth.
\end{frame}

\begin{frame}
The log-linear function $x\cdot\ln(x)$ has a similar relationship to the
polynomials $x^d$, where $d>1$.
\begin{multline*}
  \lim_{x\to\infty}\frac{x\ln(x)}{x^d}
  =\lim_{x\to\infty}\frac{x\cdot (1/x)+1\cdot\ln(x)}{dx^{d-1}}
  =\frac{1}{d}\cdot\lim_{x\to\infty}\frac{1+\ln(x)}{x^{d-1}}          \\
  =\frac{1}{d(d-1)}\cdot\lim_{x\to\infty}\frac{(1/x)}{x^{d-2}}
  =\frac{1}{d(d-1)}\cdot\lim_{x\to\infty}\frac{1}{x^{d-1}}
  =0
\end{multline*}
\end{frame}



\begin{frame}{Exponential functions}
  We can compare the  polynomial function $f(x)=x^2$ 
  with the exponential function $g(x)=2^x\!$,
  using L'H\^{o}pital's Rule.
  \begin{equation*}
    \lim_{x\to\infty}\frac{2^x}{x^2}
    =\lim_{x\to\infty}\frac{2^x\cdot\ln(2)}{2x}
    =\lim_{x\to\infty}\frac{2^x\cdot(\ln(2))^2}{2}
    =\infty
  \end{equation*}
  Thus
  $f\in\bigOh(g)$ but
  $g\notin\bigOh(f)$.
  Intuitively, the $x^2$ function grows strictly slower than does
  the exponential function~$2^x$.
  Similarly,
  % \begin{equation*}
    $\lim_{x\to\infty}2^x/x^k
    =\infty$
  % \end{equation*}
  for any~$k$.
% , and so $x^k$ is in $\bigOh(2^x)$
%   but $2^x$ is not in $\bigOh(x^k)$.
\end{frame}

\begin{frame}
\Lm
Logarithmic functions grow more slowly than polynomial functions:~if
$f(x)=\log_b(x)$ for some base~$b$ and $g(x)=a_mx^m+\cdots+a_0$ 
then $f$ is~$\bigOh(g)$ but
$g$ is not $\bigOh(f)$.
Polynomial functions grow more slowly than exponential functions:~where
$h(x)=b^x$ for some base~$b>1$ then 
then $g$ is~$\bigOh(h)$ but
$h$ is not $\bigOh(g)$.
\end{frame}




\begin{frame}{Order of growth hierarchy}\small
\noindent\begin{tabular}{@{}lll@{}}
    \multicolumn{1}{l}{\quad\tabulartext{Order}} 
      &\multicolumn{1}{l}{\quad\tabulartext{Name}}
      &\multicolumn{1}{l}{\quad\tabulartext{Examples}} \\
    \hline
    $\bigOh(1)$        &Bounded    &$f_0(n)=1$, $f_1(n)=15$  \\ 
    $\bigOh(\lg(\lg(n)))$  &Double logarithmic  &$f(n)=\ln(\ln(x))$    \\ 
    $\bigOh(\lg(n))$  &Logarithmic  &$f_0(n)=\ln(n)$, $f_1(n)=\lg(n^3)$  \\ 
    $\bigOh\bigl(\,(\lg(n))^c\,\bigr)$ &Polylogarithmic  &$f(n)=(\lg(n))^3$ \\ 
    $\bigOh(n)$    &Linear  &$f_0(n)=n$, $f_1(n)=3n+4$  \\ 
    $\bigOh(n\lg(n))=\bigOh(\lg(n!))$    &Log-linear  &$f(n)=5n\ln(n)+n$  \\ 
    $\bigOh(n^2)$  &Polynomial (quadratic)  &$f(n)=5n^2+2n+12$  \\ 
    $\bigOh(n^3)$  &Polynomial (cubic)  &$f(n)=2n^3+12n^2+5$ \\
    \quad\rule{0pt}{11pt}$\smash\vdots$  &   &   \\
    $\bigOh(2^n)$   &Exponential  &$f(n)=10\cdot 2^n$  \\ 
    $\bigOh(3^n)$   &Exponential  &$f(n)=6\cdot 3^n+n^2$  \\ 
    \quad\rule{0pt}{11pt}$\smash\vdots$  &   &   \\
    $\bigOh(n!)$  &Factorial  &$f(n)=5\cdot n!+n^{15}-7$  \\  
    $\bigOh(n^n)$  &\textit{--No standard name--}  &$f(n)=2\cdot n^n+3\cdot 2^n$  \\  
  \end{tabular}

Functions nearer the bottom grow strictly faster.
In algorithm runtime terms, being nearer the bottom is less desirable.    
\end{frame}



\begin{frame}
We often cut the hierarchy between the polynomial
and exponential functions.
The table below shows why.
It lists how long, in years, a job would take if we used an algorithm
that runs in time $\lg n$, time~$n$, etc.
(A modern computer runs at $10$~GHz,
$10\,000$ million ticks per second, and 
there are $3.16\times 10^7$~seconds in a year.)
\begin{center}\small
  \begin{tabular}{r|*{4}{l}}
    \multicolumn{1}{c}{\ }
      &\multicolumn{1}{c}{\tabulartext{$n=1$}}
      &\multicolumn{1}{c}{\tabulartext{$n=10$}}
      &\multicolumn{1}{c}{\tabulartext{$n=50$}}
      &\multicolumn{1}{c}{\tabulartext{$n=100$}}  \\ \cline{2-5}
    $\lg n$  &\multicolumn{1}{c}{--} &$\scinot{1.05}{-17}$ &$\scinot{1.79}{-17}$ &$\scinot{2.11}{-17}$ \rule{0em}{11pt} \\  
    $n$      &$\scinot{3.17}{-18}$ &$\scinot{3.17}{-17}$ &$\scinot{1.58}{-16}$ &$\scinot{3.17}{-16}$ \\  
    $n\lg n$ &\multicolumn{1}{c}{--} &$\scinot{1.05}{-16}$ &$\scinot{8.94}{-16}$ &$\scinot{2.11}{-15}$ \\  
    $n^2$    &$\scinot{3.17}{-18}$ &$\scinot{3.17}{-16}$ &$\scinot{7.92}{-15}$ &$\scinot{3.17}{-14}$ \\  
    $n^3$    &$\scinot{3.17}{-18}$ &$\scinot{3.17}{-15}$ &$\scinot{3.96}{-13}$ &$\scinot{3.17}{-12}$ \\  
    $2^n$    &$\scinot{6.34}{-18}$ &$\scinot{3.24}{-15}$ &$\scinot{3.57}{-3}$ &$\scinot{4.02}{12}$ \\  
  \end{tabular}
\end{center} 
In the $n=100$ column,
between the first few rows the relative change is an order of
magnitude but the absolute times are small.
Then we get to the final row.
That's not a typo\Dash the last entry really is on order of $10^{12}$~years.
Its huge, both relatively and absolutely.
The universe is $14\times 10^{9}$ years old
so this computation, even with input size of only~$100$,
would take longer than the age of the universe.
Exponential growth is very, very much larger than polynomial growth.
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
