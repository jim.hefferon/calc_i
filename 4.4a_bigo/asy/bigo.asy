// bigo.asy
//  Graphs and info about big-O

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "bigo%03d";
real PI = acos(-1);

import graph;


import graph;
// some parameters
real axis_arrow_size = 0.35mm;
real axis_tick_size = 0.75mm;



// ============== compare sqrt(x) and 10*ln(x) on small scale ================
picture pic;
int picnum = 0;
size(pic,6cm,0,IgnoreAspect);
real scalefactor = 3.2;
scale(pic,Linear,Linear(scalefactor));

// limits
real xmin=1;  // lg(xmin)=0
real xmax=1000;
real ymin=0.1;
real ymax=100;

// fcns
real f(real x) {return sqrt(x);}
// real g(real x) {return 10*log(x)/log(2);}  // they left out log2
real g(real x) {return 10*log(x);}  // they left out log2

// curves
path f=graph(pic,f,xmin,xmax,n=400);
path g=graph(pic,g,xmin,xmax,n=400);
// draw(pic,f,FCNPEN);
// draw(pic,g,FCNPEN);

// axes
xaxis(pic,YZero,
      xmin-50, xmax+50,
      RightTicks(Label("$%2.0f$",TICLABELPEN), Step=500, step=500,
		 beginlabel=false, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
yequals(pic, 0,   
	xmin=0, xmax=xmax+80,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));
yaxis(pic,XZero,
      ymin-5, ymax+5,
      LeftTicks(Label("$%2.0f$",TICLABELPEN), Step=50, step=50,
		beginlabel=false, endlabel=true,
		Size=axis_tick_size, size=0.5*axis_tick_size,
		extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
xequals(pic, 0,   
	ymin=0, ymax=ymax+10,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));

draw(pic,graph(pic,f,xmin,xmax,operator ..), FCNPEN);
draw(pic,graph(pic,g,xmin,xmax,operator ..), FCNPEN);
// label the curves
label(pic,"$\sqrt{x}$",Scale(pic,(900,f(900))),1.5S,TICLABELPEN);
label(pic,"$10\ln(x)$",Scale(pic,(800,g(800))),N,TICLABELPEN);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ============== compare sqrt(x) and 10*lg(x) on larger scale ================
picture pic;
int picnum = 1;
size(pic,8cm,0);
real scalefactor = 275;
scale(pic,Linear,Linear(scalefactor));

// limits
real xmin=1;  // lg(xmin)=0
real xmax=1000000;
real ymin=0.1;
real ymax=1000;

// fcns
real f(real x) {return sqrt(x);}
// real g(real x) {return 10*log(x)/log(2);}  // they left out log2
real g(real x) {return 10*log(x);}  // they left out log2


// axes
xaxis(pic,YZero,
      xmin-50000, xmax+75000,
      RightTicks(Label("$%2.0f$",TICLABELPEN), Step=500000, step=100000,
		 beginlabel=false, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      Arrow(TeXHead,axis_arrow_size));
yaxis(pic,XZero,
      ymin-75, ymax+75,
      LeftTicks(Label("$%2.0f$",TICLABELPEN), Step=500, step=100,
		beginlabel=false, endlabel=true,
		Size=axis_tick_size, size=0.5*axis_tick_size,
		extend=false, begin=false),
      p=AXISPEN,
      Arrow(TeXHead,axis_arrow_size));

// draw the graphs
real xbreak = 100;  // where shift from dots to curves
dotfactor=1.5; // http://asymptote.sourceforge.net/FAQ/section3.html
for (int i=ceil(xmin);i<=floor(xbreak); ++i) {
  dot(pic, Scale(pic,(i,f(i))), highlight_color, Fill(highlight_color));
  dot(pic, Scale(pic,(i,g(i))), highlight_color, Fill(highlight_color));
}
// Curves for part of the graph
//   because 1 000 000 was too many dots for Asymptote
path f=graph(pic,f,xbreak,xmax,n=400);
path g=graph(pic,g,xbreak,xmax,n=400);
draw(pic,f, FCNPEN); // +linewidth(2.5pt));
draw(pic,g, FCNPEN); // +linewidth(2.5pt));
// label the curves
label(pic,"$\sqrt{x}$",Scale(pic,(700000,f(700000))),2N,TICLABELPEN);
label(pic,"$10\ln(x)$",Scale(pic,(700000,g(700000))),2N,TICLABELPEN);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ============== Illustrate f=O(g) ======
picture pic;
int picnum=2;

size(pic,0,3cm,IgnoreAspect);

// limits
real xmin=0; 
real xmax=100;
real ymin=0;
real ymax=100;


// scale
real scalefactor=1;
scale(pic,Linear,Linear(scalefactor));

path g=Scale(pic,(0,0))
  ..Scale(pic,(10,5))
  ..Scale(pic,(20,15))
  ..Scale(pic,(30,20))
  ..Scale(pic,(40,45))
  ..Scale(pic,(60,60))
  ..Scale(pic,(80,70))
  ..Scale(pic,(100,100));
path f=Scale(pic,(0,2))
  ..Scale(pic,(10,5))
  ..Scale(pic,(20,10))
  ..Scale(pic,(30,30))
  ..Scale(pic,(40,40))
  ..Scale(pic,(60,53))
  ..Scale(pic,(80,60))
  ..Scale(pic,(100,63));
pen second_pen = highlight_color+opacity(.5,"Normal");
draw(pic, f, FCNPEN);
draw(pic, g, FCNPEN);


// xaxis  Draw axis without arrow, then draw without ticks and the arrow
//  far enough out to not hit a tick
xaxis(pic,YZero,
      xmin=xmin, xmax=xmax,
      RightTicks(Label("%",TICLABELPEN), Step=10, step=5,
		 beginlabel=false, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
xaxis(pic,YZero,
      xmin=35, xmax=45,
      RightTicks(Label("%",TICLABELPEN), Step=10, step=10,
		 beginlabel=true, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
yequals(pic, 0,   
	xmin=xmin, xmax=xmax+7,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));
// yaxis
yaxis(pic, XZero,
      ymin=ymin, ymax=ymax,
      LeftTicks(Label("%",TICLABELPEN), Step=10, step=5, 
		beginlabel=false, endlabel=true,
		Size=axis_tick_size, size=0.5*axis_tick_size,
		extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
xequals(pic, 0,   
	ymin=ymin, ymax=ymax+5,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));

// dotfactor=1.5; // http://asymptote.sourceforge.net/FAQ/section3.html
// pen second_pen = FCNPEN_NOCOLOR+highlight_color+opacity(.5,"Normal");
label(pic,"$g$",Scale(pic,(90,90)), 0.25W);
label(pic,"$f$",Scale(pic,(90,60)), 2SE);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ============== Illustrate f=O(g) ======
picture pic;
int picnum=3;

size(pic,0,3cm,IgnoreAspect);

// limits
real xmin=0; 
real xmax=100;
real ymin=0;
real ymax=100;


// scale
real scalefactor=1;
scale(pic,Linear,Linear(scalefactor));

path g=Scale(pic,(0,0))
  ..Scale(pic,(10,5))
  ..Scale(pic,(20,15))
  ..Scale(pic,(30,20))
  ..Scale(pic,(40,45))
  ..Scale(pic,(60,60))
  ..Scale(pic,(80,70))
  ..Scale(pic,(100,100));
path f=Scale(pic,(0,2))
  ..Scale(pic,(10,5))
  ..Scale(pic,(20,10))
  ..Scale(pic,(30,30))
  ..Scale(pic,(40,40))
  ..Scale(pic,(60,62))
  ..Scale(pic,(80,68))
  ..Scale(pic,(100,87));
pen second_pen = highlight_color+opacity(.5,"Normal");
draw(pic, f, FCNPEN);
draw(pic, g, FCNPEN); // was: second_pen


// xaxis  Draw axis without arrow, then draw without ticks and the arrow
//  far enough out to not hit a tick
xaxis(pic,YZero,
      xmin=xmin, xmax=xmax,
      RightTicks(Label("%",TICLABELPEN), Step=10, step=5,
		 beginlabel=false, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
xaxis(pic,YZero,
      xmin=35, xmax=45,
      RightTicks(Label("%",TICLABELPEN), Step=10, step=10,
		 beginlabel=true, endlabel=true,
		 Size=axis_tick_size, size=0.5*axis_tick_size,
		 extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
yequals(pic, 0,   
	xmin=xmin, xmax=xmax+7,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));
// yaxis
yaxis(pic, XZero,
      ymin=ymin, ymax=ymax,
      LeftTicks(Label("%",TICLABELPEN), Step=10, step=5, 
		beginlabel=false, endlabel=true,
		Size=axis_tick_size, size=0.5*axis_tick_size,
		extend=false, begin=false),
      p=AXISPEN,
      arrow=None);
xequals(pic, 0,   
	ymin=ymin, ymax=ymax+5,
        p=AXISPEN,
	ticks=NoTicks,
        arrow=Arrow(TeXHead,axis_arrow_size));

// dotfactor=1.5; // http://asymptote.sourceforge.net/FAQ/section3.html
// pen second_pen = FCNPEN_NOCOLOR+highlight_color+opacity(.5,"Normal");
label(pic,"$g$",Scale(pic,(90,90)), 0.25W);
label(pic,"$f$",Scale(pic,(80,70)), 4E);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





path ellipse(pair c, real a, real b)
{
  return shift(c)*scale(a,b)*unitcircle;
}






