\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.5\ \ Implicit differentiation}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}
% Change the default thickness of the underbars.
% https://tex.stackexchange.com/a/559081/339
% \usepackage{xpatch}
% \MHInternalSyntaxOn
% \xpatchcmd\upbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}

% \xpatchcmd\downbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}
% \MHInternalSyntaxOff

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................




\begin{frame}{Expressing a relationship implicitly}% \vspace*{-1ex}
Consider the unit circle $x^2+y^2=1$.
\begin{center}
  \includegraphics{asy/implicit000.pdf}    
\end{center}
Here $y$ is not a function of~$x$\Dash for some input $x$'s there are 
two $y$'s and for some input $x$'s there aren't any $y$'s at all.

So this is not
an expression of one variable as a function of the other.
Instead, it is something weaker:~a relationship between variables.

\pause
This is a problem for taking the derivative because we need
to compute $f(x+h)-f(x)$,
but there is no function~$f$.
\end{frame}

\begin{frame}
Instead, if we for instance focus on a small arc of the circle around the $\pi/4$
radian point, then $y$ is a function of~$x$ on that arc.
\begin{center}
  \includegraphics{asy/implicit002.pdf}    
\end{center}
So we will proceed by thinking of $y$ as a function of~$x$.
Sometimes people write that sentence as `$y=y(x)$'.
\end{frame}

\begin{frame}
From $x^2+y^2=1$ we want $dy/dx$.
We first think of $y$ as a function of~$x$.
I write $y(x)$ as a prompt,
to help remember to do the derivative correctly.
\begin{equation*}
  x^2+[\,y(x)\,]^2=1
\end{equation*}
\pause
Differentiate both sides.
\begin{align*}
  \frac{d\,(x^2+[y(x)]^2)}{dx}  &=\frac{d\,1}{dx}  \\
  2x  + \underbracket{2y(x)}\cdot \underbracket{\frac{dy}{dx}}  &=0
\end{align*}
\pause
Finish by solving for $dy/dx$. 
\begin{align*}
  2x  &= -2y(x)\cdot \frac{dy}{dx}  \\
  -\frac{x}{y}  &= \frac{dy}{dx}  
\end{align*}
For example, at the $\pi/4$ radian point we have $x=y=\sqrt{2}/2$,
and the slope is~$-1$.
\end{frame}


\begin{frame}
Here is that tangent line.
\begin{center}
  \includegraphics{asy/implicit001.pdf}    
\end{center}
\end{frame}


\begin{frame}{What's with the $y(x)$?}
Writing $x^2+[y(x)]^2=1$ explains why when we differentiate
\begin{align*}
  \frac{d\,(x^2+[y(x)]^2)}{dx}  &=\frac{d\,1}{dx}  \\
  2x  + \underbracket{2y(x)}\cdot \underbracket{\frac{dy}{dx}}  &=0
\end{align*}
we treat $y$ differently than~$x$.

It also reminds us that the process does not work at some points $(x,y)$.
With $dy/dx=-x/y$ the rate of change is undefined at $y=0$.
That's where $y$ is not a function
of~$x$ because no matter how small an arc you consider, 
for some $x$'s there are two $y$'s.

\pause
Often, once the differentiating is over we drop the 
`$y(x)$' and just write `$y$'.
\end{frame}





\begin{frame}
\Ex A point is moving on the ellipse $4x^2+9y^2=36$.  
When that point is at $(0,2)$ its $x$~coordinate is decreasing by
$2$ units per second.
How fast is its $y$~coordinate changing at that moment?
\begin{center}
  \includegraphics{asy/implicit003.pdf}
\end{center}
%\end{frame}

%\begin{frame}
This asks about units per second so it wants $dx/dt$ and $dy/dt$.
Start with this. 
\begin{equation*}
  4[x(t)]^2+9[y(t)]^2=36
\end{equation*}
Differentiate with respect to~$t$ and then plug in.
\begin{align*}
  4\cdot \underbracket{2x}\cdot\underbracket{\frac{dx}{dt}}
    +9\cdot \underbracket{2y}\cdot\underbracket{\frac{dy}{dt}}  &=0  \\
  4\cdot 0\cdot (-2)+9\cdot 4\cdot \frac{dy}{dt} &=0 
\end{align*}
Thus $dy/dt$ is $0$.
\end{frame}



% \begin{frame}{More practice}
% Find $dy/dx$ at the given point.
% \begin{equation*}
%   3xy-2x-2=0
%   \qquad
%   (2,1)
% \end{equation*}
% \pause
% First think of $y$ as a function of~$x$.
% \begin{equation*}
%   3x\cdot y(x)-2x-2=0
% \end{equation*}
% Differentiate with respect to~$x$.
% \pause
% \begin{equation*}
%   3x\cdot\frac{dy}{dx}+y(x)\cdot (3)-2=0
% \end{equation*}
% Solve for $dy/dx$.
% \begin{align*}
%   3x\cdot\frac{dy}{dx} &=-3y(x)+2  \\
%   \frac{dy}{dx}        &\frac{-3y+2}{3x}
% \end{align*}
% At the point $(2,1)$ we have $dy/dx=-1/6$.
% \end{frame}



% \begin{frame}{Steps for implicit differentiation}
% When some variable is an implicit function of some other one, 
% to find the desired rate of change follow these steps.
% \begin{itemize}
% \item Decide which variables are a function of the variable of interest.
%   For instance, if $y$ is to be taken as a function of~$x$ then 
%   I express that by writing $y(x)$.
%   Or, if $x$ and~$y$ are meant to be functions of time then I write
%   $x(t)$ and $y(t)$.
% \item Differentiate both sides with respect to the variable of interest.
%   Often this involves the Chain Rule.
% \item Solve for the desired rate of change.
% \end{itemize}
% \end{frame}






\begin{frame}
\Ex Where $y^4+xy=x^3-x+2$, 
the point $(1,1)$ is on the curve; find the line tangent to that point.

\pause
First, think of $y$ as a function of~$x$.
\begin{equation*}
  [y(x)]^4+x\cdot y(x)=x^3-x+2
\end{equation*}
Take $d/dx$ of both sides.
We've highlighted the Product Rule and the Chain Rule.
\begin{align*}
  \overbrace{ \underbracket{4y^3}\cdot\underbracket{\frac{dy}{dx}}+x\cdot\frac{dy}{dx} }+1\cdot y &=3x^2-1 \\
  \frac{dy}{dx}\cdot (4y^3+x) &=3x^2-1-y \\
  \frac{dy}{dx} &= \frac{3x^2-1-y}{4y^3+x}
\end{align*}
At $(1,1)$ the slope of the tangent line is $1/5$.
\begin{equation*}
  y-1=\frac{1}{5}(x-1)
\end{equation*}
\end{frame}


% \begin{frame}
% \Ex 
% Find the derivative $dy/dx$ when $y^3+x^3=3xy$ at the point $(3/2,3/2)$.

% \pause
% Think of $y$ as a function of~$x$.
% \begin{equation*}
%   [y(x)]^3+x^3=3x\cdot y(x)
% \end{equation*}
% Apply $d/dx$ to both sides.
% \begin{equation*}
%   3\underbracket{[y(x)]^2}\cdot\underbracket{\frac{dy}{dx}}+3x^2
%   =3x\cdot\frac{dy}{dx}+y(x)\cdot 3
% \end{equation*}
% Solve for $dy/dx$.
% (We can drop the `$y(x)$' prompt.)
% \begin{align*}
%   3y^2\frac{dy}{dx}-3x\frac{dy}{dx} &=3y-3x^2  \\
%   \frac{dy}{dx}\cdot\left(3y^2-3x\right) &=3y-3x^2  \\
%   \frac{dy}{dx} &=\frac{3y-3x^2}{3y^2-3x}  
% \end{align*}
% Plug in $(x,y)=(3/2,3/2)$ to get a slope of $-1$.
% \end{frame}



\begin{frame}{Practice}
\begin{enumerate}
\item Find $dy/dx$ where $2x^2+xy-y^2=2$. 
\item Find $dy/dx$ where $xe^y=x-y$. 
% \item Give an equation for the tangent line
%   when $x^{2/3}+y^{2/3}=4$ at $(-3\sqrt{3},1)$.
\end{enumerate}
\end{frame}



% \begin{frame}{Practice}
% Evaluate $y'$ at the given point.
% \begin{equation*}
%   5x^3-y-1=0 
%   \qquad (1,4)
% \end{equation*}
% \pause
% First, think of $y$ as a function of~$x$.
% \begin{equation*}
%   5x^3-y(x)-1=0
% \end{equation*}
% Next take the derivative of both sides.
% \pause
% \begin{equation*}
%   15x^2-\frac{dy}{dx} = 0
% \end{equation*}
% Finally, solve for $dy/dx$.
% \begin{equation*}
%   15x^2=\frac{dy}{dx}
% \end{equation*}
% At $(1,4)$ we have $y'(x)=15$.
% \end{frame}




% \begin{frame}
% \Ex Find $y'$ if $e^x\cos y-e^{-y}\sin x=\pi$.

% \pause  
% We start by thinking of $y$ as $y(x)$.
% \begin{equation*}
%   e^x\cos(y(x))-e^{-y(x)}\sin x=\pi
% \end{equation*}
% Differentiate.
% \begin{equation*}
%   e^x\cdot\underbracket{(-\sin(y(x)))}\cdot\underbracket{\frac{dy}{dx}}+e^x\cos(y(x))
%    -\left[e^{-y(x)}\cos x+\sin x\cdot \underbracket{e^{-y(x)}}\underbracket{(-\frac{dy}{dx})} \right]
%   = 0
% \end{equation*}
% Solve for $dy/dx$.
% \begin{align*}   
%   \frac{dy}{dx}\cdot\left(-e^x\sin y+e^{-y}\sin x\right) &=
%       -e^x\cos y+e^{-y}\cos x     \\
%   \frac{dy}{dx}  &=\frac{-e^x\cos y+e^{-y}\cos x}{-e^x\sin y+e^{-y}\sin x}
% \end{align*}
% \end{frame}

\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
