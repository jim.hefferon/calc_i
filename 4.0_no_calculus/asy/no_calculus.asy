// no_calculus.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "no_calculus%03d";
real PI = acos(-1);

import graph;



// ==== inc/dec and concavity ====

// ....... inc and up ............
picture pic;
int picnum = 0;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

draw(pic, grf, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... inc and down ............
picture pic;
int picnum = 1;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... dec and down ............
picture pic;
int picnum = 2;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,0.75)::(1.5,0.5);

draw(pic, grf, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... dec and up ............
picture pic;
int picnum = 3;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,1.25)::(1.5,0.5);

draw(pic, grf, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======= concave up ======
real f4(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f4, 0.5, 1.5);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ......... concave down ...........
real f5(real x) {return -2*(x-1)^2+1.5;}

picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f5, 0.5, 1.5);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== power curves ====

// ....... x^{even number} .........
real f6(real x) {return x^2;}

picture pic;
int picnum = 6;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f6,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... x^{odd number} .........
real f7(real x) {return x^3;}

picture pic;
int picnum = 7;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.5; xright=1.5;
ybot=-4; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f7,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... x^{1} .........
real f8(real x) {return x;}

picture pic;
int picnum = 8;
// size(pic,0,3cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.5; xright=1.5;
ybot=-4; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f8,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... x^{0} .........
real f9(real x) {return 1;}

picture pic;
int picnum = 9;
// size(pic,0,3cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f9,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ....... x^{-odd number} .........
real f10(real x) {return 1/x;}

picture pic;
int picnum = 10;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-4; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f10,xleft-0.1,0-0.25), FCNPEN);
draw(pic, graph(f10,0+0.25,xright+0.1), FCNPEN);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... x^{-even number} .........
real f11(real x) {return 1/(x^2);}

picture pic;
int picnum = 11;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=8;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f11,xleft-0.1,0-0.35), FCNPEN);
draw(pic, graph(f11,0+0.35,xright+0.1), FCNPEN);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// =============== trig ================
real f12(real x) {return sin(x);}

picture pic;
int picnum = 12;
size(pic,4cm,0,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=7;
ybot=-1; ytop=1;

path f = graph(f12,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ...... cosine .........
real f13(real x) {return cos(x);}

picture pic;
int picnum = 13;
size(pic,4cm,0,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=7;
ybot=-1; ytop=1;

path f = graph(f13,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ...... tangent .........
real f14(real x) {return tan(x);}

picture pic;
int picnum = 14;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-3; ytop=3;

draw(pic, graph(f14,xleft-0.15,-(PI/2)-0.28), FCNPEN);
draw(pic, graph(f14,-(PI/2)+0.28, (PI/2)-0.28), FCNPEN);
draw(pic, graph(f14,(PI/2)+0.28, xright+0.15), FCNPEN);

draw(pic, (-(PI/2),ybot-0.1)--(-(PI/2),ytop+0.1), dashed);
draw(pic, ((PI/2),ybot-0.1)--((PI/2),ytop+0.1), dashed);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======== exp(x) ===========
real f15(real x) {return exp(x);}

picture pic;
int picnum = 15;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-3; ytop=7;

path f = graph(f15,xleft-0.15,xright);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .......... ln(x) .............
real f16(real x) {return log(x);}

picture pic;
int picnum = 16;
// size(pic,0,4cm,keepAspect=true);
unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-3; ytop=3;

path f = graph(f16,0+0.08,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======== graph shifting ====
real f17(real x) {return x^2;}

picture pic;
int picnum = 17;
size(pic,0,3cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=5;

path f = graph(f17,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... shift up ...........
real f18(real x) {return x^2+1;}

picture pic;
int picnum = 18;
size(pic,0,3cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=5;

path f = graph(f18,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

real[] T18 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T18, Size=3pt),
      arrow=Arrows(TeXHead));
labely(pic, "$a$", 1);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... shift down ...........
real f19(real x) {return x^2-1;}

picture pic;
int picnum = 19;
size(pic,0,3cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=5;

path f = graph(f19,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

real[] T19 = {-1};

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T19, Size=3pt),
      arrow=Arrows(TeXHead));
labely(pic, "$-a$", -1);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... shift left ...........
real f20(real x) {return (x+1)^2;}

picture pic;
int picnum = 20;
size(pic,0,3cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=5;

path f = graph(f20,xleft-0.1,1+0.25);
draw(pic, f, FCNPEN);

real[] T20 = {-1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T20, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$-a$", -1);

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... shift right ...........
real f21(real x) {return (x-1)^2;}

picture pic;
int picnum = 21;
size(pic,0,3cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=5;

path f = graph(f21,-1-0.25,xright+0.1);
draw(pic, f, FCNPEN);

real[] T21 = {1};

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T21, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", 1);
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ====== stretching ========

// ......... c=1 ............
real f22(real x) {return x^2;}

picture pic;
int picnum = 22;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f22,xleft-0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ......... c>0 ............
real f23(real x) {return 2*x^2;}

picture pic;
int picnum = 23;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=8;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f23,xleft-0.1,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ......... c<0 ............
real f24(real x) {return -2*x^2;}

picture pic;
int picnum = 24;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-8; ytop=0;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f24,xleft-0.1,xright+0.1), FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======= asymptotes ===========

// .... different infinities .........
real f25(real x) {return 1+1/(x-1);}

picture pic;
int picnum = 25;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-3; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f25,xleft-0.1,1-0.25), FCNPEN);
draw(pic, graph(f25,1+0.30,xright+0.1), FCNPEN);

draw(pic, (xleft-0.2,1)--(xright+0.2,1), ASYMPTOTEPEN+red);
draw(pic, (1,ybot-0.1)--(1,ytop+0.1), ASYMPTOTEPEN+red);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
labelx(pic, "$a_0$", 1.5);
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
labely(pic, "$a_1$", 1.5);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... same vert infinity .........
real f26(real x) {return 1/((x-1)^2);}

picture pic;
int picnum = 26;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=0; ytop=6;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f26,xleft-0.1,1-0.39), FCNPEN);
draw(pic, graph(f26,1+0.39,xright+0.1), FCNPEN);

draw(pic, (1,ybot-0.1)--(1,ytop+0.1), ASYMPTOTEPEN+red);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", 1.5);
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .... sin(x)/x .........
real f27(real x) {return sin(x)/x;}

picture pic;
int picnum = 27;
size(pic,4cm,0,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=15;
ybot=-1; ytop=1;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(f27,xleft+0.1,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== oblique asymptote ========
picture pic;
int picnum = 28;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf0 = (2,2.5)..(2.5,3)::(3,3.75);
path grf1 = (2,2.35)..(2.5,2.9)::(3,3.70);
// path grf1 = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf0, highlight_color, Arrow);
label(pic,"$\ldots$", (1.75,2.45), W, highlight_color);
draw(pic, grf1, blue, Arrow);
label(pic,"$\ldots$", (1.7,2.25), W, blue);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ==== poly sketching ========

// ...... oblique asymptotes ......
picture pic;
int picnum = 29;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf0 = (4,2.5)..(4.5,3)::(5,3.75);
path grf1 = reflect((0,ybot),(0,ytop))*grf0;
// path grf2 = point(grf1,0){(1,-1)}..(-0.5,-0.25)..(0,0.5)..(1,0)..{dir(grf0,0,1)}point(grf0,0);

draw(pic, grf0, highlight_color, Arrow);
draw(pic, grf1, highlight_color, Arrow);
// draw(pic, grf2, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... include lower-order terms ......
picture pic;
int picnum = 30;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf0 = (4,2.5)..(4.5,3)::(5,3.75);
path grf1 = reflect((0,ybot),(0,ytop))*grf0;
path grf2 = point(grf1,0){(1,-1)}..(-0.5,-0.25)..(0,0.5)..(1,0)..{dir(grf0,0,1)}point(grf0,0);

draw(pic, grf0, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf1, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf2, FCNPEN);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .... even and odd roots .........
real f31(real x) {return (x-2)*x^2*(x+1);}

picture pic;
int picnum = 31;
size(pic,0,5cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=4;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path f = graph(f31,-1-0.5,xright+0.1);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== local and absolute max ====
picture pic;
int picnum = 32;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
..(4,ytop-1.5){E}..(xright+0.1,2){SE};


draw(pic, grph, FCNPEN);

filldraw(pic, circle((2,ytop),0.05), highlight_color, FCNPEN_COLOR);
label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color, FCNPEN_COLOR);
label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color, FCNPEN_COLOR);
label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// .... rational functions .........
real f33(real x) {return (x+1)^2/(x-1);}

picture pic;
int picnum = 33;
size(pic,0,5cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=5;
ybot=-4; ytop=10;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f33,xleft-0.1,1-0.5), highlight_color);
draw(pic, graph(f33,1+0.65,xright+0.1), highlight_color);

draw(pic, (1,ybot)--(1,ytop), ASYMPTOTEPEN);
dotfactor = 4;
dot(pic, (-1,0), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... rational functions .........
real f34(real x) {return (x-1)/(10*(x+2)^2);}

picture pic;
int picnum = 34;
size(pic,0,5cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-4; ytop=1;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f34,xleft-0.1,-2-0.28), highlight_color);
draw(pic, graph(f34,-2+0.25,xright+0.1), highlight_color);
dotfactor = 4;
dot(pic, (1,0), highlight_color);
draw(pic,(-2,ybot)--(-2,ytop), ASYMPTOTEPEN);
draw(pic,(xleft,0)--(xright,0), ASYMPTOTEPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// == graph with a hole ========
real f35(real x) {return 2x+1;}

picture pic;
int picnum = 35;
size(pic,0,5cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=7;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f35,xleft-0.1,1-0.05), FCNPEN);
draw(pic, graph(f35,1+0.05,xright+0.1), FCNPEN);

filldraw(pic, circle((1,3),0.1),white,highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======== rational function ==========
real f36(real x) {return (x+2)/(2*(x+1));}

picture pic;
int picnum = 36;
size(pic,0,4cm,keepAspect=true);
// unitsize(pic, 0.25cm);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=2;
ybot=-2; ytop=3;

draw_graphpaper(pic, xleft, xright, ytop, ybot);

draw(pic, graph(f36,xleft-0.1,-1-0.2), highlight_color);
draw(pic, graph(f36,-1+0.2,xright+0.1), highlight_color);
dotfactor = 4;
dot(pic, (-2,0), highlight_color);
draw(pic,(-1,ybot)--(-1,ytop), ASYMPTOTEPEN);
draw(pic,(xleft,1/2)--(xright,1/2),  ASYMPTOTEPEN);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// =====================================================================
// ======================= Exercises ===================================
// =====================================================================
string OUTPUT_FN = "no_calculus1%03d";

pen FCNCOLOR =  red;
// pen FCNPEN = FCNCOLOR+linewidth(0.8pt);
pen ASYMPTOTEPEN = lightblue+dashed+linewidth(1.2pt);
real ARROWSIZE = 4;
pen LIGHTCOLOR = blue;
// pen GRAPHPAPERPEN=(0.25*LIGHTCOLOR+0.75*white)
//   +squarecap;  // For graph paper lines



// ======== polynomials ==========


// =========== number 1 ==============
// .......... large x's ...............
real f0(real x) {return (x-4)^2*(x+5)*3;}

picture pic;
int picnum = 0;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-7; xmax=7;
ymin=-3; ymax=3;

path fcn = (-7,-3){NE}..(-5,0)..(0,3)..(4,0)..{NE}(7,3);  // Left to right fcn

draw(pic, subpath(fcn,0, 0.25), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn,3.75, 4), FCNPEN, EndArrow(size=ARROWSIZE));

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
real f0(real x) {return (x-4)^2*(x+5)*3;}

picture pic;
int picnum = 1;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-7; xmax=7;
ymin=-3; ymax=3;

draw(pic, fcn, FCNPEN, Arrows(size=ARROWSIZE));
dotfactor = 5;
dot(pic, (4,0), FCNCOLOR);
dot(pic, (-5,0), FCNCOLOR);

// Axes 
real[] T = {-5, 4}; // x ticks
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);



// =========== number 2 ==============
// .......... large x's ...............
real f0(real x) {return -3*(x+1)^3*(x-2)*2;}

picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-3; xmax=4;
ymin=-3; ymax=3;

path fcn = (-3,3){curl 0.5}..{SE}(-1,0)..(0,-1)..{E}(2,0)..{SE+S}(4,-3);  // Left to right fcn

draw(pic, subpath(fcn,0, 0.25), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn,3.75, 4), FCNPEN, EndArrow(size=ARROWSIZE));

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
real f0(real x) {return (x-4)^2*(x+5)*3;}

picture pic;
int picnum = 3;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn, FCNPEN, Arrows(size=ARROWSIZE));
real[] T = {-1, 2}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);



// =========== number 3 ==============
// .......... large x's ...............
real f0(real x) {return (1/2)*(x-4)^3*(x+3)^(58);}

picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-4; xmax=5;
ymin=-3; ymax=3;

path fcn = (xmin-0.4,-3){2*N+NE}..{E}(-3,0)..(0,-1)..(4,0)..(xmax,3);  // Left to right fcn

draw(pic, subpath(fcn,0, 0.25), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn,3.75, 4), FCNPEN, EndArrow(size=ARROWSIZE));

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 5;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn, FCNPEN, Arrows(size=ARROWSIZE));
real[] T = {-3, 4}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);



// =========== number 4 ==============
// .......... large x's ...............
real f6(real x) {return 1/(x-2);}

picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-4; xmax=5;
ymin=-3; ymax=3;

path fcn_left = (xmin,-0.1){E}::(1.5,-1)..{S}(2-0.1,ymin);  
path fcn_right = (2+0.1,ymax){S}..{SE}(2.5,1)..(xmax,0.1);  

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 7;
size(pic,0,3cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {2}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 8;
size(pic,0,3cm,keepAspect=true);

real[] A = {2}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);




// =========== number 5 ==============
// .......... large x's ...............
real f9(real x) {return 1/(x+3);}

picture pic;
int picnum = 9;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-5; xmax=2;
ymin=-3; ymax=3;

path fcn_left = (xmin,-0.1){E} :: (-3.5,-1) .. {S}(-3-0.1,ymin);  
path fcn_right = (-3+0.1,ymax){S} .. {SE}(-2,1) .. {E}(xmax,0.1);  

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 10;
size(pic,0,3cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {-3}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 11;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);




// =========== number 6 ==============
// .......... large x's ...............
real f9(real x) {return 1/( (x-2)*(x+3) );}

picture pic;
int picnum = 12;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-5; xmax=5;
ymin=-3; ymax=3;

path fcn_left = (xmin,+0.1){E} :: (-3.5,+1) .. {N}(-3-0.1,ymax);  
path fcn_mid = (-3+0.1,ymin){N} :: (-2.5,-1) .. (0,-0.5){E} .. (1.5,-1) :: {S}(2-0.1,ymin);  
path fcn_right = (2+0.1,ymax){S} .. {SE}(3,1) .. {E}(xmax,0.1);  

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 13;
size(pic,0,3cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {-3, 2}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 14;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);





// =========== number 7 ==============
// .......... large x's ...............
real f15(real x) {return 1/( (x-2)^2 );}

picture pic;
int picnum = 15;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-2; xmax=5;
ymin=-3; ymax=3;

path fcn_left = (xmin,+0.1){E} :: (1.5,+1) .. {N}(2-0.1,ymax);  
// path fcn_mid = (-3+0.1,ymin){N} :: (-2.5,-1) .. (0,-0.5){E} .. (1.5,-1) :: {S}(2-0.1,ymin);  
path fcn_right = (2+0.1,ymax){S} .. {SE}(3,1) .. {E}(xmax,0.1);  

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 16;
size(pic,0,3cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {2}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 17;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
// draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);




// =========== number 8 ==============
// .......... large x's ...............
real f18(real x) {return 1/( x^2*(x-4)^3 );}

picture pic;
int picnum = 18;
size(pic,0,3cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-2; xmax=6;
ymin=-3; ymax=3;

path fcn_left = (xmin,-0.1){E} :: (-1,-0.1) .. {S}(0-0.1,ymin);  
path fcn_mid = (0+0.1,ymin){N} :: (1,-1) .. (2,-0.5){E} .. (3,-1) :: {S}(4-0.1,ymin);  
path fcn_right = (4+0.1,ymax){S} .. {SE}(5,0.5) .. {E}(xmax,0.1);  

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 19;
size(pic,0,3cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {0,4}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 20;
size(pic,0,3cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);





// =========== number 9 ==============
// .......... large x's ...............
real f21(real x) {return (x-2)*(x+3)/(x-4);}

picture pic;
int picnum = 21;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-5; xmax=6;
ymin=-3; ymax=3;

path fcn_left = (xmin,ymin){NE+N} :: (-3,0) .. (2,0) .. (3,-1).. {S}(4-0.1,ymin);  
// path fcn_mid = (0+0.1,ymin){N} :: (1,-1) .. (2,-0.5){E} .. (3,-1) :: {S}(4-0.1,ymin);  
path fcn_right = (4+0.1,ymax){S} .. (5,2) .. {NE}(xmax,ymax);  

draw(pic, subpath(fcn_left,0, 0.35), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 22;
size(pic,4cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.15), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.65, 4), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {4}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {-3, 2}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 23;
size(pic,4cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
// draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);





// =========== number 10 ==============
// .......... large x's ...............
real f21(real x) {return (x-1)/(x+1);}

picture pic;
int picnum = 24;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-3; xmax=3;
ymin=-2; ymax=3;

path fcn_left = (xmin,1+0.1){E} .. (-2,1.5) .. {N}(-1-0.1,ymax);  
// path fcn_mid = (0+0.1,ymin){N} :: (1,-1) .. (2,-0.5){E} .. (3,-1) :: {S}(4-0.1,ymin);  
path fcn_right = (-1+0.1,ymin){N} .. (1,0) .. {E}(xmax,1-0.1);  

draw(pic, subpath(fcn_left,0, 0.75), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {1}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 25;
size(pic,4cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.75), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {-1}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {1}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  labely(pic, format("$%f$",r), r, NE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 26;
size(pic,4cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
// draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

// Asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  labely(pic, format("$%f$",r), r, NE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);





// =========== number 11 ==============
// .......... large x's ...............
real f21(real x) {return (x-1)^2*(x+2)/(x+1);}

picture pic;
int picnum = 27;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-3; xmax=3;
ymin=-3; ymax=3;

path fcn_left = (xmin,xmax) .. (-2,0) .. {S}(-1-0.1,ymin);  
// path fcn_mid = (0+0.1,ymin){N} :: (1,-1) .. (2,-0.5){E} .. (3,-1) :: {S}(4-0.1,ymin);  
path fcn_right = (-1+0.1,ymax){S} .. (1,0){E} .. {N+NE}(xmax,ymax);  

draw(pic, subpath(fcn_left,0, 0.5), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 28;
size(pic,4cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.75), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {-1}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {-2,1}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  labely(pic, format("$%f$",r), r, NE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 29;
size(pic,4cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
// draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

// Asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  labely(pic, format("$%f$",r), r, NE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);





// =========== number 12 ==============
// .......... large x's ...............
real f30(real x) {return (x-2)/( (x+1)^2(x-3)^3 );}

picture pic;
int picnum = 30;
size(pic,4cm,keepAspect=true);

real xmax, xmin, ymax, ymin; // limits of graph
xmin=-3; xmax=5;
ymin=-3; ymax=3;

path fcn_left = (xmin,0+0.1){E} .. (-2,0.5) .. {N}(-1-0.1,ymax);  
path fcn_mid = (-1+0.1,ymax){S} .. (2,0) .. {S}(3-0.1,ymin);  
path fcn_right = (3+0.1,ymax){S} .. (4,0.5) .. {E}(xmax,0+0.1);  

draw(pic, subpath(fcn_left,0, 0.5), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] B = {0}; // horiz asymptotes
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

// Axes 
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... asympotes and roots ...............
picture pic;
int picnum = 31;
size(pic,4cm,keepAspect=true);

draw(pic, subpath(fcn_left,0, 0.75), FCNPEN, BeginArrow(size=ARROWSIZE));
draw(pic, subpath(fcn_right,1.5, 2), FCNPEN, EndArrow(size=ARROWSIZE));

real[] A = {-1, 3}; // vert asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}
  
real[] T = {2}; // roots
dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  labely(pic, format("$%f$",r), r, NE);
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);


// .......... all x's ...............
picture pic;
int picnum = 32;
size(pic,4cm,keepAspect=true);

draw(pic, fcn_left, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_mid, FCNPEN, Arrows(size=ARROWSIZE));
draw(pic, fcn_right, FCNPEN, Arrows(size=ARROWSIZE));

// Asymptotes
for (real r: A) {
  draw(pic, (r,ymin)--(r,ymax), ASYMPTOTEPEN);
}
for (real r: B) {
  draw(pic, (xmin,r)--(xmax,r), ASYMPTOTEPEN);
}

dotfactor = 5;
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}

// Axes
xaxis(pic, xmin=xmin-0.2, xmax=xmax+0.2,
      RightTicks("%", Step=1, Size=2pt)
      );
dotfactor = 5; 
for (real r: T) {
  dot(pic, (r,0), FCNCOLOR);
}
for (real r: T) {
  labelx(pic, format("$%f$",r), r);
}
for (real r: A) {
  labelx(pic, format("$%f$",r), r, SE);
}
for (real r: B) {
  if (r!=0) {
    labely(pic, format("$%f$",r), r, NE);
  }
}

yaxis(pic, ymin=ymin, ymax=ymax+0.2,  
      NoTicks
      ); 

shipout(format(OUTPUT_FN,picnum),pic);




// ====== graphing polys, leading terms
picture pic;
int picnum = 33;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-3; ytop=3;

path grf0 = (-2,1.5)..(-2.75,2.5)::(-3,3);
path grf1 = reflect((0,ybot),(0,ytop))*grf0;

draw(pic, grf0, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf1, FCNPEN, Arrow(ARROWSIZE));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............ even power, neg coeff .......
picture pic;
int picnum = 34;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-3; ytop=3;

path grf0 = reflect((xleft,0),(xright,0))*((-2,1.5)..(-2.75,2.5)::(-3,3));
path grf1 = reflect((0,ybot),(0,ytop))*grf0;

draw(pic, grf0, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf1, FCNPEN, Arrow(ARROWSIZE));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............ odd power, pos coeff .......
picture pic;
int picnum = 35;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-3; ytop=3;

path grf0 = (-2,1.5)..(-2.75,2.5)::(-3,3);
path grf1 = reflect((0,ybot),(0,ytop))*grf0;
path grf2 = reflect((xleft,0),(xright,0))*grf0;

draw(pic, grf2, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf1, FCNPEN, Arrow(ARROWSIZE));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............ odd power, neg coeff .......
picture pic;
int picnum = 36;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-3; ytop=3;

path grf0 = (-2,1.5)..(-2.75,2.5)::(-3,3);
path grf1 = reflect((xleft,0),(xright,0))*reflect((0,ybot),(0,ytop))*grf0;

draw(pic, grf0, FCNPEN, Arrow(ARROWSIZE));
draw(pic, grf1, FCNPEN, Arrow(ARROWSIZE));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
