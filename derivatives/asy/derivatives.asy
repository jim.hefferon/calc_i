// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "derivatives%03d";

import graph;


// ===== Defn of slope ====
picture pic;
int picnum = 0;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=6;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

pair lower_left = (1,1);
pair lower_right = (3,1);
pair upper_right = (3,4);

path triangle_bot = lower_left--lower_right;
path triangle_right = lower_right--upper_right;
path triangle_hyp = upper_right--lower_left;

Label L_bot = Label("$\Delta x$, change in $x$", align=1.5*S, position=MidPoint,
		    filltype=Fill(white));
draw(pic, triangle_bot, L=L_bot);
Label L_right = Label("\begin{minipage}{1.75cm}\centering $\Delta y$, change\\ \ in $y$\end{minipage}",
		      align=1.25*E, position=MidPoint,
		      filltype=Fill(white));
draw(pic, triangle_right, L=L_right);
draw(pic, triangle_hyp);
label(pic,
      Label("$\text{slope}=\frac{\Delta y}{\Delta x}=\frac{3}{2}$",filltype=Fill(white)),
      (1.75,5));

filldraw(pic, circle(lower_left,0.05), white, black);
filldraw(pic, circle(lower_right,0.05), white, black);
filldraw(pic, circle(upper_right,0.05), white, black);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== various slopes ====
picture pic;
int picnum = 1;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=6;

draw(pic, (0,0)--(xright,0.25*xright),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 2;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw(pic, (0,0)--(xright,0.5*xright),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 3;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw(pic, (0,0)--(xright,0.75*xright),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 4;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

draw(pic, (0,0)--(xright,1.0*xright),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 5;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

real last_x = ytop/1.5;  
draw(pic, (0,0)--(last_x,1.5*last_x),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 6;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

real last_x = ytop/2;  
draw(pic, (0,0)--(last_x,2.0*last_x),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .................
picture pic;
int picnum = 7;
size(pic,2cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

real last_x = ytop/3;  
draw(pic, (0,0)--(last_x,3.0*last_x),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ airplane path =======
picture pic;
int picnum = 8;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


path flight = (0,1)..(2,2.25)..(3,2)..(4,2)..(5,3);
draw(pic, flight, highlight_color);
label(pic,graphic("airplane.png","width=0.40cm,angle=40"),point(flight,0.4),
      filltype=Fill(white));

path second_half = subpath(flight,2,5);
real t_pt = dirtime(second_half,(1,4));
filldraw(pic, circle(point(second_half,t_pt),0.05), white, black);
path second_half = subpath(flight,2,5);
real t_pt = dirtime(second_half,(1,2));
filldraw(pic, circle(point(second_half,t_pt),0.05), white, black);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ 5-(1/2)*(x-3)^2 =======
real f(real x) {return 5-(1/2)*(x-3)^2;}

picture pic;
int picnum = 9;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f(real x) {return 5-(1/2)*(x-3)^2;}

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
draw(pic, graph(f,-0.1,5.1), highlight_color);
filldraw(pic, circle((1,3),0.05), white, black);
label(pic,  "$(1,3)$", (1,3), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(3)
// 5
// sage: f(1)
// 3
// sage: f(2)
// 9/2
pair anchor_pt = (1,3);
pair first_secant_pt = (2,4.5);
real first_secant(real x) {return ((4.5-3)/(2-1))*(x-1)+3;}


picture pic;
int picnum = 10;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(first_secant,0.5,2.5), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(first_secant_pt,0.05), white, black);
label(pic,  "$(2,4.5)$", first_secant_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
pair second_secant_pt = (1.1,3.195);
real second_secant(real x) {return ((3.195-3)/(1.1-1))*(x-1)+3;}


picture pic;
int picnum = 11;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(second_secant,0.25,1.75), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(second_secant_pt,0.05), white, black);
label(pic,  "$(1.1,3.195)$", second_secant_pt, (0.85,0.1));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
real tangent_line(real x) {return 2*(x-1)+3;}


picture pic;
int picnum = 12;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(tangent_line,0.25,1.75), highlight_color);

label(pic,graphic("climbing.png","width=0.80cm,angle=10"),
      anchor_pt-(0.1,-0.15));

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

