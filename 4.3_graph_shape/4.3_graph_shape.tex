\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}
\usepackage{../../sansserif}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.3\ \ Graph shape}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................





\section{What the first derivative says about a graph} 



\begin{frame}{First Derivative test}
% Suppose that
% $f$ is differentiable on the interval $\open{a}{b}$.
% The Mean Value Theorem tells us that 
% if $f'(x)$ is positive for all inputs~$x$ in that interval then $f$ is 
% increasing on that interval,
% and
% if $f'(x)$ is negative on that interval then $f$ is 
% decreasing there.


\Tm Let $f$ be continuous on an open interval and let $c$ be a critical number.
If the derivative $f'$ changes from positive to negative at $c$
then the function $f$ has a local maximum there.  
Similarly, if $f'$ changes from negative to positive at $c$
then $f$ has a local minimum there.  
If the derivative does not change sign then the function does not have
a local optima there.

\begin{center}
  \includegraphics{asy/graph_shape010.pdf}
  \qquad
  \includegraphics{asy/graph_shape011.pdf}
\end{center}
\end{frame}


\begin{frame}
So, critical numbers are candidates for local optima.
Determine the sign of the derivative between them
and then quote the test. 

\Ex Apply the First Derivative test to $f(x)=x^3-27x-20$.

\pause
The derivative is $f'(x)=3x^2-27=3(x^2-9)=3(x+3)(x-3)$.
Critical numbers are $-3$ and~$3$.
We can use this table.
\begin{center}\small
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial value}}   
      &\multicolumn{1}{c}{\textit{Conclusion}}   \\ \hline
    $\open{-\infty}{-3}$ 
       &$f'(-4)$ pos %=21$  
       &$f$ increasing \\
    $\open{-3}{3}$       
       &$f'(0)$ neg %=-27$  
       &$f$ decreasing \\
    $\open{3}{\infty}$   
       &$f'(4)$ pos % =21$   
       &$f$ increasing \\
  \end{tabular}
\end{center}
We can instead convey the same information with this \alert{sign graph}.
\begin{center}
  \includegraphics{asy/graph_shape007.pdf}
\end{center}
Thus $c_1=-3$ is a local maximum while
$c_2=3$ is a local minimum.
\end{frame}


\begin{frame}{Practice}
\Ex Apply the First Derivative test, if possible, to analyze
$x^3-3x^2-9x-1$.

\pause
The derivative is $3(x-3)(x+1)$.
Critical are $-1$ and~$3$.
\begin{center}\small
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial value}}   
      &\multicolumn{1}{c}{\textit{Conclusion}}   \\ \hline
    $\open{-\infty}{-1}$ &$f'(-2)$ pos %=15$  
                         &$f$ increasing \\
    $\open{-1}{3}$       &$f'(0)$ neg %=-9$   
                         &$f$ decreasing \\
    $\open{3}{\infty}$   &$f'(4)$ pos %=15$   
                         &$f$ increasing \\
  \end{tabular}
\end{center}
Therefore there is a local maximum at $x=-1$ and a local minimum at $x=3$.
\end{frame}
% sage: def f_prime(x):
% ....:     return 3*(x-3)*(x+1)
% ....: 
% sage: f_prime(-2)
% 15
% sage: f_prime(0)
% -9
% sage: f_prime(4)
% 15

\begin{frame}
\Ex Apply the First Derivative test, if possible, to analyze
$(1/3)x^3-x^2+x-4$.

\pause
The derivative is $(x-1)^2\!$.
The only critical number is $1$, dividing the real line into two intervals.
\begin{center}\small
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial value}}   
      &\multicolumn{1}{c}{\textit{Conclusion}}   \\ \hline
    $\open{-\infty}{1}$ &$f'(0)$ pos %=1$   
                        &$f$ increasing \\
    $\open{1}{\infty}$  &$f'(2)$ pos %=1$   
                        &$f$ increasing \\
  \end{tabular}
\end{center}
There is no sign change at $c=1$ so it
is not a local optimum.
\end{frame}
% sage: def f_prime(x):
% ....:     return (x-1)^2
% ....: 
% sage: f_prime(0)
% 1
% sage: f_prime(2)
% 1


\begin{frame}
\Ex Apply the First Derivative test, if possible, to analyze
$x+(1/x)$

\pause 
The derivative is $(x+1)(x-1)/x^2\!$.
There are three critical points, $-1$, $0$, and $1$.
\begin{center}\small
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial value}}   
      &\multicolumn{1}{c}{\textit{Conclusion}}   \\ \hline
    $\open{-\infty}{-1}$ &$f'(-2)$ pos %=3/4$     
                         &$f$ increasing \\
    $\open{-1}{0}$       &$f'(-1/2)$ neg %=-3$    
                         &$f$ decreasing \\
    $\open{0}{1}$        &$f'(1/2)$ neg %=-3$     
                         &$f$ decreasing \\
    $\open{1}{\infty}$   &$f'(2)$ pos %=3/4$      
                         &$f$ increasing \\
  \end{tabular}
\end{center}
The function has a local maximum at $x=-1$ and a local minimum at $x=1$.
\end{frame}
% sage: def f_prime(x):
% ....:     return (x+1)*(x-1)/x^2
% ....: 
% sage: f_prime(-2)
% 3/4
% sage: f_prime(-1/2)
% -3
% sage: f_prime(1/2)
% -3
% sage: f_prime(2)
% 3/4




\begin{frame}{Caution}
\Ex If possible, apply the First Derivative test to $f(x)=1/(x-5)$. 

\pause 
The test does not apply because the function is not continuous at
all inputs.
\begin{center}\small
  \includegraphics{asy/graph_shape006.pdf}
\end{center}  
\end{frame}




\section{What the second derivative says about a graph}
\begin{frame}{Concavity}
If on some interval the graph of a function lies above all of its tangents
then it is \alert{concave up} on that interval.
If it lies below all of its tangents then it is \alert{concave down}.
\begin{center}\small
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{asy/graph_shape004.pdf}}  \\
    Concave up
  \end{tabular}
    \quad
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{asy/graph_shape005.pdf}}  \\
    Concave down
  \end{tabular}
\end{center}

\Tm If on an interval $f''(x)$ is positive then the graph of $f$
is concave up on that interval.
If $f''(x)$ is negative then the graph is concave down.
\end{frame}

\begin{frame}{Combining the first and second derivative information}
\begin{center}
\begin{tabular}{r|c@{\hspace{2em}}c}
  \multicolumn{1}{r}{\ }
    &\multicolumn{1}{c}{\textit{Concave up}}
    &\multicolumn{1}{r}{\textit{Concave down}}  \\ \cline{2-3}
  \rule{0pt}{50pt}
  \textit{Increasing}  &\vcenteredhbox{\includegraphics{asy/graph_shape000.pdf}} 
                       &\vcenteredhbox{\includegraphics{asy/graph_shape001.pdf}}   \\
  \rule{0pt}{50pt}
  \textit{Decreasing}  &\vcenteredhbox{\includegraphics{asy/graph_shape002.pdf}} 
                       &\vcenteredhbox{\includegraphics{asy/graph_shape003.pdf}}
\end{tabular}
\end{center}
\end{frame}


\begin{frame}{Second Derivative test}
\Tm Let $f$ be continouous near some input~$c$ and let $f'(c)=0$.
If $f''(c)>0$ then $c$ is a local minimum.
If $f''(c)<0$ then $c$ is a local maximum.
If $f''(c)$ is zero then the test is inconclusive\Dash this may be a
local maximum, 
or a local minimum, or neither.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/graph_shape008.pdf}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/graph_shape009.pdf}}
\end{center}
\end{frame}

\begin{frame}
\Df If at some $(c,f(c))$ the graph of a function changes concavity, either from 
up to down or from down to up, then $c$ is an \alert{inflection point}. 
\begin{center}
  \includegraphics[height=0.5\textheight]{pix/coaster.jpg}
\end{center}
An inflection point is a change of mode, where you go from one thing happening
on the graph to another thing. 
\end{frame}

\begin{frame}
\Ex 
Find the local optima for $x^3-3x^2-24x+32$ 
using the Second Derivative test.

\pause
The first derivative is $f'(x)=3x^2-6x-24=3(x-4)(x+2)$.
The second derivative is $f''(x)=6x-6$.
\begin{center}
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Critical number~$c$}}
      &\multicolumn{1}{c}{Value at \textit{c}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $-2$ &$f''(-2)$ neg %=-18$ 
         &Local maximum \\
    $4$  &$f''(4)$ pos %=18$   
         &Local minimum 
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{If the second derivative is zero then the test is inconclusive}
Let $y=x^4\!$.
Then $y'=4x^3$ and $y''=12x^2$ and at the critical point $c=0$ both
first and second derivatives are zero.
We know what $x^4$ looks like so we know that this is a local minimum.

Similarly, with $y=-x^4\!$, at the critical point $c=0$ both derivatives are
zero and this is a local maximum.

Finally, let $y=x^3\!$.
Then $y'=3x^2$ and $y''=6x$, and at the critical point $c=0$ 
the first and second derivatives are both zero.
But $c$ neither a local maximum nor a local minimum.

% \pause\medskip
% The intuition for the test being inconclusive is that $f''(c)=0$ gives 
% no information about the shape of the curve.
% It could for instance be that 
% the first derivative is positive and then zero and then negative, and the first
% derivative's change only paused instantaneously at~$c$, so this is a maximum.
% But the second derivative information alone is not enough to draw a conclusion.  

\pause
Because it may be inconclusive, the Second Derivative Test can be less useful
than the First Derivative Test.
\end{frame}


\begin{frame}{Practice}
Apply the Second Derivative test.
\begin{enumerate}
\item $x^4-4x^3$
\pause \\ The first derivative is $4x^3-12x^2=4x^2(x-3)$
while the second derivative is $12x^2-24x=12x(x-2)$.
\begin{center}
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Critical number~$c$}}
      &\multicolumn{1}{c}{\textit{Value at $c$}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $0$ &$f''(0)$ zero %=0$    
        &Test inconclusive \\
    $3$  &$f''(3)$ pos %=36$  
         &Local minimum \\
  \end{tabular}
\end{center}
Because $f'(-1)=-16$ and $f'(1)=-8$, by the First Derivative test 
the critical number is neither a maximum
nor a minimum.

\item $x^5-5x^3$
\pause \\ The first derivative is $5x^4-15x^2=5x^2(x^2-3)$.
The second derivative is $20x^3-30x=10x(2x^2-3)$
\begin{center}
  \begin{tabular}{c|cc}
    \multicolumn{1}{c}{\textit{Critical number~$c$}}
      &\multicolumn{1}{c}{\textit{Value at $c$}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $-\sqrt{3}$ &$f''(-\sqrt{3})$ neg %=-30\sqrt{3}$  
                &Local maximum \\
    $0$ &       $f''(0)$ zero %=0$                
                &Test inconclusive \\
    $\sqrt{3}$  &$f''(\sqrt{3})$ pos %=30\sqrt{3}$ 
                &Local minimum \\
  \end{tabular}
\end{center}
Because $f'(-1)=-10$ and $f'(1)=-10$, the critical number is neither a maximum
nor a minimum.
\end{enumerate}
\end{frame}
% sage: def f_prime(x):
% ....:     return(4*x^2*(x-3))
% ....: 
% sage: f_prime(-1)
% -16
% sage: f_prime(1)
% -8
% sage: def g_prime(x):
% ....:     return(5*x^2*(x^2-3))
% ....: 
% sage: g_prime(-1)
% -10
% sage: g_prime(1)
% -10


\begin{frame}{Practice}
Sketch the graph of a function satisfying the conditions.
\begin{enumerate}
\item $f'(x)<0$ and $f''(x)<0$ for all $x$

\pause\item
Vertical asymptote at $x=0$.
If $x<-2$ then $f'(x)>0$ and if $x>-2$ (but $x\neq 0$) then $f'(x)<0$.
If $x<0$ then $f''(x)<0$ and if $x>0$ (but $x\neq 0$) then $f''(x)>0$.
 
\pause\item
Vertical asymptote at $x=1$.
For all $x\neq 1$ we have $f'(x)>0$.
For $1<x<3$ we have $f''(x)<0$, while for other inputs $f''(x)>0$.

\end{enumerate}
\end{frame}



\begin{frame}{Practice}
First sketch without calculus, then find the intervals of increase and
decrease, and then find the intervals of concavity and inflection points.
\begin{enumerate}
\item $36x+3x^2-2x^3$

\pause\item $200+8x^3+x^4$

\pause\item $5x^3+3x^5$

\pause\item $(x^2-4)^3$
\end{enumerate}  
\end{frame}
  


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
